clear all;
close all;
clc;



joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
CONFIG.tool_stl = 'roller.stl'; % Roller Tool


part = 'Mold_Ascent.stl';
[v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
v_part(:,1:3) = v_part(:,1:3)./1000;



% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  -49.39, 0, 133.53, 1000]'./1000; % Roller

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  -49.39, 0, 133.53, 1000]'./1000; % Roller

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [-0.0494; 0; 0.1335]; % For Roller

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%





joints = csvread( 'ascent_lockheed_exec_joints.csv' );
% joints = joint_angles';
% csvwrite('send_iiwa_hs.csv',joints')
% upd_joints = [];
% resolution = 1;
% 
% for i=1:size(joints,2)-1
%     upd_joints = [ upd_joints; joints(:,i)' ];
%     d_theta = joints(:,i+1)-joints(:,i);
%     d_theta = d_theta/resolution;
%     for j=1:resolution
%         upd_joints = [ upd_joints; (joints(:,i)' + j*d_theta') ];
%     end
% end
% upd_joints = joints';
% csvwrite( 'GE90_jointConfig.csv',upd_joints );
% 
xyz_abc = [];
for i=1:size( joints,1 )
    ee_base = iiwa_FK_symbolic_WE( joints(i,:)',eye(4) ) * robot1.robot_ree_T_tee;
    xyz_abc = [ xyz_abc; [ee_base(1,4)*1000,ee_base(2,4)*1000,ee_base(3,4)*1000,rotm2eul( ee_base(1:3,1:3),'ZYX' )] ];
end
csvwrite( 'ascent_lockheed_frames.csv',xyz_abc );


% [[-9.98864466e-01 -4.47949996e-02 -1.62230272e-02  1.07881061e+03]
% [ 4.47883114e-02 -9.98996199e-01  7.75538698e-04  7.59747635e+01]
% [-1.62414827e-02  4.80560561e-05  9.99868097e-01  4.71714088e+00]
% [ 0.00000000e+00  0.00000000e+00  0.00000000e+00  1.00000000e+00]]
% format short
% % format bank %(in R2018a)
%  
% x = [0.7800   -0.2360    0.4600    0.5787   -0.0668   -0.5554    0.5935]';
% 
% R = quat2rotm(x(4:7)');
% 
% 
% R2_to_R1 = [[-9.98864466e-01 -4.47949996e-02 -1.62230272e-02  1.07881061e+03]
%             [ 4.47883114e-02 -9.98996199e-01  7.75538698e-04  7.59747635e+01]
%             [-1.62414827e-02  4.80560561e-05  9.99868097e-01  4.71714088e+00]
%             [ 0.00000000e+00  0.00000000e+00  0.00000000e+00  1.00000000e+00]];
% 
% R2_to_R1(1:3,4) = R2_to_R1(1:3,4)/1000;
% 
% P_to_ff2 = eye(4);
% P_to_ff2(1:3,1:3) = eul2rotm( [-deg2rad(30),0,0],'ZYX' );
% P_to_ff2(1:3,4) = [ -0.11732; -0.06546;  0.008 ];
% 
% P_to_R1 = eye(4);
% P_to_R1(1:3,1:3) = R;
% P_to_R1(1:3,4) = x(1:3);
% 
% 
% final_T = inv(R2_to_R1) * P_to_R1 * inv(P_to_ff2);
% 
% % final_T(1:3,4)*1000
% % rad2deg(rotm2eul(final_T(1:3,1:3),'ZYX'))
% 
% T = eye(4);
% T(1:3,1:3) = eul2rotm( deg2rad([81.72,-4.09,38.46]),'ZYX' );
% T(1:3,4) = [ 0.43953;0.46336;0.36542 ];
% 
% new_P = R2_to_R1*T*P_to_ff2;
% Xk = [new_P(1:3,4);rotm2quat(new_P(1:3,1:3))']'

% tolerance(1) = 0.003;
% tolerance(2) = 0.0873;
% point = [ final_T(1:3,4)', final_T(1:3,1)', final_T(1:3,2)', final_T(1:3,3)' ];
% [joints,status] = get_iiwa_IK( [0;0;0;0;0;0;0],point,tolerance,options,theta_lb,theta_ub,false,2 );



