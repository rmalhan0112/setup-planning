% clear all;
% close all;
% clc;
% 
% load index_voxel;


% Generate Bz vectors
theta = [ 0:0.628:2*pi ];
phi = [ 0:0.5233:pi ];
x = [];
y = [];
z = [];

for theta = [ 0:1/2:2*pi ]
    for phi = [ 0:1/2:pi ]
        
        x = [x; cos(theta)*sin(phi)];
        y = [y; sin(theta)*sin(phi)];
        z = [z; cos(phi)];
    end
end
scatter3( x,y,z,20,'r','filled' )

% vectors = [];
% for i=1:size(x,1)
%     vectors = [vectors; [0,0,0]-[x(i),y(i),z(i)] ];
% end
% 
% for i=1:size(vectors,1)
%     vectors(i,:) = vectors(i,:)/norm(vectors(i,:));
% end
% 
% zero_mat = zeros(size(x,1),1);
% quiver3( zero_mat,zero_mat,zero_mat, vectors(:,1),vectors(:,2),vectors(:,3), 'k' );





% warning 'off';
% global CONFIG;
% 
% joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
% joint_vel = joint_vel*0.8;
% CONFIG.tool_stl = 'tool.stl';
% 
% % creates a 3D space in which the robot will operate
% run map_initialization.m;
% % define all the planning params in this file
% run hybrid_planner_params.m;
% % part poses and init and goal states are defined in the following file
% run simulations_data.m; 
% 
% CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
% CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
% if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
%     % this for loop adds discrete xyz
%     for jid = 1:3
%         curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
%         CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
%     end
% end
% CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
%                                                             CONFIG.DISCRETE_MAP_BOUNDS(3));
% 
% new_map = false;
% if new_map
%     disp('Computing EDT...');
%     EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
%     save('computed_3D_EDT.mat', 'EDT_vals');
% else
%     load computed_3D_EDT.mat;
% end
% CONFIG.EDT_vals = EDT_vals;
% CONFIG.max_EDT = max(EDT_vals(:));
% CONFIG.min_EDT = min(EDT_vals(:));
% %% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%
% 
% %%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%
% 
% % CONFIG.robot_sphere_diamter = 85/1000;
% CONFIG.robot_sphere_diamter = 120/1000;
% CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];
% 
% robot1_base = eye(4);
% robot1_base(1:3,4) = [0;0;0];
% robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);
% 
% ROBOT_TOOL = {};
% ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000; % Pointy Tool
% 
% ROBOT_TOOL_FMM = {};
% ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000;
% 
% %%%%% Tool to Robot transformation is defined here
% robot1.robot_ree_T_tee = eye(4);
% robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.120]; % For Pointy tool
% 
% robot1_tool = {};
% robot1_tool{end+1} = ROBOT_TOOL;
% robot1_tool{end+1} = ROBOT_TOOL_FMM;
% robot1_tool{end+1} = robot1.robot_ree_T_tee;
% 
% robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
% CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
% 
% 
% 
% theta_lb(1) = -2.967059728390360;
% theta_lb(2) = -2.094395102393195;
% theta_lb(3) = -2.967059728390360;
% theta_lb(4) = -2.094395102393195;
% theta_lb(5) = -2.967059728390360;
% theta_lb(6) = -2.094395102393195;
% theta_lb(7) = -3.054326190990077;
% theta_lb = theta_lb';
% theta_ub = - theta_lb;
% 
% % err = 0;
% % for i=1:500
% %     theta = [  randsample(theta_lb(1):0.001:theta_ub(1),1);
% %                 randsample(theta_lb(2):0.001:theta_ub(2),1);
% %                 randsample(theta_lb(3):0.001:theta_ub(3),1);
% %                 randsample(theta_lb(4):0.001:theta_ub(4),1);
% %                 randsample(theta_lb(5):0.001:theta_ub(5),1);
% %                 randsample(theta_lb(6):0.001:theta_ub(6),1);
% %                 randsample(theta_lb(7):0.001:theta_ub(7),1);
% %                     ];
% %     FK_all_joints = get_iiwa_FK_all_joints_mex( theta, eye(4) );
% %     my_Jac = iiwa_get_jac(FK_all_joints);
% %     my_Jac2 = iiwa_get_GeoJac_symbolic(FK_all_joints);
% % %     mtlb_jac = robot1.get_jacobian( theta, 'MRT' )
% %     
% %     diff = my_Jac - my_Jac2;
% %     err = err + sum( sum(diff) );
% % end
% 
% 
% folder = 'C:\Work\USC\Composite Automation\Journal\setup-planning\reach_sphere\Reach Sphere Data';
% % size_idx = size(index_voxel,1);
% orientation = csvread('orientation.csv');
% I = eye(4);
% profile on;
% 
% % for i=1:5
% %     key = index_voxel(char(num2str(i)));
% %     x = str2double(key(1:3))/50;
% %     y = str2double(key(4:6))/50-1;
% %     z = str2double(key(7:9))/50;
% %     
%     % Computing IK
%     T = eye(4);
% %     T(1:3,4) = [ x;y;z ];
%     T(1:3,4) = [ 0.2;0.4;0.4 ];
%     reach_data = [];
%     error = 0;
%    for j=1:size(orientation,1)
%         T(1,1) = orientation(j,1);
%         T(2,1) = orientation(j,2);
%         T(3,1) = orientation(j,3);
% 
%         T(1,2) = orientation(j,4);
%         T(2,2) = orientation(j,5);
%         T(3,2) = orientation(j,6);
% 
%         T(1,3) = orientation(j,7);
%         T(2,3) = orientation(j,8);
%         T(3,3) = orientation(j,9);
% 
%         [joint_config,solnInfo] = robot1.get_iiwa_IK( T, [0;0;0;0;0;0;0] );
%         if strcmp(solnInfo.Status,'success')
%             ee_base = iiwa_FK_gateway(joint_config);
%             error = error + ( norm(ee_base(1:3,4)-T(1:3,4)) );
%             reach_data = [ reach_data; orientation(j,7:9) ];
%         end
%     end
% %     dlmwrite( fullfile(folder, strcat(key,'.csv')), reach_data );
% % end
% profile off;
% profile viewer;
% 
% error
