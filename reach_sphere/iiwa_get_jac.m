function J = iiwa_get_jac(FK_all_joints)
% Geometric Jacobian Rows in order: Wx,Wy,Wz, Vx,Vy,Vz 
    ee_base = FK_all_joints(33:36,:);
    o_n = ee_base(1:3,4);
    vec_z = [0;0;1];
    for i = 1:7
        Ti = FK_all_joints(4*(i)+1:4*(i)+4,:);
        Ri = Ti(1:3,1:3);

        z_im1 = Ri * vec_z;
        o_im1 = Ti(1:3,4);
        dir_vec = o_n-o_im1;
        dir_vec = dir_vec / norm(dir_vec);
        J(:,i) = [z_im1; cross(z_im1,(o_n-o_im1)) ];
    end
end
