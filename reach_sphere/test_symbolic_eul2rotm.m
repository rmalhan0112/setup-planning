eul = [ randsample( [-pi:0.001:pi] ,1);
        randsample( [-pi:0.001:pi] ,1)
        randsample( [-pi:0.001:pi] ,1)  ];

err = 0;
for i=1:1000
    Ro = eul2rotm( eul', 'XYZ' );
    Rdash = XYZeul2rotm_symbolic( eul, 'XYZ' );
    diff = Ro - Rdash;
    err = sum( sum(diff) );
end