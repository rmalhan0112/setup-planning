% vectors = csvread( 'bz_vectors.csv' );
% orientation = [];
% for i=1:size(vectors,1)
%     bz = vectors(i,:);
%     by = [0,1,0];
%     bx = cross(by,bz);
%     bx = bx/norm(bx);
%     by = cross(bz,bx);
%     by = by/norm(by);
%     orientation = [orientation; [bx,by,bz]];
% end
% csvwrite( 'orientation.csv', orientation );
close all;
figure(1)
hold on;
daspect([1,1,1]);
for i=1:size(orientation,1)
    quiver3( 0,0,0,orientation(i,1),orientation(i,2),orientation(i,3),'r' );
    quiver3( 0,0,0,orientation(i,4),orientation(i,5),orientation(i,6),'g' );
    quiver3( 0,0,0,orientation(i,7),orientation(i,8),orientation(i,9),'b' );
    pause(0.5);
    cla;
end
