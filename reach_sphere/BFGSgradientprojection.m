%% BFGS Gradient Projection Solver for IK.

function [xSol, exitFlag, err, iter] = BFGSgradientprojection(obj)
            
            x = obj.SeedInternal;
            
            obj.TimeObjInternal.reset();
            
            % Dimension
            n = size(x,1);
            
            [cost, ~, ~, obj.ExtraArgs] = obj.CostFcn(x, obj.ExtraArgs);
            grad = obj.GradientFcn(x, obj.ExtraArgs);
            grad = grad(:);
            
            % H starts as a positive-definite matrix (so symmetric)
            H = eye(n);
            
            if obj.ConstraintsOn
                % Identify active-set (constraints that hit equality)
                activeSet = obj.ConstraintMatrix'*x >= obj.ConstraintBound;
                A = obj.ConstraintMatrix(:, activeSet);
            else
                activeSet = false(size(obj.ConstraintBound));
                A = zeros(n,0);
            end

            % Project Hessian Inverse to the valid manifold using a way 
            % similar to Gram-Schmidt process
            % This ensures that the H update does not violate constraints
            for k = 1:size(A,2)
                a = A(:,k);
                rho = a'*H*a; % a scalar
                H = H - (1/rho)*H*(a*a')*H;
            end
            
            xNew = x;
            iter = 0;
            
            % Main loop
            for i = 1:obj.MaxNumIterationInternal
                
                if timeLimitExceeded(obj, obj.TimeObjInternal.getElapsedTime)
                    xSol = x;
                    exitFlag = robotics.core.internal.NLPSolverExitFlags.TimeLimitExceeded;
                    err = obj.SolutionEvaluationFcn(xSol, obj.ExtraArgs);
                    iter = i;
                    return 
                end
                
                if isempty(A)
                    alpha = 0;
                else
                    alpha = (A'*A)\A'*grad; % alpha is an indicator
                end
                
                Hg = H*grad;
                if atLocalMinimum(obj, Hg, alpha) 
                    % Evaluate found local minimum
                    xSol = x;
                    exitFlag = robotics.core.internal.NLPSolverExitFlags.LocalMinimumFound;
                    err = obj.SolutionEvaluationFcn(xSol, obj.ExtraArgs);
                    iter = i;
                    return;
                end
                
                
                if obj.ConstraintsOn && ~isempty(A)
                    B = inv(A'*A);
                    [threshold, p] = max(alpha./sqrt(diag(B))); 
                    
                    % If the norm of the modified gradient Hg is smaller than
                    % threshold, drop the constraint corresponding to the
                    % threshold value.
                    if norm(Hg) < 0.5*threshold
                         % Find the constraint to drop
                         activeConstraintIndices = find(activeSet);
                         idxp = activeConstraintIndices(p);

                         % Update active constraints A
                         activeSet(idxp) = false;
                         A = obj.ConstraintMatrix(:, activeSet);

                         % Compute Projection 
                         P = eye(n) - A*((A'*A)\A');

                         % Update H, allowing more flexibility in update
                         % direction
                         ap = obj.ConstraintMatrix(:, idxp);
                         H = H + (1/(ap'*P*ap))*P*(ap*ap')*P;

                         continue
                    end
                end
                
                % Take a test step, then screen current inactive constraints 
                % and see if any becomes active, and determine the step 
                % scaling factor upper bound lambda 
                % s.t. ai'*(x + lambdai*s) - bi <=0 for all i
                % lambdai needs to be positive
                
                s = - Hg;
                idxl = -1;
                if obj.ConstraintsOn ...
                        && any(~activeSet) % if there is inactive constraints

                    bIn = obj.ConstraintBound(~activeSet);
                    AIn = obj.ConstraintMatrix(:,~activeSet);
                    inactiveConstraintIndices = find(~activeSet);
                    lambdas = (bIn - AIn'*x)./(AIn'*s);
                   
                    % We don't care the case when ai'*x-bi < 0 and ai'*s<0
                    L = find(lambdas>0);
                    if ~isempty(L)
                        [lambda, l] = min(lambdas(lambdas>0));
                        idxl = inactiveConstraintIndices(L(l));
                    else
                        lambda = 0;
                    end
                    
                else
                    lambda = 0;
                end
                
                
                if lambda > 0
                    gamma = min(1, lambda);  
                else % When the test step does not hit any constraint
                    gamma = 1; 
                end
                
                s0 = s;
                
                % Line search
                
                beta = obj.ArmijoRuleBeta;
                sigma = obj.ArmijoRuleSigma;
                
                [costNew,~,~, obj.ExtraArgs] = obj.CostFcn(x+gamma*s0, obj.ExtraArgs);
                m = 0;
                while cost - costNew < -sigma * grad'*(gamma*s0)
                    
                    if stepSizeBelowMinimum(obj, gamma)
                        xSol = x;
                        exitFlag = robotics.core.internal.NLPSolverExitFlags.StepSizeBelowMinimum;
                        err = obj.SolutionEvaluationFcn(xSol, obj.ExtraArgs);
                        iter = i;
                        return
                    end
                    
                    gamma = beta * gamma;
                    m = m+1;
                    [costNew,~,~, obj.ExtraArgs] = obj.CostFcn(x + gamma*s0, obj.ExtraArgs);
                end
                xNew = x + gamma*s0;
                gradNew = obj.GradientFcn(xNew, obj.ExtraArgs);
                gradNew = gradNew(:);
                
                % Case 1: gamma = 1 < lambda, xNew has not reached new
                % new constraint.
                % Case 2: gamma = 1 and lambda = 0, no new constraints.
                % In both cases, H is updated in an unconstrained
                % manner using damped BFGS
                % When gamma == lambda, H is projected to the updated
                % constrained manifold as new constraint is activated.
                
                if m==0 && abs(gamma - lambda)< sqrt(eps) % ?
                    a = obj.ConstraintMatrix(:,idxl);
                    
                    activeSet(idxl) = true;
                    A = obj.ConstraintMatrix(:, activeSet);
                    
                    % Project the search direction to constrained manifold
                    H = H - (1/(a'*H*a))*(H*(a*a'*H));
                else
                    % Update search direction using damped BFGS
                    y = gradNew - grad;
                    
                    d1 = 0.2;
                    d2 = 0.8;
                    
                    % Hessian damping formulation 1 (update s)
                    if s'*y < d1*y'*H*y
                        theta = d2*y'*H*y/(y'*H*y - s'*y);
                    else
                        theta = 1; 
                    end
                    sNew = theta*s + (1-theta)*H*y;
                    rho = sNew'*y;
                    V = eye(n)- (sNew*y')/rho;
                    H = V*H*V' + (sNew*sNew')/rho;
                    
%                     % Hessian damping formula 2 (update y)
%                     if s'*y < d1*s'*H*s
%                         
%                         th = d2*(s'*H*s)/(s'*H*s - s'*y);
%                         y = th*y + (1 - th)*H*s;
%                     end
%         
%                     H = (eye(n) - (s*y')/(y'*s))*H*(eye(n)- (y*s')/(y'*s)) + s*s'/(y'*s);
                    
                    
                    % Making sure H is not non-PSD
                    if ~robotics.core.internal.isPositiveDefinite(H + sqrt(eps)*eye(size(H))) 
                        xSol = xNew;
                        exitFlag = robotics.core.internal.NLPSolverExitFlags.HessianNotPositiveSemidefinite;
                        err = obj.SolutionEvaluationFcn(xSol, obj.ExtraArgs);
                        iter = i;
                        return
                    end
                end
                
                if searchDirectionInvalid(obj, xNew)
                    % This really should not happen, just in case
                    xSol = x;
                    exitFlag = robotics.core.internal.NLPSolverExitFlags.SearchDirectionInvalid;
                    err = obj.SolutionEvaluationFcn(xSol, obj.ExtraArgs);
                    iter = i;
                    return
                end
                
                x = xNew;
                grad = gradNew;
                cost = costNew;
            end
            
            xSol = xNew;
            exitFlag = robotics.core.internal.NLPSolverExitFlags.IterationLimitExceeded;
            err = obj.SolutionEvaluationFcn(xSol, obj.ExtraArgs);
            iter = obj.MaxNumIterationInternal;
        end
        
        function flag = atLocalMinimum(obj, Hg, alpha)
            flag = norm(Hg)<obj.GradientTolerance && all(alpha<=0);
        end
        
        function flag = searchDirectionInvalid(obj, xNew)
            flag = obj.ConstraintsOn && any(obj.ConstraintMatrix'*xNew - obj.ConstraintBound > sqrt(eps));
        end
        
        function flag = stepSizeBelowMinimum(obj, gamma)
            flag = gamma < obj.StepTolerance;
        end
        
    end
    
    methods (Static, Hidden)
        function props = matlabCodegenNontunableProperties(~)
            props = {'MaxNumConstraints', 'MaxNumVariables'};
        end
    end