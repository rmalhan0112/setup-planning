warning 'off';
global CONFIG;

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
joint_vel = joint_vel*0.8;
CONFIG.tool_stl = 'tool.stl'; % Pointy Tool



% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.120]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


%% %%%%%% CURVE DATA %%%%%%%%%%%%%

% Armor Part
curve_file_name  = 'Armor_points2.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);


tolerances(1) = 0.002;
tolerances(2) = 0.2618;

Force_vecs = zeros(no_pts,3);
Force_vecs(:,3) = ones(no_pts,1).*30;

desired_velc = 120; % mm/s
desired_velc = desired_velc/1000;

path_len = 0;
velc_vecs = [];
for i=1:no_pts-1
    path_len = path_len + norm(xyz_bxbybz(i+1,1:3)-xyz_bxbybz(i,1:3));
    velc_vecs = [velc_vecs; xyz_bxbybz(i+1,1:3)-xyz_bxbybz(i,1:3)];
end
velc_vecs = [velc_vecs; [0,0,0]];
seg_len = path_len / (no_pts-1);
seg_time = seg_len / desired_velc;

velc_vecs = velc_vecs./seg_time;




%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 3000;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-6;
options.StepTolerance = 1e-6;
options.Display = 'off';
options.SpecifyObjectiveGradient = false;



% Actual GE90 Pose
q = eul2quat( [deg2rad(0),deg2rad(-15),deg2rad(0)],'ZYX' );
x = [ 0.4;0.15;0.35; q' ];

% Generate Random Samples with 10% Perturbation
% no_samples = 100;
% percnt = 0.2;
% counter = 0;
% X = [];
% quatr_pert = 50;
% 
% while counter ~= no_samples
%     sample = [x(1) + randsample( [ -percnt:0.001:percnt ],1 );
%          x(2) + randsample( [ -percnt:0.001:percnt ],1 );
%         x(3) + randsample( [ -percnt:0.001:percnt ],1 );
%           ];
%       
%     q = eul2quat( [deg2rad(randsample([ -quatr_pert:0.001:quatr_pert ],1)),...
%                     deg2rad(randsample([ -quatr_pert:0.001:quatr_pert ],1)),...
%                         deg2rad(randsample([ -quatr_pert:0.001:quatr_pert ],1))],'ZYX' );
%     sample(4:7) = q';
%     R = quat2rotm( sample(4:7)' );
%     bz = R*[0;0;1];
%     if acos(dot( bz,[0;0;1] )) > 1.35
%         continue;
%     end
%     transf_xyzbxbybz = transf_points( sample,xyz_bxbybz, robot1 );
%     if (get_euc_reach( transf_xyzbxbybz(:,1:3), no_pts, reach_map,capability_z) ~= 100)
%             continue;
%     else
%         counter = counter + 1;
%         X = [X, sample];
%     end
% end
% disp('Samples Generated')
% csvwrite( 'Armor_100_perturb.csv',X );

transf_xyzbxbybz = transf_points( x,xyz_bxbybz, robot1 );

% joint_config = [0;0;0;0;0;0;0];
% reach = 0;
% for i=1:no_pts
%     [joint_config,status] = get_iiwa_IK( joint_config,transf_xyzbxbybz(i,:),tolerances,...
%                                         options, theta_lb, theta_ub,true,3 );
%     if status
%         reach = reach + 1;
%     end
% end
% fprintf('Initial Reachability is: %d\n',reach);


%% Cyclic Cooridnate Descent (Reachability Only)
% Satisfy Constraint Violations in sequence Position/Orientation, Velocity,
% and Force

trans_range = [ -0.7:0.01:0.7 ];
orie_range = [ -1:0.02:1 ];

prt_poses = csvread( 'Armor_100_perturb.csv' );

success_cases = 0;
for poses = 1:size(prt_poses,2)
    Xk = prt_poses(:,poses);
    fprintf('Sample number %d\n',poses)
    
    iter = 0;
    while iter < 20
        is_sol = false;
        iter = iter + 1;
    %     coord = [1,2,4];      % Switch 6 to [1,2,4] for X,Y,Z and Q1
        coord = [ 1:6 ];
        for cnt = [1:6]
            idx = randsample([1:size(coord,2)],1);
            i = coord(idx);
            coord(idx) = [];
            obj_values = [];
            if i<4
                for j=1:size(trans_range,2)
                    xk1 = Xk;
                    xk1(i) = xk1(i) + trans_range(j);
                    transf_xyzbxbybz = transf_points(xk1,xyz_bxbybz, robot1);
                    if (get_euc_reach( transf_xyzbxbybz(:,1:3), no_pts,reach_map,capability_z) ~= 100)
                        continue;
                    end
                    obj_reach = reach_viol( transf_xyzbxbybz, no_pts,capability_z,reach_map );
                    obj_values = [ obj_values; [obj_reach,j] ];
                end
                
                if size(obj_values,1)~=0
                    [val,idx] = min(obj_values(:,1));
                    Xk(i) = Xk(i) + trans_range( obj_values(idx,2) );
                    curr_obj_reach = val;
                    if curr_obj_reach==0
                        is_sol = true;
                        break;
                    end
                end
            else
                for j=1:size(orie_range,2)
                    xk1 = Xk;
                    xk1(i) = xk1(i) + orie_range(j);
                    xk1(4:7) = xk1(4:7)/norm(xk1(4:7));
                    R = quat2rotm( x(4:7)' );
                    bz = R*[0;0;1];
                    if acos(dot( bz,[0;0;1] )) > 1.22
                        continue;
                    end

                    transf_xyzbxbybz = transf_points(xk1,xyz_bxbybz, robot1);
                    if (get_euc_reach( transf_xyzbxbybz(:,1:3), no_pts,reach_map,capability_z) ~= 100)
                        continue;
                    end

                    obj_reach = reach_viol( transf_xyzbxbybz, no_pts,capability_z,reach_map );
                    obj_values = [ obj_values; [obj_reach,j] ];
                end

                if size(obj_values,1)~=0
                    [val,idx] = min(obj_values(:,1));
                    Xk(i) = Xk(i) + orie_range( obj_values(idx,2) );
                    Xk(4:7) = Xk(4:7)/norm(Xk(4:7));
                    curr_obj_reach = val;
                    if curr_obj_reach==0
                        is_sol = true;
                        break;
                    end
                end
            end
        end
        if is_sol
            fprintf('Solution Found in %d iterations.\n',iter)
            break;
        end 
    end
    
    if iter==20
        disp('Solution not found');
    end
    
    success_cases = success_cases + 1;
    fprintf('Approximation of Reachability Violation:  %d\n\n',curr_obj_reach);
end

fprintf( 'Success:  %d\n', success_cases/size(prt_poses,2)*100 );



















%% Functions
function violation = reach_viol(points, no_pts,capability_z,reach_map)
    violation = no_pts;
    
    for i = 1:no_pts
        % Voxelize
        x = int8(points(i,1)*50);
        y = int8(points(i,2)*50+50);
        z = int8(points(i,3)*50);
        bz = points(i,10:12)';
        
        key = char(strcat( to_string_v(x), to_string_v(y), to_string_v(z)));
        if capability_z{ reach_map.voxel_index(key) }.reachable
            ori_cones = capability_z{ reach_map.voxel_index(key) }.ori_cones;
            for j = 1:size(ori_cones,1)
                centroid = ori_cones(j,1:3);
                beta = ori_cones(j,4);
                angle = real(acos( centroid(1)*bz(1) + centroid(2)*bz(2) + centroid(3)*bz(3) ));
                if angle < beta
                    violation = violation - 1;
                    break;
                end  
            end
        end
    end
end


function euc_reach = get_euc_reach( points, no_pts,reach_map,capability_z )
    reach = 0;
    
    for i=1:no_pts
        % Voxelize
        x = int8(points(i,1)*50);
        y = int8(points(i,2)*50+50);
        z = int8(points(i,3)*50);
        key = char(strcat( to_string_v(x), to_string_v(y), to_string_v(z)));
        if isKey( reach_map.voxel_index,key )
            if capability_z{ reach_map.voxel_index(key) }.reachable
                reach = reach + 1;
            end
        end
    end
    euc_reach = (reach/no_pts)*100;
end










% [reach,time,joint_angles] = eval_constr(transf_xyzbxbybz,tolerances,options,...
%                                         theta_lb, theta_ub,velc_vecs, Force_vecs, joint_vel,seg_time);
% d_theta = [];
% for i=1:reach-1
%    d_theta = [ d_theta; max((abs(joint_angles(:,i+1)-joint_angles(:,i)))) ]; 
% end  
% fprintf('Initial time taken is: %d\n',time);

