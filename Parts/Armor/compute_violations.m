function err = compute_violations( x,xyz_bxbybz,tolerances, IKoptions, theta_lb, theta_ub,...
                                    seg_time, velocities, Force_vecs,joint_vel)
    global robot1;
    global q3;
    global solution_pose;
    
    x = [x; q3];
    x(4:7) = x(4:7)/norm(x(4:7));
    q3 = x(7);
    
    no_pts = size( xyz_bxbybz, 1 );
    
    err = 0;
    violation = no_pts;
    [ ee_points, velc_vecs ] = transform_points( x,xyz_bxbybz,velocities );
    
    joint_config = [0;0;0;0;0;0;0];
    reach = 0;                                    
    for i = 1:no_pts
        [joint_config,status] = constr_IK( joint_config,ee_points(i,:),tolerances,...
                                            IKoptions, theta_lb, theta_ub,...
                                            seg_time,i, velc_vecs, Force_vecs,joint_vel );
        if status
            reach = reach + 1;
            violation = violation - 1;
        end        
        ee_base = get_iiwa_FK_mex( joint_config,eye(4) );
        ee_base = ee_base * robot1.robot_ree_T_tee;
        
% Error Position
        err_pos = ( ee_points(i,1:3)'-ee_base(1:3,4) );
        if norm(err_pos) > tolerances(1)
            err = err + err_pos(1)^2 + err_pos(2)^2 + err_pos(3)^2;
        end
% Error Orientation
        err_orientation = ee_points(i,10)*ee_base(1,3) + ...
                          ee_points(i,11)*ee_base(2,3) + ...
                          ee_points(i,12)*ee_base(3,3);
        if err_orientation < cos(tolerances(2))
            err = err + 0.25*(1-err_orientation)^2;
        end        
        
        err = err*0.5;
    end
    solution_pose = [ solution_pose, x ];
    fprintf(' Violations:  %d\n ',violation);
    if reach==no_pts
        fprintf('Reachable and error is:  %d\n',err);
%         solution_pose = [ solution_pose, x ];
    end
end