% Bonet Part
curve_file_name  = 'Bonet_points2.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);


% % Add one point in between
% upd_pts = [];
% for i=1:no_pts-1
%     upd_pts = [upd_pts; xyz_bxbybz(i,:)];
%     extra_pt = (xyz_bxbybz(i+1,:) + xyz_bxbybz(i,:))/2;
%     extra_pt(4:6) = extra_pt(4:6)/norm(extra_pt(4:6));
%     extra_pt(7:9) = extra_pt(7:9)/norm(extra_pt(7:9));
%     extra_pt(10:12) = extra_pt(10:12)/norm(extra_pt(10:12));
%     upd_pts = [upd_pts; extra_pt];
% end
% upd_pts = [upd_pts; xyz_bxbybz(end,:)];



% % Add two points in between
% upd_pts = [];
% for i=1:no_pts-1
%     upd_pts = [upd_pts; xyz_bxbybz(i,:)];
%     dist = norm(xyz_bxbybz(i+1,1:3) - xyz_bxbybz(i,1:3));
%     u_vec = (xyz_bxbybz(i+1,1:3) - xyz_bxbybz(i,1:3))/dist;
%     prt1 = dist/3;
%     
%     extra_pt1(1:3) = xyz_bxbybz(i,1:3) + u_vec*prt1;
%     extra_pt1(4:12) = (xyz_bxbybz(i+1,4:12) + xyz_bxbybz(i,4:12))/2;
%     extra_pt1(4:6) = extra_pt1(4:6)/norm(extra_pt1(4:6));
%     extra_pt1(7:9) = extra_pt1(7:9)/norm(extra_pt1(7:9));
%     extra_pt1(10:12) = extra_pt1(10:12)/norm(extra_pt1(10:12));
%     upd_pts = [upd_pts; extra_pt1];
%     
%     prt1 = 2*dist/3;
%     extra_pt1(1:3) = xyz_bxbybz(i,1:3) + u_vec*prt1;
%     extra_pt1(4:12) = (xyz_bxbybz(i+1,4:12) + xyz_bxbybz(i,4:12))/2;
%     extra_pt1(4:6) = extra_pt1(4:6)/norm(extra_pt1(4:6));
%     extra_pt1(7:9) = extra_pt1(7:9)/norm(extra_pt1(7:9));
%     extra_pt1(10:12) = extra_pt1(10:12)/norm(extra_pt1(10:12));
%     upd_pts = [upd_pts; extra_pt1];
% end
% upd_pts = [upd_pts; xyz_bxbybz(end,:)];




% Add three points in between
upd_pts = [];
for i=1:no_pts-1
    upd_pts = [upd_pts; xyz_bxbybz(i,:)];
    dist = norm(xyz_bxbybz(i+1,1:3) - xyz_bxbybz(i,1:3));
    u_vec = (xyz_bxbybz(i+1,1:3) - xyz_bxbybz(i,1:3))/dist;
    prt1 = dist/4;
    
    extra_pt1(1:3) = xyz_bxbybz(i,1:3) + u_vec*prt1;
    extra_pt1(4:12) = (xyz_bxbybz(i+1,4:12) + xyz_bxbybz(i,4:12))/2;
    extra_pt1(4:6) = extra_pt1(4:6)/norm(extra_pt1(4:6));
    extra_pt1(7:9) = extra_pt1(7:9)/norm(extra_pt1(7:9));
    extra_pt1(10:12) = extra_pt1(10:12)/norm(extra_pt1(10:12));
    upd_pts = [upd_pts; extra_pt1];
    
    prt1 = dist/2;
    extra_pt1(1:3) = xyz_bxbybz(i,1:3) + u_vec*prt1;
    extra_pt1(4:12) = (xyz_bxbybz(i+1,4:12) + xyz_bxbybz(i,4:12))/2;
    extra_pt1(4:6) = extra_pt1(4:6)/norm(extra_pt1(4:6));
    extra_pt1(7:9) = extra_pt1(7:9)/norm(extra_pt1(7:9));
    extra_pt1(10:12) = extra_pt1(10:12)/norm(extra_pt1(10:12));
    upd_pts = [upd_pts; extra_pt1];
    
    prt1 = 3*dist/4;
    extra_pt1(1:3) = xyz_bxbybz(i,1:3) + u_vec*prt1;
    extra_pt1(4:12) = (xyz_bxbybz(i+1,4:12) + xyz_bxbybz(i,4:12))/2;
    extra_pt1(4:6) = extra_pt1(4:6)/norm(extra_pt1(4:6));
    extra_pt1(7:9) = extra_pt1(7:9)/norm(extra_pt1(7:9));
    extra_pt1(10:12) = extra_pt1(10:12)/norm(extra_pt1(10:12));
    upd_pts = [upd_pts; extra_pt1];
end
upd_pts = [upd_pts; xyz_bxbybz(end,:)];




part = 'Bonet.STL';
[v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
v_part = v_part./1000;
part_plt(v_part, f_part, name_part, [1,0,0]); % plot tool stl
xyz_bxbybz = upd_pts;
quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),xyz_bxbybz(:,4),xyz_bxbybz(:,5),xyz_bxbybz(:,6),'r','AutoScaleFactor',1 );
quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),xyz_bxbybz(:,7),xyz_bxbybz(:,8),xyz_bxbybz(:,9),'g','AutoScaleFactor',1 );
quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),xyz_bxbybz(:,10),xyz_bxbybz(:,11),xyz_bxbybz(:,12),'b','AutoScaleFactor',1 );
daspect([1,1,1])