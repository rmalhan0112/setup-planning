warning 'off';
global CONFIG;

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
CONFIG.tool_stl = 'tool.stl'; % Pointy Tool



% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.1]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% GE90 Part
curve_file_name  = 'GE90_points.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  %d\n ',no_pts);


tolerances(1) = 0.002;
tolerances(2) = 0.2618;

Force_vecs = zeros(no_pts,6);
Force_vecs(:,3) = ones(no_pts,1).*30;   % 30 N force

desired_velc = 150; % mm/s
desired_velc = desired_velc/1000;

path_len = 0;
velocities = [];
seg_time = [];
seg_time = [ 600 ];
for i=1:no_pts-1
    dist_vec = (xyz_bxbybz(i+1,1:3)-xyz_bxbybz(i,1:3));
    dist = norm( dist_vec );
    dist_vec = dist_vec/dist;
    path_len = path_len + dist;
    velocities = [velocities; dist_vec*desired_velc  ];
    seg_time = [ seg_time; dist/desired_velc ];
end
velocities = [velocities; [0,0,0]];
seg_time = seg_time*1.5;


%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 1500;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-8;
options.StepTolerance = 1e-8;
options.Display = 'off';
options.SpecifyObjectiveGradient = false;
options.ObjectiveLimit = 1e-8;

% Actual GE90 Pose
q = eul2quat( [deg2rad(0),deg2rad(0),deg2rad(0)],'ZYX' );
CONFIG.pose = [ 0.4; 0.2; 0.3; q' ];

% Optimization routine parameters
A = []; b = [];

% Defining the Lower and Upper Bounds
% Lower Bound for X,Y,Z
lb(1) = 0;
lb(2) = -1;
lb(3) = 0;
lb(4) = -1;
lb(5) = -1;
lb(6) = -1;

ub(1) = 1;
ub(2) = 1;
ub(3) = 1;
ub(4) = 1;
ub(5) = 1;
ub(6) = 1;

Aeq = []; beq = [];
CONFIG.options = optimoptions('fmincon');
CONFIG.options.MaxIterations = 1e5;
CONFIG.options.MaxFunctionEvaluations = 1e7;
CONFIG.options.OptimalityTolerance = 1e-10;
CONFIG.options.StepTolerance = 1e-10;
CONFIG.options.Display = 'iter';
CONFIG.options.ObjectiveLimit = 1e-15;


%% Random generation for capability map
% no_samples = 50;
% counter = 0;
% X = [];
% 
% tic;
% while counter ~= 200
%     x = [randsample( [ 0:0.001:1 ],1 );
%          randsample( [ -1:0.001:1 ],1 );
%         randsample( [ 0:0.001:1 ],1 );
%         randsample( [ -1: 0.001: 1 ],1 );
%         randsample( [ -1: 0.001: 1 ],1 );
%         randsample( [ -1: 0.001: 1 ],1 );
%         randsample( [ -1: 0.001: 1 ],1 )  ];
% 
%     x(4:7) = x(4:7)/norm(x(4:7));
%     bz = quat2rotm( x(4:7)' ) * [0;0;1];
%     
%     if acos( bz(3) ) > 1.4
%         continue;
%     end
%     
%     % Make sure that pose is in capability map
%     transf_xyzbxbybz = transf_points(x,xyz_bxbybz, robot1);
%     if (get_euc_reach( transf_xyzbxbybz(:,1:3), no_pts,reach_map,capability_z) ~= 100)
%         continue;
%     end
%     
%     obj_eval = reach_viol( transf_xyzbxbybz, no_pts,capability_z,reach_map );
%     
%     counter = counter + 1;
%     X = [X; [x',obj_eval]];
% end
% X = sortrows( X,8,'ascend' );
% csvwrite('ge90_random.csv',X(1:50,1:7)');
% toc;

%% Cyclic Cooridnate Descent (Reachability Only)
% Satisfy Constraint Violations in sequence Position/Orientation, Velocity,
% and Force


random_samples = csvread('ge90_random.csv');
Max_iter = 25;
avg_time = 0;
sucess_cases = 0;
s_time = [];
f_time = [];

for poses = 1:size(random_samples,2)
    strt = tic;
    no_equal = 0;
    fileid = fullfile('C:\Users\RRoS User\setup-planning\Parts\GE90\Iterative descent',...
                    strcat('Sample_',num2str(poses),'.csv'));
    
    Xk = random_samples(:,poses);
    fprintf('Sample number: %d\n',poses)
    
    transf_xyzbxbybz = transf_points(Xk,xyz_bxbybz, robot1);    
    curr_obj_reach = reach_viol( transf_xyzbxbybz, no_pts,capability_z,reach_map );
    
    X = [];
    X = [X, Xk];
    iter = 0;
    strt = tic;
    
    if curr_obj_reach==0
        is_sol = true;
        fprintf('Random Seed has 0 violations\n');
        sucess_cases = sucess_cases + 1;
        % Write to the file
        csvwrite(fileid,X);
        fid = fopen( fileid, 'a' );
        fprintf(fid,'%d\n',curr_obj_reach);
        fclose(fid);
        s_time = [ s_time; toc(strt) ];
        continue;
    else
        fprintf('Random Seed has violations:  %d\n',curr_obj_reach);
        is_sol = false;
    end
            
    while iter < Max_iter
        iter = iter + 1;
        indices = get_random_coord( [1,2,3],[4,5,6] );
        
        for cnt = 1:size(indices,1)
            i = indices(cnt);
            obj_values = [];
                    
            if ~is_pose_valid(Xk)
                disp('Invalid Pose')
                break;
            end
                    
            range = get_range( Xk,i );  % Should never receive invalid pose
            
            if i<4
                for j=1:size(range,1)
                    xk1 = Xk;
                    xk1(i) = xk1(i) + range(j);
                    if ~is_pose_valid(xk1)
                        continue;
                    end
                    transf_xyzbxbybz = transf_points(xk1,xyz_bxbybz, robot1);
                    if (get_euc_reach( transf_xyzbxbybz(:,1:3), no_pts,reach_map,capability_z) ~= 100)
                        continue;
                    end
                    obj_reach = reach_viol( transf_xyzbxbybz, no_pts,capability_z,reach_map );
                    obj_values = [ obj_values; [obj_reach,j] ];
                end
                
                if size(obj_values,1)~=0
                    [val,idx] = min(obj_values(:,1));
                    if val > curr_obj_reach
                        continue;
                    end
                    Xk(i) = Xk(i) + range( obj_values(idx,2) );
                    curr_obj_reach = val;
                    if curr_obj_reach==0
                        is_sol = true;
                        break;
                    end
                end
            else
                for j=1:size(range,1)
                    xk1 = Xk;
                    xk1(i) = xk1(i) + range(j);
                    xk1(4:7) = xk1(4:7)/norm(xk1(4:7));
                    R = quat2rotm( xk1(4:7)' );
                    bz = R*[0;0;1];
                    if acos( bz(3) ) > 1.4
                        continue;
                    end
                    transf_xyzbxbybz = transf_points(xk1,xyz_bxbybz, robot1);
                    if (get_euc_reach( transf_xyzbxbybz(:,1:3), no_pts,reach_map,capability_z) ~= 100)
                        continue;
                    end

                    obj_reach = reach_viol( transf_xyzbxbybz, no_pts,capability_z,reach_map );
                    obj_values = [ obj_values; [obj_reach,j] ];
                end

                if size(obj_values,1)~=0
                    [val,idx] = min(obj_values(:,1));
                    if val > curr_obj_reach
                        continue;
                    end
                    Xk(i) = Xk(i) + range( obj_values(idx,2) );
                    Xk(4:7) = Xk(4:7)/norm(Xk(4:7));
                    curr_obj_reach = val;
                    if curr_obj_reach==0
                        is_sol = true;
                        break;
                    end
                end
            end
        end
        if is_sol
            fprintf('Solution Found in %d iterations.\n',iter)
            s_time = [ s_time; toc(strt) ];
            sucess_cases = sucess_cases + 1;
            break;
        end
        
        if  isequal( X(1:7,end),Xk )
            no_equal = no_equal + 1;
            if no_equal == 4
                fprintf('Stuck in local minima.\n');
                f_time = [ f_time; toc(strt) ];
                break;
            end
        end
        
        X = [X, Xk];
    end
    end_time = toc(strt);
    avg_time = avg_time + end_time;
    fprintf('Average time so far:  %d\n', avg_time/poses);
    
    X = [X, Xk];
    
    csvwrite(fileid,X);
    
    if iter==Max_iter
        disp('Solution not found');
        f_time = [ f_time; toc(strt) ];
    end
    
    fid = fopen( fileid, 'a' );
    fprintf(fid,'%d\n',curr_obj_reach);
    fclose(fid);
    
    fprintf('Violations nailed down to:  %d\n',curr_obj_reach);
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    fprintf('\n\n')
end

fprintf('Total Success Cases:  %d\n',sucess_cases);


















%% Functions
function violation = reach_viol(points, no_pts,capability_z,reach_map)
    violation = no_pts;
    
    for i = 1:no_pts
        % Voxelize
        x = int8(points(i,1)*50);
        y = int8(points(i,2)*50+50);
        z = int8(points(i,3)*50);
        bz = points(i,10:12)';
        
        key = char(strcat( to_string_v(x), to_string_v(y), to_string_v(z)));
        ori_cones = capability_z{ reach_map.voxel_index(key) }.ori_cones;
        for j = 1:size(ori_cones,1)
            centroid = ori_cones(j,1:3);
            beta = ori_cones(j,4);
            angle = real(acos( centroid(1)*bz(1) + centroid(2)*bz(2) + centroid(3)*bz(3) ));
            if angle < beta*0.8
                violation = violation - 1;
                break;
            end  
        end
    end
end


function euc_reach = get_euc_reach( points, no_pts,reach_map,capability_z )
    reach = 0;
    
    for i=1:no_pts
        % Voxelize
        x = int8(points(i,1)*50);
        y = int8(points(i,2)*50+50);
        z = int8(points(i,3)*50);
        key = char(strcat( to_string_v(x), to_string_v(y), to_string_v(z)));
        if isKey( reach_map.voxel_index,key )
            if capability_z{ reach_map.voxel_index(key) }.reachable
                reach = reach + 1;
            end
        end
    end
    euc_reach = (reach/no_pts)*100;
end










% [reach,time,joint_angles] = eval_constr(transf_xyzbxbybz,tolerances,options,...
%                                         theta_lb, theta_ub,velc_vecs, Force_vecs, joint_vel,seg_time);
% d_theta = [];
% for i=1:reach-1
%    d_theta = [ d_theta; max((abs(joint_angles(:,i+1)-joint_angles(:,i)))) ]; 
% end  
% fprintf('Initial time taken is: %d\n',time);

