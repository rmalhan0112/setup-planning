warning 'off';
global CONFIG;
global robot1;
global q3;
global solution_pose;
solution_pose = [];


joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
CONFIG.tool_stl = 'tool.stl'; % Pointy Tool


% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.1]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% Bonet Part
curve_file_name  = 'bonet_points2.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);



tolerances(1) = 0.002;
tolerances(2) = 0.2618;

Force_vecs = zeros(no_pts,6);
Force_vecs(:,3) = ones(no_pts,1).*30;   % 30 N force

desired_velc = 150; % mm/s
desired_velc = desired_velc/1000;

path_len = 0;
velocities = [];
seg_time = [];
seg_time = [ 600 ];
dist_vecs = [0];

for i=1:no_pts-1
    dist_vec = (xyz_bxbybz(i+1,1:3)-xyz_bxbybz(i,1:3));
    dist = norm( dist_vec );
    dist_vecs = [ dist_vecs; dist ];
    dist_vec = dist_vec/dist;
    path_len = path_len + dist;
    velocities = [velocities; dist_vec*desired_velc  ];
    seg_time = [ seg_time; dist/desired_velc ];
end
velocities = [velocities; [0,0,0]];



%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 1500;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-8;
options.StepTolerance = 1e-8;
options.Display = 'off';
options.SpecifyObjectiveGradient = false;
options.ObjectiveLimit = 1e-8;


% Actual Pose
q = eul2quat( [deg2rad(180),deg2rad(10),deg2rad(0)],'ZYX' );
CONFIG.pose = [ 0.6;0.2;0.4; q' ];


% Optimization routine parameters
A = []; b = [];

% Defining the Lower and Upper Bounds
% Lower Bound for X,Y,Z
lb(1) = 0;
lb(2) = -1;
lb(3) = 0;
lb(4) = -1;
lb(5) = -1;
lb(6) = -1;
lb(7) = -1;

ub(1) = 1;
ub(2) = 1;
ub(3) = 1;
ub(4) = 1;
ub(5) = 1;
ub(6) = 1;
ub(7) = 1;

Aeq = []; beq = [];

CONFIG.options = optimoptions('fmincon');
CONFIG.options.MaxIterations = 1e5;
CONFIG.options.MaxFunctionEvaluations = 1e7;
CONFIG.options.OptimalityTolerance = 1e-10;
CONFIG.options.StepTolerance = 1e-10;
CONFIG.options.Display = 'iter';
CONFIG.options.ObjectiveLimit = 1e-15;


%% Exact Solution
% Satisfy Constraint Violations in sequence Position/Orientation, Velocity,
% and Force
% 
% reach_poses = csvread('bonet10_reach_poses.csv');
% prt_poses = [];
% for i=1:42
%     if reach_poses(8,i) == no_pts
%         prt_poses = [prt_poses, reach_poses(1:7,i)];
%     end
% end

% no_samples = size(prt_poses,2);
no_samples = 1;
iter = 0;
success_cases = 0;
success_time = [];
fail_time = [];

avg_time = 0;
overall_strt = tic;
for poses = 1:no_samples
    strt = tic;
    solution_pose = [];
    
%     Xk = prt_poses(:,poses);
    Xk = CONFIG.pose;
    
    fprintf('Sample Index:  %d\n',poses)
    % Evaluate Intial Reachability
    joint_angles = [];
    [ ee_points, velc_vecs ] = transform_points( Xk,xyz_bxbybz,velocities );
    no_pts = size( xyz_bxbybz, 1 );
    joint_config = [0;0;0;0;0;0;0];
    reach = 0;                                    
    for i = 1 : no_pts
        [joint_config,status] = constr_IK( joint_config,ee_points(i,:),tolerances,...
                                            options, theta_lb, theta_ub,...
                                            seg_time,i, velc_vecs, Force_vecs,joint_vel );
        if status
%             if( i > 1)
%                 sanity_inf_norm_check = norm(joint_angles(:,end) - joint_config, inf);
%                 disp(rad2deg(sanity_inf_norm_check))
%             end
            reach = reach + 1;
            joint_angles = [ joint_angles, joint_config ];

        end
    end
    fprintf( 'Initial Reachability is:  %d\n', reach );
    
    if reach == no_pts
        disp('Solution Found! Initially Reachable')
        success_cases = success_cases + 1;
        end_time = toc(strt);
        success_time = [ success_time; end_time ];
        disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
        fprintf('\n\n\n\n')
        continue;
    end
    
    
    fprintf( 'Reachability after optimization is:  %d\n', reach );
    fprintf('Success so far:    %d\n',success_cases);
    
    iter = iter + 1;
    
% Fmincon Routine
    x0 = Xk(1:6);
    q3 = Xk(7);
    
    % % SOLVE USING fmincon % % 
    disp('Solving for Exact Reachability......')
    
    [x,err] = fmincon( @(x)compute_violations( x,xyz_bxbybz,tolerances, options, theta_lb,theta_ub,...
                            seg_time, velocities, Force_vecs,joint_vel,dist_vecs ),...
                            x0,A,b,Aeq,beq,lb,ub,[],CONFIG.options );
    x = [x; q3];
    x(4:7) = x(4:7)/norm(x(4:7));
    q3 = x(7);
        
    joint_angles = [];
    [ ee_points, velc_vecs ] = transform_points( x,xyz_bxbybz,velocities );
    no_pts = size( xyz_bxbybz, 1 );
    joint_config = [0;0;0;0;0;0;0];
    reach = 0;                                    
    for i = 1:no_pts
        [joint_config,status] = constr_IK( joint_config,ee_points(i,:),tolerances,...
                                            options, theta_lb, theta_ub,...
                                            seg_time,i, velc_vecs, Force_vecs,joint_vel,dist_vecs );
        if status
            reach = reach + 1;
            joint_angles = [ joint_angles, joint_config ];
        end
    end
    end_time = toc(strt);
    
    if reach ~= no_pts
        disp('Solution not found')
        fail_time = [fail_time; end_time];
    else
        disp('Solution Found! Part Pose is:  ')
        success_time = [success_time; end_time];
        success_cases = success_cases + 1;
    end
    
    
    fprintf( 'Reachability after optimization is:  %d\n', reach );
    fprintf('Success so far:    %d\n',success_cases);
    
    avg_time = avg_time + end_time;
    fprintf('Avg time so far:    %d\n',avg_time/poses);
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    fprintf('\n\n\n\n')
end
overall_end = toc(overall_strt);

fprintf( 'Success:  %d\n', success_cases );


% Feasible Indices:  1,2,4,8,10,12,16,17,22,25,26