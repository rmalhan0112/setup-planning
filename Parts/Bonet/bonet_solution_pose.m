clear all;
close all;
clc;

warning 'off';
global CONFIG;

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
CONFIG.tool_stl = 'tool.stl'; % Pointy Tool



% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.1]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


%% %%%%%% CURVE DATA %%%%%%%%%%%%%
% Bonet Part
curve_file_name  = 'Bonet_points2.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);

%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 3000;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-6;
options.StepTolerance = 1e-8;
options.Display = 'off';
options.SpecifyObjectiveGradient = false;

tolerance(1) = 0.003;
tolerance(2) = 0.3491;

Force_vecs = zeros(no_pts,3);
Force_vecs(:,3) = ones(no_pts,1).*30;   % 30 N force

desired_velc = 80; % mm/s
desired_velc = desired_velc/1000;

path_len = 0;
velc_vecs = [];
for i=1:no_pts-1
    path_len = path_len + norm(xyz_bxbybz(i+1,1:3)-xyz_bxbybz(i,1:3));
    velc_vecs = [velc_vecs; xyz_bxbybz(i+1,1:3)-xyz_bxbybz(i,1:3)];
end
velc_vecs = [velc_vecs; [0,0,0]];
seg_len = path_len / (no_pts-1);
seg_time = seg_len / desired_velc;

velc_vecs = velc_vecs./seg_time;




% Actual Pose
q = eul2quat( [deg2rad(180),deg2rad(10),deg2rad(0)],'ZYX' );
x = [ 0.6;0.2;0.4; q' ];

transf_xyzbxbybz = transf_points( x,xyz_bxbybz, robot1 );
no_pts = size( xyz_bxbybz, 1 );

% Plotting
% figure(1); 
% plot_robot([0;0;0;0;0;0;0],robot1); daspect([1,1,1])
% hold on;
% part = 'Bonet.STL';
% [v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
% v_part = v_part./1000;
% 
% 
% robot_T_part = eye(4);
% robot_T_part(1:3,1:3) = quat2rotm(x(4:7)');
% robot_T_part(1:3,4) = x(1:3);
% [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,robot_T_part);
% part_plt(v_part_transf, f_part, name_part, [0.2,0,0]); % plot tool stl
% daspect([1,1,1]);
% 
% 
% scatter3( transf_xyzbxbybz(:,1),transf_xyzbxbybz(:,2),transf_xyzbxbybz(:,3),20,'k','filled' );
% quiver3( transf_xyzbxbybz(:,1),transf_xyzbxbybz(:,2),transf_xyzbxbybz(:,3),...
%         transf_xyzbxbybz(:,10),transf_xyzbxbybz(:,11),transf_xyzbxbybz(:,12), 'b','AutoScaleFactor',5 );

joint_angles = [];
joint_config = [0;0;0;0;0;0;0];

% For first point
[joint_config,status] = get_iiwa_IK( joint_config,transf_xyzbxbybz(1,:),tolerance,...
                                    options, theta_lb, theta_ub,true,3 );
                                
for i = 1:no_pts                
    [joint_config,status] = constr_IK( joint_config,transf_xyzbxbybz(i,:),tolerance,...
                                        options, theta_lb, theta_ub,seg_time,true,3, i, velc_vecs, Force_vecs,joint_vel );
%     [joint_config,status] = get_iiwa_IK( joint_config,transf_xyzbxbybz(i,:),tolerance,...
%                                         options, theta_lb, theta_ub,false,3 );
    if status
        joint_angles = [joint_angles, joint_config ];
    end
end
reach = size( joint_angles,2 )

remainder = no_pts - reach;
    
% Evaluate Velocity
tot_time = [];
for i = 1 : reach-1
    jconfig_init = joint_angles(:,i);
    jconfig_fin = joint_angles(:,i+1);
    delta_j = jconfig_fin - jconfig_init;
    time_vec = [];
    for j=1:7
        time_vec = [ time_vec; abs((delta_j(j)/joint_vel(j))) ];
    end
    tot_time = [ tot_time; max(time_vec) ];
end
tot_time = [ tot_time; max(tot_time)*remainder  ];
exe_time = sum(tot_time)
