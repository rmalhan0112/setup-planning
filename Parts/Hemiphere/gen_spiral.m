clear all;
close all
clc

figure(1)
hold on;
xlabel('X axis')
ylabel('Y axis')
zlabel('Z axis')
daspect([1,1,1])
part = 'HS.STL';

% Plot the Transformed Part
[v, f, n, name_part] = stlRead(part); % read tool stl
% v_part = v_part./1000;
% [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,R_to_Part); % transform tool stl to attach on flange
part_plt(v, f, name_part, [0.8,0.4,0]); % plot tool stl

N = 170;
volLim = pi; % volume limit. volLim = pi will give a hemisphere.
for ii = 1:N
    z(ii) = -((volLim/pi)*(ii-1)-N)/N;
    x(ii) = cos(sqrt(N*pi)*asin(z(ii)))*sqrt(1-z(ii)^2);
    y(ii) = sin(sqrt(N*pi)*asin(z(ii)))*sqrt(1-z(ii)^2);
end
x = x*90;
y = y*90;
z = z*60;
% plot3(x,y,z,'-o')

T = eye(4);
T(1:3,4) = [95;95;3];

pts = [x',y',z',ones(size(x',1),1)];
pts = T*pts';
pts = pts(1:3,:)';

daspect([1,1,1])

points = [];
for i=1:size(pts,1)
    if pts(i,3)>45
        points = [points; pts(i,:)];
    end
end

%% Mapping Points to the 3D Surface
% This section takes a point on the line, shifts the point and triangle 
% to XY plane
% Then checks if point lies in the bounds. If it does, it dictates
% equation of plane of triangle in 3D and solves for it finding the new
% coordinates and stores it in matrix.

%% Filtering the faces which lie on surface only
idx_face = [];
for i=1:size(f)
    c = dot([0.0,0.0,1],n(i,:)); %Dot product between Z and normal
    if c>0 & c<=1
        idx_face = [idx_face;[i]];
    end
end

xyz = [];
normals = [];
faces = [];
for j=1:size(idx_face)
    p1 = v(f(idx_face(j),1),:);
    p2 = v(f(idx_face(j),2),:);
    p3 = v(f(idx_face(j),3),:);
    triangle = [p1;p2;p3;p1];
    % Check if the all grid points lie inside any triangle for face j.
    in = inpolygon(points(:,1),points(:,2),triangle(:,1),triangle(:,2));
    % Find the row numbers of elements that actually lie inside.
    k = find(in);

    if isempty(k)
    else
        faces = [faces;f(idx_face(j),:)];
        temp = [points(k,1),points(k,2)];
        normal = n(idx_face(j),:) / norm(n(idx_face(j),:));
        for norm_cnt = 1:size(k)
            normals = [normals; normal];
        end
        % Transfer the point to the plane
        a = ((p2(2)-p1(2))*(p3(3)-p1(3)))-((p3(2)-p1(2))*(p2(3)-p1(3)));
        b = ((p2(3)-p1(3))*(p3(1)-p1(1)))-((p3(3)-p1(3))*(p2(1)-p1(1)));
        c = ((p2(1)-p1(1))*(p3(2)-p1(2)))-((p3(1)-p1(1))*(p2(2)-p1(2)));
        d = -(a*p1(1))-(b*p1(2))-(c*p1(3));

        for count = 1:size(k)
            zval = ((-d-(a*temp(count,1))-(b*temp(count,2)))/c)-0.08;
            xyz = [xyz;[temp(count,1)],[temp(count,2)],[zval]];
        end
    end
end


%% Reorder the Points in xyz according to points-array
points = [xyz, normals];
points = sortrows(points, 3, 'descend');
       
% for i=1:size(points,1)
%     plot3(points(i,1),points(i,2),points(i,3),'-o')
%     quiver3(points(i,1),points(i,2),points(i,3),points(i,4),points(i,5),points(i,6),...
%             'k','linewidth',2,'AutoScaleFactor',5,'MaxHeadSize',3);
%     hold on; 
%     pause(0.000001)
% end


% Compute Direction Vector for Y axis

dir_vec=[];
by = [];
bz = [];
bx = [];
points = points(6:end,:);
for j=1:size(points,1)
    if j==size(points,1)
    else
       direction = points(j+1,1:3) - points(j,1:3);
       dir_vec = direction / norm(direction);
    end
    tool_z = -points(j,4:6);
    tool_x = dir_vec;
    tool_y = cross(tool_z,tool_x);
    tool_y = tool_y / norm(tool_y);
    tool_x = cross(tool_y,tool_z);
    tool_x = tool_x / norm(tool_x);
    bx = [bx; tool_x];
    by = [by; tool_y];
    bz = [bz; tool_z];
end


plot3(points(:,1),points(:,2),points(:,3),'-o','linewidth',5)
quiver3(points(:,1),points(:,2),points(:,3),bx(:,1),bx(:,2),bx(:,3),...
        'r','linewidth',2,'AutoScaleFactor',0.5,'MaxHeadSize',0.25);
quiver3(points(:,1),points(:,2),points(:,3),by(:,1),by(:,2),by(:,3),...
        'g','linewidth',2,'AutoScaleFactor',0.5,'MaxHeadSize',0.25);
quiver3(points(:,1),points(:,2),points(:,3),-bz(:,1),-bz(:,2),-bz(:,3),...
        'b','linewidth',2,'AutoScaleFactor',0.5,'MaxHeadSize',0.25);

csvwrite('HS_points.csv',[points(:,1:3),bx,by,bz]);