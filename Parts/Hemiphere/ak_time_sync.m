clear all;close all; clc
time_part = dlmread('C:\Users\RRoS User\Documents\KUKA_LOG\time_part.csv');
time_tool = dlmread('C:\Users\RRoS User\Documents\KUKA_LOG\time_tool.csv');

N = size(time_part,1);

v = 0.01;
Vpart = [];
Vtool = [];
for i = 1:N
    
    r = time_part(i,2)/time_tool(i,2);
    % if part moves faster, then speed up tool
    if(r<1)
        Vpart = [Vpart;v];
        Vtool = [Vtool;v/r];
    end
    % if tool moves faster, then slowdown part
    if(r>1)
        Vtool = [Vtool;v];
        Vpart = [Vpart;v/r];
    end
    
    disp([r,Vpart(end),Vtool(end)])
end

dlmwrite('C:\Users\RRoS User\Documents\KUKA_LOG\speed_part.csv',Vpart);
dlmwrite('C:\Users\RRoS User\Documents\KUKA_LOG\speed_tool.csv',Vtool);