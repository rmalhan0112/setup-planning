warning 'off';
global CONFIG;
global robot1; 

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
CONFIG.tool_stl = 'roller.stl'; % Roller Tool



% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  -49.39, 0, 133.53, 1000]'./1000; % Roller

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  -49.39, 0, 133.53, 1000]'./1000; % Roller

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [-0.0494; 0; 0.1335]; % For Roller

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

%% %%%%%% CURVE DATA %%%%%%%%%%%%%
% Ascent Part
curve_file_name  = 'Ascent_points.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
grp_idx = csvread('Ascent_Group_IDX.txt');
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);



tolerances(1) = 0.003;
tolerances(2) = 0.0524;
tolerances(3) = 0.0524;
tolerances(4) = 1.0472;

Force_vecs = zeros(no_pts,6);
Force_vecs(:,3) = ones(no_pts,1).*30;   % 30 N force

desired_velc = 150; % mm/s
desired_velc = desired_velc/1000;

path_len = 0;
velocities = [];
seg_time = [];

for i = 1:size(grp_idx,1)
    seg_time = [ seg_time; 600 ];    
    for j = grp_idx(i,1):grp_idx(i,2)-1
        dist_vec = (xyz_bxbybz(j+1,1:3)-xyz_bxbybz(j,1:3));
        dist = norm( dist_vec );
        dist_vec = dist_vec/dist;
        path_len = path_len + dist;
        velocities = [velocities; dist_vec*desired_velc  ];
        seg_time = [ seg_time; dist/desired_velc ];
    end
    velocities = [velocities; [0,0,0]];
end
seg_time = seg_time*1.5;


%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 1500;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-8;
options.StepTolerance = 1e-8;
options.Display = 'off';
options.SpecifyObjectiveGradient = false;
options.ObjectiveLimit = 1e-8;


% Actual Pose
q = eul2quat( [deg2rad(90),deg2rad(0),deg2rad(0)],'ZYX' );
CONFIG.pose = [ 0.8;-0.3;0.0467; q' ];

% Optimization routine parameters
A = []; b = [];

% Defining the Lower and Upper Bounds
lb(1) = 0;
lb(2) = -1;
lb(3) = 0.0467;
lb(4) = -1;
lb(5) = 0;
lb(6) = 0;

ub(1) = 1;
ub(2) = 1;
ub(3) = 0.1467;
ub(4) = 1;
ub(5) = 0;
ub(6) = 0;


Aeq = []; beq = [];
CONFIG.options = optimoptions('fmincon');
CONFIG.options.MaxIterations = 1e5;
CONFIG.options.MaxFunctionEvaluations = 1e7;
CONFIG.options.OptimalityTolerance = 1e-10;
CONFIG.options.StepTolerance = 1e-10;
CONFIG.options.Display = 'iter';
CONFIG.options.ObjectiveLimit = 1e-15;


% Range vectors for tolerance exploration in capability map
range_y = [0];  %Initialize
range_x = [0];  %Initialize
populate = true;
ub_count = 0.7;
lb_count = 0.7;
lb = -0.9;
ub = 0.9;
while populate
    populate = false;
    val = 0.08*ub_count;
    if val < ub
        range_y = [ range_y; val ];
        ub_count = ub_count + 1;
        populate = true;
    end

    val = -0.08*lb_count;
    if val > lb
        range_y = [ range_y; val ];
        lb_count = lb_count + 1; 
        populate = true;
    end
end



% Store points which violate cap map for video
% Define Poses
X =  [0.206; -0.081; 0.0467;  -0.64745; 0; 0; 0.76211];
X = [X,  [0.865; 0.57; 0.0467;  -0.49076; 0; 0; -0.87129] ];
X = [X,  [0.975; -0.197; 0.0467; -0.3013;  0; 0; -0.95353] ];
X = [X,  [0.829; 0.217; 0.0467; 0.53342;  0; 0; 0.84585] ];
X = [X,  [0.133; 0.675; 0.0467; -0.80313;  0;0;0.59581] ];
X = [X,  [0.336; -0.078; 0.0467; -0.96247; 0; 0; -0.2714] ];
X = [X,  [0.103; 0.203; 0.0467; 0.99856;  0; 0; -0.05372] ];

for i=1:size(X,2)
    target_pts = transf_target( X(:,i),xyz_bxbybz );     %NOTE: Gives EE points. Not ff. As reach violation under tol req that
    reach_violation = reach_viol( target_pts, no_pts,capability_z,reach_map,range_x,range_y );
end

return;

% % Generate Random Samples
% no_samples = 200;
% X = [];
% counter = 0;


% while counter ~= no_samples
%     x = [randsample( [ 0:0.001:1 ],1 );
%          randsample( [ -1:0.001:1 ],1 );
%         0.0467;
%         randsample( [ -1: 0.001: 1 ],1 );
%         0;
%         0;
%         randsample( [ -1: 0.001: 1 ],1 )  ];
% 
%     x(4:7) = x(4:7)/norm(x(4:7));
% %     x = CONFIG.pose;
%     
%     target_pts = transf_target( x,xyz_bxbybz );     %NOTE: Gives EE points. Not ff. As reach violation under tol req that
%     reach_violation = reach_viol( target_pts, no_pts,capability_z,reach_map,range_x,range_y );
%     fprintf('Sample:  %d     Violation:    %d\n',counter,reach_violation);
%     counter = counter + 1;
%     
% %     if reach_violation > 5
% %         continue;
% %     end
% %     x
%     
%     joint_angles = [];
%     [ ee_points, velc_vecs ] = transform_points( x,xyz_bxbybz,velocities );
%     no_pts = size( xyz_bxbybz, 1 );
%     reach = 0;                                    
%     for i = 1:size(grp_idx,1)
%         joint_config = [0;0;0;0;0;0;0];
%         for j = grp_idx(i,1):grp_idx(i,2)
%             [joint_config,status] = ascent_IK_constr( joint_config,ee_points(j,:),tolerances,options,theta_lb,theta_ub,...
%                                 seg_time,j, velc_vecs, Force_vecs,joint_vel,grp_idx );
%             if status
%                 reach = reach + 1;
%                 joint_angles = [ joint_angles, joint_config ];
%             end
%         end
%     end
%     fprintf( 'Reachability is:  %d\n', reach );
%     disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
%     fprintf('\n\n')
% end




%% Random generation for capability map
% no_samples = 50;
% counter = 0;
% X = [];
% 
% tic;
% while counter ~= 200
%     x = [randsample( [ 0:0.001:1 ],1 );
%      randsample( [ -1:0.001:1 ],1 );
%     0.0467;
%     randsample( [ -1: 0.001: 1 ],1 );
%     0;
%     0;
%     randsample( [ -1: 0.001: 1 ],1 )  ];
% 
%     x(4:7) = x(4:7)/norm(x(4:7));
%     bz = quat2rotm( x(4:7)' ) * [0;0;1];
%     
%     if acos( bz(3) ) > 1.4
%         continue;
%     end
%     
%     % Make sure that pose is in capability map
%     target_pts = transf_target( x,xyz_bxbybz );
%     obj_eval = reach_viol( target_pts, no_pts,capability_z,reach_map,range_x,range_y );
%     
%     counter = counter + 1;
%     X = [X; [x',obj_eval]];
% end
% X = sortrows( X,8,'ascend' );
% toc;
% csvwrite('ascent_random.csv',X(1:30,1:7)');


%% Cyclic Cooridnate Descent (Reachability Only)
% Satisfy Constraint Violations in sequence Position/Orientation, Velocity,
% and Force


random_samples = csvread('ascent_random.csv');
Max_iter = 15;
avg_time = 0;
sucess_cases = 0;
s_time = [];
f_time = [];

for poses = 1:size(random_samples,2)
    no_equal = 0;
    fileid = fullfile('C:\Users\RRoS User\setup-planning\Parts\Mold_ascent\Iterative descent',...
                    strcat('Sample_',num2str(poses),'.csv'));
    
%     Xk = random_samples(:,poses);
    Xk = [0.7750,   -0.2120,    0.0467, 0.8212, 0,0,0.5707]';
    fprintf('Sample number: %d\n',poses)
    
    target_pts = transf_target( Xk,xyz_bxbybz );    
    curr_obj_reach = reach_viol( target_pts, no_pts,capability_z,reach_map,range_x,range_y );
    
    X = [];
    X = [X, Xk];
    iter = 0;
    strt = tic;
    
    if curr_obj_reach==0
        is_sol = true;
        fprintf('Random Seed has 0 violations\n');
        sucess_cases = sucess_cases + 1;
        % Write to the file
        csvwrite(fileid,X);
        fid = fopen( fileid, 'a' );
        fprintf(fid,'%d\n',curr_obj_reach);
        fclose(fid);
        s_time = [ s_time; toc(strt) ];
        continue;
    else
        fprintf('Random Seed has violations:  %d\n',curr_obj_reach);
        is_sol = false;
    end
            
    while iter < Max_iter
        iter = iter + 1;
        indices = get_random_coord( [1,2],[6] );
        
        for cnt = 1:size(indices,1)
            i = indices(cnt);
            obj_values = [];
                    
            if ~is_pose_valid(Xk)
                disp('Invalid Pose')
                break;
            end
                    
            range = ascent_range( Xk,i );  % Should never receive invalid pose
            
            if i<4
                for j=1:size(range,1)
                    xk1 = Xk;
                    xk1(i) = xk1(i) + range(j);
                    if ~is_pose_valid(xk1)
                        continue;
                    end
                    target_pts = transf_target( Xk,xyz_bxbybz );
                    obj_reach = reach_viol( target_pts, no_pts,capability_z,reach_map,range_x,range_y );
                    obj_values = [ obj_values; [obj_reach,j] ];
                end
                
                if size(obj_values,1)~=0
                    [val,idx] = min(obj_values(:,1));
                    if val > curr_obj_reach
                        continue;
                    end
                    Xk(i) = Xk(i) + range( obj_values(idx,2) );
                    curr_obj_reach = val;
                    if curr_obj_reach==0
                        is_sol = true;
                        break;
                    end
                end
            else
                for j=1:size(range,1)
                    xk1 = Xk;
                    xk1(i) = xk1(i) + range(j);
                    xk1(4:7) = xk1(4:7)/norm(xk1(4:7));
                    R = quat2rotm( xk1(4:7)' );
                    bz = R*[0;0;1];
                    if acos( bz(3) ) > 1.4
                        continue;
                    end
                    target_pts = transf_target( Xk,xyz_bxbybz );
                    obj_reach = reach_viol( target_pts, no_pts,capability_z,reach_map,range_x,range_y );
                    obj_values = [ obj_values; [obj_reach,j] ];
                end

                if size(obj_values,1)~=0
                    [val,idx] = min(obj_values(:,1));
                    if val > curr_obj_reach
                        continue;
                    end
                    Xk(i) = Xk(i) + range( obj_values(idx,2) );
                    Xk(4:7) = Xk(4:7)/norm(Xk(4:7));
                    curr_obj_reach = val;
                    if curr_obj_reach==0
                        is_sol = true;
                        break;
                    end
                end
            end
        end
        if is_sol
            fprintf('Solution Found in %d iterations.\n',iter)
            s_time = [ s_time; toc(strt) ];
            sucess_cases = sucess_cases + 1;
            break;
        end
        
        if  isequal( X(1:7,end),Xk )
            no_equal = no_equal + 1;
            if no_equal == 3
                fprintf('Stuck in local minima.\n');
                f_time = [ f_time; toc(strt) ];
                break;
            end
        end
        
        X = [X, Xk];
    end
    end_time = toc(strt);
    avg_time = avg_time + end_time;
    fprintf('Average time so far:  %d\n', avg_time/poses);
    
    X = [X, Xk];
    
%     csvwrite(fileid,X);
    
    if iter==Max_iter
        disp('Solution not found');
        f_time = [ f_time; toc(strt) ];
    end
    
%     fid = fopen( fileid, 'a' );
%     fprintf(fid,'%d\n',curr_obj_reach);
%     fclose(fid);
    
    fprintf('Violations nailed down to:  %d\n',curr_obj_reach);
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
    fprintf('\n\n')
end

fprintf('Total Success Cases:  %d\n',sucess_cases);


















%% Functions
function violation = reach_viol(points, no_pts,capability_z,reach_map,range_x,range_y)
    global robot1;
    violation = no_pts;
    violation_points = [];
    
    for i = 1:no_pts
        % Voxelize
        x = int8(points(i,1)*50);
        y = int8(points(i,2)*50+50);
        z = int8(points(i,3)*50);
        
        key = char(strcat( to_string_v(x), to_string_v(y), to_string_v(z)));
        
        T_ee_w = eye(4);
        T_ee_w(1:3,1:3) = [ points(i,4:6)',points(i,7:9)',points(i,10:12)' ];
        T_ee_w(1:3,4) = points(i,1:3)';
        
        [voxels,z_vecs] = get_voxels_within_tolerance( range_x, range_y, T_ee_w, capability_z, reach_map, robot1 );
        
        for vxls = 1:size(voxels,1)
            flag = false;
            ori_cones = capability_z{ voxels(vxls) }.ori_cones;
            bz = z_vecs( vxls,: );
            for j = 1:size(ori_cones,1)
                centroid = ori_cones(j,1:3);
                beta = ori_cones(j,4);
                angle = real(acos( centroid(1)*bz(1) + centroid(2)*bz(2) + centroid(3)*bz(3) ));
                if angle < beta*0.8
                    violation = violation - 1;
                    flag = true;
                    break;
                end  
            end
            if flag
                violation_points = [violation_points;  [points(i,1),points(i,2),points(i,3),1] ];
                break;
            end
        end
        if ~flag
            violation_points = [violation_points;  [points(i,1),points(i,2),points(i,3),0] ];
        end
    end
end




% function violation = reach_viol(points, no_pts,capability_z,reach_map)
%     violation = no_pts;
%     
%     for i = 1:no_pts
%         % Voxelize
%         x = int8(points(i,1)*50);
%         y = int8(points(i,2)*50+50);
%         z = int8(points(i,3)*50);
%         bz = points(i,10:12)';
%         
%         key = char(strcat( to_string_v(x), to_string_v(y), to_string_v(z)));
%         ori_vectors = capability_z{ reach_map.voxel_index(key) }.ori_vecs;
%         for j = 1:size(ori_vectors,1)
%             vec = ori_vectors(j,:);
%             angle = real(acos( vec(1)*bz(1) + vec(2)*bz(2) + vec(3)*bz(3) ));
%             if angle < 0.2618
%                 violation = violation - 1;
%                 break;
%             end  
%         end
%     end
% end





function euc_reach = get_euc_reach( points, no_pts,reach_map,capability_z )
    reach = 0;
    
    for i=1:no_pts
        % Voxelize
        x = int8(points(i,1)*50);
        y = int8(points(i,2)*50+50);
        z = int8(points(i,3)*50);
        key = char(strcat( to_string_v(x), to_string_v(y), to_string_v(z)));
        if isKey( reach_map.voxel_index,key )
%             if capability_z{ reach_map.voxel_index(key) }.reachable
            if reach_map.reachability{ reach_map.voxel_index(key) }
                reach = reach + 1;
            end
        end
    end
    euc_reach = (reach/no_pts)*100;
end






function range = ascent_range(Xk,idx)
    if idx>3
        range = [];
        count = 1;
        while count*0.04 <= 1
            range = [range; (count*0.04) ];
            range = [range; -(count*0.04) ];
            count = count + 1;
        end
        return;
    elseif idx==2
        ub = 1;
        lb = -1;
    elseif (idx==1)
        ub = 1;
        lb = 0;
    elseif (idx==3)
        ub = 0.2467;
        lb = 0.0467;
    end
    range = [];

    populate = true;
    ub_count = 1;
    lb_count = 1;
    while populate
        populate = false;
        val = (Xk(idx) + 0.03*ub_count);
        if val < ub
            range = [ range; val - Xk(idx) ];
            ub_count = ub_count + 1;
            populate = true;
        end
        
        val = (Xk(idx) - 0.03*lb_count);
        if val > lb
            range = [ range; val - Xk(idx) ];
            lb_count = lb_count + 1; 
            populate = true;
        end
    end
end







% [reach,time,joint_angles] = eval_constr(transf_xyzbxbybz,tolerances,options,...
%                                         theta_lb, theta_ub,velc_vecs, Force_vecs, joint_vel,seg_time);
% d_theta = [];
% for i=1:reach-1
%    d_theta = [ d_theta; max((abs(joint_angles(:,i+1)-joint_angles(:,i)))) ]; 
% end  
% fprintf('Initial time taken is: %d\n',time);



% Solutions

% 0.7530
% 0.5890
% 0.0467
% 0.3205
%      0
%      0
% -0.9472



% 0.8580
% -0.7000
% 0.0467
% 0.5176
%      0
%      0
% 0.8556


% 0.8690
% -0.4780
% 0.0557
% 0.3369
%      0
%      0
% 0.9415


% 0.6220
% -0.5040
% 0.0467
% 0.9017
%      0
%      0
% 0.4323