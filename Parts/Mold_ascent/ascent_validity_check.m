function is_valid = ascent_validity_check( prev_config,curr_config,point,pt_idx,velocity_vec,joint_vel,force_vec,grp_idx )
%     is_valid = true;
%     d_theta = abs(curr_config - prev_config)/seg_time;
%     velc_violation = min(joint_vel - d_theta);
%     if velc_violation < 0
%         is_valid = false;
%         return;
%     end
    
    [c,ceq] = ascent_placement_constr(curr_config,prev_config,point,pt_idx,velocity_vec,force_vec,joint_vel,grp_idx );
    is_valid = all(c<=0);
end