clear all;
close all;
clc;

points = csvread('Ascent_additive.csv');

points(:,3) = points(:,3) - 0.0467*1000;

% new_pts = [];
% x_incr = [0:30:570];
% 
% for i = 1:size(points,1)
%     if ismember(points(i,1),x_incr)
%         new_pts = [ new_pts; points( i, : ) ];
%     end
% end

% new_pts(:,1:3) = new_pts(:,1:3)./1000;
 
lb = [ 0.05, 0.05 ] *1000;
ub = [ 0.55, 0.55 ] *1000;

% points = new_pts;
new_pts = [];
for i=1:size(points,1)
    if (points(i,1) < lb(1) || points(i,1) > ub(1) || points(i,2) < lb(2) || points(i,2) > ub(2))
        continue;
    else
        new_pts = [ new_pts; points(i,:) ];
    end
end
% 
% i = 0;
% points = new_pts;
% new_pts = [];
% new_pts = [ new_pts; points(1,:) ];
% cnt = 1;
% while i < size(points,1)-2
%     while true
%         i = i + 1;
%         if norm(points(i,1:3)-new_pts(cnt,1:3)) < 0.025
%             continue;
%         else
%             new_pts = [ new_pts; points(i,:) ];
%             cnt = cnt + 1;
%             break;
%         end
%     end
% end


points = new_pts;
new_pts = [];
for i=1:size(points,1)-1
    t_z = (points(i,4:6)/norm(points(i,4:6)));
    t_x = points(i+1,1:3) - points(i,1:3);
    t_y = cross( t_z, t_x );
    t_y = t_y / norm(t_y);
    t_x = cross( t_y,t_z );
    t_x = t_x/norm(t_x);
    new_pts = [ new_pts; [points(i,1:3), t_x, t_y, t_z] ];
end
new_pts(:,1:3) = new_pts(:,1:3)./1000;


part = 'Mold_Ascent.stl';
[v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
v_part = v_part./1000;
part_plt(v_part, f_part, name_part, [0.2,0,0]);
figure(1)
part_plt(v_part, f_part, name_part, [0.2,0,0]);
daspect([1,1,1])
hold on;
scatter3( new_pts(:,1),new_pts(:,2),new_pts(:,3), 20, 'y', 'filled' )
xlabel('X Axis')
ylabel('Y Axis')
zlabel('Z Axis')


quiver3( new_pts(:,1),new_pts(:,2),new_pts(:,3),new_pts(:,4),new_pts(:,5),new_pts(:,6), 'r' );
quiver3( new_pts(:,1),new_pts(:,2),new_pts(:,3),new_pts(:,7),new_pts(:,8),new_pts(:,9), 'g' );
quiver3( new_pts(:,1),new_pts(:,2),new_pts(:,3),new_pts(:,10),new_pts(:,11),new_pts(:,12), 'b' );
