clear all;
close all;
clc;

warning 'off';
global CONFIG;
global robot1;
global q3;
global solution_pose;
solution_pose = [];

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
CONFIG.tool_stl = 'roller.stl'; % Roller Tool


part = 'Mold_Ascent.stl';
[v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
v_part(:,1:3) = v_part(:,1:3)./1000;



% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  -49.39, 0, 133.53, 1000]'./1000; % Roller

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  -49.39, 0, 133.53, 1000]'./1000; % Roller

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [-0.0494; 0; 0.1335]; % For Roller

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

%% %%%%%% CURVE DATA %%%%%%%%%%%%%
% Ascent Part
curve_file_name  = 'ascent_lockheed_points.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
grp_idx = csvread('ascent_lockheed_grp.csv');
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);



tolerances(1) = 0.003;
tolerances(2) = 0.0524;
tolerances(3) = 0.0524;
tolerances(4) = 0.7;

Force_vecs = zeros(no_pts,6);
Force_vecs(:,3) = ones(no_pts,1).*30;   % 30 N force

desired_velc = 50; % mm/s
desired_velc = desired_velc/1000;

path_len = 0;
velocities = [];
seg_time = [];

for i = 1:size(grp_idx,1)
    seg_time = [ seg_time; 600 ];    
    for j = grp_idx(i,1):grp_idx(i,2)-1
        dist_vec = (xyz_bxbybz(j+1,1:3)-xyz_bxbybz(j,1:3));
        dist = norm( dist_vec );
        dist_vec = dist_vec/dist;
        path_len = path_len + dist;
        velocities = [velocities; dist_vec*desired_velc  ];
        seg_time = [ seg_time; dist/desired_velc ];
    end
    velocities = [velocities; [0,0,0]];
end
seg_time = seg_time;


%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

theta_lb = theta_lb*0.95;
theta_ub = theta_ub*0.95;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 3000;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-8;
options.StepTolerance = 1e-8;
options.Display = 'off';
options.SpecifyObjectiveGradient = false;
options.ObjectiveLimit = 1e-10;


% Actual Pose
q = eul2quat( [deg2rad(90),deg2rad(0),deg2rad(0)],'ZYX' );
CONFIG.pose = [ 0.8;-0.3;0.1267; q' ];

% Optimization routine parameters
A = []; b = [];

% Defining the Lower and Upper Bounds
lb(1) = 0;
lb(2) = -1;
lb(3) = 0.0467;
lb(4) = -1;
lb(5) = 0;
lb(6) = 0;

ub(1) = 1;
ub(2) = 1;
ub(3) = 0.0467;
ub(4) = 1;
ub(5) = 0;
ub(6) = 0;


Aeq = []; beq = [];
CONFIG.options = optimoptions('fmincon');
CONFIG.options.MaxIterations = 1e5;
CONFIG.options.MaxFunctionEvaluations = 1e7;
CONFIG.options.OptimalityTolerance = 1e-10;
CONFIG.options.StepTolerance = 1e-10;
CONFIG.options.Display = 'iter';
CONFIG.options.ObjectiveLimit = 1e-15;



%% Exact Optimization for IK Eval
% Satisfy Constraint Violations in sequence Position/Orientation, Velocity,
% and Force


% reach_poses = csvread('ascent_reach_poses.csv');
% prt_poses = [];
% feasible_poses = [];
% for i=1:21
%     if reach_poses(8,i) == no_pts
%         prt_poses = [prt_poses, reach_poses(1:7,i)];
%     end
% end

% no_samples = size(prt_poses,2);
no_samples = 1;
iter = 0;
success_cases = 0;
success_time = [];
fail_time = [];
feasible_indices = [];
avg_time = 0;
overall_strt = tic;

failed_idx = [];
for poses = 1:no_samples
    strt = tic;
    solution_pose = [];
    
%     Xk = [0.865, 0.57, 0.0467, -0.49076, 0, 0, -0.87129]';  % Out of reach
%     Xk = [0.775, -0.182, 0.0467,  0.82119,  0, 0 ,0.57066]'; % Intermediate
%     Xk = [0.829,  0.217,  0.0467, 0.5334, 0,  0,  0.8458]';    % Solution
    
%     Xk = [0.7138    0.1087    0.0532    0.6220   -0.0005   -0.0020    0.7830]'; % Demo pose

    Xk = [0.8,0.25,0.0467,0,0,0,1]';    % Lockheed Demo Pose
    
%     world_T_part = eye(4);
%     world_T_part(1:3,1:3) = eul2rotm( [pi,0,0],'ZYX' );
%     world_T_part(1:3,4) = Xk(1:3);
%     % Plot Part and Robot
%     plot_robot([pi/4;0;0;-pi/2;0;pi/2;0],robot1,CONFIG.tool_stl);
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,world_T_part);
%     part_plt(v_part_transf, f_part, name_part, [0.2,0,0]); % plot tool stl
%     daspect([1,1,1]);
%     return;
    
%     Xk = prt_poses(:,poses);
%     Xk = CONFIG.pose;
    fprintf('Sample Index:  %d\n',poses)
    % Evaluate Intial Reachability
    joint_angles = [];
    [ ee_points, velc_vecs ] = transform_points( Xk,xyz_bxbybz,velocities );
    no_pts = size( xyz_bxbybz, 1 );
    reach = 0;
    for i = 1:size(grp_idx,1)
        joint_config = [0.31189,0.2209,-0.1785,-1.5357,0.0176,1.3463,0]';
        for j = grp_idx(i,1):grp_idx(i,2)
            [joint_config,status] = ascent_IK_constr( joint_config,ee_points(j,:),tolerances,options,theta_lb,theta_ub,...
                                seg_time,j, velc_vecs, Force_vecs,joint_vel,grp_idx );
            if status
%                 if ~ismember( j, grp_idx(:,1) )
%                     sanity_inf_norm_check = norm(joint_angles(:,end) - joint_config, inf);
%                     disp(rad2deg(sanity_inf_norm_check))
%                 end
                reach = reach + 1;
            else
                failed_idx = [failed_idx, j ];
            end
            joint_angles = [ joint_angles, joint_config ];
        end
    end
    fprintf( 'Initial Reachability is:  %d\n', reach );
    
    return;
%     if reach==no_pts
%         feasible_poses = [feasible_poses, Xk];
%     end
%     continue;
    
%     if reach == no_pts
%         disp('Solution Found! Initially Reachable')
%         feasible_indices = [feasible_indices; poses];
%         success_cases = success_cases + 1;
%         end_time = toc(strt);
%         success_time = [ success_time; end_time ];
%         disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
%         fprintf('\n\n\n\n')
%         continue;
%     end
    
    iter = iter + 1;
    continue;
    
% Fmincon Routine
    x0 = Xk(1:6);
    q3 = Xk(7);
    
    % % SOLVE USING fmincon % % 
    disp('Solving for Exact Reachability......')
    
    [x,err] = fmincon( @(x)ascent_violations( x,xyz_bxbybz,tolerances, options, theta_lb,theta_ub,...
                            seg_time, velocities, Force_vecs,joint_vel ),...
                            x0,A,b,Aeq,beq,lb,ub,[],CONFIG.options );
    x = [x; q3];
    x(4:7) = x(4:7)/norm(x(4:7));
    q3 = x(7);
    
    
    joint_angles = [];
    [ ee_points, velc_vecs ] = transform_points( x,xyz_bxbybz,velocities );
    no_pts = size( xyz_bxbybz, 1 );
    reach = 0;                                    
    for i = 1:size(grp_idx,1)
        joint_config = [0;0;0;0;0;0;0];
        for j = grp_idx(i,1):grp_idx(i,2)
            [joint_config,status] = ascent_IK_constr( joint_config,ee_points(j,:),tolerances,options,theta_lb,theta_ub,...
                                seg_time,j, velc_vecs, Force_vecs,joint_vel,grp_idx );
            if status
                reach = reach + 1;
                joint_angles = [ joint_angles, joint_config ];
            end
        end
    end
    
    if reach ~= no_pts
        disp('Solution not found')
        fail_time = [ fail_time; end_time ];
    else
        disp('Solution Found! Part Pose is:  ')
        feasible_indices = [feasible_indices; poses];
        success_time = [ success_time; end_time ];
        success_cases = success_cases + 1;
    end
    
    fprintf( 'Reachability after optimization is:  %d\n', reach );
    fprintf('Success so far:    %d\n',success_cases);
    
    avg_time = avg_time + end_time;
    fprintf('Avg time so far:    %d\n',avg_time/poses);
    disp('%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%')
    fprintf('\n\n\n\n')
end
overall_end = toc(overall_strt);

fprintf( 'Success:  %d\n', success_cases );


% Feasible indices: 1,2,4
% Reachable but not feasible: 3,5


% Solution
% 0.829
% 0.217
% 0.0467
% 0.5334
% 0
% 0
% 0.8458
