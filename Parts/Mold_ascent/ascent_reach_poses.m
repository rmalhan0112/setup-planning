warning 'off';
global CONFIG;
global robot1;

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
CONFIG.tool_stl = 'roller.stl'; % Roller Tool



% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  -49.39, 0, 133.53, 1000]'./1000; % Roller

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  -49.39, 0, 133.53, 1000]'./1000; % Roller

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [-0.0494; 0; 0.1335]; % For Roller

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

%% %%%%%% CURVE DATA %%%%%%%%%%%%%
% Ascent Part
curve_file_name  = 'Ascent_points.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
grp_idx = csvread('Ascent_Group_IDX.txt');
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);



tolerances(1) = 0.003;
tolerances(2) = 0.0524;
tolerances(3) = 0.0524;
tolerances(4) = 1;


%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 1500;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-8;
options.StepTolerance = 1e-8;
options.Display = 'off';
options.SpecifyObjectiveGradient = false;
options.ObjectiveLimit = 1e-8;


% Reading Part Poses from successful iterative descent cases
% prt_poses = [];
% for i=1:30
%     x = csvread( strcat('Sample_',num2str(i),'.csv') );
%     if x(8,1) == 0
%         prt_poses = [prt_poses, x(1:7,end)];
%     end
% end
% 
% no_samples = size(prt_poses,2);
no_samples = 1;
success_cases = 0;


avg_time = 0;
overall_strt = tic;
s_time = [];
f_time = [];
updated_X = [];
reach_points = [];

for poses = 1:no_samples
    strt = tic;
    fprintf('Sample Index:  %d\n',poses)
%     x = prt_poses(:,poses);
    
    x = [0.775, -0.182, 0.0467,  0.8212,  0, 0 ,0.57066]';
    
    target_pts = transf_target( x,xyz_bxbybz );
    reach = 0;
    joint_angles = [];
    failed_idx = [];
    
    for i = 1:size(grp_idx,1)
        joint_config = [0.31189,0.2209,-0.1785,-1.5357,0.0176,1.3463,0]';
        for j = grp_idx(i,1):grp_idx(i,2)
            [joint_config,status] = ascent_IK( joint_config,target_pts(j,:),tolerances,...
                                        options, theta_lb, theta_ub );
            if status
%                 if ~ismember( j, grp_idx(:,1) )
%                     sanity_inf_norm_check = norm(joint_angles(:,end) - joint_config, inf);
%                     disp(rad2deg(sanity_inf_norm_check))
%                 end
                reach = reach + 1;
            else
                failed_idx = [failed_idx; j ];
            end
            joint_angles = [ joint_angles, joint_config ];
        end
    end
    
    end_time = toc(strt);
    if reach==no_pts
        disp('Solution Verified.')
        s_time = [s_time; end_time];
        success_cases = success_cases + 1;
    else
        disp('Solution doesnot exist')
        f_time = [f_time; end_time];
    end
    fprintf('\n\n\n');
    updated_X = [updated_X, [x;reach]];
end
fprintf('Success Cases:  %d\n',success_cases);
% csvwrite('ascent_reach_poses.csv',updated_X);
