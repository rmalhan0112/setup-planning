function err = compute_ascent_err( x, xyz_bxbybz,tolerances, IKoptions, theta_lb, theta_ub)
    global robot1;
    
    x(4:7) = x(4:7)/norm(x(4:7));
    
    err = 0;
    transf_xyzbxbybz = transf_points( x,xyz_bxbybz, robot1 );
    no_pts = size( xyz_bxbybz, 1 );
    
    joint_config = [0;0;0;0;0;0;0];
    reach = 0;                                    
    for i = 1:no_pts
        [joint_config,status] = ascent_IK( joint_config,transf_xyzbxbybz(i,:),tolerances,...
                                            IKoptions, theta_lb, theta_ub,false,3 );
        if status
            reach = reach + 1;
            continue;
        else
            ee_base = get_iiwa_FK_mex( joint_config,eye(4) );
% Error Position
            err_pos = transf_xyzbxbybz(i,1:3)'-ee_base(1:3,4);
% Error Orientation
            err_orientation = [ 1 - ( transf_xyzbxbybz(i,4)*ee_base(1,1) + ...
                                transf_xyzbxbybz(i,5)*ee_base(2,1) + ...
                                transf_xyzbxbybz(i,6)*ee_base(3,1) );
                                
                                1 - ( transf_xyzbxbybz(i,7)*ee_base(1,2) + ...
                                transf_xyzbxbybz(i,8)*ee_base(2,2) + ...
                                transf_xyzbxbybz(i,9)*ee_base(3,2) );
                                
                                1 - ( transf_xyzbxbybz(i,10)*ee_base(1,3) + ...
                                transf_xyzbxbybz(i,11)*ee_base(2,3) + ...
                                transf_xyzbxbybz(i,12)*ee_base(3,3) )           ];
                            
            err = err + 0.5*(  0.25*err_orientation(1)^2 + 0.25*err_orientation(2)^2 + 0.25*err_orientation(3)^2 + ...
                                err_pos(1)^2 + err_pos(2)^2 + err_pos(3)^2  );
        end
    end
end