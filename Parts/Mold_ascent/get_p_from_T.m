function p = get_p_from_T(T)

    p = [T(1:3,4)',T(1:3,1)',T(1:3,2)',T(1:3,3)'];

end