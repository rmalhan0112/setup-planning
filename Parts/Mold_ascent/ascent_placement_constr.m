function [c,ceq] = ascent_placement_constr( theta,prev_config,point,pt_idx,velocity_vec,force_vec,joint_vel,grp_idx )
    global robot1;
    
    c = [];
    ceq = [];
    
% Velocity Constraint
    FK_all = get_iiwa_FK_all_joints_mex( theta,eye(4) );
    ee_base = FK_all(33:36,:);
    Jac = get_iiwa_GeoJac( FK_all ); % Wxyz, Vxyz
    Jac = [  Jac(4:6,:); Jac(1:3,:)  ];
    J_inv = pinv( Jac(1:3,:) );
    joint_vec = J_inv * velocity_vec';  % Upper Bound
    c = [ c; [ (joint_vec - joint_vel)] ];
    c = [ c; [ (-joint_vec - joint_vel)] ];
    
% Force Constraint    
    force_vec(1:3) = ee_base(1:3,1:3) * force_vec(1:3)';
    joint_torq = Jac' * force_vec';  % Upper Bound
    c = [ c; [ (joint_torq - [ 176; 176; 110; 110; 110; 40; 40 ])] ];
    c = [ c; [ (-joint_torq - [ 176; 176; 110; 110; 110; 40; 40 ])] ];
    
    if ~ismember(pt_idx,grp_idx(:,1))
% Correlation Based Continuity Estimate
        FK_prev = get_iiwa_FK_all_joints_mex( prev_config,eye(4) );
        Jac_prev = get_iiwa_GeoJac( FK_prev ); % Wxyz, Vxyz
        Jac_prev = [  Jac_prev(4:6,:); Jac_prev(1:3,:)  ];
        c = [c;  -corr2( Jac,Jac_prev ) + 0.9];
    end

% ROBOT end-effector
    transf_mat = ee_base * robot1.robot_ree_T_tee; % For attaching tool
    
% Error Function. Donot Change
    error = transf_mat(1,3)*point(10) + transf_mat(2,3)*point(11) + transf_mat(3,3)*point(12);
    c = [ c; -error + 0.7 ];
end