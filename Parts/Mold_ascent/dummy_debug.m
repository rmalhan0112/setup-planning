warning 'off';
global CONFIG;

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
CONFIG.tool_stl = 'roller.stl'; % Roller Tool



% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  -49.39, 0, 133.53, 1000]'./1000; % Roller

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  -49.39, 0, 133.53, 1000]'./1000; % Roller

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [-0.0494; 0; 0.1335]; % For Roller

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%



%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 50000;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-8;
options.StepTolerance = 1e-8;
options.Display = 'off';
options.SpecifyObjectiveGradient = false;
options.ConstraintTolerance = 1e-6;
options.ObjectiveLimit = 1e-8;

%% 

% tolerances(1) = 0.003;
% tolerances(2) = 0.0524;
% tolerances(3) = 0.0524;
% tolerances(4) = 1.0472;

tolerances(1) = 0.0001;
tolerances(2) = 0.000;
tolerances(3) = 0.000;
tolerances(4) = 0.000;

theta = get_random_theta();
w_T_f = get_iiwa_FK_mex(theta,eye(4));
f_T_tcp = robot1.robot_ree_T_tee;
p = get_p_from_T(w_T_f);
[jt,s] = ascent_IK( [0;0;0;0;0;0;0],p,tolerances,options,theta_lb,theta_ub,false,3 );

w_T_f_fromIK = get_iiwa_FK_mex(jt,eye(4));

disp(sum(sum(abs(w_T_f-w_T_f_fromIK))))


plot_robot(theta,robot1,CONFIG.tool_stl); daspect([1,1,1])





