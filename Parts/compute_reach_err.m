function err = compute_reach_err( x,xyz_bxbybz,tolerances, IKoptions, theta_lb, theta_ub,...
                                    seg_time, velocity_vecs, Force_vecs,joint_vel)
    global robot1;
    global q3;
    global solution_pose;
    
    x = [x; q3];
    x(4:7) = x(4:7)/norm(x(4:7));
    q3 = x(7);
    
    err = 0;
    transf_xyzbxbybz = transf_points( x,xyz_bxbybz, robot1 );
    no_pts = size( xyz_bxbybz, 1 );
    
    joint_config = [0;0;0;0;0;0;0];
    reach = 0;                                    
    for i = 1:no_pts
        [joint_config,status] = constr_IK( joint_config,transf_xyzbxbybz(i,:),tolerances,...
                                            IKoptions, theta_lb, theta_ub,...
                                            seg_time,i, velocity_vecs, Force_vecs,joint_vel,false,3 );
        if status
            reach = reach + 1;
            continue;
        else
            ee_base = get_iiwa_FK_mex( joint_config,eye(4) );
% Error Position
            err_pos = transf_xyzbxbybz(i,1:3)'-ee_base(1:3,4);
% Error Orientation
            err_orientation = 1 - ( transf_xyzbxbybz(i,10)*ee_base(1,3) + ...
                            transf_xyzbxbybz(i,11)*ee_base(2,3) + ...
                            transf_xyzbxbybz(i,12)*ee_base(3,3) );
            err = err + 0.5*(  0.25*err_orientation^2 + err_pos(1)^2 + err_pos(2)^2 + err_pos(3)^2  );
        end
    end
%     err = tanh( err*10 );
    if reach==no_pts
        fprintf('Reachable and error is:  %d\n',err);
        solution_pose = [ solution_pose, x ];
    end
end