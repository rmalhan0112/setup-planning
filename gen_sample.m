% Generate samples
X = [0:0.01:0.9];
Y = [-0.9:0.01:0.9];
Z = [0:0.01:0.7];
A = [-pi:0.0873:pi];
B = [-pi:0.0873:pi];
C = [-pi:0.0873:pi];
%% %%%%%% CURVE DATA %%%%%%%%%%%%%
curve_file_name  = 'HS_points.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
close all;
figure(1)
plot_robot([0;0;0;0;0;0;0],robot1);
pause(0.000001)

figure(2)
boundary_X = [0.3,0.8,0.8,0.3,0.3];
boundary_Y = [-0.8,-0.8,0.8,0.8,-0.8];
xlabel('X')
ylabel('Y')
part = 'HS.STL';

% Plot the Transformed Part
[v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
v_part = v_part./1000;

minX = min(v_part(:,1));
minY = min(v_part(:,2));
maxX = max(v_part(:,1));
maxY = max(v_part(:,2));

p1 = [maxX;minY;0;1];
p2 = [maxX;maxY;0;1];
p3 = [minX;maxY;0;1];
p4 = [minX;minY;0;1];

figure(2)
hold on;
plot(boundary_X,boundary_Y,'r','linewidth',2);
counter = 1;
samples = [];
while true
    pose = [randsample(X,1);
             randsample(Y,1);
             randsample(Z,1);
             randsample(A,1);
             randsample(B,1);
             randsample(C,1);
             ];
    robot_T_part = eye(4);
    robot_T_part(1:3,4) = [pose(1); pose(2); pose(3)];
    robot_T_part(1:3,1:3) = eul2rotm([pose(4), pose(5), pose(6)],'ZYX');
    rotation = robot_T_part(1:3,1:3);
    prt_Z = rotation*[0;0;1];
    if abs(acos(dot(prt_Z,[0;0;1]))) > 1.22
        continue;
    end
    
    p1_dash = robot_T_part*p1;
    p2_dash = robot_T_part*p2;
    p3_dash = robot_T_part*p3;
    p4_dash = robot_T_part*p4;
    
    X_pts = [p1_dash(1),p2_dash(1),p3_dash(1),p4_dash(1),p1_dash(1)];
    Y_pts = [p1_dash(2),p2_dash(2),p3_dash(2),p4_dash(2),p1_dash(2)];
%     figure(2); plot(X_pts,Y_pts,'b','linewidth',1);
    in = inpolygon(X_pts,Y_pts,boundary_X,boundary_Y);
    if ~in
        continue;
    end
    temp = (rotation * xyz_bxbybz(:,1:3)')';
    parfor i = 1:no_pts
        temp(i,:) = temp(i,:) + pose(1:3)';
    end
    
    transf_xyzbxbybz(:,1:3) = temp(:,1:3);
    transf_xyzbxbybz(:,4:6) = (rotation * xyz_bxbybz(:,4:6)')';
    transf_xyzbxbybz(:,7:9) = (rotation * xyz_bxbybz(:,7:9)')';
    transf_xyzbxbybz(:,10:12) = (rotation * xyz_bxbybz(:,10:12)')';

    for i = 1:no_pts
        transf_xyzbxbybz(i,4:6) = transf_xyzbxbybz(i,4:6)./norm(transf_xyzbxbybz(i,4:6));
        transf_xyzbxbybz(i,7:9) = transf_xyzbxbybz(i,7:9)./norm(transf_xyzbxbybz(i,7:9));
        transf_xyzbxbybz(i,10:12) = transf_xyzbxbybz(i,10:12)./norm(transf_xyzbxbybz(i,10:12));
    end 
    
    points = transf_xyzbxbybz(:,1:3);
    
%     figure(1)
%     hold on;
%     part = 'HS.STL';
%     [v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
%     v_part = v_part./1000;
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,robot_T_part);
%     part_plt(v_part_transf, f_part, name_part, [0.5,0.6,0.8]); % plot tool stl
    samples = [samples; [pose(1),pose(2),pose(3),rotm2quat(robot_T_part(1:3,1:3))]];
    if counter==2000
        break;
    end
    counter = counter + 1;

    % hold on;
    % pause(1.5);
    % plot_robot([0;0;0;-pi/2;0;pi/2;0],robot1);
    % a = quiver3(points(:,1),points(:,2),points(:,3),...
    %        bx(:,1),bx(:,2),bx(:,3),'r','linewidth',2,'AutoScaleFactor',0.5);
    %    hold on;
    % b = quiver3(points(:,1),points(:,2),points(:,3),...
    %        by(:,1),by(:,2),by(:,3),'g','linewidth',2,'AutoScaleFactor',0.5);
    %    hold on;
    % c = quiver3(points(:,1),points(:,2),points(:,3),...
    %        bz(:,1),bz(:,2),bz(:,3),'b','linewidth',2,'AutoScaleFactor',0.5);
    daspect([1,1,1]);
    pause(0.000001);
end

csvwrite('Samples.csv',samples);