function fetch = acq_data_QP(x,init_joints,xyz_bxbybz, theta_lb, theta_ub, options, joint_vel,tolerances,robot1)
    no_pts = size(xyz_bxbybz,1);
    
    fetch.translation = x(1:3);
    fetch.quat = x(4:7);
    rotation = quat2rotm(fetch.quat');
    temp = (rotation * xyz_bxbybz(:,1:3)')';
    parfor i = 1:no_pts
        temp(i,:) = temp(i,:) + x(1:3)';
    end
    
    transf_xyzbxbybz(:,1:3) = temp(:,1:3);
    transf_xyzbxbybz(:,4:6) = (rotation * xyz_bxbybz(:,4:6)')';
    transf_xyzbxbybz(:,7:9) = (rotation * xyz_bxbybz(:,7:9)')';
    transf_xyzbxbybz(:,10:12) = (rotation * xyz_bxbybz(:,10:12)')';

    for i = 1:no_pts
        transf_xyzbxbybz(i,4:6) = transf_xyzbxbybz(i,4:6)./norm(transf_xyzbxbybz(i,4:6));
        transf_xyzbxbybz(i,7:9) = transf_xyzbxbybz(i,7:9)./norm(transf_xyzbxbybz(i,7:9));
        transf_xyzbxbybz(i,10:12) = transf_xyzbxbybz(i,10:12)./norm(transf_xyzbxbybz(i,10:12));
    end 
    d_theta_init = init_joints;
    prev_config = init_joints;
    pt_data = zeros(no_pts,12); % IsReachable, t1-t7 joint values, Max Velcotiy Poss, pose_error, manip index, C.N. 
    pt_data(:,1) = ones(no_pts,1);
    A = [eye(7);eye(7)];
%     A(1:7,:) = -1*A(1:7,:);
    A(8:14,:) = -1*A(8:14,:);
    b = ones(14,1);
%     b(1:7,1) = zeros(7,1);
    b(1:7,1) = -(prev_config-theta_ub);
    b(8:14,1) = -(theta_lb-prev_config);
    % For first point
    [prev_config,err_theta] = fmincon(@(theta)reachability_err(theta,transf_xyzbxbybz,...
                            1,tolerances,robot1),prev_config,[],[],[],[],theta_lb,theta_ub,[],options); 
%     angles = [];
    for i = 1:no_pts
%         J = robot1.get_jacobian(prev_config);
        J = iiwa_analytical_jacobian(prev_config);
        H = 2*(J'*J);
        F = QP_gradient(prev_config,transf_xyzbxbybz,i,robot1,J);
        [joints,err_theta] = quadprog(H,F,A,b,[],[],[],[],d_theta_init,options); 
        prev_config = prev_config + joints;
%         angles = [angles, prev_config];
        % Debugging err
        compute_pose_err(prev_config,transf_xyzbxbybz,i,tolerances,robot1)
        pt_data(i,2:8) = prev_config';
        J = robot1.get_jacobian(prev_config);
        [U,D,V] = svd(J);
        vec = D*ones(7,1);
        K = (abs(max(vec))^0.5) / (abs(min(vec))^0.5);
        if err_theta>2
           pt_data(i,1) = 0; 
           pt_data(i,9) = 0;
        else
            velc = J*joint_vel;
            pt_data(i,9) = norm(velc(1:3));
        end
        pt_data(i,10) = err_theta;
        pt_data(i,11) = (abs(det(J*J')))^0.5;
        pt_data(i,12) = K;
    end
    fetch.pt_data = pt_data;
end