% function handles 
classdef custom_IKHelpers < robotics.manip.internal.InternalAccess
    %This class is for internal use only. It may be removed in the future.
    
    %   Copyright 2016 The MathWorks, Inc.
    
    %#codegen

    %IKHELPERS Defines IK-specific function handles required by NLP solver
    methods(Static)
        
        function [cost, W, Jac, args] = computeCost(x, args)
            %computeCost 
            robot = args.Robot;
            bodyName = args.BodyName;
            Td = args.Tform; 
            weightMatrix = args.WeightMatrix;

            % T = robot.getTransform(x, bodyName);
            % J = robot.geometricJacobian(x, bodyName); % To be optimized
            
            [T, J] = efficientFKAndJacobianForIK(robot.TreeInternal, x, bodyName);
            
            % Note the geometric Jacobian of the robot and the Jacobian of
            % the cost error differ by a negative sign.
            Jac = -J;
            e = robotics.manip.internal.IKHelpers.poseError(Td, T);
            args.ErrTemp = e;
            args.CostTemp = 0.5*e'*weightMatrix*e; % 0.5 need justification
            args.GradTemp = (e'*weightMatrix*Jac)';
            cost = args.CostTemp;
            W = weightMatrix;
        end

        function grad = computeGradient(x, args)
            %computeGradient
            grad = args.GradTemp;
        end  

        function [en, evec] = evaluateSolution(x, args)
            %evaluateSolution
            en = norm(args.WeightMatrix * args.ErrTemp);
            evec = args.ErrTemp;
        end 

        function rc = randomConfig(args)
            %randomConfig
            rc = randomJointPositions(args.Robot.TreeInternal);
        end

        function xc = clamping(x, args)
            %clamping
              xc = min(args.Limits(:,2), max(args.Limits(:,1), x) );
        end
        

        function errorvec = poseError(Td, T)
            %poseError
            R = T(1:3,1:3);
            Rd = Td(1:3,1:3);
            p = T(1:3,4);
            pd = Td(1:3,4);

%             v = rotm2axang(Rd*R');
%             err_orientation = v(4)*v(1:3)';
            err_linear = pd - p;
% Rishi changes
            bx = Rd*[1;0;0];
            by = Rd*[0;1;0];
            bz = Rd*[0;0;1];
            
            toolx = R*[1;0;0];
            tooly = R*[0;1;0];
            toolz = R*[0;0;1];
            
            ee_pose = [  dot(toolx,bx),  dot(tooly,bx),  dot(toolz,bx);
                         dot(toolx,by),  dot(tooly,by),  dot(toolz,by);
                         dot(toolx,bz),  dot(tooly,bz),  dot(toolz,bz)    ];
                     
            v = rotm2axang( ee_pose );
            err_orientation = v(4)*v(1:3)';
            errorvec = [err_orientation; err_linear ];
        end

        
  
    end

end