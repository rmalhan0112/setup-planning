% Implementation of a simple policy of HGD on the Spiral part

% Objective is to minimize time subject to different constraints.
% close all;
% clear all;
% clc
% format short g;

warning 'off';
global CONFIG;

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
joint_vel = joint_vel*0.8;
CONFIG.tool_stl = 'tool.stl';

% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.120]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


%% %%%%%% CURVE DATA %%%%%%%%%%%%%
curve_file_name  = 'HS_points.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
tol_file_name  = 'HS_tolfile.csv'; % n x 4 
tolerances = dlmread(tol_file_name);
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);
% 0.0122 is distance from one point to another. Total length 0.61


%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 1e7;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-4;
options.StepTolerance = 1e-4;
options.Display = 'off';


% no_samples = 2000;
% samples = csvread('Samples.csv');

% x0 = samples(randsample([1:2000],1),:)';
% Bounds on XYZ and quat.
lb = [ 0;-0.7;0;-1;-1;-1;-1 ];
ub = [ 0.7;0.7;0.7;1;1;1;1 ];
% Xk(4:7,1) = [ 0.81988;
%             -0.42573;
%             -0.17642;
%             0.33975 ];
% Reach 17
% Xk(1:3,1) = [0.2;   0.8;    0.3];


% Sampling to determine the best reachable sample
tic;
Xk = random_gen(lb,ub,xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);
toc;

fprintf('Initial pose: \n'); disp(Xk); fprintf('\n\n');
iter = 0;
max_iter = 50;
d_step = 2;
            
Poses_traversed = [];
perturb_idx = [ [1,1]; [1,-1]; [2,1]; [2,-1]; [3,1]; [3,-1]; [4,1]; [4,-1];...
                [5,1]; [5,-1]; [6,1]; [6,-1]; [7,1]; [7,-1] ];

% figure(1)
% hold on;
% xlabel('Iteration Number')
% ylabel('Pose Error')
% xlim([0, max_iter]);
% 
% figure(2)
% hold on;
% xlabel('Iteration Number')
% ylabel('Reachability')
% xlim([0, max_iter]);
% pause(0.1);

%% Hierarchical Gradient Descent
tic;
while iter<max_iter
    tic;
    iter = iter + 1;
    fprintf('Iteration Number:  %d\n',iter);
    sample_data = {};
    sample_data{1} = acq_data(Xk, [0;0;0;0;0;0;0],xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);
% Analyzing current sample
    sample_reach = sample_data{1}.reachability;
    sample_pose_err = sum(sample_data{1}.pt_data(:,10)) + sum(sample_data{1}.pt_data(:,11));
%     figure(1); scatter( iter, sample_pose_err, 100, 'b', '.' );  pause(0.0001);
%     figure(2); scatter( iter, sample_reach, 100, 'b', '.' );  pause(0.0001);
    fprintf( 'Current pose: \n'); disp(Xk);
    fprintf( 'Reachability: %d\n',sample_reach);
    fprintf( 'Pose Error: %d\n',sample_pose_err);
    fprintf( 'Minimum Velocity: %d\n\n\n', min(sample_data{1}.pt_data(:,9)) );
    Poses_traversed = [ Poses_traversed, [Xk; sample_reach; sample_pose_err] ];
    if sample_reach==100
        disp('Reachability Optimized');
        break;
    end
    Q = perturb(Xk);
%     Q = Q(:,[1,3,5,7,9,11]); % Only for forward difference
    Q_size = size(Q,2);
    parfor counter = 1:Q_size
       sample_data{counter+1} =  acq_data(Q(:,counter), [0;0;0;0;0;0;0],xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);
    end
% Analyzing the gradient
    reachabilities = [];
    pose_err_gradient = [];
    parfor i=2:Q_size+1
        reachabilities = [reachabilities; sample_data{i}.reachability];
        pose_err_gradient = [pose_err_gradient; ...
                            sum(sample_data{i}.pt_data(:,10)) + sum(sample_data{i}.pt_data(:,11))  ];
    end
    % Compute Central difference
    temp = (reachabilities([1,3,5,7,9,11],1)-reachabilities([2,4,6,8,10,12],1))/0.1;
%     temp = (reachabilities - sample_reach*ones(6,1))/0.05;
    reach_gradient = [temp;0];
    reach_gradient = reach_gradient/norm(reach_gradient);
%     pose_gradient = (pose_err_gradient([1,3,5,7,9,11],1)-pose_err_gradient([2,4,6,8,10,12],1))/0.02;
%     pose_gradient = [pose_gradient;0];
%     pose_gradient = -pose_gradient/norm(pose_gradient);
    
    gradient = reach_gradient;
    
    [step,dir_data] = get_Step(Xk, gradient, xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,...
                            robot1, d_step, sample_pose_err, sample_reach, lb, ub);
    csvwrite( fullfile( 'dirc_reach', strcat('Pose_',num2str(iter),'.csv') ), dir_data );
%     if step~=0
%         fprintf('Step Length:  %d\n',step);
%         Xk = Xk + step*gradient;
%         Xk(4:7) = Xk(4:7)/norm(Xk(4:7));
%     else
%         fprintf('No direction of descent available. Generating another random sample\n');
    Xk = random_gen(lb,ub,xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);
%     end
    toc;
end
toc;
% csvwrite('Poses_traversed.csv',Poses_traversed);




function [step,direction_data] = get_Step(Xk, gradient, xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,...
                            robot1, d_step, pose_err, reach, lb, ub)
    ref_pose_err = pose_err;
    ref_reach = reach;
% Determining the Step size
    direction_data = [];
    direction_data = [direction_data, [Xk; pose_err; reach; 0] ];
    step = 0.005/d_step;
    iter = 0;
    while iter < 10
        iter = iter + 1;
        step = step*d_step;
        xk1 = Xk + gradient*step;
        xk1(4:7) = xk1(4:7)/norm(xk1(4:7));
        if ~(all(ub-xk1)>=0) || ~(all(xk1-lb)>=0)
            break;
        end
        eval_obj = acq_data(xk1, [0;0;0;0;0;0;0],xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);
        pose_err = ( sum(eval_obj.pt_data(:,10)) + sum(eval_obj.pt_data(:,11)) );
        reach = eval_obj.reachability;
        direction_data = [direction_data, [xk1; pose_err; reach; step] ];                
    end
    step_flag = false;
    pose_data = direction_data(8,:);
    reach_data = direction_data(9,:);
    dirc_eval = [reach_data; pose_data];
    while size(dirc_eval,2)~=0
        [val,idx] = max(dirc_eval(1,:));
        if ( dirc_eval(2,idx) <= ref_pose_err ) && (val >= ref_reach)
            step = direction_data(end,idx);
            step_flag = true;
            break;
        end
        dirc_eval(1,idx) = -inf;
    end
    if ~step_flag
        step = 0;
    end
end



function X = random_gen(lb,ub,xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1)
    valid_samples = 0;
    while valid_samples~=1
        sample = [ randsample([lb(1):0.01:ub(1)],1);
                        randsample([lb(2):0.01:ub(2)],1);
                        randsample([lb(3):0.01:ub(3)],1);
                        randsample([lb(4):0.01:ub(4)],1);
                        randsample([lb(5):0.01:ub(5)],1);
                        randsample([lb(6):0.01:ub(6)],1);
                        randsample([lb(7):0.01:ub(7)],1);  ];
        sample(4:7) = sample(4:7)/norm(sample(4:7));
        Rot = quat2rotm(sample(4:7)');
        bz = Rot*[0;0;1];
        bz = bz/norm(bz);
        angle = abs( acos(dot(bz,[0;0;1])) );
        if angle > 1.2217 % 70 degrees
            continue;
        end
        tic;
        fetch_data = acq_data(sample, [0;0;0;0;0;0;0],xyz_bxbybz, theta_lb,...
                                theta_ub, options, joint_vel,tolerances,robot1);
        toc;
        if fetch_data.reachability > 5
            X = sample;
            valid_samples = valid_samples + 1;
        end
    end
end



% if curr_pose_err > sample_pose_err
%         if curr_reach >= sample_reach
%             idx = idx2;
%         else
%             fprintf('Current pose: \n'); disp(Xk);
%             fprintf('Reachability: %d\n',curr_reach);
%             fprintf('Condition Number: %d\n\n\n',curr_pose_err);
%             
%             % Make a random move. If not feasible select another sample
%             % from initial list
%             assign_flag = false;
%             for i=1:3
%                 idx = randsample([1:Q_size],1);
%                 Xk = Q(:,idx);
%                 if ~all( (ub-Xk)>=0 )
%                     continue;
%                 elseif ~all( (Xk-lb)>=0 )
%                     continue;
%                 else
%                     assign_flag = true;
%                     random_move = 1;
%                     break;
%                 end
%             end
%             if ~assign_flag
%                 Xk = list(:,1);
%                 list(:,1) = [];
%                 random_move = 1;
%             end
%             continue;
%         end
% end
%     




% b = sym('b',[no_pts,1]);
% R = {};
% for i=1:no_pts
%     Rz{i} = [ cos(b(i)), -sin(b(i)), 0; sin(b(i)), cos(b(i)), 0; 0,0,1 ];
% end