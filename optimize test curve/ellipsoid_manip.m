clear all;
clc;
close all;


l1 = 0.5;
l2 = 0.3;
l3 = 0.1;
% figure(1)
% hold on;
% figure(2)
% hold on;
dcart = 10;
dtheta = deg2rad(5);
ee = [];

for i=-180:10:180
    for j=-180:10:180
        for k=-180:10:180
            t1 = deg2rad(i);
            t2 = deg2rad(j);
            t3 = deg2rad(k);
            J = [ -l3*sin(t1+t2+t3)-l2*sin(t1+t2)-l1*sin(t1), -l3*sin(t1+t2+t3)-l2*sin(t1+t2), -l3*sin(t1+t2+t3);
                l3*cos(t1+t2+t3)+l2*cos(t1+t2)+l1*cos(t1), l3*cos(t1+t2+t3)+l2*cos(t1+t2), l3*cos(t1+t2+t3);
                1,1,1
                ];
%             J = [ -l2*sin(t1+t2)-l1*sin(t1), -l2*sin(t1+t2);
%                 l2*cos(t1+t2)-l1*cos(t1), l2*cos(t1+t2); 
%                 ];
            [U,D,V] = svd(J);
            vec = D*ones(3,1);
%             detr = (vec(1)^0.5)*(vec(2)^0.5);
            detr = vec(1) / vec(2);
            X = l1*cos(t1)+l2*cos(t1+t2)+l3*cos(t1+t2+t3);
            Y = l1*sin(t1)+l2*sin(t1+t2)+l3*sin(t1+t2+t3);
            
%             detr = (abs(det(J)))^0.5;
            ee = [ee; [X,Y,detr] ];
%             fprintf('%f + %fj\n',real(detr), imag(detr));
%             bst = U(1,1:2)'*dcart;
%             bst(end+1) = U(1,3)'*dtheta;
%             wrst = U(2,1:2)'*dcart;
%             wrst(end+1) = U(3,3)'*dtheta;
              
%             Jinv = J^-1;
%             a = max(rad2deg(Jinv*bst));
%             b = max(rad2deg(Jinv*wrst));
            
%             figure(1)
%             scatter3(t1,t2,a-b,50,'r','.');
%             figure(2)
%             scatter(cnt,b,50,'b');
%             pause(0.00000001);
        end
    end
end

% ee(:,end) = ee(:,end)/norm(ee(:,end));
figure
scatter(ee(:,1),ee(:,2),100,ee(:,end),'.');
xlabel('X')
ylabel('Y')
colorbar
hold on;
t1 = deg2rad(60);
t2 = deg2rad(40);
t3 = deg2rad(50);
X = [0; l1*cos(t1); l1*cos(t1)+l2*cos(t2); l1*cos(t1)+l2*cos(t2)+l3*cos(t3)];
Y = [0; l1*sin(t1); l1*sin(t1)+l2*sin(t2); l1*sin(t1)+l2*sin(t2)+l3*sin(t3)];
            
plot(X,Y,'r');
ylim([0,1]);
% imagesc(ee(:,end)); 
% colormap jet; 

% grid on;
% [X,Y] = meshgrid(-3.14:3.14,-3.14:3.14);
% patch( [0,3.14,3.14,0,0], [0,0,3.14,3.14,0], [0,0,0,0,0], 'cyan' );
% xlabel('theta 1');
% ylabel('theta 2');
% zlabel('Norm(theta Best - Worst)');
 