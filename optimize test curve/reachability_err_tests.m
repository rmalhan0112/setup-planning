function [erf_theta,grad] = reachability_err_tests(theta,transf_xyzbxbybz,i,tolerances,robot1)    
%     erf_theta = 0;
    
% loop through each point
%     tol_xyz = tolerances(i,1);
%     tol_alpha = tolerances(i,2);
%     tol_beta = tolerances(i,3);
%     tol_gamma = tolerances(i,4);

% represent the point on the part w.r.t. robot-base
    pointi = transf_xyzbxbybz(i,1:3)';
    pbx = transf_xyzbxbybz(i,4:6)';
    pby = transf_xyzbxbybz(i,7:9)';
    pbz = transf_xyzbxbybz(i,10:12)';
    
    % ROBOT end-effector
%     all_transf_mat = robot1.fwd_kin_all_joints(theta);
    b1_T_ree = iiwa_FK_symbolic_WE(theta,eye(4)); % eff transf
%     b1_T_ree = all_transf_mat{end};
    b1_T_tee = b1_T_ree*robot1.robot_ree_T_tee;
    
    tool_xyz = b1_T_tee(1:3,4);
    toolX = b1_T_tee(1:3,1:3) * [1;0;0]; %toolX = toolX/norm(toolX);
    toolY = b1_T_tee(1:3,1:3) * [0;1;0]; %toolY = toolY/norm(toolY);
    toolZ = b1_T_tee(1:3,1:3) * [0;0;1]; %toolZ = toolZ/norm(toolZ);

    
%     rot_pose = [  dot(pbx,[1;0;0]),  dot(pby,[1;0;0]),  dot(pbz,[1;0;0]);
%                          dot(pbx,[0;1;0]),  dot(pby,[0;1;0]),  dot(pbz,[0;1;0]);
%                          dot(pbx,[0;0;1]),  dot(pby,[0;0;1]),  dot(pbz,[0;0;1])    ];
                     
    ee_pose = [  dot(toolx,bx),  dot(tooly,bx),  dot(toolz,bx);
             dot(toolx,by),  dot(tooly,by),  dot(toolz,by);
             dot(toolx,bz),  dot(tooly,bz),  dot(toolz,bz)    ];
                     
    v = rotm2axang( rot_pose*b1_T_tee(1:3,1:3)' );
    err_orien = v(4)*v(1:3)';
    tool_xyz = b1_T_tee(1:3,4);
    err_pose = pointi-tool_xyz;
    err = [ err_pose;err_orien ];
    erf_theta = 0.5*(  err'*diag([1,1,1,0.25,0.25,0.25])*err  ); 
    
    Jac = -(robot1.get_jacobian( theta ));
    grad = (err'*diag([1,1,1,0.25,0.25,0.25])*Jac)';
end