close all;

% grp_idx = csvread('Ascent_Group_IDX3.txt');
% joint_angles = csvread('carhood_joints.csv');
% joint_angles = csvread('2stepGE90_Joints.csv');
v = VideoWriter('Ascent.avi');
% v = VideoWriter('GE90.avi');

% load HS_joints.mat;
% joint_angles = HS_joints;

open(v);
figure(1)
hold on;
view(60,20);
% tool_stl = 'roller_model.stl'; % Tool to be attached to EE to visualize
tool_stl = 'roller.stl'; % Tool to be attached to EE to visualize
part = 'Mold_Ascent.STL';
% part = 'GE90_v2.stl';
% x = [0.6010;
%    -0.4750;
%     0.3230;
%    -1.1656;
%     2.6904;
%     3.0744];

xlabel('X-Axis');
ylabel('Y-Axis');
zlabel('Z-Axis');


% Plot the Transformed Part
R_to_Part = eye(4);
R_to_Part(1:3,1:3) = quat2rotm( x(4:7)' );
R_to_Part(1:3,4) = [ x(1);x(2);x(3) ];
[v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
v_part = v_part./1000;
[v_part_transf,n_part_transf] = stlTransform(v_part,n_part,R_to_Part); 
part_plt(v_part_transf, f_part, name_part, [0.2,0,0]); % plot tool stl
 ylim([-0.2,1]);zlim([0,1.5]);xlim([-0.2,1]);
% daspect([1,1,1])
pause(1); 
% grp_idx = [1,11041];
% for j=1:size(grp_idx,1)
%     joints = [[0;pi/4;0;-pi/4;0;pi/2;0],joint_angles(:,grp_idx(j,1):grp_idx(j,2))];
    joints = [[0;pi/4;0;-pi/4;0;pi/2;0],joint_angles];
    for i=1:size(joints,2)
        state = joints(:,i);
        H = plot_robot(state,robot1,tool_stl);
        if i==1
            pause(2);
        end
        for cnt = 1:5
            frame = getframe(gcf);
            writeVideo(v,frame);
        end
%         view(70,10); % For Ascent
        if i<size(joint_angles,2)
            for rnge=1:9; delete(H{rnge}); end;
        end
    %     view(60,20); % For GE90
    end
% end
close(v);
