classdef(StrictDefaults) custom_InverseKinematics < matlab.System & ...
                               robotics.manip.internal.InternalAccess
    %INVERSEKINEMATICS Inverse kinematics solver for rigid body tree
    %   Given the desired SE3 pose of an end-effector, the inverse  
    %   kinematics solver computes the joint configurations that realize
    %   the desired end-effector pose.
    %
    %   IK = robotics.INVERSEKINEMATICS returns an inverse kinematics 
    %   system object, IK, with the rigid body tree model unset. The solver
    %   algorithm is default to 'BFGSGradientProjection'
    %
    %   IK = robotics.INVERSEKINEMATICS('PropertyName', PropertyValue, ...) 
    %   returns an inverse kinematics system object, IK, with each specified
    %   property set to the specified value.
    %
    %   Step method syntax:
    %
    %   [QSOL, SOLINFO] = step(IK, ENDEFFECTORNAME, TFORM, WEIGHTS, INITIALGUESS) 
    %   finds the joint configuration, QSOL (a structure array),
    %   given the end-effector (body) with name ENDEFFECTORNAME,
    %   the desired SE3 pose of that end-effector, TFORM (in the
    %   form of 4x4 a homogeneous transformation), the weight on each
    %   component of the desired pose, WEIGHTS, and an initial guess of the
    %   solution, INITIALGUESS (a structure array). A second output,  
    %   SOLINFO, provides more detailed information about the solving process.
    %
    %   System objects may be called directly like a function instead of 
    %   using the step method. For example, 
    %   QSol = step(ik, endEffectorName, tform, weights, initialGuess) and 
    %   QSol = ik(endEffectorName, tform, weights, initialGuess) are
    %   equivalent.
    %
    %   INVERSEKINEMATICS properties:
    %
    %   RigidBodyTree          - Rigid body tree model 
    %   SolverAlgorithm        - Solver algorithm name
    %   SolverParameters       - Parameters for specific algorithm 
    %
    %   INVERSEKINEMATICS methods:
    %
    %   step        - Compute IK solution
    %   release     - Allow property value changes
    %
    %   Example
    %
    %       % Load rigid body tree model
    %       load exampleRobots.mat
    %
    %       % Create an inverse kinematics object
    %       ik = robotics.InverseKinematics('RigidBodyTree', puma1);
    %
    %       % Set up desired end-effector pose, weights and initial guess
    %       tform = [  0.7071         0   -0.7071    0.1408
    %                       0         1         0   -0.1500
    %                  0.7071         0    0.7071    0.3197
    %                       0         0         0    1     ];
    %       weights = [0.25 0.25 0.25 1 1 1];
    %       initialGuess = puma1.homeConfiguration;
    %
    %       % Call IK solver, the end-effector body name is 'L6'
    %       [QSol, solnInfo] = ik('L6', tform, weights, initialGuess);
    %      
    %   See also robotics.RigidBodyTree.
    
    %   Copyright 2016 The MathWorks, Inc.
    
    %#codegen
    
    properties (Nontunable, Dependent)
        %RigidBodyTree The rigid body tree model
        %
        %   Default: []
        RigidBodyTree 

        %SolverAlgorithm The algorithm used to solve the IK problem
        %   Possible choices are 'BFGSGradientProjection' and 'LevenbergMarquardt'
        %
        %   Default: 'BFGSGradientProjection'
        SolverAlgorithm 
    end
    
    properties (Dependent)
        %SolverParameters Parameters associated with the specified algorithm
        %   Solver parameters vary depending on the solver. 
        SolverParameters
        
    end
    
    properties (Access = {?robotics.manip.internal.InternalAccess, ...
                          ?robotics.InverseKinematics})
        %Solver Solver object
        Solver = []
        
        %SolverAlgorithmInternal
        SolverAlgorithmInternal
        
        %Limits Joint limit matrix extracted from the rigid body tree model
        Limits
        
        %LastGoal
        LastGoal
        
        %RigidBodyTreeInternal
        RigidBodyTreeInternal

    end
    
    methods (Access = protected)
        
        function setupImpl(obj)
            %setupImpl Assemble IK problem 
            
            obj.assembleProblem();
            
            % Load variable bounds, optional for solvers
            obj.Limits = obj.RigidBodyTreeInternal.TreeInternal.JointPositionLimits;
            
            % Configure extra arguments to be passed to callbacks
            obj.setupExtraArgs();
        end
        
        function setupExtraArgs(obj)
            %setupExtraArgs
            % Initialize extra arguments
            obj.Solver.ExtraArgs = robotics.manip.internal.IKExtraArgs();
            obj.Solver.ExtraArgs.WeightMatrix = zeros(6);
            obj.Solver.ExtraArgs.Robot = obj.RigidBodyTreeInternal;
            obj.Solver.ExtraArgs.Limits = obj.Limits;
            obj.Solver.ExtraArgs.Tform = eye(4);
            
            coder.varsize('bname', [1, obj.RigidBodyTreeInternal.TreeInternal.MaxNameLength], [false, true]);
            bname = '';
            obj.Solver.ExtraArgs.BodyName = bname;
            
            coder.varsize('errTmp', [6,1], [true, false]);
            errTmp = zeros(6,1);
            obj.Solver.ExtraArgs.ErrTemp = errTmp;
            obj.Solver.ExtraArgs.CostTemp = 0;
            
            coder.varsize('gradTmp', [obj.RigidBodyTreeInternal.TreeInternal.MaxNumBodies*obj.RigidBodyTreeInternal.TreeInternal.MaxJointPositionNumber, 1], [true, false]);
            gradTmp = zeros(obj.RigidBodyTreeInternal.TreeInternal.PositionNumber,1);
            obj.Solver.ExtraArgs.GradTemp = gradTmp;
            
        end

        function releaseImpl(obj) %#ok<MANU>
            % Release resources, such as file handles
        end
        
        function [QSol, solutionInfo] = stepImpl(obj, endEffectorName, tform, weights, initialGuess)
            %stepImpl Solve IK
            
            setPoseGoal(obj, endEffectorName, tform, weights);
            [QSol, solutionInfo] = solve(obj, initialGuess);            
        end



        function num = getNumInputsImpl(obj) %#ok<MANU>
            % Define total number of inputs for system with optional inputs
            
            num = 4;
        end

        function num = getNumOutputsImpl(obj) %#ok<MANU>
            % Define total number of outputs 
            
            num = 2;

        end                

        function loadObjectImpl(obj,s,wasLocked)
            %loadObjectImpl

            loadObjectImpl@matlab.System(obj,s,wasLocked);

            obj.RigidBodyTreeInternal = s.RigidBodyTreeInternal;
            obj.SolverAlgorithmInternal = s.SolverAlgorithmInternal;
            obj.Solver = copy(s.Solver);

            obj.Limits = s.Limits;
            obj.LastGoal = s.LastGoal;
        end

        function s = saveObjectImpl(obj)
            %saveObjectImpl
            
            s = saveObjectImpl@matlab.System(obj);
            
            s.RigidBodyTreeInternal = obj.RigidBodyTreeInternal;
            s.SolverAlgorithmInternal = obj.SolverAlgorithmInternal;
            s.Solver = copy(obj.Solver);
            
            s.Limits = obj.Limits;
            s.LastGoal = obj.LastGoal;
            
        end
    end
    
    methods 
        function obj = InverseKinematics(varargin)
            % InverseKinematics Constructor
            
            if ~coder.target('MATLAB')
                hasTree = containsName('RigidBodyTree', varargin{:});
                coder.internal.assert(hasTree, 'robotics:robotmanip:inversekinematics:RBTRequiredAtConstructionForCodegen');
                
                hasAlg = containsName('SolverAlgorithm', varargin{:});
                if ~hasAlg
                    obj.SolverAlgorithm = 'BFGSGradientProjection'; 
                end
                
            else
                obj.SolverAlgorithm = 'BFGSGradientProjection'; 
                obj.RigidBodyTreeInternal = robotics.RigidBodyTree();
            end
            
            setProperties(obj,nargin,varargin{:});

        end
        

    end
    
    methods (Access = private)
        function setPoseGoal(obj, endEffectorName, tform, weights)
            %setPoseGoal Create cost and gradient function handles 
            if strcmp(obj.RigidBodyTreeInternal.BaseName, endEffectorName)
                robotics.manip.internal.error('inversekinematics:EndEffectorIsBase'); 
            end
            validateInputBodyName(obj.RigidBodyTreeInternal.TreeInternal, endEffectorName);
            
            validateattributes(tform, {'double'}, {'nonempty','real', ...
                'size',[4, 4]}, 'setGoalPose', 'tform'); 
            
            R = tform(1:3,1:3);
            lastrow = tform(4,:);
            % a rudimentary check on the validity of tform matrix and will
            % provide a warning if the check fails
            if max(max(abs(inv(R) - R')))>1e-4 || norm(lastrow - [0 0 0 1])>1e-7
               robotics.manip.internal.warning(...
                 'inversekinematics:HomogeneousTransformInvalid'); 
            end
            
            validateattributes(weights, {'double'}, ...
              {'nonempty', 'real', 'vector', 'nonnan', 'nonnegative','finite', 'numel', 6},...
                    'setGoalPose', 'weights');
            weightMatrix = diag(weights);
            
            args = obj.Solver.ExtraArgs;
            args.WeightMatrix = weightMatrix;
            
            args.BodyName = endEffectorName;
            args.Tform = tform;


%             obj.Solver.CostFcn = @robotics.manip.internal.custom_IkHelpers.computeCost;
%             obj.Solver.GradientFcn = @robotics.manip.internal.custom_IkHelpers.computeGradient;
%             
%             obj.Solver.SolutionEvaluationFcn = @robotics.manip.internal.custom_IkHelpers.evaluateSolution;
%             
%             obj.Solver.BoundHandlingFcn = @robotics.manip.internal.custom_IkHelpers.clamping;
%             
%            
%             obj.Solver.RandomSeedFcn = @robotics.manip.internal.custom_IkHelpers.randomConfig;
            obj.Solver.CostFcn = @custom_IkHelpers.computeCost;
            obj.Solver.GradientFcn = @custom_IkHelpers.computeGradient;
            
            obj.Solver.SolutionEvaluationFcn = @custom_IkHelpers.evaluateSolution;
            
            obj.Solver.BoundHandlingFcn = @custom_IkHelpers.clamping;
            
           
            obj.Solver.RandomSeedFcn = @robotics.manip.internal.custom_IkHelpers.randomConfig;
            goal = struct();
            goal.EE = endEffectorName;
            goal.Tf = tform;
            goal.W = weights;
            
            obj.LastGoal = goal;
            
        end
        
        function [QSol, solutionInfo] = solve(obj, initialGuess)
            %solve
            iniGuessVec = obj.RigidBodyTreeInternal.TreeInternal.validateConfigurationWithLimits(initialGuess, true); % check joint limits, autoAdjust

            [qvSolRaw, sol] = obj.Solver.solve(iniGuessVec);
            
            % for joints not on the shortest path from end-effector to
            % base, their position values are kept the same as initial
            % guess.
            qvSol = iniGuessVec;
            bodyIndices = obj.RigidBodyTreeInternal.TreeInternal.kinematicPathToBase(obj.Solver.ExtraArgs.BodyName);
            positionIndices = bodyIndicesToPositionIndices( ...
                obj.RigidBodyTreeInternal.TreeInternal, bodyIndices);
            qvSol(positionIndices) = qvSolRaw(positionIndices);
            
            QSol = formatConfiguration(obj.RigidBodyTree.TreeInternal, qvSol);

            solutionInfo.Iterations = sol.Iterations;
            solutionInfo.NumRandomRestarts = sol.RRAttempts;
            solutionInfo.PoseErrorNorm = sol.Error;
            solutionInfo.ExitFlag = sol.ExitFlag;
            solutionInfo.Status = sol.Status;
        end
        
    end
    
    methods 
        function solverparams = get.SolverParameters(obj)
            %get.SolverParameters
            params = obj.Solver.getSolverParams;
            
            solverparams.MaxIterations = params.MaxNumIteration;
            solverparams.MaxTime = params.MaxTime;
            solverparams.GradientTolerance = params.GradientTolerance;
            solverparams.SolutionTolerance = params.SolutionTolerance;
            solverparams.EnforceJointLimits = params.ConstraintsOn;
            solverparams.AllowRandomRestart = params.RandomRestart;
            solverparams.StepTolerance = params.StepTolerance;            
            
            switch (obj.SolverAlgorithmInternal)
                case 'BFGSGradientProjection'

                case 'LevenbergMarquardt'  
                    solverparams.ErrorChangeTolerance = params.ErrorChangeTolerance;
                    solverparams.DampingBias = params.DampingBias;
                    solverparams.UseErrorDamping = params.UseErrorDamping;
            end
            
        end
        
        function set.SolverParameters(obj, solverparams)
            %set.SolverParameters Validate and set solver parameters

            params = obj.Solver.getSolverParams;
            
            validateattributes(solverparams, {'struct'},{'scalar'},...
                'InverseKinematics', 'SolverParameters')
            
            if isfield(solverparams, 'MaxIterations')
                validateattributes(solverparams.MaxIterations, {'double'}, ...
                     {'nonempty',  'nonnan', 'finite', 'integer', 'scalar', '>',1}, ...
                      'SolverParameters','MaxIterations');
                params.MaxNumIteration = solverparams.MaxIterations;
            end

            if isfield(solverparams, 'MaxTime')
                validateattributes(solverparams.MaxTime, {'double'}, ...
                     {'nonempty', 'nonnan', 'finite', 'real', 'scalar', '>', 0}, 'SolverParameters','MaxTime');                   
                params.MaxTime = solverparams.MaxTime;
            end

            if isfield(solverparams, 'GradientTolerance')
                validateattributes(solverparams.GradientTolerance, {'double'}, ...
                     {'nonempty', 'nonnan', 'finite', 'real', 'scalar', '>', 1e-14}, 'SolverParameters','GradientTolerance');
                params.GradientTolerance = solverparams.GradientTolerance;
            end

            if isfield(solverparams, 'SolutionTolerance')
                validateattributes(solverparams.SolutionTolerance, {'double'}, ...
                     {'nonempty', 'nonnan', 'finite', 'real', 'scalar', '>', 1e-14}, 'SolverParameters','SolutionTolerance');
                params.SolutionTolerance = solverparams.SolutionTolerance;
            end

            if isfield(solverparams, 'EnforceJointLimits') 
                validateattributes(solverparams.EnforceJointLimits, {'logical', 'numeric'}, ...
                     {'nonempty', 'scalar', 'nonnan', 'finite', 'real'}, 'SolverParameters','EnforceJointLimits');
                params.ConstraintsOn = logical(solverparams.EnforceJointLimits);
            end

            if isfield(solverparams, 'AllowRandomRestart')
                validateattributes(solverparams.AllowRandomRestart, {'logical','numeric',}, ...
                     {'nonempty','scalar', 'nonnan', 'finite', 'real'}, 'SolverParameters','AllowRandomRestart');
                params.RandomRestart = logical(solverparams.AllowRandomRestart);
            end

            if isfield(solverparams, 'StepTolerance')
                validateattributes(solverparams.StepTolerance, {'double'}, ...
                     {'nonempty','scalar', 'nonnan', 'finite', 'real', '>', 1e-18}, 'SolverParameters','StepTolerance');
                params.StepTolerance = solverparams.StepTolerance;
            end
            
            switch (obj.Solver.Name)
                case 'BFGSGradientProjection'
                    
                case 'LevenbergMarquardt' 
                    if isfield(solverparams, 'ErrorChangeTolerance')
                        validateattributes(solverparams.ErrorChangeTolerance, {'double'}, ...
                            {'nonempty','scalar', 'nonnan', 'finite', 'real', '>', 1e-14}, 'SolverParameters', 'ErrorChangeTolerance');
                        params.ErrorChangeTolerance = solverparams.ErrorChangeTolerance;
                    end
                    
                    if isfield(solverparams, 'DampingBias')
                        validateattributes(solverparams.DampingBias, {'double'}, ...
                            {'nonempty','scalar', 'nonnan', 'finite', 'real', '>', 1e-14}, 'SolverParameters', 'DampingBias');
                        params.DampingBias = solverparams.DampingBias;
                    end
                    
                    if isfield(solverparams, 'UseErrorDamping')
                        validateattributes(solverparams.UseErrorDamping, {'logical','numeric',}, ...
                            {'nonempty','scalar', 'nonnan', 'finite', 'real'}, 'SolverParameters','UseErrorDamping');
                        params.UseErrorDamping = logical(solverparams.UseErrorDamping);
                    end
            end
            
            obj.Solver.setSolverParams(params);
        end
        
        function set.SolverAlgorithm(obj, salgorithm)
            %set.SolverAlgorithm
            
            obj.SolverAlgorithmInternal = validatestring(salgorithm, ...
                     {'BFGSGradientProjection','LevenbergMarquardt'}, ...
                     'InverseKinematics', 'SolverAlgorithm');
            
            switch (obj.SolverAlgorithmInternal)
                case 'BFGSGradientProjection' 
                    obj.Solver = robotics.core.internal.DampedBFGSwGradientProjection();
                case 'LevenbergMarquardt'
                    obj.Solver = robotics.core.internal.ErrorDampedLevenbergMarquardt();
            end
            
        end
        
        function salgorithm = get.SolverAlgorithm(obj)
            %get.SolverAlgorithm
            salgorithm = obj.SolverAlgorithmInternal;
        end
        
        function set.RigidBodyTree(obj, rigidbodytree)
            %set.RigidBodyTree 
            
            validateattributes(rigidbodytree, {'robotics.RigidBodyTree'},...
                {'nonempty','scalar'},'InverseKinematics', 'rigidbodytree');

            if rigidbodytree.NumNonFixedBodies == 0
                robotics.manip.internal.error(...
                  'inversekinematics:RigidBodyTreeFixed'); 
            end           
            
            obj.RigidBodyTreeInternal = copy(rigidbodytree);   
        end
        
        function rigidbodytree = get.RigidBodyTree(obj)
            %get.RigidBodyTree
            rigidbodytree = copy(obj.RigidBodyTreeInternal);
        end

    end
    
    methods (Access = private) 
        function assembleProblem(obj)
            % To be optimized (to only include the support set of end-eff)
            n = obj.RigidBodyTreeInternal.TreeInternal.PositionNumber;
            A = zeros(n, 2*n);
            b = zeros(2*n,1);
            k = 1; m = 1;
            
            % Form constraints (to be double-checked)  A'*x = b
            for i = 1 : obj.RigidBodyTreeInternal.NumBodies
                joint = obj.RigidBodyTreeInternal.Bodies{i}.Joint;
                pnum = joint.PositionNumber; 
                if ~strcmp(joint.Type, 'fixed')
                    A(k:k+pnum-1, m:m+pnum-1) = eye(pnum);
                    A(k:k+pnum-1, m+pnum:m+2*pnum-1) = -eye(pnum);
                    b(m) = joint.PositionLimits(2);
                    b(m+1) = -joint.PositionLimits(1);
                    m = m + 2*pnum;
                end
                k = k + pnum;
            end
            
            obj.Solver.ConstraintMatrix = A;
            obj.Solver.ConstraintBound = b;            
        end

    end

    methods (Static, Hidden)
        function props = matlabCodegenNontunableProperties(~)
            % In codegen, IK solver can only be specified once
            props = {'SolverAlgorithmInternal'};% 'RigidBodyTreeInternal'
        end
    end
    
end


function hasName = containsName(argName, varargin)
%containsName During codegen, check if the given Name-Value pairs contain a
%   certain ARGNAME. To be called before SetProperties.
    hasName = false;
    for i = 1:length(varargin)
        if strcmp(varargin{i}, argName)
            hasName = true;
            return;
        end
    end
end


