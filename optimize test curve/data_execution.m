% Implementation of a simple policy of HGD on the Spiral part

% Objective is to minimize time subject to different constraints.
close all;
clear all;
clc
format short g;

warning 'off';
global CONFIG;

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
joint_vel = joint_vel*0.8;
CONFIG.tool_stl = 'tool.stl';

% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.120]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


%% %%%%%% CURVE DATA %%%%%%%%%%%%%
curve_file_name  = 'HS_points.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
tol_file_name  = 'HS_tolfile.csv'; % n x 4 
tolerances = dlmread(tol_file_name);
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);
% 0.0122 is distance from one point to another.

%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon');
options.MaxIterations = 1e5;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-3;
options.StepTolerance = 1e-3;
options.Display = 'off';
options.FiniteDifferenceStepSize = 0.0044*ones(7,1);

lb = [ 0;-0.7;0;-1;-1;-1;-1 ];
ub = [ 0.7;0.7;0.7;1;1;1;1 ];

perturb_idx = [ [1,0.02]; [1,-0.02]; [2,0.02]; [2,-0.02]; [3,0.02]; [3,-0.02]; [4,0.05]; [4,-0.05];...
                [5,0.05]; [5,-0.05]; [6,0.05]; [6,-0.05]; [7,0.05]; [7,-0.05] ];
            
Poses_traversed = [];
iter = 0;
max_iter = 500;
random = 0;
Xk = random_gen(lb,ub,xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);            

%% Hierarchical Gradient Descent
tic;
while iter < max_iter
    tic;
    iter = iter + 1;
    fprintf('Iteration Number:  %d\n',iter);
    sample_data = {};
    sample_data{1} = acq_data(Xk, [0;0;0;0;0;0;0],xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);
% Analyzing current sample
    sample_reach = sample_data{1}.reachability;
    sample_pose_err = sum(sample_data{1}.pt_data(:,10)) + sum(sample_data{1}.pt_data(:,11));
    fprintf( 'Current pose: \n'); disp(Xk);
    fprintf( 'Reachability: %d\n',sample_reach);
    fprintf( 'Pose Error: %d\n',sample_pose_err);
    fprintf( 'Minimum Velocity: %d\n\n\n', min(sample_data{1}.pt_data(:,9)) );
    Poses_traversed = [ Poses_traversed, [Xk; sample_reach; sample_pose_err; random] ];
%     if sample_reach==100
%         disp('Reachability Optimized');
%         break;
%     end
    Q = perturb(Xk);
    Q_size = size(Q,2);
    parfor counter = 1:Q_size
       sample_data{counter+1} =  acq_data(Q(:,counter), [0;0;0;0;0;0;0],xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);
    end
% Analyzing the gradient
    pose_errs = [];
    reachabilities = [];
    for i = 2:Q_size+1
        pose_errs = [pose_errs; ( sum(sample_data{i}.pt_data(:,10)) + sum(sample_data{i}.pt_data(:,11))) ];
        reachabilities = [reachabilities; sample_data{i}.reachability];
    end
    if sample_reach < 90
        gradient = 2;
        [error,idx] = min( pose_errs );
    else
        gradient = 1;
        [max_reach, idx] = max(reachabilities);
    end
    step = get_Step(Xk, idx, xyz_bxbybz, theta_lb,...
                    theta_ub, options, joint_vel,tolerances,...
                    robot1, sample_pose_err, sample_reach, lb, ub, gradient);
    if step ~= 0
        fprintf('Current Step: %d\n', step);
        Xk(perturb_idx(idx,1)) = Xk(perturb_idx(idx,1)) + perturb_idx(idx,2);
        Xk(4:7) = Xk(4:7)/norm(Xk(4:7));
        random = 0;
    else
        disp('No descent available. Generating random seeds');
        Xk = random_gen(lb,ub,xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);
        random = 1;
    end
    toc;
end
toc;

csvwrite( fullfile('Execution Trials', 'Progression.csv' ),  Poses_traversed );




function step = get_Step(Xk, idx, xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,...
                            robot1, pose_err, reach, lb, ub, gradient)
    perturb_idx = [ [1,0.02]; [1,-0.02]; [2,0.02]; [2,-0.02]; [3,0.02]; [3,-0.02]; [4,0.05]; [4,-0.05];...
                [5,0.05]; [5,-0.05]; [6,0.05]; [6,-0.05]; [7,0.05]; [7,-0.05] ];
    ref_pose_err = pose_err;
    ref_reach = reach;
    step = [ perturb_idx(idx,2):perturb_idx(idx,2)/3:perturb_idx(idx,2)*4 ]';
    direction_data = [];
    
% Determining the Step size
    parfor iter = 1:size(step,1)
        xk1 = Xk;
        xk1(perturb_idx(idx,1)) = xk1(perturb_idx(idx,1)) + step(iter);
        xk1(4:7) = xk1(4:7)/norm(xk1(4:7));
        eval_obj = acq_data(xk1, [0;0;0;0;0;0;0],xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);
        pose_err = ( sum(eval_obj.pt_data(:,10)) + sum(eval_obj.pt_data(:,11)) );
        reach = eval_obj.reachability;
        direction_data = [direction_data, [xk1; pose_err; reach; step(iter)] ];                
    end
    direction_data = direction_data';
    direction_data = sortrows(direction_data,10);
    direction_data = direction_data';
    step_flag = false;
    pose_data = direction_data(8,:);
    reach_data = direction_data(9,:);
    dirc_eval = [reach_data; pose_data];
    if gradient == 2
        [val,idx] = min(dirc_eval(2,:));
        if ( dirc_eval(2,idx) <= ref_pose_err ) 
            step = direction_data(end,idx);
            step_flag = true;
        end
    elseif gradient == 1
        [val,idx] = max(dirc_eval(1,:));
        if ( dirc_eval(1,idx) >= ref_reach ) 
            step = direction_data(end,idx);
            step_flag = true;
        end
    end
    if ~step_flag
        step = 0;
    end
end



function X = random_gen(lb,ub,xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1)
    valid_samples = 0;
    while valid_samples~=1
        sample = [ randsample([lb(1):0.01:ub(1)],1);
                        randsample([lb(2):0.01:ub(2)],1);
                        randsample([lb(3):0.01:ub(3)],1);
                        randsample([lb(4):0.01:ub(4)],1);
                        randsample([lb(5):0.01:ub(5)],1);
                        randsample([lb(6):0.01:ub(6)],1);
                        randsample([lb(7):0.01:ub(7)],1);  ];
        sample(4:7) = sample(4:7)/norm(sample(4:7));
        Rot = quat2rotm(sample(4:7)');
        bz = Rot*[0;0;1];
        bz = bz/norm(bz);
        angle = abs( acos(dot(bz,[0;0;1])) );
        if angle > 1.2217 % 70 degrees
            continue;
        end
        fetch_data = acq_data(sample, [0;0;0;0;0;0;0],xyz_bxbybz, theta_lb,...
                                theta_ub, options, joint_vel,tolerances,robot1);
        if fetch_data.reachability > 25
            X = sample;
            valid_samples = valid_samples + 1;
        end
    end
end



% if curr_pose_err > sample_pose_err
%         if curr_reach >= sample_reach
%             idx = idx2;
%         else
%             fprintf('Current pose: \n'); disp(Xk);
%             fprintf('Reachability: %d\n',curr_reach);
%             fprintf('Condition Number: %d\n\n\n',curr_pose_err);
%             
%             % Make a random move. If not feasible select another sample
%             % from initial list
%             assign_flag = false;
%             for i=1:3
%                 idx = randsample([1:Q_size],1);
%                 Xk = Q(:,idx);
%                 if ~all( (ub-Xk)>=0 )
%                     continue;
%                 elseif ~all( (Xk-lb)>=0 )
%                     continue;
%                 else
%                     assign_flag = true;
%                     random_move = 1;
%                     break;
%                 end
%             end
%             if ~assign_flag
%                 Xk = list(:,1);
%                 list(:,1) = [];
%                 random_move = 1;
%             end
%             continue;
%         end
% end
%     




% b = sym('b',[no_pts,1]);
% R = {};
% for i=1:no_pts
%     Rz{i} = [ cos(b(i)), -sin(b(i)), 0; sin(b(i)), cos(b(i)), 0; 0,0,1 ];
% end