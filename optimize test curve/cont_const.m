function [c,ceq] = cont_const(theta,i,prev_config)
    ceq = [];
    c = [];
    if i>1
        c = [c; abs(norm(theta-prev_config,inf))-1.7453];
    end
end