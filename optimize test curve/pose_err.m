% function erf_theta = pose_err(joints)
%     global CONFIG;
%     global robot1;
%     
%     erf_theta = 0;
%     jt_strt = 1;
%     jt_end = 7;
%     for i=1:CONFIG.no_pts
%     % loop through each point
%         tol_xyz = CONFIG.tolerances(i,1);
%         tol_alpha = CONFIG.tolerances(i,2);
% 
%     % represent the point on the part w.r.t. robot-base
%         pointi = CONFIG.transf_xyzbxbybz(i,1:3)';
%         pbz = CONFIG.transf_xyzbxbybz(i,10:12)';
% 
%         % ROBOT end-effector
%         all_transf_mat = robot1.fwd_kin_all_joints(joints(jt_strt:jt_end,1));
%         b1_T_ree = all_transf_mat{end}; % eff transf    
%         b1_T_tee = b1_T_ree*robot1.robot_ree_T_tee;
% 
%         tool_xyz = b1_T_tee(1:3,4);
%         toolZ = b1_T_tee(1:3,1:3) * [0;0;1]; toolZ = toolZ/norm(toolZ);
% 
%         % Error Without Tolerances
%         error_xyz = norm( pointi-tool_xyz );        
%         error_nz = abs( acos(dot(pbz, toolZ)) );
% 
%         % Adding errors only if outside tolerance bounds.
%         erf_theta = erf_theta + exp( error_xyz-tol_xyz );
%         erf_theta = erf_theta + exp( (error_nz-tol_alpha)/pi );
%         
%         jt_strt = jt_strt + 7;
%         jt_end = jt_end + 7;
%     end
% end