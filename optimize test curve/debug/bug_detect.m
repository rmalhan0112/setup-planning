% Implementation of a simple policy of HGD on the Spiral part

% Objective is to minimize time subject to different constraints.
close all;
clear all;
clc

warning 'off';
global CONFIG;

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
joint_vel = joint_vel*0.8;
CONFIG.tool_stl = 'tool.stl';

% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.120]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon');
options.MaxIterations = 1e5;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-3;
options.StepTolerance = 1e-3;
options.Display = 'off';


%% Generate Points
rot = eul2rotm([0,pi,0],'ZYX');
T = eye(4);
T(1:3,4) = [ 0.5; 0.1; 0.3 ];
T(1:3,1:3) = rot;

bx = [1;0;0];
by = [0;1;0];
bz = [0;0;1];

Xk = [ 0,0,0,bx',by',bz';
        0,20,0,bx',by',bz';
        0,40,0,bx',by',bz';
        0,60,0,bx',by',bz';
        20,20,0,bx',by',bz';
        20,40,0,bx',by',bz';
        40,30,0,bx',by',bz'   ];
Xk(:,1:3) = Xk(:,1:3)./1000;

transf_xyzbxbybz(:,1:4) = (  T * [Xk(:,1:3)'; ones(1,size(Xk,1))]  )';
transf_xyzbxbybz(:,4) = [];
for i=1:size(Xk,1)
    transf_xyzbxbybz(i,4:6) = ( rot * bx )';
    transf_xyzbxbybz(i,4:6) = transf_xyzbxbybz(i,4:6) / norm(transf_xyzbxbybz(i,4:6));
    transf_xyzbxbybz(i,7:9) = ( rot * by )';
    transf_xyzbxbybz(i,7:9) = transf_xyzbxbybz(i,7:9) / norm(transf_xyzbxbybz(i,7:9));
    transf_xyzbxbybz(i,10:12) = ( rot * bz )';
    transf_xyzbxbybz(i,10:12) = transf_xyzbxbybz(i,10:12) / norm(transf_xyzbxbybz(i,10:12));
end


figure(1)
hold on;
plot_robot([0;0;0;-pi/2;0;pi/2;0],robot1);
daspect([1,1,1]);
for i=1:size(transf_xyzbxbybz,1)
    [handler_a,handler_b,handler_c] = plot_axes( transf_xyzbxbybz(i,1:3), transf_xyzbxbybz(i,4:6),...
                    transf_xyzbxbybz(i,7:9), transf_xyzbxbybz(i,10:12), 0.05);
end

tolerances = zeros( size(transf_xyzbxbybz,1),4 );
tolerances(:,1) = 0.003;
tolerances(:,2) = 0.14;
tolerances(:,3) = 3.14;
tolerances(:,4) = 3.14;




%% Main
% Compute IK using Matlab toolbox
mtlb_reach = [];
for i=1:size(transf_xyzbxbybz,1)
    T = eye(4);
    T(1:3,4) = transf_xyzbxbybz(i,1:3)';
    bx = transf_xyzbxbybz(i,4:6)';
    by = transf_xyzbxbybz(i,7:9)';
    bz = transf_xyzbxbybz(i,10:12)';
    
    T(1:3,1:3) = [  dot(bx,[1;0;0]),  dot(by,[1;0;0]),  dot(bz,[1;0;0]);
                    dot(bx,[0;1;0]),  dot(by,[0;1;0]),  dot(bz,[0;1;0]);
                    dot(bx,[0;0;1]),  dot(by,[0;0;1]),  dot(bz,[0;0;1])  ];
    X = robot1.robot_ree_T_tee'\T';
    T = X';
    init_guess = [0;0;0;0;0;0;0];
    joint_config = robot1.get_iiwa_IK( T, init_guess );
    [err_xyz,err_z] = compute_pose_err(joint_config,transf_xyzbxbybz,i,tolerances,robot1);
    if err_xyz > 1 || err_z > 1
        mtlb_reach = [mtlb_reach; 0];
    else
        mtlb_reach = [mtlb_reach; 1];
    end
    
    
    % Plot the robot
%     robot_handler = plot_robot(  joint_config,robot1  );
%     pause(0.5)
%     for cnt = 1:size(robot_handler,2)
%         delete(robot_handler{cnt});
%     end
end




% Compute IK using Fmincon
fmincon_reach = [];
init_guess = [0;0;0;0;0;0;0];
for i=1:size(transf_xyzbxbybz,1)
    [joint_config,err_theta] = fmincon(@(theta)reachability_err(theta,transf_xyzbxbybz,...
                            i,tolerances,robot1),init_guess,[],[],[],[],theta_lb,theta_ub,...
                            [],options);
    [err_xyz,err_z] = compute_pose_err(joint_config,transf_xyzbxbybz,i,tolerances,robot1);
    if err_xyz > 1 || err_z > 1
        fmincon_reach = [fmincon_reach; 0];
    else
        fmincon_reach = [fmincon_reach; 1];
    end
end



%% Plot the reachabilites
figure(2)
hold on;
plot( [1:size(transf_xyzbxbybz,1)]', mtlb_reach, 'r', 'linewidth', 2 );

figure(3)
hold on;
plot( [1:size(transf_xyzbxbybz,1)]', fmincon_reach, 'b', 'linewidth', 2 );



function [a,b,c] = plot_axes(point, bx, by, bz, scale)
    a = quiver3(point(1),point(2),point(3),...
           bx(1),bx(2),bx(3),'r','linewidth',2, 'AutoScaleFactor', scale);
    b = quiver3(point(1),point(2),point(3),...
           by(1),by(2),by(3),'g','linewidth',2, 'AutoScaleFactor', scale);
    c = quiver3(point(1),point(2),point(3),...
           bz(1),bz(2),bz(3),'b','linewidth',2, 'AutoScaleFactor', scale);
end