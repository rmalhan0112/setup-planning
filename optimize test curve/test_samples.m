samples = csvread('Samples.csv');

close all;
figure(1)
hold on;
plot_robot([0;0;0;0;0;0;0],robot1);
pause(0.000001)
daspect([1,1,1]);
xlabel('X')
ylabel('Y')
zlabel('Z');
hold on;
part = 'HS.STL';
    
for i=1:size(samples,1)
    robot_T_part = eye(4);
    robot_T_part(1:3,4) = [samples(i,1); samples(i,2); samples(i,3)];
    robot_T_part(1:3,1:3) = quat2rotm(samples(i,4:7));     
    [v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
    v_part = v_part./1000;
    [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,robot_T_part);
    part_plt(v_part_transf, f_part, name_part, [0.5,0.6,0.8]); % plot tool stl
    pause(0.00001);
end