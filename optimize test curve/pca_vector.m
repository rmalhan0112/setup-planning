clear all;
close all;
clc;

q0 = [-1:0.001:1];
q1 = [-1:0.001:1];
q2 = [-1:0.001:1];
q3 = [-1:0.001:1];

counter = 0;
vectors = [];
while counter~=100
    sample=  [ randsample(q0,1),...
                randsample(q1,1),...
                randsample(q2,1),...
                randsample(q3,1)];
    sample = sample / norm(sample);
    R = quat2rotm( sample );
    if rad2deg( abs( acos( dot(R(:,3),[0;0;1]) ) ) ) < 180 && rad2deg( abs( acos( dot(R(:,3),[0;0;1]) ) ) ) > 88
        vectors = [vectors; R(:,3)'];
        counter = counter + 1;
    end
end

figure(2)
hold on;
% Plot main frame
quiver3( 0,0,0,1,0,0,'r','AutoScaleFactor',1 );
quiver3( 0,0,0,0,1,0,'g','AutoScaleFactor',1 );
quiver3( 0,0,0,0,0,1,'b','AutoScaleFactor',1 );

% Plot the vectors
quiver3( zeros(size(vectors,1),1),zeros(size(vectors,1),1),zeros(size(vectors,1),1),vectors(:,1),vectors(:,2),vectors(:,3),'k','AutoScaleFactor',0.5 );
daspect([1,1,1])

% % Conducting the Singular Value Decomposition
% no_elemtns = size(vectors,1); 
% mean_val = mean(vectors,1);
% % A = vectors - repmat(mean_val,no_elemtns,1);
% A = vectors;
% % [U,D,V] = svd(A'*A);
% [X,D] = eig(A'*A);
% vec = D*ones(size(D,1),1);
% 
% [val,idx] = max(vec);
% p_vec = X(:,idx)/norm(X(:,idx));
% if (dot(p_vec',[0;0;1])<0)
%     p_vec = -p_vec;
% end
% quiver3(0,0,0,p_vec(1),p_vec(2),p_vec(3),'o','AutoScaleFactor',2);
% 
% 
% [val,idx] = min(vec);
% p_vec = X(:,idx)/norm(X(:,idx));
% if (dot(p_vec',[0;0;1])<0)
%     p_vec = -p_vec;
% end
% quiver3(0,0,0,p_vec(1),p_vec(2),p_vec(3),'o','AutoScaleFactor',2);




% Conducting the Singular Value Decomposition
no_elemtns = size(vectors,1); 
mean_val = mean(vectors,1)
norm(mean_val)
% A = vectors - repmat(mean_val,no_elemtns,1);
A = vectors;
% [U,D,V] = svd(A'*A);
[U,D,V] = svd(A);
% vec = D*ones(size(D,1),1);

% [val,idx] = max(vec);
p_vec = V(:,1)/norm(V(:,1));
quiver3(0,0,0,p_vec(1),p_vec(2),p_vec(3),'o','AutoScaleFactor',2);


% [val,idx] = min(vec);
p_vec = V(:,2)/norm(V(:,2));
quiver3(0,0,0,p_vec(1),p_vec(2),p_vec(3),'o','AutoScaleFactor',2);