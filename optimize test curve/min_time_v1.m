% Objective is to minimize time subject to different constraints.
close all;
clear all;
clc

global robot1;
global CONFIG;

CONFIG.tool_stl = 'tool.stl';

% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.120]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


%% %%%%%% CURVE DATA %%%%%%%%%%%%%
curve_file_name  = 'HS_points.csv'; % n x 12
CONFIG.xyz_bxbybz = dlmread(curve_file_name);
CONFIG.xyz_bxbybz(:,1:3) = CONFIG.xyz_bxbybz(:,1:3)./1000;
tol_file_name  = 'HS_tolfile.csv'; % n x 4 
CONFIG.tolerances = dlmread(tol_file_name);
CONFIG.no_pts = size(CONFIG.xyz_bxbybz,1);

%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',CONFIG.no_pts);

%%%%%%%%%%%%%%%%%%%%  Calculating Minimum Objective Value
tols = CONFIG.tolerances(1,:);
% min_obj = (1/exp(tols(1))) + (1/exp(tols(2))) + 2*(1/exp(tols(3)));
min_obj = (1/exp(tols(1))) + (1/exp(tols(2)));
min_obj = min_obj*CONFIG.no_pts;
fprintf('Solution should have Objective function value within:  \n[ %f  ,  %f ] \n\n ',min_obj,2*CONFIG.no_pts);


%% Optimizer
CONFIG.theta_lb(1) = -2.967059728390360;
CONFIG.theta_lb(2) = -2.094395102393195;
CONFIG.theta_lb(3) = -2.967059728390360;
CONFIG.theta_lb(4) = -2.094395102393195;
CONFIG.theta_lb(5) = -2.967059728390360;
CONFIG.theta_lb(6) = -2.094395102393195;
CONFIG.theta_lb(7) = -3.054326190990077;
CONFIG.theta_ub = - CONFIG.theta_lb;

CONFIG.options = optimoptions('fmincon');
CONFIG.options.MaxIterations = 1e5;
CONFIG.options.MaxFunctionEvaluations = 1e7;
CONFIG.options.OptimalityTolerance = 1e-3;
CONFIG.options.StepTolerance = 1e-3;
CONFIG.options.Display = 'off';
CONFIG.options.HessianApproximation = 'bfgs';

theta_rout = @pose_err;

% flag = true;
% cnt = 1;
% figure(1)
% hold on;


% while flag
%     alpha = -2.79:0.008:2.79;
%     beta = -2.79:0.008:2.79;
%     gamma = -2.79:0.008:2.79;
%     X = -0.9:0.001:0.9;
%     Y = 0:0.001:0.9;
%     Z = 0:0.001:0.9;
%     R = eul2rotm( [randsample(alpha,1),randsample(beta,1),randsample(gamma,1)] ,'ZYX' );
%     trans = [randsample(X,1),randsample(Y,1),randsample(Z,1)];
%     
%     bx = R*bx;
%     by = R*by;
%     bz = R*bz;
% 
%     bx = bx/norm(bx);
%     by = by/norm(by);
%     bz = bz/norm(bz);
%     point = trans';
% 
%     joints = [0;0;0;0;0;0;0];
%     [joints,err_theta] = fmincon(theta_rout,joints,[],[],[],[],theta_lb,theta_ub,[],options); 
%     
%     cla;
%     plot_robot(joints,robot1);
%     scatter3(point(1),point(2),point(3),100,'k','*');
%     quiver3(point(1),point(2),point(3),bx(1),bx(2),bx(3),...
%         'r','linewidth',2,'AutoScaleFactor',0.2,'MaxHeadSize',0.1);
%     quiver3(point(1),point(2),point(3),by(1),by(2),by(3),...
%         'g','linewidth',2,'AutoScaleFactor',0.2,'MaxHeadSize',0.1);
%     quiver3(point(1),point(2),point(3),bz(1),bz(2),bz(3),...
%         'b','linewidth',2,'AutoScaleFactor',0.2,'MaxHeadSize',0.1);
%     daspect([1,1,1]);
%     
%     cnt = cnt + 1;
%     if cnt==1001
%         flag = false;
%     end
% end


X = [0:0.001:0.9];
Y = [-0.9:0.001:0.9];
Z = [0:0.001:0.7];
A = [-pi:0.001:pi];
B = [-pi:0.001:pi];
C = [-pi:0.001:pi];

X_set = [];
Angle_set = {};

flag = true;
cnt = 1;
while flag
    fprintf('Conducting trial:  %d\n',cnt)
    x = [randsample(X,1);
         randsample(Y,1);
         randsample(Z,1);
         randsample(A,1);
         randsample(B,1);
         randsample(C,1);
         ];
    x = [0.5;0.1;0.3;0;0;0]; 
    CONFIG.robot_T_part = eye(4);
    CONFIG.robot_T_part(1:3,4) = [x(1); x(2); x(3)];
    CONFIG.robot_T_part(1:3,1:3) = eul2rotm([x(4), x(5), x(6)],'ZYX');
    temp = (CONFIG.robot_T_part * [CONFIG.xyz_bxbybz(:,1:3)'; ones(1,size(CONFIG.xyz_bxbybz,1)) ] )';
    CONFIG.transf_xyzbxbybz(:,1:3) = temp(:,1:3);
    CONFIG.transf_xyzbxbybz(:,4:6) = (CONFIG.robot_T_part(1:3,1:3) * CONFIG.xyz_bxbybz(:,4:6)')';
    CONFIG.transf_xyzbxbybz(:,7:9) = (CONFIG.robot_T_part(1:3,1:3) * CONFIG.xyz_bxbybz(:,7:9)')';
    CONFIG.transf_xyzbxbybz(:,10:12) = (CONFIG.robot_T_part(1:3,1:3) * CONFIG.xyz_bxbybz(:,10:12)')';

    for i=1:size(CONFIG.transf_xyzbxbybz,1)
        CONFIG.transf_xyzbxbybz(i,4:6) = CONFIG.transf_xyzbxbybz(i,4:6)./norm(CONFIG.transf_xyzbxbybz(i,4:6));
        CONFIG.transf_xyzbxbybz(i,7:9) = CONFIG.transf_xyzbxbybz(i,7:9)./norm(CONFIG.transf_xyzbxbybz(i,7:9));
        CONFIG.transf_xyzbxbybz(i,10:12) = CONFIG.transf_xyzbxbybz(i,10:12)./norm(CONFIG.transf_xyzbxbybz(i,10:12));
    end 
    
%     points = CONFIG.transf_xyzbxbybz(:,1:3);
%     bx = CONFIG.transf_xyzbxbybz(:,4:6);
%     by = CONFIG.transf_xyzbxbybz(:,7:9);
%     bz = CONFIG.transf_xyzbxbybz(:,10:12);
%     figure(1)
%     hold on;
%     part = 'HS.STL';
%     [v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
%     v_part = v_part./1000;
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,CONFIG.robot_T_part);
%     part_plt(v_part_transf, f_part, name_part, [0.5,0.6,0.8]); % plot tool stl
%     hold on;
%     pause(1.5);
%     plot_robot([0;0;0;-pi/2;0;pi/2;0],robot1);
%     a = quiver3(points(:,1),points(:,2),points(:,3),...
%            bx(:,1),bx(:,2),bx(:,3),'r','linewidth',2,'AutoScaleFactor',0.5);
%        hold on;
%     b = quiver3(points(:,1),points(:,2),points(:,3),...
%            by(:,1),by(:,2),by(:,3),'g','linewidth',2,'AutoScaleFactor',0.5);
%        hold on;
%     c = quiver3(points(:,1),points(:,2),points(:,3),...
%            bz(:,1),bz(:,2),bz(:,3),'b','linewidth',2,'AutoScaleFactor',0.5);
%     daspect([1,1,1]);

    joints = [0;0;0;0;0;0;0];
    angles = [];
    err = 0;
    for i=1:size(CONFIG.transf_xyzbxbybz,1)
        CONFIG.curr_idx = i;
        [joints,err_theta] = fmincon(theta_rout,joints,[],[],[],[],CONFIG.theta_lb,CONFIG.theta_ub,[],CONFIG.options); 
        err = err + err_theta;
        angles = [angles, joints];
    end
    
    if err<460
        x
        X_set = [X_set, x];
        Angle_set{end+1} = angles;
    end
    cnt = cnt + 1;
end
save('X_set.mat');    


function erf_theta = pose_err(joints)
    global CONFIG;
    global robot1;
    
    i = CONFIG.curr_idx;
    erf_theta = 0;
    
% loop through each point
    tol_xyz = CONFIG.tolerances(i,1);
    tol_alpha = CONFIG.tolerances(i,2);
    tol_beta = CONFIG.tolerances(i,3);
    tol_gamma = CONFIG.tolerances(i,4);

% represent the point on the part w.r.t. robot-base
    pointi = CONFIG.transf_xyzbxbybz(i,1:3)';
    pbx = CONFIG.transf_xyzbxbybz(i,4:6)';
    pby = CONFIG.transf_xyzbxbybz(i,7:9)';
    pbz = CONFIG.transf_xyzbxbybz(i,10:12)';
    
    % ROBOT end-effector
    all_transf_mat = robot1.fwd_kin_all_joints(joints);
    b1_T_ree = all_transf_mat{end}; % eff transf    
    b1_T_tee = b1_T_ree*robot1.robot_ree_T_tee;

    tool_xyz = b1_T_tee(1:3,4);
    toolX = b1_T_tee(1:3,1:3) * [1;0;0]; toolX = toolX/norm(toolX);
    toolY = b1_T_tee(1:3,1:3) * [0;1;0]; toolY = toolY/norm(toolY);
    toolZ = b1_T_tee(1:3,1:3) * [0;0;1]; toolZ = toolZ/norm(toolZ);

    % Error Without Tolerances
    error_xyz = norm( pointi-tool_xyz );        
    error_nx = abs( acos(dot(pbx, toolX)) );
    error_ny = abs( acos(dot(pby, toolY)) );
    error_nz = abs( acos(dot(pbz, toolZ)) );

    % Adding errors only if outside tolerance bounds.
    erf_theta = erf_theta + exp( error_xyz-tol_xyz );
    erf_theta = erf_theta + exp( (error_nx-tol_gamma)/pi );
%     erf_theta = erf_theta + exp( (error_ny-tol_beta) /pi );
%     erf_theta = erf_theta + exp( (error_nz-tol_alpha) /pi );
end




% 0.6010
%    -0.4750
%     0.3230
%    -1.1656
%     2.6904
%     3.0744



% 0.5790
%    -0.2070
%     0.6220
%    -0.9266
%     0.3814
%     2.2104