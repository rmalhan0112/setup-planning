% Implementation of a simple policy of HGD on the Spiral part

% Objective is to minimize time subject to different constraints.
close all;
clear all;
clc

warning 'off';
global CONFIG;

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
joint_vel = joint_vel*0.8;
CONFIG.tool_stl = 'tool.stl';

% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.120]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

IP_options = optimoptions('fmincon','Algorithm', 'interior-point');
IP_options.MaxIterations = 1e7;
IP_options.MaxFunctionEvaluations = 1e7;
IP_options.OptimalityTolerance = 1e-10;
IP_options.StepTolerance = 1e-10;
IP_options.Display = 'off';
IP_options.SpecifyObjectiveGradient = true;
IP_options.FunctionTolerance = 1e-8;



TR_options = optimoptions('fmincon','Algorithm', 'trust-region-reflective');
TR_options.MaxIterations = 1e7;
TR_options.MaxFunctionEvaluations = 1e7;
TR_options.OptimalityTolerance = 1e-10;
TR_options.StepTolerance = 1e-10;
TR_options.Display = 'off';
TR_options.SpecifyObjectiveGradient = true;
TR_options.FunctionTolerance = 1e-8;




%% Generate Points

rot = eul2rotm([0,pi,0],'ZYX');
Part_T = eye(4);
Part_T(1:3,4) = [ 0.5; 0.1; 0.3 ];
Part_T(1:3,1:3) = rot;

bx = [1;0;0];
by = [0;1;0];
bz = [0;0;1];


% [ 0,0,0,bx',by',bz';
%             0,20,0,bx',by',bz';
%             0,40,0,bx',by',bz';
%             0,60,0,bx',by',bz';
%             20,20,0,bx',by',bz';
%             20,40,0,bx',by',bz';
%             40,30,0,bx',by',bz'   ];
% 
% 
% Xk = [];
% counter = 0;
% while counter < 21
%     sample = [ randsample([-0.03:0.001:0.03],1);
%                 randsample([-0.03:0.001:0.03],1);
%                 randsample([-0.03:0.001:0.03],1);
%                 randsample([-1:0.001:1],1);
%                 randsample([-1:0.001:1],1);
%                 randsample([-1:0.001:1],1);
%                 randsample([-1:0.001:1],1)
%                 ];
%     sample(4:7) = sample(4:7) / norm(sample(4:7));
%     Rot = quat2rotm(sample(4:7)');
%     bz = Rot*[0;0;1];
%     bx = Rot*[1;0;0];
%     by = Rot*[0;1;0];
%     bz = bz/norm(bz);
%     bx = bx/norm(bx);
%     by = by/norm(by);
%     angle = rad2deg(abs( acos(dot(bz,[0;0;1])) ));
%     if (angle < 90)
%         counter = counter + 1;
%         Xk = [Xk; [sample(1:3)',bx',by',bz'] ];
%     end
% end
           

Xk = csvread('bug_random_samples.csv');

transf_xyzbxbybz(:,1:4) = (  Part_T * [Xk(:,1:3)'; ones(1,size(Xk,1))]  )';
transf_xyzbxbybz(:,4) = [];
for i=1:size(Xk,1)
    transf_xyzbxbybz(i,4:6) = ( rot * Xk(i,4:6)' )';
    transf_xyzbxbybz(i,4:6) = transf_xyzbxbybz(i,4:6) / norm(transf_xyzbxbybz(i,4:6));
    transf_xyzbxbybz(i,7:9) = ( rot * Xk(i,7:9)' )';
    transf_xyzbxbybz(i,7:9) = transf_xyzbxbybz(i,7:9) / norm(transf_xyzbxbybz(i,7:9));
    transf_xyzbxbybz(i,10:12) = ( rot * Xk(i,10:12)' )';
    transf_xyzbxbybz(i,10:12) = transf_xyzbxbybz(i,10:12) / norm(transf_xyzbxbybz(i,10:12));
end


figure(1)
hold on;
plot_robot([0;0;0;-pi/2;0;pi/2;0],robot1);
daspect([1,1,1]);
for i=1:size(transf_xyzbxbybz,1)
    [handler_a,handler_b,handler_c] = plot_axes( transf_xyzbxbybz(i,1:3), transf_xyzbxbybz(i,4:6),...
                    transf_xyzbxbybz(i,7:9), transf_xyzbxbybz(i,10:12), 0.05);
end

% tolerances = zeros( size(transf_xyzbxbybz,1),4 );
% tolerances(:,1) = 0.002;
% tolerances(:,2) = 0.05236;
% tolerances(:,3) = 0.05236;
% tolerances(:,4) = 0.05236;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 1500;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-6;
options.StepTolerance = 1e-6;
options.Display = 'off';
options.SpecifyObjectiveGradient = true;


tolerance(1) = 0.003;
tolerance(2) = 0.05236;

step = 0.06;
samples = {};
Part_T(1,4) = -0.8;

%% Main
while Part_T(1,4) < 0.8
    disp(Part_T(1,4));
    temp = (  Part_T * [Xk(:,1:3)'; ones(1,size(Xk,1))]  )';
    transf_xyzbxbybz(:,1:3) = temp(:,1:3);

%         transf_xyzbxbybz(21,:) = [-0.5900;
%     0.0880;
%     0.3010;
%     0.3053;
%     0.9304;
%     0.2028;
%    -0.9328;
%     0.3350;
%    -0.1328;
%    -0.1915;
%    -0.1487;
%     0.9702]';

%     Compute IK using Matlab toolbox

%     mtlb_reach = [];
%     init_guess = [0;0;0;0;0;0;0];
%     tic;
%     parfor i=1:size(transf_xyzbxbybz,1)
%         point = transf_xyzbxbybz(i,:);
% %     for i=21:21
%         T = eye(4);
%         T(1:3,4) = transf_xyzbxbybz(i,1:3)';
%         bx = transf_xyzbxbybz(i,4:6)';
%         by = transf_xyzbxbybz(i,7:9)';
%         bz = transf_xyzbxbybz(i,10:12)';
% 
%         T(1:3,1:3) = [  dot(bx,[1;0;0]),  dot(by,[1;0;0]),  dot(bz,[1;0;0]);
%                         dot(bx,[0;1;0]),  dot(by,[0;1;0]),  dot(bz,[0;1;0]);
%                         dot(bx,[0;0;1]),  dot(by,[0;0;1]),  dot(bz,[0;0;1])  ];
% %         X = robot1.robot_ree_T_tee'\T';
% %         T = X';
%         [joint_config,solnInfo] = robot1.get_iiwa_IK( T, init_guess );
%         mtlb_trn = get_iiwa_FK_mex( joint_config,eye(4) );
%         err_xyz = norm(point(1:3)'-mtlb_trn(1:3,4));
% %         err_bx = acos( point(4)*mtlb_trn(1,1) + point(5)*mtlb_trn(2,1) + point(6)*mtlb_trn(3,1) );
% %         err_by = acos( point(7)*mtlb_trn(1,2) + point(8)*mtlb_trn(2,2) + point(9)*mtlb_trn(3,2) );
%         err_bz = acos( point(10)*mtlb_trn(1,3) + point(11)*mtlb_trn(2,3) + point(12)*mtlb_trn(3,3) );
%         if err_xyz > tolerance(1) || err_bz > tolerance(2)
%             mtlb_reach = [mtlb_reach; [0,i]];
%         else
%             mtlb_reach = [mtlb_reach; [1,i]];
%         end  
    
        
        
%         [err_xyz,err_z] = compute_pose_err(joint_config,transf_xyzbxbybz,i,tolerances,robot1);
%         if err_xyz > 1 || err_z > 1
%             mtlb_reach = [mtlb_reach; [0,i]];
%         else
%             mtlb_reach = [mtlb_reach; [1,i]];
%         end
%     end
%     toc;
%     mtlb_reach = sortrows(mtlb_reach, 2);
%     mtlb_reach = mtlb_reach(:,1);



    % Compute IK using Fmincon
    fmincon_reach = [];
    init_guess = [0;0;0;0;0;0;0];
    tic;
    parfor i=1:size(transf_xyzbxbybz,1)
%     for i=21:21
        [joint_config,fmn] = get_iiwa_IK( [0;0;0;0;0;0;0],transf_xyzbxbybz(i,:),tolerance,options,theta_lb,theta_ub,true,5 );    
        if fmn
            fmincon_reach = [fmincon_reach; [1,i]];
        else
            fmincon_reach = [fmincon_reach; [0,i]];
        end

%         [joint_config,error] = Optimizer(transf_xyzbxbybz,tolerances,i,robot1,...
%                                 init_guess,theta_lb,theta_ub,IP_options, TR_options);
                            
%         if error(1) > 1 || error(2) > 1
%             fmincon_reach = [fmincon_reach; [0,i]];
%         else
%             fmincon_reach = [fmincon_reach; [1,i]];
%         end
    end
    toc;
    fprintf("\n\n");
    fmincon_reach = sortrows(fmincon_reach, 2);
    fmincon_reach = fmincon_reach(:,1);
    
% Collect Data
    Part_T(1,4) = Part_T(1,4) + step;
%     fetch.toolbox = mtlb_reach;
    fetch.fmn = fmincon_reach;
    fetch.X = Part_T(1,4);
    samples{end+1} = fetch;
end


figure(2)
hold on;
title("ToolBox")
figure(3)
title("Fmincon")
hold on;
%% Plot figures
for i=1:size(samples,2)
    obj = samples{i};
%     tb_reach = obj.toolbox;
    fmn_reach = obj.fmn;
    x = obj.X;
    for j=1:size(fmn_reach,1)
%         figure(2)
%         if tb_reach(j)==1
%             scatter(x, j, 30, 'g', 'filled');
%         else
%             scatter(x, j, 30, 'r', 'filled');
%         end
        
        figure(3)
        if fmn_reach(j)==1
            scatter(x, j, 30, 'g', 'filled');
        else
            scatter(x, j, 30, 'r', 'filled');
        end
    end
end



% %% Plot the reachabilites
% figure(2)
% hold on;
% plot( [1:size(transf_xyzbxbybz,1)]', mtlb_reach, 'r', 'linewidth', 2 );
% 
% figure(3)
% hold on;
% plot( [1:size(transf_xyzbxbybz,1)]', fmincon_reach, 'b', 'linewidth', 2 );



function [a,b,c] = plot_axes(point, bx, by, bz, scale)
    a = quiver3(point(1),point(2),point(3),...
           bx(1),bx(2),bx(3),'r','linewidth',2, 'AutoScaleFactor', scale);
    b = quiver3(point(1),point(2),point(3),...
           by(1),by(2),by(3),'g','linewidth',2, 'AutoScaleFactor', scale);
    c = quiver3(point(1),point(2),point(3),...
           bz(1),bz(2),bz(3),'b','linewidth',2, 'AutoScaleFactor', scale);
end