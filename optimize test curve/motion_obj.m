function err = motion_obj(joints)
    global CONFIG;
    jt_upperidx = joints(8:end,1);
    jt_strt = 1;
    jt_end = 7;
    err = 0;
    for i=1:CONFIG.no_pts-1
        err = err + norm( jt_upperidx(jt_strt:jt_end,1)-joints(jt_strt:jt_end,1) );
        jt_strt = jt_strt + 7;
        jt_end = jt_end + 7;
    end
end