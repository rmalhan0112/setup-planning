function [err_xyz,err_z] = compute_pose_err(prev_config,transf_xyzbxbybz,i,robot1)
% loop through each point
    tol_xyz = 0.002;
%     tol_alpha = 0.0524;
    err_z = 0;
% represent the point on the part w.r.t. robot-base
    pointi = transf_xyzbxbybz(i,1:3)';
%     pbz = transf_xyzbxbybz(i,10:12)';
    
    % ROBOT end-effector
%     all_transf_mat = robot1.fwd_kin_all_joints(prev_config);
    b1_T_ree = get_iiwa_FK_mex(prev_config,eye(4)); % eff transf
%     b1_T_ree = all_transf_mat{end};
%     b1_T_tee = b1_T_ree*robot1.robot_ree_T_tee;
    b1_T_tee = b1_T_ree;
    tool_xyz = b1_T_tee(1:3,4);
%     toolZ = b1_T_tee(1:3,1:3) * [0;0;1]; toolZ = toolZ/norm(toolZ);

    % Error Without Tolerances
    err_xyz = exp( norm(tool_xyz-pointi) - tol_xyz);
%     err_z = exp ( acos(dot(pbz, toolZ)) - tol_alpha);
end




% robot_T_part = eye(4);
%     robot_T_part(1:3,1:3) = rotation;
%     robot_T_part(1:3,4) = fetch.translation;
%     
%     
%     figure(1)
%     hold on;
%     part = 'HS.STL';
%     [v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
%     v_part = v_part./1000;
% %     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,CONFIG.robot_T_part);
%     part_plt(v_part, f_part, name_part, [0.5,0.6,0.8]); % plot tool stl
%     hold on;
%     pause(1.5);
%     plot_robot([0;0;0;-pi/2;0;pi/2;0],robot1);
%     a = quiver3(xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),...
%            xyz_bxbybz(:,4),xyz_bxbybz(:,5),xyz_bxbybz(:,6),'r','linewidth',2,'AutoScaleFactor',0.5);
%        hold on;
%     b = quiver3(xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),...
%            xyz_bxbybz(:,7),xyz_bxbybz(:,8),xyz_bxbybz(:,9),'g','linewidth',2,'AutoScaleFactor',0.5);
%        hold on;
%     c = quiver3(xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),...
%            xyz_bxbybz(:,10),xyz_bxbybz(:,11),xyz_bxbybz(:,12),'b','linewidth',2,'AutoScaleFactor',0.5);
%     daspect([1,1,1]);
    
    



% hold on;
% [v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
% v_part = v_part./1000;
% [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,robot_T_part);
% part_plt(v_part_transf, f_part, name_part, [0.5,0.6,0.8]); % plot tool stl
% 
% pause(1.5);
% plot_robot([0;0;0;-pi/2;0;pi/2;0],robot1);
% a = quiver3(transf_xyzbxbybz(:,1),transf_xyzbxbybz(:,2),transf_xyzbxbybz(:,3),...
%        transf_xyzbxbybz(:,4),transf_xyzbxbybz(:,5),transf_xyzbxbybz(:,6),'r','linewidth',2,'AutoScaleFactor',0.5);
%    hold on;
% b = quiver3(transf_xyzbxbybz(:,1),transf_xyzbxbybz(:,2),transf_xyzbxbybz(:,3),...
%        transf_xyzbxbybz(:,7),transf_xyzbxbybz(:,8),transf_xyzbxbybz(:,9),'g','linewidth',2,'AutoScaleFactor',0.5);
%    hold on;
% c = quiver3(transf_xyzbxbybz(:,1),transf_xyzbxbybz(:,2),transf_xyzbxbybz(:,3),...
%        transf_xyzbxbybz(:,10),transf_xyzbxbybz(:,11),transf_xyzbxbybz(:,12),'b','linewidth',2,'AutoScaleFactor',0.5);
% daspect([1,1,1]);