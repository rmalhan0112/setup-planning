% Objective is to minimize time subject to different constraints.
close all;
clear all;
clc

warning 'off';
% global robot1;
global CONFIG;

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
joint_vel = joint_vel*0.8;
CONFIG.tool_stl = 'tool.stl';

% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 120, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.120]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


%% %%%%%% CURVE DATA %%%%%%%%%%%%%
curve_file_name  = 'HS_points.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
tol_file_name  = 'HS_tolfile.csv'; % n x 4 
tolerances = dlmread(tol_file_name);
no_pts = size(xyz_bxbybz,1);
%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);
% 0.0159 is distance from one point to another.

%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

% beta_lb = -pi/2*ones(no_pts,1);
% beta_ub = -beta_lb;

% options = optimoptions('fmincon');
options.MaxIterations = 1e5;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-3;
options.StepTolerance = 1e-3;
options.Display = 'off';
options.FiniteDifferenceStepSize = 0.0044*ones(7,1);

no_samples = 2000;
samples = csvread('Samples.csv');

tic;
for i=1:no_samples
    fprintf('Conducting trial:  %d\n',i)
    sample_data = {};
    x = [samples(i,1);samples(i,2);samples(i,3);samples(i,4:7)'];
    x = [ 0.9;	0.87;	0.7;	0.98125;	0.53964;	0.54099;	0.96166];

    sample_data{1} = acq_data(x, [0;0;0;0;0;0;0],xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);
    Q = perturb(x);
    parfor counter = 1:12
       sample_data{counter+1} =  acq_data(Q(:,counter), [0;0;0;0;0;0;0],xyz_bxbybz, theta_lb,...
                            theta_ub, options, joint_vel,tolerances,robot1);
    end
    filename = strcat('sample_', num2str(i), '.mat');
    save(fullfile('Sample Data Reach',filename), 'sample_data');
end
toc;

function Q = perturb(x)
    Q = zeros(7,12);
    Q(1:7,1) = x;   Q(1,1) = Q(1,1) + 0.01;
    Q(1:7,2) = x;   Q(1,2) = Q(1,2) - 0.01;
    Q(1:7,3) = x;   Q(2,3) = Q(2,3) + 0.01;
    Q(1:7,4) = x;   Q(2,4) = Q(2,4) - 0.01;
    Q(1:7,5) = x;   Q(3,5) = Q(3,5) + 0.01;
    Q(1:7,6) = x;   Q(3,6) = Q(3,6) + 0.01;
    
    Q(1:3,7) = x(1:3);
    Q(1:3,8) = x(1:3);
    Q(1:3,9) = x(1:3);
    Q(1:3,10) = x(1:3);
    Q(1:3,11) = x(1:3);
    Q(1:3,12) = x(1:3);
%     Q(1:3,13) = x(1:3);
%     Q(1:3,14) = x(1:3);
    
    q = x(4:7);
    Q(4:7,7) = x(4:7);
    Q(4:7,8) = x(4:7);
    Q(4:7,9) = x(4:7);
    Q(4:7,10) = x(4:7);
    Q(4:7,11) = x(4:7);
    Q(4:7,12) = x(4:7);
%     Q(4:7,13) = x(4:7);
%     Q(4:7,14) = x(4:7);
    
    Q(4,7) = q(1) + 0.01;    Q(4:7,7) = Q(4:7,7)./norm(Q(4:7,7));
    Q(4,8) = q(1) - 0.01;    Q(4:7,8) = Q(4:7,8)./norm(Q(4:7,8));
    Q(5,9) = q(2) + 0.01;    Q(4:7,9) = Q(4:7,9)./norm(Q(4:7,9));
    Q(5,10) = q(2) - 0.01;   Q(4:7,10) = Q(4:7,10)./norm(Q(4:7,10));
    Q(6,11) = q(3) + 0.01;   Q(4:7,11) = Q(4:7,11)./norm(Q(4:7,11));
    Q(6,12) = q(3) - 0.01;   Q(4:7,12) = Q(4:7,12)./norm(Q(4:7,12));
%     Q(7,13) = q(4) + 0.01;   Q(4:7,13) = Q(4:7,13)./norm(Q(4:7,13));
%     Q(7,14) = q(4) - 0.01;   Q(4:7,14) = Q(4:7,14)./norm(Q(4:7,14));
end



% b = sym('b',[no_pts,1]);
% R = {};
% for i=1:no_pts
%     Rz{i} = [ cos(b(i)), -sin(b(i)), 0; sin(b(i)), cos(b(i)), 0; 0,0,1 ];
% end