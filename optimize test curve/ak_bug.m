clear all;
close all;
clc;

nonlcon = @fcon;

x0 = [1,1];
lb = [-1,-inf];
ub = [inf,1];

A = []; b = []; Aeq = []; beq = [];

options = optimoptions('fmincon');
options.MaxIterations = 1e5;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-250;
options.StepTolerance = 1e-250;
% options.ConstraintTolerance = -1;
options.SpecifyConstraintGradient = true;
options.SpecifyObjectiveGradient = true;
options.display('iter');
% options.Algorithm = 'interior-point';
% options.Algorithm = 'sqp';
% options.Algorithm = 'active-set';
    
x = fmincon(@fun,x0,A,b,Aeq,beq,lb,ub,nonlcon,options)

function [f,g] = fun(x)
    f = x(2);
    if nargout > 1
        g = [
          0;
          1;    
        ];
    end

end

function [c,ceq, DC, DCeq] = fcon(x)
    c = [];
    ceq = x(1)^2+x(2)^2-1;
    if nargout > 2
        DC = [];
        DCeq = [2*x(1); 2*x(2);];
    end
end