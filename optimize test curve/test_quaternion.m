clear all;
close all;
clc;

gx = [1;0;0];
gy = [0;1;0];
gz = [0;0;1];


figure(1)
hold on;
% Plot global Frame
quiver3(0,0,0,1,0,0,'r','linewidth',2,'AutoScaleFactor',0.5);
quiver3(0,0,0,0,1,0,'g','linewidth',2,'AutoScaleFactor',0.5);
quiver3(0,0,0,0,0,1,'b','linewidth',2,'AutoScaleFactor',0.5);
daspect([1,1,1]);


for i=1:10
    quaternion = [ randsample([-1:1],1); 
                    randsample([-1:1],1);
                     randsample([-1:1],1);
                     randsample([-1:1],1)   ];
    quaternion = quaternion/norm(quaternion);
    rot = quat2rotm( quaternion' );
    mx = rot*gx;
    my = rot*gy;
    mz = rot*gz;
    mx = mx/norm(mx);
    my = my/norm(my);
    mz = mz/norm(mz);
    
    % Plot rotated frame
    quiver3(0,0,0,...
           mx(1),mx(2),mx(3),'r','linewidth',2,'AutoScaleFactor',1);

    quiver3(0,0,0,...
           my(1),my(2),my(3),'g','linewidth',2,'AutoScaleFactor',1);

    quiver3(0,0,0,...
           mz(1),mz(2),mz(3),'b','linewidth',2,'AutoScaleFactor',1);
    pause(0.0001);   
    % Plot perturbed frame       
    for j=1:3
        pertrb = quaternion;
        pertrb(j) = quaternion(j) + 0.05;
        pertrb = pertrb/norm(pertrb);
        rot = quat2rotm( pertrb' );
        bx = rot*gx;
        by = rot*gy;
        bz = rot*gz;
        bx = bx/norm(bx);
        by = by/norm(by);
        bz = bz/norm(bz);
        rad2deg(abs( acos( dot(bx,mx) ) ))
        rad2deg(abs( acos( dot(by,my) ) ))
        rad2deg(abs( acos( dot(bz,mz) ) ))
        % Plot rotated frame
        a = quiver3(0,0,0,...
           bx(1),bx(2),bx(3),'r','linewidth',2,'AutoScaleFactor',2);

        b = quiver3(0,0,0,...
               by(1),by(2),by(3),'g','linewidth',2,'AutoScaleFactor',2);

        c = quiver3(0,0,0,...
               bz(1),bz(2),bz(3),'b','linewidth',2,'AutoScaleFactor',2);
        pause(1);
        delete(a);
        delete(b);
        delete(c);
    end
end