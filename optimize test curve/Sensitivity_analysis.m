% SA for the continuity files.
close all;
clear all;
clc;
% Format of Handler
% translation: [3�1 double]
% quat: [4�1 double]
% reachability: 16
% strt_end_cont: [3�1 double]
% end_strt_cont: [3�1 double]
% pt_data: [115�12 double]

foldername = 'Sample Data Reach';
resultsfile = 'Reach results';

% Reachability
list = [];
for i=1:1:2000
    file = fullfile( foldername, strcat('sample_', num2str(i),'.mat') );
    s = load(file);
    handler1 = s.sample_data{1};
    sens_data = [];
    for j=2:13
        handler = s.sample_data{j};
        change = handler.reachability-handler1.reachability;
        sens_data = [sens_data, change ];
    end
    list = [list; [handler1.translation',handler1.quat',sens_data]];
end
csvwrite(fullfile(resultsfile,'Reachability_Sensitivity.csv'),list);




% Start-End Continuity
list = [];
for i=1:1:2000
    file = fullfile( foldername, strcat('sample_', num2str(i),'.mat') );
    s = load(file);
    handler1 = s.sample_data{1};
    sens_data = [];
    for j=2:13
        handler = s.sample_data{j};
        change = handler.strt_end_cont(1)-handler1.strt_end_cont(1);
        sens_data = [sens_data, change ];
    end
    list = [list; [handler1.translation',handler1.quat',sens_data]];
end
csvwrite(fullfile(resultsfile,'Start-End_Sensitivity.csv'),list);

% Start-End Time Sensitivity
list = [];
for i=1:1:2000
    file = fullfile( foldername, strcat('sample_', num2str(i),'.mat') );
    s = load(file);
    handler1 = s.sample_data{1};
    sens_data = [];
    for j=2:13
        handler = s.sample_data{j};
        change = handler.strt_end_cont(3)-handler1.strt_end_cont(3);
        sens_data = [sens_data, change ];
    end
    list = [list; [handler1.translation',handler1.quat',sens_data]];
end
csvwrite(fullfile(resultsfile,'Start-End_Time_Sensitivity.csv'),list);





% End-Start Continuity
list = [];
for i=1:1:2000
    file = fullfile( foldername, strcat('sample_', num2str(i),'.mat') );
    s = load(file);
    handler1 = s.sample_data{1};
    sens_data = [];
    for j=2:13
        handler = s.sample_data{j};
        change = handler.end_strt_cont(1)-handler1.end_strt_cont(1);
        sens_data = [sens_data, change ];
    end
    list = [list; [handler1.translation',handler1.quat',sens_data]];
end
csvwrite(fullfile(resultsfile,'End-Start_Sensitivity.csv'),list);

% End-Start Time Sensitivity
list = [];
for i=1:1:2000
    file = fullfile( foldername, strcat('sample_', num2str(i),'.mat') );
    s = load(file);
    handler1 = s.sample_data{1};
    sens_data = [];
    for j=2:13
        handler = s.sample_data{j};
        change = handler.end_strt_cont(3)-handler1.end_strt_cont(3);
        sens_data = [sens_data, change ];
    end
    list = [list; [handler1.translation',handler1.quat',sens_data]];
end
csvwrite(fullfile(resultsfile,'End-Start_Time_Sensitivity.csv'),list);





% Total Manipulability
list = [];
for i=1:1:2000
    file = fullfile( foldername, strcat('sample_', num2str(i),'.mat') );
    s = load(file);
    handler1 = s.sample_data{1};
    sens_data = [];
    for j=2:13
        handler = s.sample_data{j};
        change = sum(handler.pt_data(:,11))-sum(handler1.pt_data(:,11));
        sens_data = [sens_data, change ];
    end
    list = [list; [handler1.translation',handler1.quat',sens_data]];
end
csvwrite(fullfile(resultsfile,'Manipulability_Sensitivity.csv'),list);






% Total Condition Number
list = [];
for i=1:1:2000
    file = fullfile( foldername, strcat('sample_', num2str(i),'.mat') );
    s = load(file);
    handler1 = s.sample_data{1};
    sens_data = [];
    for j=2:13
        handler = s.sample_data{j};
        change = sum(handler.pt_data(:,12))-sum(handler1.pt_data(:,12));
        sens_data = [sens_data, change ];
    end
    list = [list; [handler1.translation',handler1.quat',sens_data]];
end
csvwrite(fullfile(resultsfile,'CN_Sensitivity.csv'),list);