function fetch = acq_data(x,init_joints,xyz_bxbybz, theta_lb, theta_ub, options, joint_vel,tolerances,robot1)
    no_pts = size(xyz_bxbybz,1);
    t_min = 0.2033;   % Time in seonds per segment max
    vmax = 0.06;
    
    fetch.translation = x(1:3);
    fetch.quat = x(4:7);
    rotation = quat2rotm(fetch.quat');

    temp = (rotation * xyz_bxbybz(:,1:3)')';
    parfor i = 1:no_pts
        temp(i,:) = temp(i,:) + x(1:3)';
    end
    
    transf_xyzbxbybz(:,1:3) = temp(:,1:3);
    transf_xyzbxbybz(:,4:6) = (rotation * xyz_bxbybz(:,4:6)')';
    transf_xyzbxbybz(:,7:9) = (rotation * xyz_bxbybz(:,7:9)')';
    transf_xyzbxbybz(:,10:12) = (rotation * xyz_bxbybz(:,10:12)')';

    for i = 1:no_pts
        transf_xyzbxbybz(i,4:6) = transf_xyzbxbybz(i,4:6)./norm(transf_xyzbxbybz(i,4:6));
        transf_xyzbxbybz(i,7:9) = transf_xyzbxbybz(i,7:9)./norm(transf_xyzbxbybz(i,7:9));
        transf_xyzbxbybz(i,10:12) = transf_xyzbxbybz(i,10:12)./norm(transf_xyzbxbybz(i,10:12));
    end 
    pt_data = zeros(no_pts,13); % IsReachable, t1-t7 joint values,Max Velcotiy Poss,err_xyz,err_z manip index, C.N.)
    pt_data(:,1) = ones(no_pts,1);
    Joints_data = [];
    
% IK solution
    parfor i = 1:no_pts
        joints = init_joints;
        reach = 1;
        [joints,err_theta] = fmincon(@(theta)reachability_err(theta,transf_xyzbxbybz,...
                            i,robot1),joints,[],[],[],[],theta_lb,theta_ub,...
                            [],options);
	% @(theta)cont_const(theta,i,joints) constraint
        [err_xyz,err_z] = compute_pose_err(joints,transf_xyzbxbybz,i,tolerances,robot1);
        if (err_xyz > 1 || err_z > 1)
           reach = 0; 
        end
        Joints_data = [Joints_data; [i, reach, joints', err_xyz-1, err_z-1]];
    end
    Joints_data = sortrows(Joints_data,1);
    pt_data(:,[1:8,10:11]) = Joints_data(:,2:11);
    
    Joints_data = [];
% Evaluate Velocity
    parfor i = 1:no_pts
        % Analysis for Analytical
        J_analy = iiwa_analytical_jacobian(pt_data(i,2:8)');
        [U,D,V] = svd(J_analy);
        vec = D*ones(7,1);
        K = (abs(max(vec))^0.5) / (abs(min(vec))^0.5);
        manip = (abs(det(J_analy*J_analy')))^0.5;
        
        if pt_data(i,1)==0
            velc = 0;
        else
            if i<no_pts
                jt_velc = (pt_data(i+1,2:8)'-pt_data(i,2:8)')/t_min; % Max Joint Velocity req for motion
                if ~all( (joint_vel-jt_velc)>0)
                    velc = 0;
                else
                    vel_obt = J_analy(1:3,:)*(jt_velc);
                    if norm(vel_obt)>vmax
                        velc = vmax;
                    else
                        velc = norm(vel_obt);
                    end
                end
            else
                velc = vmax;
            end
        end
        Joints_data = [Joints_data; [velc, manip, K]];
    end
    pt_data(:,[9,12:13]) = Joints_data;
% Store number of reachable points
    fetch.reachability = nnz(pt_data(:,1)==1);
%     fetch.reachability = (fetch.reachability/no_pts)*100;
% Analysis of Continuity from start to end
    counter = 0;
    time = 0;
    for i=1:no_pts
        if pt_data(i,1)==0
            break;
        else
            counter = counter + 1;
            time = time + 0.0159/pt_data(i,9);
        end
    end
    fetch.strt_end_cont = [counter; (counter/no_pts)*100; time];
% Analysis of Continuity from end to start
    counter = 0;
    time = 0;
    for i=no_pts:-1:1
        if pt_data(i,1)==0
            break;
        else
            counter = counter + 1;
            time = time + 0.0159/pt_data(i,9);
        end
    end
    fetch.end_strt_cont = [counter; (counter/no_pts)*100; time];
    fetch.pt_data = pt_data;
end