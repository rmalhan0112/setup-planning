#include "get_iiwa_FK.hpp"
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include "eigen-eigen-5a0156e40feb/Eigen/Core"
#include "eigen-eigen-5a0156e40feb/Eigen/Dense"
#include "eigen-eigen-5a0156e40feb/Eigen/Geometry"
#include <ctime>
#include <string>
#include <fstream>
#include <ios>
#include <map>
#include <vector>
// #include <thread>
#define PI 3.1415926535897
// #define delta 1.57
// #define delta 0.785
// #define delta 0.2618
#define delta 0.0872665
// #define delta 0.174533

using namespace std;

int get_dimension( int joint_no, Eigen::VectorXd theta_UB , Eigen::VectorXd theta_LB )
{
	int dimension;
	dimension = floor(( (theta_UB[joint_no-1]-theta_LB[joint_no-1]) / delta ) + 1);
	return dimension;
}


Eigen::VectorXd assign_thetas( int joint_no, int n, Eigen::VectorXd theta_LB )
{
	Eigen::VectorXd theta(n);
	theta[0] = theta_LB[joint_no-1];
	for (int i=1; i<theta.rows(); i++)
	{
		theta[i] = theta[i-1] + delta;
	}
	return theta;
}


string insert_leading_zeros( int val )
{
	string reply;
	if (val < 10)
		reply = "00" + to_string(val); 
	else if (val < 100)
		reply = '0' + to_string(val); 
	else
		reply = to_string(val); 
	return reply;
}


int main(int arc, char *argv[])
{
	ofstream data_file;
	string folder;
	string file;
	folder = "Voxel Data";
	// int counter = 1;

	clock_t strt;
	double duration;
	long int node_id = 0;
	// long int reset_val = 50000000;
	

// Defining the Upper Bounds
	Eigen::VectorXd theta_UB = Eigen::VectorXd::Ones(7);
	theta_UB[0] = 2.967059728390360;
	theta_UB[1] = 2.094395102393195;
	theta_UB[2] = 2.967059728390360;
	theta_UB[3] = 2.094395102393195;
	theta_UB[4] = 2.967059728390360;
	theta_UB[5] = 2.094395102393195;
	theta_UB[6] = 3.054326190990077;

// Defining the Upper Bounds
	Eigen::VectorXd theta_LB = Eigen::VectorXd::Ones(7);
	// theta_LB[0] = -2.967059728390360;
	theta_LB[0] = -1.047197551196598;
	// theta_LB[1] = -2.094395102393195;
	theta_LB[1] = -1.570796326794897;
	theta_LB[2] = -2.967059728390360;
	theta_LB[3] = -2.094395102393195;
	theta_LB[4] = -2.967059728390360;
	theta_LB[5] = -2.094395102393195;
	theta_LB[6] = -3.054326190990077;

	Eigen::VectorXd theta_1_range( get_dimension( 1, theta_UB , theta_LB ) ); 
	theta_1_range = assign_thetas(1, get_dimension( 1, theta_UB , theta_LB ), theta_LB);
	
	Eigen::VectorXd theta_2_range( get_dimension( 2, theta_UB , theta_LB ) ); 
	theta_2_range = assign_thetas(2, get_dimension( 2, theta_UB , theta_LB ), theta_LB);

	Eigen::VectorXd theta_3_range( get_dimension( 3, theta_UB , theta_LB ) ); 
	theta_3_range = assign_thetas(3, get_dimension( 3, theta_UB , theta_LB ), theta_LB);

	Eigen::VectorXd theta_4_range( get_dimension( 4, theta_UB , theta_LB ) ); 
	theta_4_range = assign_thetas(4, get_dimension( 4, theta_UB , theta_LB ), theta_LB);

	Eigen::VectorXd theta_5_range( get_dimension( 5, theta_UB , theta_LB ) ); 
	theta_5_range = assign_thetas(5, get_dimension( 5, theta_UB , theta_LB ), theta_LB);

	Eigen::VectorXd theta_6_range( get_dimension( 6, theta_UB , theta_LB ) ); 
	theta_6_range = assign_thetas(6, get_dimension( 6, theta_UB , theta_LB ), theta_LB);
	


// Main Loops
	Eigen::MatrixXd ee_base(4,4);
	Eigen::MatrixXd I = Eigen::MatrixXd::Identity(4,4);
	Eigen::VectorXd joint_config(7);
	Eigen::VectorXd xyz_UB(3);
	Eigen::VectorXd xyz_LB(3);
	Eigen::VectorXd ee_position(3);
	Eigen::VectorXd error(3);
	
	int x;
	int y;
	int z;
	string voxel_name;
	int bz1;
	int bz2;
	int bz3;

	xyz_UB[0] = 1;
	xyz_UB[1] = 1;
	xyz_UB[2] = 1;

	xyz_LB[0] = 0.0;
	xyz_LB[1] = -1;	
	xyz_LB[2] = 0.0;

// // Initializing Parallel Computing
// 	const int n_threads = std::thread::hardware_cocurrency();
// 	cout<< "Initializing Multi-thread computing......." <<endl;
// 	cout<< "Threads Available:  " << n_threads <<endl;
	
	// strt = std::clock();
	for (int th1=0; th1<theta_1_range.rows(); th1++)
	{
		cout<< "Theta 1 remaining......." << (theta_1_range.rows()-1)-th1 << endl;
		for (int th2=0; th2<theta_2_range.rows(); th2++)
		{
			for (int th3=0; th3<theta_3_range.rows(); th3++)
			{
				for (int th4=0; th4<theta_4_range.rows(); th4++)
				{
					for (int th5=0; th5<theta_5_range.rows(); th5++)
					{
						for (int th6=0; th6<theta_6_range.rows(); th6++)
						{
							joint_config[0] =  theta_1_range[th1];
							joint_config[1] =  theta_2_range[th2];
							joint_config[2] =  theta_3_range[th3];
							joint_config[3] =  theta_4_range[th4];
							joint_config[4] =  theta_5_range[th5];
							joint_config[5] =  theta_6_range[th6];
							joint_config[6] =  0;
							
							ee_base = get_iiwa_FK( joint_config, I );
							ee_position[0] = ee_base(0,3);
							ee_position[1] = ee_base(1,3);
							ee_position[2] = ee_base(2,3);
							
							// Check if XYZ are less than upper bounds
							error = xyz_UB - ee_position;
							if  (error[0] < 0.0 || error[1] < 0.0 || error[2] < 0.0)
								continue;
							// Check if XYZ are greater than lower bounds
							error = ee_position - xyz_LB;
							if  (error[0] < 0.0 || error[1] < 0.0 || error[2] < 0.0)
								continue;
							node_id += 1;
							x = (int) (ee_base(0,3)*100);
                        	y = (int) ((ee_base(1,3)*100)+100);
                        	z = (int) (ee_base(2,3)*100);
                        	
					    	bz1 = (int) (ee_base(0,2)*100);
					    	bz2 = (int) (ee_base(1,2)*100);
					    	bz3 = (int) (ee_base(2,2)*100);

					    	
					    	voxel_name = insert_leading_zeros(x) + insert_leading_zeros(y) + insert_leading_zeros(z);
					    	data_file.open( folder + "/" + voxel_name + ".csv", ios::app );
					    	data_file << bz1 << "," << bz2 << "," << bz3 << endl;
					        data_file.close();

						}
					}
				}
			}
		}
	}
	data_file.close();
// Storage Ends

	cout<< "Voxel Data Collection Succesfully Completed" << endl;
	// duration = ( std::clock() - strt ) / (double) CLOCKS_PER_SEC;
	// cout<< duration <<endl;
	return 0;
}
