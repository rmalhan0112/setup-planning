function fetch = acq_data_redn(x,Rz,init_betas,xyz_bxbybz, theta_lb, theta_ub,...
                                beta_lb, beta_ub, options, joint_vel,tolerances,robot1)
    no_pts = size(xyz_bxbybz,1);
    
    fetch.translation = x(1:3);
    fetch.quat = x(4:7);
    rotation = quat2rotm(fetch.quat');
    Pose = {};
    R_total = {};
    parfor i=1:no_pts
        pose_vec = [];
        R_total{i} = rotation * Rz{i};
% Position
        temp = (R_total{i} * xyz_bxbybz(i,1:3)')';
        temp = temp + x(1:3)';
        pose_vec = [pose_vec, temp];
% Orientation
        pose_vec = [pose_vec, R_total{i} * xyz_bxbybz(i,4:6)'];
        pose_vec = [pose_vec, R_total{i} * xyz_bxbybz(i,7:9)'];
        pose_vec = [pose_vec, R_total{i} * xyz_bxbybz(i,10:12)'];
        Pose{i} = pose_vec;
    end
    betas = init_betas;
    [betas,pose_err] = fmincon(@(beta)redundant_angle(beta,Pose,tolerances,R_total,...
                        robot1, theta_lb, theta_ub, joint_vel, options),...
                         betas,[],[],[],[],beta_lb,beta_ub,[],options);
    pose_err/115
    betas
    
	% @(theta)cont_const(theta,i,joints) constraint
    
% % Collecting Data    
%     for i = 1:no_pts
% %Store Joint Angles
%         pt_data(i,2:8) = joints';
%         J_analy = iiwa_analytical_jacobian(joints);
% % Analysis for Analytical
%         [U,D,V] = svd(J_analy);
%         vec = D*ones(7,1);
%         K = (abs(max(vec))^0.5) / (abs(min(vec))^0.5);
%         if err_theta>2
%            pt_data(i,1) = 0; 
%            pt_data(i,9) = 0;
%         else
%             velc = J_analy*joint_vel;
%             pt_data(i,9) = norm(velc(1:3));
%         end
%         pt_data(i,10) = err_theta;
%         pt_data(i,11) = (abs(det(J_analy*J_analy')))^0.5;
%         pt_data(i,12) = K;
%     end
% % Store number of reachable points
%     fetch.reachability = nnz(pt_data(:,1)==1);
% % Analysis of Continuity from start to end
%     counter = 0;
%     time = 0;
%     for i=1:115
%         if pt_data(i,1)==0
%             break;
%         else
%             counter = counter + 1;
%             time = time + 0.0073/pt_data(i,9);
%         end
%     end
%     fetch.strt_end_cont = [counter; (counter/115)*100; time];
% % Analysis of Continuity from end to start
%     counter = 0;
%     time = 0;
%     for i=115:-1:1
%         if pt_data(i,1)==0
%             break;
%         else
%             counter = counter + 1;
%             time = time + 0.0073/pt_data(i,9);
%         end
%     end
%     fetch.end_strt_cont = [counter; (counter/115)*100; time];
%     fetch.pt_data = pt_data;
end