close all;
clear all;
clc

% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
% ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 153, 1000]'./1000; % Pointy Tool
% ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  -45.2, 0, 131.7, 1000]'./1000; % Roller
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  6.5, -32.5, 85.6, 1000]'./1000; % Extruder

ROBOT_TOOL_FMM = {};
% ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 153, 1000]'./1000;
% ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  -45.2, 0, 131.7, 1000]'./1000; % Roller
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  6.5, -32.5, 85.6, 1000]'./1000; % Extruder

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
% robot1_ree_T_tee(1:3,4) = [0; 0; 0.153]; % For Pointy tool
% robot1.robot_ree_T_tee(1:3,4) = [-0.0452; 0; 0.1317]; % For Roller
robot1.robot_ree_T_tee(1:3,4) = [0.0065; -0.0325; 0.0856]; % For Extruder

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


%% Main
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_ub = - theta_lb;

t1 = [ theta_lb(1) : 0.008 : theta_ub(1) ];
t2 = [ theta_lb(2) : 0.008 : theta_ub(2) ];
t3 = [ theta_lb(3) : 0.008 : theta_ub(3) ];
t4 = [ theta_lb(4) : 0.008 : theta_ub(4) ];
t5 = [ theta_lb(5) : 0.008 : theta_ub(5) ];
t6 = [ theta_lb(6) : 0.008 : theta_ub(6) ];
t7 = [ theta_lb(7) : 0.008 : theta_ub(7) ];

cnt = 1;
flag = true;
ee_K = [];
ee_detr = [];
while flag
    state = [   randsample(t1,1);
                randsample(t2,1);
                randsample(t3,1);
                randsample(t4,1);
                randsample(t5,1);
                randsample(t6,1);
                randsample(t7,1)    ] ;

    all_transf_mat = robot1.fwd_kin_all_joints(state);
    b1_T_ree = all_transf_mat{end}; % eff transf. All Transf is a dictionary with all T
    if (abs(b1_T_ree(1,4)) > 1.5) || b1_T_ree(2,4)>1.5 || ...
        b1_T_ree(2,4)<0 || b1_T_ree(3,4)>1.5 || b1_T_ree(3,4)<0
        continue;
    end
    J = iiwa_analytical_jacobian(state); % Takes 0.03 seconds
    [U,D,V] = svd(J);
    vec = D*ones(7,1);
%     K = max(vec)^0.5 / min(vec)^0.5;
    detr = (det(J*J'))^0.5;
    if detr > 0.5
        continue;
    end
%     ee_K = [ee_K; [b1_T_ree(1,4), b1_T_ree(2,4), b1_T_ree(3,4), K] ];
    ee_detr = [ee_detr; [b1_T_ree(1,4), b1_T_ree(2,4), b1_T_ree(3,4), detr] ];
    cnt = cnt + 1;
    if cnt==20000
        flag = false;
    end
end

% figure(1)
% hold on;
% scatter3( ee_K(:,1),ee_K(:,2),ee_K(:,3), 20, ee_K(:,end), 'filled' );
% plot_robot( [ -pi/2;0;0;pi/2;0;-pi/2;0 ], robot1 );
% xlabel('X axis');
% ylabel('Y axis');
% zlabel('Z axis');
% title('Condition Number');
% colormap(jet);
% colorbar;
% daspect([1,1,1]);

figure(2)
hold on;
scatter3( ee_detr(:,1),ee_detr(:,2),ee_detr(:,3), 20, ee_detr(:,end), 'filled' );
plot_robot( [ -pi/2;0;0;pi/2;0;-pi/2;0 ], robot1 );
xlabel('X axis');
ylabel('Y axis');
zlabel('Z axis');
title('Manipulability');
colormap(jet);
colorbar;
daspect([1,1,1]);

% Condition Number VS singularity
% K_sort = sort(ee_K(:,end));
% detr_Sort = sort(ee_detr(:,end));
% figure(3)
% scatter( [1:size(K_sort,1)], K_sort, 20, 'r', '.' );
% figure(4)
% scatter( [1:size(K_sort,1)], detr_Sort, 20, 'b', '.' );
