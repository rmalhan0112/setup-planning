function [joint_config, error] = Optimizer(transf_xyzbxbybz,tolerances,i,robot1,...
                                init_guess,theta_lb,theta_ub,IP_options, TR_options)
                            
    joint_config = fmincon(@(theta)reachability_err(theta,transf_xyzbxbybz,...
                                i,robot1),init_guess,[],[],[],[],theta_lb,theta_ub,...
                                [],IP_options);
    [err_xyz,err_z] = compute_pose_err(joint_config,transf_xyzbxbybz,i,tolerances,robot1);
        if err_xyz > 1 || err_z > 1
            joint_config = fmincon(@(theta)reachability_err(theta,transf_xyzbxbybz,...
                                i,robot1),init_guess,[],[],[],[],theta_lb,theta_ub,...
                                [],TR_options);
            [err_xyz,err_z] = compute_pose_err(joint_config,transf_xyzbxbybz,i,tolerances,robot1);
            error = [ err_xyz; err_z ];
                
        else
            error = [ err_xyz; err_z ];
        end
end