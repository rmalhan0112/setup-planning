function pose_err = redundant_angle(beta,Pose,tolerances,...
                         R_total,robot1, theta_lb, theta_ub, joint_vel, options)
    no_pts = size(Pose,2);
    parfor i = 1:no_pts
        curr_pos = subs(Pose{i},beta(i));
        curr_pos = double(curr_pos);
        R_total{i} = double(subs(R_total{i},beta(i)));
        position(i,:) = curr_pos(:,1)'; 
        temp_Bx(i,:) = curr_pos(:,2)';
        temp_By(i,:) = curr_pos(:,3)';
        temp_Bz(i,:) = curr_pos(:,4)';
    end
    transf_xyzbxbybz(:,1:3) = position;
    transf_xyzbxbybz(:,4:6) = temp_Bx;
    transf_xyzbxbybz(:,7:9) = temp_By;
    transf_xyzbxbybz(:,10:12) = temp_Bz;
    
    for i = 1:no_pts
        transf_xyzbxbybz(i,4:6) = transf_xyzbxbybz(i,4:6)./norm(transf_xyzbxbybz(i,4:6));
        transf_xyzbxbybz(i,7:9) = transf_xyzbxbybz(i,7:9)./norm(transf_xyzbxbybz(i,7:9));
        transf_xyzbxbybz(i,10:12) = transf_xyzbxbybz(i,10:12)./norm(transf_xyzbxbybz(i,10:12));
    end 
    
    d_theta_init = [0;0;0;0;0;0;0];
    prev_config = d_theta_init;
    A = [eye(7);eye(7)];
%     A(1:7,:) = -1*A(1:7,:);
    A(8:14,:) = -1*A(8:14,:);
    b = ones(14,1);
%     b(1:7,1) = zeros(7,1);
    b(1:7,1) = -(prev_config-theta_ub);
    b(8:14,1) = -(theta_lb-prev_config);
    % For first point
    [prev_config,err_theta] = fmincon(@(theta)reachability_err(theta,transf_xyzbxbybz,...
                        1,tolerances,robot1),prev_config,[],[],[],[],theta_lb,theta_ub,[],options); 
    pose_err = 0;                    
    for i=1:no_pts
        J = iiwa_analytical_jacobian(prev_config);
        H = 2*(J'*J);
        F = QP_gradient(prev_config,transf_xyzbxbybz,R_total,i,robot1,J);
        [joints,err_theta] = quadprog(H,F,A,b,[],[],[],[],d_theta_init,options);
        pse_err_jt = compute_pose_err(prev_config,transf_xyzbxbybz,i,tolerances,robot1)
        pose_err = pose_err + pse_err_jt;
        prev_config = prev_config + joints;
    end
end