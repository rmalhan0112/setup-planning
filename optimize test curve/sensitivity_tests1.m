% SA for the continuity files.
close all;
clear all;
clc;
% Format of Handler
% translation: [3�1 double]
% quat: [4�1 double]
% reachability: 16
% strt_end_cont: [3�1 double]
% end_strt_cont: [3�1 double]
% pt_data: [115�12 double]


foldername = 'Sample Data Reach';
resultsfile = 'Reach results';


% Reachability
list = [];
reach = [];
for i=1:1:2000
    file = fullfile( foldername, strcat('sample_', num2str(i),'.mat') );
    s = load(file);
    handler = s.sample_data{1};
    reach = [reach; [handler.reachability, sum(handler.pt_data(:,end))]];
    diff_reac_cont = handler.reachability-handler.strt_end_cont(1);
%     CN = [];
%     for cnt = 1:size(handler.pt_data,1)
%         if handler.pt_data(cnt,1)==1
%             CN = [CN; handler.pt_data(cnt,end)];
%         end
%     end
%     if size(CN,1)==0
%         min_CN = 100;
%     else
%         min_CN = min(CN);
%     end
    list = [list; [handler.reachability, handler.strt_end_cont(1), handler.translation',...
                    handler.quat', (sum(handler.pt_data(:,10))+sum(handler.pt_data(:,11))) ] ];
end

list = sortrows(list,1);
reach = sortrows(reach,1);
figure(1)
hold on;
scatter(list(1000:end,1),list(1000:end,end),20,'b','filled');
% scatter( reach(:,1),reach(:,2),20,'b','filled');
xlabel('Reachability');
ylabel('Summation Pose Error');
title('Pose Error vs Reachability');