#include "mex.h"
#include "matrix.h"
#include "FK.hpp"
#include <stdio.h>
#include <math.h>  
#include <stdlib.h>
#include <typeinfo>
#include <iostream>
#include <array>

using namespace std;

void mexFunction( int _no_op_args,  mxArray *FF__Base[],
                        int _no_ip_args, const mxArray **theta_baseT )
{
    double *ip_data_1 = NULL;
    ip_data_1 = mxGetDoubles( theta_baseT[0] );
    
    double *ip_data_2 = NULL;
    ip_data_2 = mxGetDoubles( theta_baseT[1] );
    
    double thetas[7][1];
    for (int i=0; i<7; i++)
    {
        thetas[i][0] = (double) ip_data_1[i];
    }
    double Base_T[4][4];
    for (int i=0; i<4; i++)
    {
        for (int j=0; j<4; j++)
        {
            Base_T[i][j] = (double) ip_data_2[ 4*i + j ];
        }
    }
    
    FF__Base[0] = mxCreateNumericMatrix(4, 4, mxDOUBLE_CLASS, mxREAL);
    iiwa_FK_symbolic_WE( thetas, Base_T );
//     FF__Base[0] = iiwa_FK_symbolic_WE
//     mxComplexity  *is_complex;
//     is_complex = 0;
//     FF__Base[0] = mxCreateDoubleMatrix( 7, 1, is_complex[0] );
//     for (int i=0; i<7; i++)
//     {
//         op_data[i][0] = thetas[i];
//     }
    
//     double c = a + b;
    
//     if (c > 5)
//     {
//         mexErrMsgIdAndTxt( "MyToolbox:iiwa_FK_gateway:s","test");
//     }
    
// // Make sure that number of input arguments are correct
//     if ( _no_ip_args != 2 )
//     {
//          mexErrMsgIdAndTxt("MyToolbox:iiwa_FK_gateway:_no_ip_args","Insufficient Inputs.");
//     }
//     
// // Make sure that number of output arguments the C function is sending are correct
//     if( _no_op_args != 1 ) 
//     {
//         mexErrMsgIdAndTxt("MyToolbox:iiwa_FK_gateway:_no_op_args", "Insufficient Output. Check function.");
//     }
//     
// // Check to see that we received a 7x1 matrix as thetas
//     const int *theta_size = mxGetDimensions( theta_baseT[0] );
//     int rows;
//     int cols;
//     rows = *theta_size;
//     cols = *(theta_size+1);
//     if ( rows != 7 && cols != 1 )
//     {
//         mexErrMsgIdAndTxt("MyToolbox:iiwa_FK_gateway:theta_baseT","Size of joint angles not as expected. Must be 7x1 vector");
//     }
//     
// // Check dimensions of the Base_T matrix
//     const int *base_t_size = mxGetDimensions( theta_baseT[1] );
//     rows = *base_t_size;
//     cols = *(base_t_size+1);
//     if ( rows != 4 && cols != 4 )
//     {
//         mexErrMsgIdAndTxt("MyToolbox:iiwa_FK_gateway:theta_baseT","Size of Base_T not as expected. Must be 4x4 matrix");
//     }
    
//     FK( _theta, _Base_T );
    
}



