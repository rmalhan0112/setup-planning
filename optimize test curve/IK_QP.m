function F = IK_QP(prev_config,transf_xyzbxbybz,i,tolerances,robot1,J)
% loop through each point
    tol_xyz = tolerances(i,1);
    tol_alpha = tolerances(i,2);

% represent the point on the part w.r.t. robot-base
    pointi = transf_xyzbxbybz(i,1:3)';
    pbz = transf_xyzbxbybz(i,10:12)';
    
    % ROBOT end-effector
    all_transf_mat = robot1.fwd_kin_all_joints(prev_config);
    b1_T_ree = all_transf_mat{end}; % eff transf    
    b1_T_tee = b1_T_ree*robot1.robot_ree_T_tee;

    tool_xyz = b1_T_tee(1:3,4);
    toolZ = b1_T_tee(1:3,1:3) * [0;0;1]; toolZ = toolZ/norm(toolZ);

    % Error Without Tolerances
    error = tool_xyz-pointi;
    error_nz = abs( acos(dot(pbz, toolZ)) );
    pose_err(1,1) = erf_theta + exp( abs(error(1))-tol_xyz/3 );
    pose_err(2,1) = erf_theta + exp( abs(error(2))-tol_xyz/3 );
    pose_err(3,1) = erf_theta + exp( abs(error(3))-tol_xyz/3 );
    pose_err(4,1) = exp( (error_nz-tol_alpha) /pi );
    pose_err(5:6,1) = 0;
    F = (-2*J'*pose_err)';
end