close all;

folder = 'Execution Trials';
progression = csvread( fullfile(folder, 'Progression.csv' ) );
count = 0;
figure(1)
hold on;
figure(2)
hold on;

reachabilities = [];
errors = [];
counter = [];

for i=1:size(progression,2)
    count = count + 1;
    curr_data = progression(:,i);
    if curr_data(end) == 1
        count = count - 1;
    end
    counter = [counter; count];
    reachabilities = [reachabilities; curr_data( 8,i )];
    errors = [errors; curr_data( 9,i )];
end

figure(1)
plot( counter, reachabilities, 'r', 'linewidth', 2 );

figure(2)
plot( counter, errors, 'b', 'linewidth', 2 );
