#include <vector>
#include <thread>
#include <iostream>
#include <random>
#include <ctime>

#define MAX(a,b) (a > b ? a : b)

int main(int argc, char *argv[]) {
  	

  	double r1,r2;
	clock_t strt;
	double duration;
	unsigned long int N = 1000000000;

	const size_t nthreads = std::thread::hardware_concurrency();
	  {
	    // Pre loop
	    std::cout<<"parallel ("<<nthreads<<" threads):"<<std::endl;
	    std::vector<std::thread> threads(nthreads);
	    std::mutex critical;
	    for(int t = 0; t<nthreads; t++)
	    {
	      threads[t] = std::thread(std::bind(
	        [&](const int bi, const int ei, const int t)
	        {
	          // loop over all items
	          for(int i = bi;i<ei;i++)
	          {
	            // inner loop
	            {
	              const int j = i*i;
	              // (optional) make output critical
	              std::lock_guard<std::mutex> lock(critical);
	              std::cout<<j<<std::endl;
	            }
	          }
	        },t*nloop/nthreads,(t+1)==nthreads?nloop:(t+1)*nloop/nthreads,t));
	    }
	    std::for_each(threads.begin(),threads.end(),[](std::thread& x){x.join();});
	    // Post loop
	    std::cout<<std::endl;
	  }

	strt = std::clock();
  	for (int i = 0; i < N; i++ ) {
	  	r1 = ((double) rand()/(RAND_MAX));
		r2 = ((double) rand()/(RAND_MAX));
		MAX(r1,r2);	
  	}
	
	duration = ( std::clock() - strt ) / (double) CLOCKS_PER_SEC;
	std::cout << duration << std::endl;

	return 0; 
}
