function F = QP_gradient(prev_config,transf_xyzbxbybz,R_total,i,robot1,J)
% loop through each point

% represent the point on the part w.r.t. robot-base
    pointi = transf_xyzbxbybz(i,1:3)';
    pbx = transf_xyzbxbybz(i,4:6)';
    pby = transf_xyzbxbybz(i,7:9)';
    pbz = transf_xyzbxbybz(i,10:12)';
    
    % ROBOT end-effector
    all_transf_mat = robot1.fwd_kin_all_joints(prev_config);
    b1_T_ree = all_transf_mat{end}; % eff transf    
    b1_T_tee = b1_T_ree*robot1.robot_ree_T_tee;
    tool_xyz = b1_T_tee(1:3,4);

    R_nxtpt = R_total{i};
    eul1 = rotm2eul(b1_T_tee(1:3,1:3),'XYZ');
    eul2 = rotm2eul(R_nxtpt,'XYZ');
    
    % Error Without Tolerances
    error = pointi-tool_xyz;
    pose_err(1,1) = error(1);
    pose_err(2,1) = error(2);
    pose_err(3,1) = error(3);
    pose_err(4,1) = angdiff(eul2(1),eul1(1));
    pose_err(5,1) = angdiff(eul2(2),eul1(2));
    pose_err(6,1) = angdiff(eul2(3),eul1(3));
    F = (-2*(J'*pose_err))';
end