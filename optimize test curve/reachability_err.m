function [erf_theta] = reachability_err(theta,transf_xyzbxbybz,i,robot1)    
% represent the point on the part w.r.t. robot-base
    pointi = transf_xyzbxbybz(i,1:3)';
%     pbx = transf_xyzbxbybz(i,4:6)';
%     pby = transf_xyzbxbybz(i,7:9)';
%     pbz = transf_xyzbxbybz(i,10:12)';
%     rot_pts = [ pbx(1),  pby(1),  pbz(1);
%                 pbx(2),  pby(2),  pbz(2);
%                 pbx(3),  pby(3),  pbz(3)   ];
    
    % ROBOT end-effector
%     all_transf_mat = robot1.fwd_kin_all_joints(theta);
    b1_T_ree = iiwa_FK_symbolic_WE(theta,eye(4)); % eff transf
%     b1_T_ree = all_transf_mat{end};
    b1_T_tee = b1_T_ree*robot1.robot_ree_T_tee;
    b1_T_tee = b1_T_ree;
    
    
    tool_xyz = b1_T_tee(1:3,4);
    
    % Error Without Tolerances
    error_xyz = norm( pointi-tool_xyz );   
    
    erf_theta = 0;
    % Adding errors only if outside tolerance bounds.
    erf_theta = erf_theta + 10^( error_xyz-0.002 );
    
    
    
    
    
% If redundant angle present
%     euls_trgt = rotm2eul( rot_pts, 'XYZ' );
%     euls_tool = rotm2eul( b1_T_tee(1:3,1:3), 'XYZ' );
%     Rd = eul2rotm( [euls_trgt(1:2),0],'XYZ' );
%     R = eul2rotm( [euls_tool(1:2),0],'XYZ' );
%     v = rotm2axang( Rd*R' );
    
% If redundant angle absent
%     v = rotm2axang( rot_pts*b1_T_tee(1:3,1:3)' );
    
% Error Function. Donot Change
%     err_orientation = v(4)*v(1:3)';
%     tool_xyz = b1_T_tee(1:3,4);
%     err_pose = pointi-tool_xyz;
% %     err = [ err_pose;err_orientation ];
%     err = [ err_pose;[0;0;0] ];
%     erf_theta = 0.5*(  err'*diag([1,1,1,0.25,0.25,0.25])*err  ); 
%     
%     Jac = -(robot1.get_jacobian( theta ));
%     grad = (err'*diag([1,1,1,0.25,0.25,0.25])*Jac)';
end







%     erf_theta = 0;
    
% loop through each point
%     tol_xyz = tolerances(i,1);
%     tol_alpha = tolerances(i,2);
%     tol_beta = tolerances(i,3);
%     tol_gamma = tolerances(i,4);






%     tool_xyz = b1_T_tee(1:3,4);
%     toolX = b1_T_tee(1:3,1:3) * [1;0;0]; %toolX = toolX/norm(toolX);
%     toolY = b1_T_tee(1:3,1:3) * [0;1;0]; %toolY = toolY/norm(toolY);
%     toolZ = b1_T_tee(1:3,1:3) * [0;0;1]; %toolZ = toolZ/norm(toolZ);
%     
%     % Error Without Tolerances
%     error_xyz = norm( pointi-tool_xyz );   
%     error_nx = abs( acos(dot(pbx, toolX)) );
%     error_ny = abs( acos(dot(pby, toolY)) );
%     error_nz = abs( acos(dot(pbz, toolZ)) );
% % 
%     % Adding errors only if outside tolerance bounds.
%     erf_theta = erf_theta + 10^( error_xyz-tol_xyz );
%     erf_theta = erf_theta + 10^( (error_nx-tol_beta) /pi );
%     erf_theta = erf_theta + 10^( (error_ny-tol_gamma) /pi );
%     erf_theta = erf_theta + 10^( (error_nz-tol_alpha) /pi );






%     points = CONFIG.transf_xyzbxbybz(:,1:3);
%     bx = CONFIG.transf_xyzbxbybz(:,4:6);
%     by = CONFIG.transf_xyzbxbybz(:,7:9);
%     bz = CONFIG.transf_xyzbxbybz(:,10:12);
%     figure(1)
%     hold on;
%     part = 'HS.STL';
%     [v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
%     v_part = v_part./1000;
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,CONFIG.robot_T_part);
%     part_plt(v_part_transf, f_part, name_part, [0.5,0.6,0.8]); % plot tool stl
%     hold on;
%     pause(1.5);
%     plot_robot([0;0;0;-pi/2;0;pi/2;0],robot1);
%     a = quiver3(points(:,1),points(:,2),points(:,3),...
%            bx(:,1),bx(:,2),bx(:,3),'r','linewidth',2,'AutoScaleFactor',0.5);
%        hold on;
%     b = quiver3(points(:,1),points(:,2),points(:,3),...
%            by(:,1),by(:,2),by(:,3),'g','linewidth',2,'AutoScaleFactor',0.5);
%        hold on;
%     c = quiver3(points(:,1),points(:,2),points(:,3),...
%            bz(:,1),bz(:,2),bz(:,3),'b','linewidth',2,'AutoScaleFactor',0.5);
%     daspect([1,1,1]);