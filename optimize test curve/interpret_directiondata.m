close all;

xyz_step = [-0.08:0.01:0.08]';
q_step = [-0.2:0.025:0.2]';
folder = 'Obj behavior';

for cnt=1:4
    c_f1 = figure(1);
    title('Reachability');
    daspect([1,1,1]);
    set(c_f1,'units','normalized','outerposition',[0 0 1 1])
    
    c_f2 = figure(2);
    title('Pose Error');
    daspect([1,1,1]);
    set(c_f2,'units','normalized','outerposition',[0 0 1 1])
    
    for i=1:6
        figure(1);
        subplot(2,3,i);
        xtickangle(90)
        hold on;
        curr_data = csvread( fullfile(folder,strcat('Pose_',num2str(cnt),'.csv') ) );
        data = curr_data( 9*(i-1)+1:9*i,: );
        
        if i<4
            plot( xyz_step, data(9,:), 'r', 'linewidth', 2 );
            plot( [xyz_step(1),xyz_step(end)],[data(9,9),data(9,9)], 'k', 'linewidth', 2 );
%             plot( xyz_step, data(8,:), 'b', 'linewidth', 2 );
%             plot( [xyz_step(1),xyz_step(end)],[data(8,9),data(8,9)], 'k', 'linewidth', 2 );
            set(gca, 'XTick', xyz_step)
        else
            plot( q_step, data(9,:), 'r', 'linewidth', 2 );
            plot( [q_step(1),q_step(end)],[data(9,9),data(9,9)], 'k', 'linewidth', 2 );
%             plot( q_step, data(8,:), 'b', 'linewidth', 2 );
%             plot( [q_step(1),q_step(end)],[data(8,9),data(8,9)], 'k', 'linewidth', 2 );
            set(gca, 'XTick', q_step)
        end
        
        % Pose Error
        figure(2);
        subplot(2,3,i);
        xtickangle(90)
        hold on;
        curr_data = csvread( fullfile(folder,strcat('Pose_',num2str(cnt),'.csv') ) );
        data = curr_data( 9*(i-1)+1:9*i,: );
        
        if i<4
            plot( xyz_step, data(8,:), 'b', 'linewidth', 2 );
            plot( [xyz_step(1),xyz_step(end)],[data(8,9),data(8,9)], 'k', 'linewidth', 2 );
            set(gca, 'XTick', xyz_step)
        else
            plot( q_step, data(8,:), 'b', 'linewidth', 2 );
            plot( [q_step(1),q_step(end)],[data(8,9),data(8,9)], 'k', 'linewidth', 2 );
            set(gca, 'XTick', q_step)
        end
    end
    saveas(c_f1, fullfile(folder,strcat('Reachability_',num2str(cnt),'.png')) );
    saveas(c_f2, fullfile(folder,strcat('Pose_Error_',num2str(cnt),'.png')) );
    delete(c_f1);
    delete(c_f2);
end


% function plot_prt(R_to_Part)
%     part = 'HS.STL';
%     % Plot the Transformed Part
%     [v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
%     v_part = v_part./1000;
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,R_to_Part); 
%     part_plt(v_part_transf, f_part, name_part, [0.2,0,0]); % plot tool stl
% end