#define __random_gen_hpp__
#ifdef __random_gen_hpp__
#include <random>
#include <iostream>

void random_gen( double* thetas )
{
    double r1,r2,r3,r4,r5,r6,r7;
    
    r1 = ((double) rand()/(RAND_MAX));
    r2 = ((double) rand()/(RAND_MAX));
    r3 = ((double) rand()/(RAND_MAX));
    r4 = ((double) rand()/(RAND_MAX));
    r5 = ((double) rand()/(RAND_MAX));
    r6 = ((double) rand()/(RAND_MAX));
    r7 = ((double) rand()/(RAND_MAX));
    
    thetas[0] =  -2.967059728390360 + (r1 * 5.9341194567807);
    thetas[1] =  -2.094395102393195 + (r2 * 4.188790204786390);
    thetas[2] =  -2.967059728390360 + (r3 * 5.9341194567807);
    thetas[3] =  -2.094395102393195 + (r4 * 4.188790204786390);
    thetas[4] =  -2.967059728390360 + (r5 * 5.9341194567807);
    thetas[5] =  -2.094395102393195 + (r6 * 4.188790204786390);
    thetas[6] =  -3.054326190990077 + (r7 * 6.108652381980);
}
#endif