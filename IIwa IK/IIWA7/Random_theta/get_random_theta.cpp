#include "random_gen.hpp"
#include "mex.h"
#include "matrix.h"
#include "string.h"

void mexFunction (int _no_op_args, mxArray *FF__Base[], int _no_ip_args, const mxArray *b_ip[] )
{
    // I/O variables
    mxDouble theta[7];
        
    // process
    random_gen( theta );
    
    // output
    FF__Base[0] = mxCreateDoubleMatrix( 7, 1, mxREAL );    
    memcpy(mxGetPr(FF__Base[0]), theta, 7 * sizeof(double));
    
    return;
    
}