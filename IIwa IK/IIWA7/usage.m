%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 3000;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-6;
options.StepTolerance = 1e-8;
options.Display = 'off';
options.SpecifyObjectiveGradient = true;

tolerance(1) = 0.002;
tolerance(2) = 0.2618;

% xyz_bxbybz:  1 x 12 vector
% seed: 7 x 1 vector

[joint_config,status] = get_iiwa_IK( seed,xyz_bxbybz,tolerance,...
                                    options, theta_lb, theta_ub,true,3 );   
                                % 3 specifies, 1 iteration for given seed and 2 iterations for random restart
if status
    joint_angles = [joint_angles, joint_config ];
end
