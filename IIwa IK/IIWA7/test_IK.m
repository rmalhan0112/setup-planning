clear all;
clc;
close all;

%%%%%%%% Draping robot AND TOOL DEFINITION %%%%%%%%%%%%%
robot.base_T_robot = eye(4);
robot.base_T_robot(1:3,4) = [0;0;0];
robot.base_T_robot(1:3,1:3) = eul2rotm([0.0,0,0]);

robot.robot_ree_T_tee = eye(4);
% robot_ree_T_tee(1:3,4) = [0; 0; 0]; % For Gripper
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% Optimizer Bounds
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = -theta_lb;

% IKoptions = optimoptions('fmincon','Algorithm', 'interior-point');
IKoptions = optimoptions('fmincon','Algorithm', 'sqp');
IKoptions.MaxIterations = 5000;
IKoptions.MaxFunctionEvaluations = 1e7;
IKoptions.OptimalityTolerance = 1e-8;
IKoptions.StepTolerance = 1e-8;
IKoptions.Display = 'off';
IKoptions.SpecifyObjectiveGradient = false;
IKoptions.ObjectiveLimit = 1e-8;


I = eye(4);
init_guess = [ 0;0;0;0;0;0;0 ];
tolerance(1) = 0.003;
tolerance(2) = 0.0524;
tolerance(3) = 0.0524;
tolerance(4) = 0.0524;

sol = 0;
counter = 0;
tot_no = 100;
tot_time = 0;

for i = 1:tot_no
    theta = get_random_theta();
    T = get_iiwa_FK_mex( theta,I );
    point = [ T(1:3,4)',T(1:3,1)',T(1:3,2)',T(1:3,3)' ];
%     tic;
    [joints,status] = get_iiwa_IK( init_guess,point,tolerance,IKoptions,theta_lb,theta_ub,robot.robot_ree_T_tee );
%     tot_time = tot_time + toc;
    if status
        sol =  sol + 1;
%         figure(1);
%         scatter3( point(1),point(2),point(3),30,'g','filled' )
%         quiver3(point(1),point(2),point(3),point(10),point(11),point(12),'b','AutoScaleFactor',0.5)
%         daspect([1,1,1]);
%         bck_color = [ 0.2,0.2,0.2 ];
%         set(gcf,'color',bck_color);
%         set(gca,'color',bck_color);
%         camlight; camlight;
%         h = plot_robot(joints,false,robot);
%         
%         axis off

%     else
%         disp('Bug')
%         theta'
%         break;
    end
end

fprintf( 'Solved for %d percent points\n', sol/tot_no*100 );
% fprintf('Time per IK:  %f\n', tot_time / tot_no );