clear all;
close all;
clc;

curr_dir = pwd;
fk_path = fullfile( curr_dir,'FK' );
fkAK_path = fullfile( curr_dir,'FK_ak' );
jac_path = fullfile( curr_dir,'Jacobian' );
random_path = fullfile( curr_dir,'Random_theta' );

disp('Compiling dependencies for IK solver.......')

% Mex FK all
cd( fk_path );
mex -R2018a get_iiwa_FK_all_joints_mex.cpp
disp('Forward Kinematics All Compiled')

% Mex FK
cd( fkAK_path );
mex -R2018a get_iiwa_FK_mex.cpp
disp('Forward Kinematics Compiled')


% Mex Jacobian
cd( jac_path )
mex -R2018a get_iiwa_GeoJac.cpp
disp('Geometric Jacobian Compiled')


% Mex random generator
cd( random_path )
mex -R2018a get_random_theta.cpp
disp('Random Generator Compiled')


cd( curr_dir )
