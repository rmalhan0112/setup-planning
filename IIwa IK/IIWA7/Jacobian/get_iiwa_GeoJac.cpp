#include "compute_GeoJac.hpp"

#include "mex.h"
#include "matrix.h"
#include <stdio.h>
#include <string.h>

void mexFunction (int _no_op_args, mxArray *FF__Base[], int _no_ip_args, const mxArray *b_ip[] )
{
    // I/O variables
    double* FK_all_jts;
    mxDouble Jac[42];

    // input
    FK_all_jts = mxGetDoubles( b_ip[0] );
    
    // process
    compute_GeoJac( FK_all_jts, Jac );
    
    // output
    FF__Base[0] = mxCreateDoubleMatrix( 6, 7, mxREAL );    
    memcpy(mxGetPr(FF__Base[0]), Jac, 6 * 7 * sizeof(double));
    
    return;
    
}