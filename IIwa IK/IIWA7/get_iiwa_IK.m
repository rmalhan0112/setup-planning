function [joints,status] = get_iiwa_IK( init_guess,point,tolerance,options,theta_lb,theta_ub,transformation )
    
% Input:
% init_guess: Seed 7x1 vector

% point: x,y,z,bx1,bx2,bx3,by1,by2,by3,bz1,bz2,bz3 i.e 1 x 12 vector

% tolerance: tolerance in euclidean distance and orientation angle in
% radians 1 x 2 vector

% options for fmincon

% theta lb and ub

% Attempt IK with init_guess.
% Singularity and collision constraints on robot.
%         [joints] = fmincon(@(theta)compute_redundancy_cost(theta,point,transformation),init_guess,...
%                                     [],[],[],[],theta_lb,theta_ub,[],options);
        [joints] = fmincon(@(theta)compute_redundancy_cost(theta,point,transformation),init_guess,...
                                    [],[],[],[],theta_lb,theta_ub,@(theta) redundancyZ_constraint(theta, point, transformation, tolerance),options);

%         [joints] = fmincon(@(theta)compute_cost(theta,point,transformation),init_guess,...
%                                     [],[],[],[],theta_lb,theta_ub,[],options);
        
% Verify the Solution
        ee_base = get_iiwa_FK_mex( joints,eye(4) );
        ee_base = ee_base * transformation; % Apply transformation for the end-effector
% Error Position
        err_xyz = norm(point(1:3)'-ee_base(1:3,4));
% Error Orientation
%         err_bx = acos( point(4)*ee_base(1,1) + point(5)*ee_base(2,1) + point(6)*ee_base(3,1) );
%         err_by = acos( point(7)*ee_base(1,2) + point(8)*ee_base(2,2) + point(9)*ee_base(3,2) );
        err_bz = acos( point(10)*ee_base(1,3) + point(11)*ee_base(2,3) + point(12)*ee_base(3,3) );
%         if err_xyz > tolerance(1) || err_bx > tolerance(2) || err_by > tolerance(3) || err_bz > tolerance(4)
        if err_xyz > tolerance(1) || err_bz > tolerance(4) + 1e-4
            status = false;
        else
            status = true;
        end     
end