function error = compute_redundancy_cost( theta, point,transformation )
% represent the point on the part w.r.t. robot-base
    pointi = point(1:3)';
         
% ROBOT end-effector
    ee_base_all = get_iiwa_FK_all_joints_mex( theta,eye(4) ); % eff transf
    transf_mat = ee_base_all(33:36,:) * transformation; % For aligning tool TCP
%     transf_mat = ee_base_all(33:36,:); % uncomment out if aligning flange
    tool_xyz = transf_mat(1:3,4);
    
% Error Function. Donot Change
%     err_orientation = 1 - ( point(10)*transf_mat(1,3) + point(11)*transf_mat(2,3) + point(12)*transf_mat(3,3) );
    err_orientation = 1 - ( point(7)*transf_mat(1,2) + point(8)*transf_mat(2,2) + point(9)*transf_mat(3,2) );
    err_pose = pointi-tool_xyz;
    error = 0.5*(  0.25*err_orientation^2 + err_pose(1)^2 + err_pose(2)^2 + err_pose(3)^2  );
%     error = err_orientation + norm(err_pose);
end