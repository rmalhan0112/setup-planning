function error = compute_cost( theta, point, transformation )         
    FK_all = get_iiwa_FK_all_joints_mex( theta,eye(4) ); % flange transf
    transf_mat = FK_all(33:36,:) * transformation; % For attaching tool and getting IK for tool
  
    err_ori = [ 1 - point(4:6) * transf_mat(1:3,1);
                1 - point(7:9) * transf_mat(1:3,2);
                1 - point(10:12) * transf_mat(1:3,3)  ];
    err_pos = point(1:3)' - transf_mat(1:3,4);
    error = 0.5 * ( 0.25*(err_ori(1)^2+err_ori(2)^2+err_ori(3)^2) +... 
                    err_pos(1)^2 + err_pos(2)^2 + err_pos(3)^2 );
    
%     Jac = -(get_iiwa_GeoJac( FK_all ));
%     gradient = ([err_ori;err_pos]' * diag([0.25,0.25,0.25,1,1,1]) * Jac)';
end