function [c,ceq] = redundancyZ_constraint(theta,xyz_bxbybz,ff_T_ee,tolerance)
    ceq = [];
    
    FK_all = get_iiwa_FK_all_joints_mex(theta,eye(4));
    ff_base = FK_all(33:36,:) * ff_T_ee;
    % Z-Orientation Error
    angle = real (acos( xyz_bxbybz(10)*ff_base(1,3) +...
        xyz_bxbybz(11)*ff_base(2,3) + xyz_bxbybz(12)*ff_base(3,3) ) );
    c = angle - tolerance(4);
end