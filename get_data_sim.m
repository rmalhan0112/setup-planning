% % No orientation
% t = [0.7750,   -0.2120,    0.0467]';
% q = [ 0.8212,0,0,0.5707]';
% 
% % No continuity
% t = [0.657, 0.2201, 0.0467]';
% q = [0.6168, 0,0,0.7872]';
% 
% % No Position
% % t = [0.865, 0.57, 0.0467]';
% % q = [-0.49076, 0, 0, -0.87129]';
% 
% 
% % Solution
% t = [0.829, 0.217, 0.0467]';
% q = [0.5334, 0, 0, 0.8458]';

q = Xk(4:7);
t = Xk(1:3);

R = quat2rotm(q');

new_points = [];
for i=1:size(xyz_bxbybz,1)
    if ismember(i,failed_idx)
        new_points = [new_points; [ ((R*xyz_bxbybz(i,1:3)')' + t'),0 ] ];
    else
        new_points = [new_points; [ ((R*xyz_bxbybz(i,1:3)')' + t'),1 ] ];
    end
end

csvwrite('reachable.csv',new_points)


% Color Code:  0- Not reachable at all. 1-Cap Map reachable 2-IK reachable
% 3- Solution