%% Compute Transformation of Points and Euler Angles from part to Robot

function [points,bx,by,bz] = robot_to_part(points,bx,by,bz)
    xyz_abc = [];

% % Blade
%     part_points = [ 
%                     ];
% 
%     robot_points = [ 
%                     ];
%      [R,t] = point_pairs_transformation(part_points,robot_points);

    T = [ -0.9702972,  0.0000073,  0.2419160,  0.9271;
      -0.0000071, -1.0000000,  0.0000018, 0.5461;
       0.2419160,  0.0000000,  0.9702972, 0.0381;
       0, 0,0, 1 ];
   
    Rdash = T;
%      T = zeros(4,4);
%      T(1:3,1:3) = R;
%      T(1:3,4) = t;
%      T(4,4) = 1;
%      Rdash = T;
     Rdash(1:3,4) = [0;0;0];
     for i=1:size(points)
         homogeneous_pt = zeros(1,4);
         homogeneous_pt(1,1:3) = points(i,:);
         homogeneous_pt(1,4) = 1;
         transformed_pt = T*homogeneous_pt(1,:)';
         pt = transformed_pt';
         points(i,:) = pt(1,1:3);
     end
     
     if size(bx,1)~=0
         for i=1:size(bx)
             homogeneous_norm = zeros(1,4);
             homogeneous_norm(1,1:3) = bx(i,:);
             homogeneous_norm(1,4) = 1;
             transformed_norm = Rdash*homogeneous_norm(1,:)';
             norm = transformed_norm';
             bx(i,:) = norm(1,1:3);
         end
     end
     
     if size(by,1)~=0
         for i=1:size(by)
             homogeneous_norm = zeros(1,4);
             homogeneous_norm(1,1:3) = by(i,:);
             homogeneous_norm(1,4) = 1;
             transformed_norm = Rdash*homogeneous_norm(1,:)';
             norm = transformed_norm';
             by(i,:) = norm(1,1:3);
         end
     end
     
     if size(bz,1)~=0
         for i=1:size(bz)
             homogeneous_norm = zeros(1,4);
             homogeneous_norm(1,1:3) = bz(i,:);
             homogeneous_norm(1,4) = 1;
             transformed_norm = Rdash*homogeneous_norm(1,:)';
             norm = transformed_norm';
             bz(i,:) = norm(1,1:3);
         end
     end
     
    % This function computes the Rotation and Translation matrix. 
    function [R,T] = point_pairs_transformation(A,B)
        centroid_A = mean(A);
        centroid_B = mean(B);
        A = A - centroid_A;
        B = B - centroid_B;
        H = A'*B;
        [U,S,V] = svd(H);
        R = V*U';
        
        if det(R)<0
            V(3,:) = V(3,:) .* -1;
            R = V*U';
        end
        T = -R*centroid_A' + centroid_B';
    end
end