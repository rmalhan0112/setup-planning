#include "mex.h"
#include "class_handle.hpp"

#include <queue>
#include <tuple>
#include <cmath>

using namespace std;

typedef std::tuple<int, double, double> queue_entry;

// Custom comparator for queue_entry
const double TIE_BREAK_TOL = 0.05;
struct queue_entry_comparator
{
    inline bool operator() (const queue_entry &a, const queue_entry &b)
    {
        
//         const bool tie_break_needed = (get<2>(a) == get<2>(b));
        const double cost_diff = (get<2>(a) - get<2>(b));
        const bool tie_break_needed = false; //(std::fabs(cost_diff) < TIE_BREAK_TOL);

        if (true == tie_break_needed)
        {
            return std::get<1>(a) < get<1>(b);
        }
        else
        {
            return get<2>(a) > get<2>(b);  
        }
    }
};

// The class that we are interfacing to
class PriorityQueue
{
    priority_queue<queue_entry, std::vector<queue_entry>, queue_entry_comparator > pq;
public:
    void push(const int node_id, const double gcost, const double total_cost)
    {
        pq.push(queue_entry(node_id,gcost,total_cost));
    };
    double top()
    {
        const queue_entry top_entry = pq.top();
        return get<0>(top_entry); // return node_id
    };
    double top_value()
    {
        const queue_entry top_entry = pq.top();
        return get<2>(top_entry); // return node_id
    };
    void pop()
    {
        pq.pop();
    };
    double size()
    {
        return pq.size();
    }
private:
};

const int NEW_CMD_ID = 0;
const int DELETE_CMD_ID = 1;
const int PUSH_CMD_ID = 2;
const int POP_CMD_ID = 3;
const int TOP_CMD_ID = 4;
const int SIZE_CMD_ID = 5;
const int TOP_VALUE_CMD_ID = 6;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{	

	if (nrhs < 1)
    {
		mexErrMsgTxt("First input should be a command id");
    }
    // Get the command id
    const int cmd_id = (const int) mxGetScalar(prhs[0]);
    
    // New
    if ( NEW_CMD_ID == cmd_id ) {
        // Check parameters
        if (nlhs != 1)
            mexErrMsgTxt("New: One output expected.");
        // Return a handle to a new C++ instance
        plhs[0] = convertPtr2Mat<PriorityQueue>(new PriorityQueue);
        return;
    }
    
    // Check there is a second input, which should be the class instance handle
    if (nrhs < 2)
		mexErrMsgTxt("Second input should be a class instance handle.");
    
    // Delete
    if ( DELETE_CMD_ID == cmd_id ) {
        // Destroy the C++ object
        destroyObject<PriorityQueue>(prhs[1]);
        // Warn if other commands were ignored
        if (nlhs != 0 || nrhs != 2)
            mexWarnMsgTxt("Delete: Unexpected arguments ignored.");
        return;
    }
    
    // Get the class instance pointer from the second input
    PriorityQueue *pq_instance = convertMat2Ptr<PriorityQueue>(prhs[1]);
    
    // Call the various class methods
    // Train    
    if ( PUSH_CMD_ID == cmd_id ) {
        // Check parameters
        if (nlhs < 0 || nrhs < 5)
            mexErrMsgTxt("Push: Unexpected arguments.");
        const int node_id = mxGetScalar(prhs[2]);
        const double gcost = mxGetScalar(prhs[3]);
        const double total_cost = mxGetScalar(prhs[4]);
        // Call the method
//         mexWarnMsgTxt("PUSH");
        pq_instance->push(node_id,gcost,total_cost);
        return;
    }
    // Test    
    if ( POP_CMD_ID == cmd_id ) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("Pop: Unexpected arguments.");
        // Call the method
//         mexWarnMsgTxt("POP");
        pq_instance->pop();
        return;
    }
    if ( TOP_CMD_ID == cmd_id ) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("Top: Unexpected arguments.");
        // Call the method
//         mexWarnMsgTxt("TOP");
        const double value = pq_instance->top();
        if (nlhs == 1)
        {
            plhs[0] = mxCreateDoubleScalar(value);
        }
        return;
    }
    if ( SIZE_CMD_ID == cmd_id ) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("Size: Unexpected arguments.");
        // Call the method
//         mexWarnMsgTxt("SIZE");
        const double value = pq_instance->size();
        if (nlhs == 1)
        {
            plhs[0] = mxCreateDoubleScalar(value);
        }
        return;
    }
    if ( TOP_VALUE_CMD_ID == cmd_id ) {
        // Check parameters
        if (nlhs < 0 || nrhs < 2)
            mexErrMsgTxt("Top Value: Unexpected arguments.");
        // Call the method
//         mexWarnMsgTxt("TOP");
        const double value = pq_instance->top_value();
        if (nlhs == 1)
        {
            plhs[0] = mxCreateDoubleScalar(value);
        }
        return;
    }
    // Got here, so command not recognized
    mexErrMsgTxt("Command not recognized.");
}
