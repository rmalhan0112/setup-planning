classdef PriorityQueue < handle
    properties (Constant)
        NEW_CMD_ID = 0;
        DELETE_CMD_ID = 1;
        PUSH_CMD_ID = 2;
        POP_CMD_ID = 3;
        TOP_CMD_ID = 4;
        SIZE_CMD_ID = 5;
        TOP_VALUE_CMD_ID = 6;
    end
    properties (SetAccess = private, Hidden = true)
        objectHandle; % Handle to the underlying C++ class instance
    end
    methods
        %% Constructor - Create a new C++ class instance 
        function this = PriorityQueue(varargin)
            this.objectHandle = priority_queue_interface_mex(PriorityQueue.NEW_CMD_ID, varargin{:});
        end
        
        %% Destructor - Destroy the C++ class instance
        function delete(this)
            priority_queue_interface_mex(PriorityQueue.DELETE_CMD_ID, this.objectHandle);
        end

        %% Train - an example class method call
        function varargout = push(this, varargin)
            [varargout{1:nargout}] = priority_queue_interface_mex(PriorityQueue.PUSH_CMD_ID, this.objectHandle, varargin{:});
        end

        %% Test - another example class method call
        function varargout = pop(this, varargin)
            [varargout{1:nargout}] = priority_queue_interface_mex(PriorityQueue.POP_CMD_ID, this.objectHandle, varargin{:});
        end
        %% Test - another example class method call
        function varargout = top(this, varargin)
            [varargout{1:nargout}] = priority_queue_interface_mex(PriorityQueue.TOP_CMD_ID, this.objectHandle, varargin{:});
        end
        %% Test - another example class method call
        function varargout = size(this, varargin)
            [varargout{1:nargout}] = priority_queue_interface_mex(PriorityQueue.SIZE_CMD_ID, this.objectHandle, varargin{:});
        end
        %% Test - another example class method call
        function varargout = top_value(this, varargin)
            [varargout{1:nargout}] = priority_queue_interface_mex(PriorityQueue.TOP_VALUE_CMD_ID, this.objectHandle, varargin{:});
        end
    end
end