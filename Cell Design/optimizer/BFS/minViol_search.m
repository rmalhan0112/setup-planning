clear all;
close all;
clc;

part_name = 'Mold_Ascent';
% part_name = 'GE90';
% part_name = 'custom';
wich_robot = 2;

if isequal(part_name, 'Mold_Ascent')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/Mold Ascent';
elseif isequal(part_name, 'GE90')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/GE90';
elseif isequal(part_name, 'custom')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/Custom';
end


disp('Loading dependencies..........')
map_path = '/home/rmalhan/Work/USC/Composite Automation/Journal/Cap Maps';
load( fullfile( map_path, 'c_map.mat' ) );
load( fullfile( map_path, 'r_map.mat' ) );
disp('Files loaded. Initializing Optimization Parameters.......')


% creates a 3D space in which the robot will operate
run map_initialization.m;
run opt_config.m;


%%%%%%%% Draping robot AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.1]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


% Sample around the grasp cone axis
disp('Sampling within approach cone....')
axis = grasp_voxels(1,4:6);
angle = grasp_voxels(1,7);

no_samples = 500;
values = [-1:0.01:1];
counter = 1;
approach_vecs = [];
while counter < no_samples
    bx_dash = randsample(values,1);
    by_dash = randsample(values,1);
    bz_dash = randsample(values,1);
    vec = [bx_dash,by_dash,bz_dash];
    vec = vec / norm(vec);
    if acos( vec(1) * axis(1) + vec(2) * axis(2) + vec(3) * axis(3) ) > angle
        continue;
    end
    approach_vecs = [ approach_vecs; vec ];
    counter = counter + 1;
end



disp('Solving for Robot Placement Using BFS......')

visited_nodes = containers.Map( int32(1),true );
remove(visited_nodes,1);

% Running BFS
% Randomly Sample Using Capability Map
pose_viol = [];
for i=1:1000
    x = [ randsample(lb(1):0.01:ub(1),1), randsample(lb(2):0.01:ub(2),1), randsample(lb(3):0.01:ub(3),1) ];
    pose_viol = [pose_viol; x, volume_violations( x,grasp_voxels, c_map,r_map,approach_vecs ) ];
    if pose_viol(end,end) == 0
        break;
    end
end
pose_viol = sortrows(pose_viol,4);
x0 = pose_viol(1,1:3)';
queue = PriorityQueue();

counter = 1;
norm_val = (x0 - lb')./(ub-lb)';
key = int32(norm_val(1)*100*10000 + norm_val(2)*100*100 + norm_val(3)*100);
visited_nodes(key) = true;
node(counter) = states( x0,volume_violations( x0,grasp_voxels, c_map,r_map,approach_vecs ),counter );
queue.push( counter,node(counter).cost,0 );
counter = counter + 1;

fprintf('Initial Cost is:  %d\n',node(1).cost);

best_cost = node(1).cost;
best_pose = node(1).pose;
bounds = [lb',ub'];

while (queue.size()~=0) && best_cost > 2
%     if mod(counter,10)==0
        fprintf('Nodes Expanded: %d\n',counter);
%     end
    
    curr_id = queue.top();
    queue.pop();
    
    if node(curr_id).cost < best_cost
        best_pose = node(curr_id).pose;
        best_cost = node(curr_id).cost;
    end 
    
    act_states = action_states( node(curr_id).pose,bounds );
    
    for i = 1:size(act_states,2)
        norm_val = (act_states(:,i) - bounds(:,1))./(bounds(:,2)-bounds(:,1));
        key = int32(norm_val(1)*100*10000 + norm_val(2)*100*100 + norm_val(3)*100);
        % Check if the position is being revisited
        if isKey( visited_nodes,key )
            continue;
        else
            visited_nodes(key) = true;
            node(counter) = states( act_states(:,i),...
                volume_violations( act_states(:,i),grasp_voxels, c_map,r_map,approach_vecs ),counter );
            % Pushing the node and cost in the priority queue
            queue.push( counter,node(counter).cost,0 );
            counter = counter + 1;
        end
    end
    if queue.size()==0
        disp('State Space Exhausted...')
    end
end

fprintf('Best Pose is: \n');
best_pose
fprintf('Minimum Cost is: %d\n',best_cost);







% [pose,viol] = bfs( queue,node,visited_nodes,[lb',ub'],inf,x0,counter,...
%                     grasp_voxels, c_map,r_map,approach_vecs)
% 
% 
% function [pose,viol] = bfs( queue,node,visited_nodes,bounds,best_cost,best_pose,counter,...
%                             grasp_voxels, c_map,r_map,approach_vecs)
%     if mod(counter,10)==0
%         fprintf('Nodes Expanded: %d\n',counter);
%     end
%     if (queue.size()==0) || best_cost == 0
%         disp('Sending best found solution')
%         pose = best_pose;
%         viol = best_cost;
%         return;
%     end
%     curr_id = queue.top();
%     queue.pop();
%     
%     if node(curr_id).cost < best_cost
%         best_pose = node(curr_id).pose;
%         best_cost = node(curr_id).cost;
%     end
%     
%     node(curr_id).open = false;
%     act_states = action_states( node(curr_id).pose,bounds );
%     
%     for i = 1:size(act_states,2)
%         norm_val = (act_states(:,i) - bounds(:,1))./(bounds(:,2)-bounds(:,1));
%         key = int32(norm_val(1)*100*10000 + norm_val(2)*100*100 + norm_val(3)*100);
%         % Check if the position is being revisited
%         if isKey( visited_nodes,key )
%             continue;
%         else
%             visited_nodes(key) = true;
%             node(counter) = states( act_states(:,i),volume_violations( act_states(:,i),grasp_voxels, c_map,r_map,approach_vecs ),counter );
%             % Pushing the node and cost in the priority queue
%             queue.push( counter,node(counter).cost,0 );
%             counter = counter + 1;
%         end
%     end
%     [pose,viol] = bfs( queue,node,visited_nodes,bounds,best_cost,best_pose,counter,...
%                         grasp_voxels, c_map,r_map,approach_vecs);
% end


