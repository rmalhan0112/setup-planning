function states = action_states( curr_pose,bounds )
    step = 0.01;
    states = [];
    for i = 1:size(curr_pose,1)
        states = [ states, curr_pose ];
        states(i,end) = states(i,end) + step;
        % Eliminating the states if violates the bounds
        if any(states(:,end) - bounds(:,1) < 0) || any(bounds(:,2) - states(:,end) < 0)
            states(:,end) = [];
        end
        states = [ states, curr_pose ];
        states(i,end) = states(i,end) - step;
        % Eliminating the states if violates the bounds
        if any(states(:,end) - bounds(:,1) < 0) || any(bounds(:,2) - states(:,end) < 0)
            states(:,end) = [];
        end
    end
end