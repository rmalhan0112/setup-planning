classdef states < handle
    properties
        pose
        cost
%         open
%         is_visited
        node_id
    end
    methods
        function node = states( pose,cost,counter )
            node.pose = pose;
            node.cost = cost;
%             node.open = true;
%             node.is_visited = true;
            node.node_id = counter;
        end
    end
end
            