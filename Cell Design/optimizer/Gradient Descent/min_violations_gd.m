clear all;
% close all;
clc;

part_name = 'Mold_Ascent';
% part_name = 'GE90';
% part_name = 'custom';
wich_robot = 1;


stl_path = '/home/rmalhan/Work/USC/Composite Automation/Journal/Meshes';
iiwa_stls = '/home/rmalhan/Work/USC/Composite Automation/Journal/Meshes/iiwa7_stls';
path_to_maps = '/home/rmalhan/Work/USC/Composite Automation/Journal/Cap Maps';

if isequal(part_name, 'Mold_Ascent')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/Mold Ascent';
elseif isequal(part_name, 'GE90')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/GE90';
elseif isequal(part_name, 'custom')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/Custom';
end


disp('Loading dependencies..........')
map_path = '/home/rmalhan/Work/USC/Composite Automation/Journal/Cap Maps';
load( fullfile( map_path, 'c_map.mat' ) );
load( fullfile( map_path, 'r_map.mat' ) );
disp('Files loaded. Initializing Optimization Parameters.......')
run opt_config.m;
run ascent_init.m

%%%%%%%% Draping robot AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
robot1.base_T_robot = eye(4);
robot1.base_T_robot(1:3,4) = [0;0;0];
robot1.base_T_robot(1:3,1:3) = eul2rotm([0.0,0,0]);

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.137]; % For gripper tool
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


% Sample around the grasp cone axis
% Grasp Voxels are for the flange of robot.
% The transformation for tool is taken care of in grasp voxels
% The grasp voxels are wrt mold voxels in Ascent folder
% The grasp voxels are wrt origin translated in mat file
disp('Sampling within approach cone....')
axes = grasp_voxels(1,4:6);
angle = grasp_voxels(1,7);

no_samples = 50;
values = [-1:0.01:1];
counter = 1;
approach_vecs = [];
while counter < no_samples
    bx_dash = randsample(values,1);
    by_dash = randsample(values,1);
    bz_dash = randsample(values,1);
    vec = [bx_dash,by_dash,bz_dash];
    vec = vec / norm(vec);
    if acos( vec(1) * axes(1) + vec(2) * axes(2) + vec(3) * axes(3) ) > angle
        continue;
    end
    approach_vecs = [ approach_vecs; vec ];
    counter = counter + 1;
end



% disp('Solving for Robot Placement Using Gradient Descent......')
% 
% tic;
% pose_viol = [];
% for i=1:2000
%     x = [ randsample(-2:0.01:2,1); randsample(-2:0.01:2,1); 0; eul2quat( [randsample(-pi:0.01:pi,1),0,0],'ZYX' )' ];
%     pose_viol = [pose_viol; x', volume_violations( x,grasp_voxels, c_map,approach_vecs ) ];
%     if pose_viol(end,end) == 0
%         break;
%     end
% end
% pose_viol = sortrows(pose_viol,8);
% 
% pose_viol(1,end)
% pose_viol(1,1:end-1)
% toc;
% return;

% Testing gradient descent for images


%%%%%%%%%%%%%% ASCENT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Obtained from Sampling
X = [ 0.23;  0.72; 0.0467; eul2quat( [-1.24,0,0],'ZYX')' ]; % Use this
% Solution for ascent
% C map generated samples for ascent
% X = [0.6300    0.3300         0    0.5269         0         0   -0.8499]';
% X = [0.2100    0.7900         0    0.7986         0         0   -0.6018]';
% X = [-0.2400    0.5300         0    0.9799         0         0   -0.1994]';

% Bad Pose for Rviz plot i = 49
% X = [0.0200    0.1800         0    0.9992         0         0   -0.0408]';


% Use this for robot 2
% X = [1.5400   -0.2700         0    0.6183         0         0    0.7859]';
%%%%%%%%%%%%%% CUSTOM %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'sqp');
options.MaxIterations = 1500;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-8;
options.StepTolerance = 1e-8;
options.Display = 'off';
options.SpecifyObjectiveGradient = false;
options.ObjectiveLimit = 1e-8; 


% Define point and frame for grasping robot 1
no_vxls = size(grasp_voxels,1);
no_violations = no_vxls;

grasp_voxels(:,1:3) = grasp_voxels(:,1:3)./1000;
R = quat2rotm(X(4:7)');
t = X(1:3);
grasp_voxels(:,1:3) = (R*grasp_voxels(:,1:3)' + t)';
appr_vec = (R * approach_vecs(:,1:3)')';
 
% % Plotting Points on the Part
% part_name = 'Mold_Ascent.stl';
% [v_part, f_part, n_part, name_part] = stlRead(part_name); % read tool stl
% v_part(:,1:3) = v_part(:,1:3)./1000;
% figure(1);
% T = eye(4);
% T(1:3,4) = X(1:3);
% T(1:3,1:3) = quat2rotm( X(4:7)' ); 
% vxl_T_prt = eye(4);
% vxl_T_prt(1:3,4) = [ 0.2;0.2;0.04673 ];
% T = T * vxl_T_prt;
% [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,T);
% part_plt(v_part_transf, f_part, name_part, [0.64,0.64,0.64]); % plot tool stl
% scatter3( grasp_voxels(:,1),grasp_voxels(:,2),grasp_voxels(:,3),30,'g','filled' )
% daspect([1,1,1]);
% bck_color = [ 0.2,0.2,0.2 ];
% set(gcf,'color',bck_color);
% set(gca,'color',bck_color);
% camlight; camlight;
% axis off
    
tic;
for i = 1:no_vxls
%     all_apprch_reach = true;
%     for p=1:size(appr_vec,1)
    point(1,1:3) = grasp_voxels(i,1:3);

    bx = [1;0;0];
    bz = grasp_voxels(i,4:6)';
    by = cross(bz,bx);
    by = by / norm( by );
    bx = cross( by,bz );
    bx = bx / norm( bx );

    point( 1,4:12 ) = [ (R*bx)', (R*by)',  (R*bz)' ];
%     temp = robot1.robot_ree_T_tee \ [point(1,1:3)';1];
%     point(1,1:3) = temp(1:3)';

    point(1,4:6) = ( robot1.robot_ree_T_tee(1:3,1:3)' * point(1,4:6)' )';
    point(1,7:9) = ( robot1.robot_ree_T_tee(1:3,1:3)' * point(1,7:9)' )';
    point(1,10:12) = ( robot1.robot_ree_T_tee(1:3,1:3)' * point(1,10:12)' )';

    [joints,status] = get_iiwa_IK( [0;0;0;-pi/2;0;pi/2;0],...
        point,[0.003,0.0524,0.0524,0.0524],options,theta_lb,theta_ub,robot1.robot_ree_T_tee );
    
%     % Plot draping robot
%     h = plot_robot(joints,'robotiq.stl',robot1);
%     pause(0.0001)
%     for idx = 1:size(h,2)
%         delete( h{idx} );
%     end
    
    if status
        no_violations = no_violations - 1;
    end
%     if ~status
%             all_apprch_reach = false;
%             break;
%         end
%     end
%     if all_apprch_reach
%         no_violations = no_violations - 1;
%     end
end
toc;
no_violations

% steps = [steps; X'];
% 
% init_viol = volume_violations( X,grasp_voxels, c_map,approach_vecs );
% fprintf('Initial Violations:  %d\n',init_viol)
% least_viol = init_viol;
% while true
%     states = get_actions( X );
%     for i = 1:size(states,2)
%         viol = volume_violations( X,grasp_voxels, c_map,approach_vecs );
%         if viol <= least_viol
%             least_viol = viol;
%             idx = i;
%         end
%     end
%     X = states(:,idx);
%     least_viol
%     steps = [steps; X'];
%     if least_viol == 0
%         disp('Solution Found')
%         X
%         break;
%     end
% end
% 
% return;
% 
% 
% function states = get_actions( pose )
%     step = 0.02;
%     states = [];
%     for i = [1,2,4,7]
%         states = [ states, pose ];
%         states(i,end) = states(i,end) + step;
%         states(4:7,end) = states(4:7,end)/norm(states(4:7,end));
%         
%         states = [ states, pose ];
%         states(i,end) = states(i,end) - step;
%         states(4:7,end) = states(4:7,end)/norm(states(4:7,end));
%     end
% end


% Good initialization poses
% x1 = [ 0.23,  0.72,  -1.24 ];
% x2 = [ 1.18,  -0.52,  1.21 ];