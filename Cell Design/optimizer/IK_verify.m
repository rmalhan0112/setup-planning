clear all;
close all;
clc;

part_name = 'Mold_Ascent';
% part_name = 'GE90';
% part_name = 'custom';
wich_robot = 1;
sol1 = [ 0.23,  0.72,  -1.24 ];


if isequal(part_name, 'Mold_Ascent')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/Mold Ascent';
elseif isequal(part_name, 'GE90')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/GE90';
elseif isequal(part_name, 'custom')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/Custom';
end


disp('Loading dependencies..........')
run opt_config.m;

%%%%%%%% Draping robot AND TOOL DEFINITION %%%%%%%%%%%%%
robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

robot_ree_T_tee = eye(4);
robot_ree_T_tee(1:3,4) = [0; 0; 0.137]; % For Gripper
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


% Sample around the grasp cone axis
disp('Sampling within approach cone....')
axis = grasp_voxels(1,4:6);
angle = grasp_voxels(1,7);

no_samples = 100;
values = [-1:0.01:1];
counter = 1;
approach_vecs = [];
while counter < no_samples
    bx_dash = randsample(values,1);
    by_dash = randsample(values,1);
    bz_dash = randsample(values,1);
    vec = [bx_dash,by_dash,bz_dash];
    vec = vec / norm(vec);
    if acos( vec(1) * axis(1) + vec(2) * axis(2) + vec(3) * axis(3) ) > angle
        continue;
    end
    approach_vecs = [ approach_vecs; vec ];
    counter = counter + 1;
end

disp('Verifying Solution Using IK')

