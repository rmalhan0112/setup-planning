clear all;
close all;
clc;

is_plot = true;

addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/'))

part = 'Mold_Ascent.stl';
[v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl

disp('Initializing voxel parameters..........');
voxel_dim = 5;     % mm
mold_voxels.voxel_dim = voxel_dim;

x_max = max(v_part(:,1));
y_max = max(v_part(:,2));
z_max = max(v_part(:,3));

x_min = min(v_part(:,1));
y_min = min(v_part(:,2));
z_min = min(v_part(:,3));

% Defining the maximum integer extend to which voxels will be generated. In
% other words lengths of each of edges.
X_len = ceil(abs(x_max));
Y_len = ceil(abs(y_max));
Z_len = ceil(abs(z_max));
box_len = [ X_len,Y_len,Z_len ];
mold_voxels.box_len = box_len;

% Getting the number of voxels along each of the lengths.
no_x = ceil(X_len / voxel_dim);
no_y = ceil(Y_len / voxel_dim);
no_z = ceil(Z_len / voxel_dim);
no_vxls = [ no_x,no_y,no_z ];
mold_voxels.no_vxls = no_vxls;

% Defining the translation between the voxel origin and part origin.
origin_t_part = [0 0 0]; % This will be used to translate each and every point of part first.

% Plotting the part
if is_plot
    figure(1); hold on;
    daspect([1,1,1]);
    xlabel('X')
    ylabel('Y')
    zlabel('Z')
end

transform = eye(4);
transform(1:3,4) = origin_t_part';
[v_part_transf,n_part_transf] = stlTransform(v_part,n_part,transform);

mold_voxels.orig_trans = origin_t_part;

collision = zeros( no_x,no_y,no_z );

disp('Generating voxel keys..........');

[a, b, c] = ndgrid([0:1:no_x-1], [0:1:no_y-1], [0:1:no_z-1]);
indices = [a(:), b(:), c(:)] + 1;
cart_coord = [a(:), b(:), c(:)] * voxel_dim;
plot_voxels = cart_coord;

fprintf("\n")
disp('Checking for Collisions..........');
FV.vertices = v_part_transf;
FV.faces = f_part;

IN = inpolyhedron( FV,cart_coord);

coll_idx = find(IN==1);

disp('Updating the data structure..........');
points = cart_coord(coll_idx,:);

for i=1:size(coll_idx,1)    
    collision( indices(coll_idx(i),1)+1,...
                indices(coll_idx(i),2)+1,...
                indices(coll_idx(i),3)+1 ) = 1;
end


fprintf("\n")
disp('Plotting Results..........');

mold_voxels.collision = collision;


if is_plot
    view_stl(part,[],[],[0.4],[])
    scatter3( points(:,1),points(:,2),points(:,3),20,'r','filled' );
    daspect([1,1,1]);
end

save('voxels.mat','mold_voxels')
toc;


map = mold_voxels.collision;
edt_map = bwdistsc(map) - bwdistsc(imcomplement(map));

save('edt_map.mat','edt_map')

function voxel_coord = to_vxl_coord( point,no_vxls,box_len,voxel_dim )
    % Takes the point in the voxel origin.
    voxel_coord = [  floor(point(1)/box_len(1)*no_vxls(1)),...
                        floor(point(2)/box_len(2)*no_vxls(2)),...
                        floor(point(3)/box_len(3)*no_vxls(3)) ]*voxel_dim;
end


function key = to_string( x,y,z )
    key = x*10^6 + y*10^3 + z;
end