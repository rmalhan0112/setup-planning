function violations = chck_apprx_reach( X,red_vec,xyz_bxbybz,cap_map  )
    rotation = quat2rotm(X(4:7)');
% % Checking Positional Reachability
%     xyz_bxbybz(:,1:3) = (rotation * xyz_bxbybz(:,1:3)' + X(1:3))';
%     no_pts = size(xyz_bxbybz,1);
%     
%     for i = 1:no_pts
%         key = gen_cmap_key( xyz_bxbybz(i,1:3) );
%         if ~isKey( c_map,key )
%             violations = inf;
%             return;
%         end
%     end
        
% Checking orientation
%     violations = no_pts;
    violations = 0;
    for i = 1:size(red_vec,2)
        set_vecs = red_vec{i};
        % Applying Translation for current part position
        set_vecs(:,1:3) = (rotation * set_vecs(:,1:3)' + X(1:3))';
        set_vecs(:,4:6) = (rotation * set_vecs(:,4:6)')';
        
        % Major Assumption. Atleast one set_vec will be collision free
        for j = 1:size(set_vecs,1)
            if set_vecs(j,7)==1
                continue;
            end
            key = gen_cmap_key( set_vecs(j,1:3) );
            if isKey( cap_map,key )
                ori_cone = cap_map(key);
                angle = acos( ori_cone(1) * set_vecs(j,4) + ori_cone(2) * set_vecs(j,5) +...
                        ori_cone(3) * set_vecs(j,6) );
                if ~(angle >= ori_cone(4) && angle <= ori_cone(5))    
%                     violations = violations - 1;
%                     break;
                    violations = violations + 1;
                end
            else
                violations = violations + 1;
            end
        end
    end
end