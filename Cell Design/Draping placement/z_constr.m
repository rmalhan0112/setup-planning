function [c,ceq] = z_constr( theta, xyz_bxbybz, ff_T_ee, tolerance,velocity,motor_vel,theta_prev,frst_idx )
%     FK_all = get_iiwa_FK_all_joints_mex(theta,eye(4));  %iiwa7
    FK_all = get_iiwa14_FK_all_joints_mex(theta,eye(4));   % iiwa14
    w_T_ee = FK_all(33:36,:) * ff_T_ee;
    
    ceq = [];
    % Z-Orientation Error
    angle = real (acos( xyz_bxbybz(10)*w_T_ee(1,3) +...
        xyz_bxbybz(11)*w_T_ee(2,3) + xyz_bxbybz(12)*w_T_ee(3,3) ) );
    c = angle - tolerance(4);
    
    jac_curr = get_iiwa_GeoJac( FK_all );
        
    % Instantaneous Velocity Constraint
%     c = [c; abs(pinv(jac_curr) * velocity) - motor_vel];


    % Continuity Constraint
    if ~frst_idx
        FK_all_prev = get_iiwa_FK_all_joints_mex(theta_prev,eye(4));
        jac_prev = get_iiwa_GeoJac( FK_all_prev );
        c = [c; -corr2( jac_prev,jac_curr ) + 0.9];
    end
end