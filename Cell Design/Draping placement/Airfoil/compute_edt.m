clear all;
close all;
clc;


vxl_path = '/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Draping placement/Airfoil/';
load(fullfile(vxl_path,'mold_voxels.mat'))

map = mold_voxels.collision;
edt_map = bwdistsc(map) - bwdistsc(imcomplement(map));

save(fullfile(vxl_path,'edt_map.mat'),'edt_map')

disp('EDT computed. Plotting Color Map')

[a, b, c] = ndgrid([1:1:mold_voxels.no_vxls(1)], [1:1:mold_voxels.no_vxls(2)],...
            [1:1:mold_voxels.no_vxls(3)]);
coord = [a(:), b(:), c(:)];
indices = sub2ind(size(edt_map),coord(:,1),coord(:,2),coord(:,3));
vals = edt_map(indices);
scatter3( coord(:,1),coord(:,2),coord(:,3),20,vals,'filled' )
daspect([1,1,1])