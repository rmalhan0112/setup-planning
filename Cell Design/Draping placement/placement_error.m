function error = placement_error( X, X_fixed, xyz_bxbybz, grp_idx, ff_T_ee, tolerance,theta_lb,theta_ub,SimtrajOptions )
    X = [ X(1:2); X_fixed(1); X(3); X_fixed(2:3); X(4) ];
    
    % Simulate the trajectory for the given part pose and find error
%     no_pts = size(xyz_bxbybz,1);
    rotation = quat2rotm(X(4:7)');
    
    % Translating and rotating points
    xyz_bxbybz(:,1:3) = (rotation * xyz_bxbybz(:,1:3)' + X(1:3))';
    
    % Rotating Orientations
    xyz_bxbybz(:,4:6) = (rotation * xyz_bxbybz(:,4:6)')';
    xyz_bxbybz(:,7:9) = (rotation * xyz_bxbybz(:,7:9)')';
    xyz_bxbybz(:,10:12) = (rotation * xyz_bxbybz(:,10:12)')';
    
    
%     % Plotting Points on the Part
%     part_name = 'Mold_Ascent.stl';
%     [v_part, f_part, n_part, name_part] = stlRead(part_name); % read tool stl
%     v_part(:,1:3) = v_part(:,1:3)./1000;
%     figure(1);
%     T = eye(4);
%     T(1:3,4) = X(1:3)';
%     T(1:3,1:3) = quat2rotm( X(4:7)' ); 
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,T);
%     part_plt(v_part_transf, f_part, name_part, [0.64,0.64,0.64]); % plot tool stl
%     
%     scatter3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),20,'r','filled' );
%     quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),...
%             xyz_bxbybz(:,4),xyz_bxbybz(:,5),xyz_bxbybz(:,6),'r')
%     quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),...
%             xyz_bxbybz(:,7),xyz_bxbybz(:,8),xyz_bxbybz(:,9),'g')
%     quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),...
%         xyz_bxbybz(:,10),xyz_bxbybz(:,11),xyz_bxbybz(:,12),'b')
%     % bck_color = [0.1882,0.1882,0.1882];
%     bck_color = [ 0.2,0.2,0.2 ];
%     set(gcf,'color',bck_color);
%     set(gca,'color',bck_color);
% 
%     % Trials
%     joints1 = [0;0;0;-pi/2;0;pi/2;0];
%     % Plot draping robot
%     plot_robot(joints1,'roller.stl',ff_T_ee);
%     daspect([1,1,1]);

    
    % Initialize Copies of Manipulator
%     joint_angles = [];
    I = eye(4);
    error = 0;
    
    for i = 1:size(grp_idx,1)
        joint_angle = [0.31189,0.2209,-0.1785,-1.5357,0.0176,1.3463,0]';
        for j = grp_idx(i,1):grp_idx(i,2)
            % Finding the trajectory
            [joint_angle,f_val] = fmincon( @(theta) trajSim_error( theta, xyz_bxbybz(j,:),...
                ff_T_ee ),joint_angle, [],[],[],[],theta_lb,theta_ub,...
                @(theta) get_constraints(theta, xyz_bxbybz(j,:), ff_T_ee, tolerance), SimtrajOptions );
            
            error = error + f_val;
            
%             joint_angles = [joint_angles; joint_angle'];
%             ff_base = get_iiwa_FK_mex(joint_angle,I) * ff_T_ee;
%             % Position Error
%             if norm( xyz_bxbybz(j,1:3) - ff_base(1:3,4)' ) > tolerance(1)
%                 status = false;
%             end
%             % Y-Orientation Error
%             if acos( xyz_bxbybz(j,7)*ff_base(1,2) + ...
%                     xyz_bxbybz(j,8)*ff_base(2,2)+ xyz_bxbybz(j,9)*ff_base(3,2) ) > tolerance(2)
%                 status = false;
%             end
%             % Z-Orientation Error
%             angle = acos( xyz_bxbybz(j,10)*ff_base(1,3) +...
%                 xyz_bxbybz(j,11)*ff_base(2,3) + xyz_bxbybz(j,12)*ff_base(3,3) );
%             if angle - 1e-4 > tolerance(4)
%                 status = false;
%             end
        end
    end
%     csvwrite('ascent_trajTest.csv',joint_angles);
end

% [joint_angle,error] = fminunc( @(theta) trajSim_error( theta, xyz_bxbybz(j,:),...
%                 ff_T_ee, theta_lb,theta_ub, tolerance ),joint_angle, SimtrajOptions );
        
