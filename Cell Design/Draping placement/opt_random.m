clear all;
close all;
clc;

global status;
status = true;

addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/STL_plot/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/manipulator_planners/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/'));

plot_point = false;

% Load Dependencies
% Paths to Different Files
disp('Loading dependencies..........')
stl_path = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes';
iiwa_stls = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/iiwa7_stls';
path_to_maps = '/home/rmalhan/Work/USC/Composite Automation/Modules/Cap Maps';

disp('Initializing Robot Parameters.......')
run drape_robot_initialize.m
% run ascent_init.m
% run dome_init.m
run ge90_init.m
% run ramp_init.m
% run airfoil_init.m
% run lockheed_init.m
% run concave_init.m
% run vertical_init.m

load( fullfile( path_to_maps, 'cap_map.mat' ) );
load( fullfile( path_to_maps, 'kine_map.mat' ) );
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);

disp('Initialized Robot Parameters. Loading Part.......')
[v_part, f_part, n_part, name_part] = stlRead(part_name); % read tool stl
v_part(:,1:3) = v_part(:,1:3)./1000;

disp('Generating range vectors for tolerance.......')
% This cell will contain information of point and bz vectors for the flange
% under tolerances. Cap map can be directly checked for these vectors by
% applying translation to the points which is faster.
red_vec = ff_voxel_sets( range_x, range_y, robot.robot_ree_T_tee, xyz_bxbybz,...
                    sphere_approx,edt_map,mold_voxels.box_len/1000,mold_voxels.no_vxls,mold_voxels.voxel_dim,vxl_T_orig,tcp_idx,tool_tcps );

% Determine the total number of collision free orientations including
% tolerances. tot_ori is required to normalize the reachability cost in
% compute_reach function
tot_ori = 0;
for i = 1:size(red_vec,2)
    vecs = red_vec{i};
    tot_ori = tot_ori + size( find(vecs(:,end)==0),1 );
end


% Plotting points and part
if plot_point
    figure(1);
    hold on;
    part_plt(v_part, f_part, name_part, [0.64,0.64,0.64]); % plot tool stl
%     scatter3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),20,'b','filled' )
    for i = 1:size(grp_idx)
        plot3( xyz_bxbybz(grp_idx(i,1):grp_idx(i,2),1),xyz_bxbybz(grp_idx(i,1):grp_idx(i,2),2),...
            xyz_bxbybz(grp_idx(i,1):grp_idx(i,2),3),'b','linewidth',1 )
    end
%     for i = 1:size(red_vec,2)
%         vecs = red_vec{i};
%         scatter3( xyz_bxbybz(i,1),xyz_bxbybz(i,2),xyz_bxbybz(i,3),50,'r','filled' )
%         quiver3( vecs(:,1),vecs(:,2),vecs(:,3),...
%                     vecs(:,4),vecs(:,5),vecs(:,6),'k','AutoScaleFactor',1);
%     end
    daspect([1,1,1]);
    camlight;
    return;
end



placementOptions = optimoptions( 'fminunc', 'MaxIterations',5000,...
    'MaxFunctionEvaluations', 1e7, 'OptimalityTolerance', 1e-8,...
    'StepTolerance', 1e-8, 'Display', 'iter', 'SpecifyObjectiveGradient', false,...
    'ObjectiveLimit', 1e-12, 'HessUpdate','bfgs' );


% placementOptions = optimoptions('fmincon','Algorithm', 'sqp');
% placementOptions.MaxIterations = 5000;
% placementOptions.MaxFunctionEvaluations = 1e12;
% placementOptions.OptimalityTolerance = 1e-12;
% placementOptions.StepTolerance = 1e-12;
% placementOptions.Display = 'iter';
% placementOptions.SpecifyObjectiveGradient = false;
% placementOptions.ObjectiveLimit = 1e-14;


lb = [-1;-1;-1;-1];
ub = -lb;



I = eye(4);
    
for i = 1:size(tool_tcps,3)
    % Provide all transformations from roller origin to discrete tcps
    inv_tcps(:,:,i) = I;
    inv_tcps(1:3,1:3,i) = tool_tcps(1:3,1:3,i)' ;
    inv_tcps(1:3,4,i) = -tool_tcps(1:3,1:3,i)' * tool_tcps(1:3,4,i);
end



% Completely Random Samples
req_samples = 15;
samples = [];
no_successful = 0;
for i=1:req_samples
    fprintf('Sample: %d, ',i)
    X = [ randsample(0:0.01:1,1); randsample(-1:0.01:1,1) ; origin_z; eul2quat( [randsample(-pi:0.01:pi,1),0,0],'ZYX' )' ];
    
    X_fixed = [origin_z;0;0];

    init_seeds{i} = [];
    
    X0 = X([1,2,4,7]);
    
    % Optimizing the reachability over the part
%     [x_p,err] = fmincon(@(x_p)eval_reach_cost( x_p, X_fixed, xyz_bxbybz,...
%                     grp_idx, robot.robot_ree_T_tee, tolerance,theta_lb,theta_ub,SimtrajOptions, seg_time,velocities,motor_vel,...
%                     sphere_approx,edt_map,mold_voxels.box_len/1000,mold_voxels.no_vxls,mold_voxels.voxel_dim,vxl_T_orig,...
%                     init_seeds{i},tcp_idx,inv_tcps),X0,[],[],[],[],lb,ub,[],placementOptions);
    tic;
    [x_p,err] = fminunc(@(x_p)eval_reach_cost( x_p, X_fixed, xyz_bxbybz,...
                    grp_idx, robot.robot_ree_T_tee, tolerance,theta_lb,theta_ub,SimtrajOptions, seg_time,velocities,motor_vel,...
                    sphere_approx,edt_map,mold_voxels.box_len/1000,mold_voxels.no_vxls,mold_voxels.voxel_dim,vxl_T_orig,...
                    init_seeds{i},tcp_idx,tool_tcps),X0,placementOptions);
    toc;
    X([1,2,4,7]) = x_p;
    
    if status
        fprintf('Success\n')
        no_successful = no_successful + 1;
    else
        fprintf('Failure\n')
    end
    samples = [samples, X];
end
fprintf('Number of Successful Samples: %d\n',no_successful)
return;
csvwrite(fullfile(csv_path,'random_samples.csv'),samples);