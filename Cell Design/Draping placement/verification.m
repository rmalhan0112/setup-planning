function [joint_angles,status,error] = verification( X, X_fixed, xyz_bxbybz,...
                                    grp_idx, ff_T_tcps, tolerance,theta_lb,theta_ub,SimtrajOptions,seg_time,velocities,motor_vel,...
                                    tool_spheres,edt_map,box_len,no_vlxs,voxel_dim,vxl_T_orig,init_seeds,tcp_idx,tool_tcps)
    X = [ X(1:2); X_fixed(1); X(3); X_fixed(2:3); X(4) ];
    I = eye(4);
    
    bk_pt = false;
    isreturn = false;
    no_fail = 0;
    
%     yo = eye(4);
%     yo(1:3,1:3) = quat2rotm(X(4:7)');
%     yo(1:3,4) = X(1:3);
%     figure(1); camlight;
%     hold on;
%     daspect([1,1,1])      
%     [v_part, f_part, n_part, name_part] = stlRead('GE90_scaled.stl'); % read tool stl
% %     [v_part, f_part, n_part, name_part] = stlRead('Mold_Ascent.stl'); % read tool stl
% %     [v_part, f_part, n_part, name_part] = stlRead('airfoil.stl'); % read tool stl
%     v_part(:,1:3) = v_part(:,1:3)./1000;
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,yo);
%     part_plt(v_part_transf, f_part, name_part, [0.64,0.64,0.64]); % plot tool stl

    
    for i = 1:size(tool_tcps,3)
        % Provide all transformations from roller origin to discrete tcps
        inv_tcps(:,:,i) = I;
        inv_tcps(1:3,1:3,i) = tool_tcps(1:3,1:3,i)' ;
        inv_tcps(1:3,4,i) = -tool_tcps(1:3,1:3,i)' * tool_tcps(1:3,4,i);
    end
    
    % Simulate the trajectory for the given part pose and find error
    rotation = quat2rotm(X(4:7)');
    
    % Translating and rotating points
    xyz_bxbybz(:,1:3) = (rotation * xyz_bxbybz(:,1:3)' + X(1:3))';
    
    % Rotating Orientations
    xyz_bxbybz(:,4:6) = (rotation * xyz_bxbybz(:,4:6)')';
    xyz_bxbybz(:,7:9) = (rotation * xyz_bxbybz(:,7:9)')';
    xyz_bxbybz(:,10:12) = (rotation * xyz_bxbybz(:,10:12)')';
    
    orig_T_w = eye(4);  % World to origin of the part
    orig_T_w(1:3,1:3) = rotation';
    orig_T_w(1:3,4) = -rotation' * X(1:3);
    
    % Initialize Copies of Manipulator
    error = 0;
    status = true;
    app_coll = false;
    
    
%     csvwrite('/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Draping placement/Simulations/points.csv',xyz_bxbybz(:,1:3))
    
    for i = 1:size(grp_idx,1)
        joint_angle = [0.31189,0.2209,-0.1785,-1.5357,0.0176,1.3463,0]';
        frst_idx = true;
        for j = grp_idx(i,1):grp_idx(i,2)
            prev_theta = joint_angle;
            ff_T_ee = ff_T_tcps(:,:,tcp_idx(j));    % Which TCP to be used. FK will give location of new TCP
%             joint_angle = init_seeds(:,j);
            % Finding the trajectory
            
            % Position and Orientation
            [joint_angle,f_val] = fmincon( @(theta) trajSim_error( theta, xyz_bxbybz(j,:),...
                ff_T_ee),joint_angle, [],[],[],[],theta_lb,theta_ub,...
                @(theta)z_constr(theta, xyz_bxbybz(j,:), ff_T_ee, tolerance,velocities(j,:)',motor_vel,...
                    prev_theta,frst_idx),SimtrajOptions );
                
            % Velocity and Force
            [joint_angle,f_val] = fmincon( @(theta) trajSim_error( theta, xyz_bxbybz(j,:),...
                ff_T_ee),joint_angle, [],[],[],[],theta_lb,theta_ub,...
                @(theta) get_constraints(theta, xyz_bxbybz(j,:), ff_T_ee, tolerance,prev_theta,...
                seg_time,velocities(j,:),motor_vel,j,frst_idx,tool_spheres,edt_map,box_len,no_vlxs,voxel_dim,...
                vxl_T_orig,orig_T_w,inv_tcps(:,:,tcp_idx(j)),app_coll),...
                SimtrajOptions );
            
            joint_angles(j,:) = joint_angle';
            frst_idx = false;
            error = error + f_val;
            
%             w_T_ee = get_iiwa_FK_mex(joint_angle,I) * ff_T_ee;  % IIWA 7
            fk_all = get_iiwa14_FK_all_joints_mex(joint_angle,I);
            w_T_ee = fk_all(33:36,:) * ff_T_ee;  % IIWA 14
            
            % Position Error
            if norm( xyz_bxbybz(j,1:3) - w_T_ee(1:3,4)' ) > tolerance(1)
                status = false;
                if bk_pt
                    disp('Found')
                end
            end
            % Y-Orientation Error
            if acos( xyz_bxbybz(j,7)*w_T_ee(1,2) + ...
                    xyz_bxbybz(j,8)*w_T_ee(2,2)+ xyz_bxbybz(j,9)*w_T_ee(3,2) ) > tolerance(2)
                status = false;
                if bk_pt
                    disp('Found')
                end
            end
            % Z-Orientation Error
            angle = acos( xyz_bxbybz(j,10)*w_T_ee(1,3) +...
                xyz_bxbybz(j,11)*w_T_ee(2,3) + xyz_bxbybz(j,12)*w_T_ee(3,3) );
            if angle > tolerance(4) + 1e-4
                status = false;
                if bk_pt
                    disp('Found')
                end
            end
            
%             if j==100
%             figure(1); hold on; daspect([1,1,1]); camlight
%             robot.base_T_robot = eye(4);
%             robot.robot_ree_T_tee = ff_T_tcps(:,:,8);
%             plot_robot( joint_angle,'roller.stl',robot )
%             quiver3( xyz_bxbybz(j,1),xyz_bxbybz(j,2),xyz_bxbybz(j,3),...
%                     xyz_bxbybz(j,10),xyz_bxbybz(j,11),xyz_bxbybz(j,12),'b','linewidth',2,'AutoScaleFactor',0.1)
%             end

            % Check for collision. Collision Function should be provided
            % roller origin wrt voxel frame of the mold
            if iscolliding( tool_spheres,edt_map,box_len,no_vlxs,voxel_dim,vxl_T_orig*orig_T_w*w_T_ee*inv_tcps(:,:,tcp_idx(j)) )==1
                app_coll = true;
                [joint_angle,f_val] = fmincon( @(theta) trajSim_error( theta, xyz_bxbybz(j,:),...
                ff_T_ee),joint_angle, [],[],[],[],theta_lb,theta_ub,...
                @(theta) get_constraints(theta, xyz_bxbybz(j,:), ff_T_ee, tolerance,joint_angle,...
                seg_time,velocities(j,:),motor_vel,j,frst_idx,tool_spheres,edt_map,box_len,no_vlxs,voxel_dim,...
                vxl_T_orig,orig_T_w,inv_tcps(:,:,tcp_idx(j)),app_coll),...
                SimtrajOptions );
            
%                 w_T_ee = get_iiwa_FK_mex(joint_angle,I) * ff_T_ee;  % iiwa7
                fk_all = get_iiwa14_FK_all_joints_mex(joint_angle,I);
                w_T_ee = fk_all(33:36,:) * ff_T_ee;  % IIWA 14
                
                if iscolliding( tool_spheres,edt_map,box_len,no_vlxs,voxel_dim,vxl_T_orig*orig_T_w*w_T_ee )==1
                    status = false;
                    if bk_pt
                        disp('Found')
                    end
                end
                % Position Error
                if norm( xyz_bxbybz(j,1:3) - w_T_ee(1:3,4)' ) > tolerance(1)
                    status = false;
                    if bk_pt
                        disp('Found')
                    end
                end
                % Y-Orientation Error
                if acos( xyz_bxbybz(j,7)*w_T_ee(1,2) + ...
                        xyz_bxbybz(j,8)*w_T_ee(2,2)+ xyz_bxbybz(j,9)*w_T_ee(3,2) ) > tolerance(2)
                    status = false;
                    if bk_pt
                        disp('Found')
                    end
                end
                % Z-Orientation Error
                angle = acos( xyz_bxbybz(j,10)*w_T_ee(1,3) +...
                    xyz_bxbybz(j,11)*w_T_ee(2,3) + xyz_bxbybz(j,12)*w_T_ee(3,3) );
                if angle > tolerance(4) + 1e-4
                    status = false;
                    if bk_pt
                        disp('Found')
                    end
                end
                joint_angles(j,:) = joint_angle';
                app_coll = false;
            end
            if ~status 
                if no_fail <= 2
                    no_fail = no_fail + 1;
                    status = true;
                elseif isreturn
                    return;
                end
            end
        end
    end
end