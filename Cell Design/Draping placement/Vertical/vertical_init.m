csv_path = '/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Draping placement/Vertical';
part_name = fullfile( stl_path, 'vertical.stl' );
path_to_points = '/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Draping placement/Vertical/';
coll_data_path = '/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Draping placement/Collision Data/';
xyz_bxbybz = csvread( fullfile(path_to_points,'vertical_points.csv') );
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
grp_idx = csvread( fullfile(path_to_points,'vertical_grps.csv') );

load(fullfile(csv_path,'mold_voxels.mat'))
load(fullfile(csv_path,'edt_map.mat'))
load(fullfile(coll_data_path,'tool_spheres.mat'))
tcp_idx = csvread(fullfile(csv_path,'tcp_idx.csv'));

% tcp_idx = ones(size(xyz_bxbybz,1),1)*8;

sphere_approx(:,1:3) = sphere_approx(:,1:3)./1000;

vxl_T_orig = eye(4);
vxl_T_orig(1:3,4) = mold_voxels.orig_trans'/1000;


no_pts = size(xyz_bxbybz,1);

tolerance(1) = 0.003;
tolerance(2) = 0.0524;
tolerance(3) = 0.0524;
tolerance(4) = 0.8;

Force_vecs = zeros(no_pts,6);
Force_vecs(:,3) = ones(no_pts,1).* 30;   % 30 N force

% Computation times are determined for 130 mm/s Maximum should be 220
desired_velc = 50; % mm/s
desired_velc = desired_velc/1000;
fprintf('Desired Velocity from the robot: %d mm/s\n',desired_velc);


path_len = 0;
velocities = [];
seg_time = [];
tot_time = 0;

for i = 1:size(grp_idx,1)
    seg_time = [ seg_time; 600 ];
    for j = grp_idx(i,1):grp_idx(i,2)-1
        ff_T_ee = robot.robot_ree_T_tee(:,:,tcp_idx(j));
        dist_vec = (xyz_bxbybz(j+1,1:3)-xyz_bxbybz(j,1:3));
        
        curr_rt = [xyz_bxbybz(j,4:6)',xyz_bxbybz(j,7:9)',xyz_bxbybz(j,10:12)'];
        nxt_rt = [xyz_bxbybz(j+1,4:6)',xyz_bxbybz(j+1,7:9)',xyz_bxbybz(j+1,10:12)'];
        curr_T = eye(4);
        nxt_T = eye(4);
        curr_T(1:3,4) =  [xyz_bxbybz(j,1:3)'];
        curr_T(1:3,1:3) =  curr_rt;
        curr_T = curr_T * inv(ff_T_ee); % Flange to world for j point
        
        nxt_T(1:3,4) =  [xyz_bxbybz(j+1,1:3)'];
        nxt_T(1:3,1:3) =  nxt_rt;
        nxt_T = nxt_T * inv(ff_T_ee); % Flange to world for j+1 point
        
        v_vec = nxt_T(1:3,4)' - curr_T(1:3,4)';
        
        rt = [ dot(nxt_T(1:3,1),curr_T(1:3,1)),  dot(nxt_T(1:3,2),curr_T(1:3,1)),  dot(nxt_T(1:3,3),curr_T(1:3,1));
            dot(nxt_T(1:3,1),curr_T(1:3,2)),  dot(nxt_T(1:3,2),curr_T(1:3,2)),  dot(nxt_T(1:3,3),curr_T(1:3,2));
            dot(nxt_T(1:3,1),curr_T(1:3,3)),  dot(nxt_T(1:3,2),curr_T(1:3,3)),  dot(nxt_T(1:3,3),curr_T(1:3,3))   ];
        
        l = rotm2axang( rt );
        
        dist = norm( dist_vec );
        dist_vec = dist_vec/dist;
        path_len = path_len + dist;
        time = dist/desired_velc;
        velocities = [velocities; [l(4)*l(1:3)/time,v_vec/time]  ];
        seg_time = [ seg_time; time ];
        tot_time = tot_time + dist/desired_velc; 
    end
    velocities = [velocities; [0,0,0,0,0,0]];
end
fprintf('Computed time for execution of process: %d s\n',tot_time);
% seg_time = seg_time*1.5;


% Range vectors for tolerance exploration in capability map
range_y(1,1) = 0;
discretization = 0.08;
step = discretization;
while abs(range_y(end,1)) <= tolerance(4)
    range_y(end+1,1) = step;
    range_y(end+1,1) = -step;
    step = step + discretization;
end
range_x = [0];

origin_z = 0;