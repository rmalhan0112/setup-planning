function coll_check = iscolliding( tool_spheres,edt_map,box_len,no_vxls,voxel_dim,voxel_T_ee )
%     figure(1); hold on; daspect([1,1,1]);
%     vxl_T_orig = [1.0000         0         0    0.2000
%          0    1.0000         0    0.2000
%          0         0    1.0000    0.0467
%          0         0         0    1.0000];
%     [v_part, f_part, n_part, name_part] = stlRead('Mold_Ascent.stl'); % read tool stl
%     v_part(:,1:3) = v_part(:,1:3)./1000;
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,vxl_T_orig);
%     part_plt(v_part_transf, f_part, name_part, [0.64,0.64,0.64]); % plot tool stl

    
    % voxel_T_ee is the end-effector to the mold voxels voxel frame
    coll_check = 0;
    tool_spheres(:,1:3) = (voxel_T_ee(1:3,1:3)*tool_spheres(:,1:3)' + voxel_T_ee(1:3,4))';
    
%     sphere_radius = tool_spheres(1,4)/1000;
%     [x y z] = sphere;
%     for l=1:size(tool_spheres,1)
%         surf(x*sphere_radius+tool_spheres(l,1),y*sphere_radius+tool_spheres(l,2),z*sphere_radius+tool_spheres(l,3));
%     end
    
    for i = 1:size(tool_spheres)
        if tool_spheres(i,1)<0 || tool_spheres(i,2)<0 || tool_spheres(i,3)<0 ||...
            tool_spheres(i,1)>=box_len(1) || tool_spheres(i,2)>=box_len(2) || tool_spheres(i,3)>=box_len(3)
            continue;
        end
        
        if tool_spheres(i,4) > edt_map( floor(tool_spheres(i,1)/box_len(1)*no_vxls(1))+1,...
                floor(tool_spheres(i,2)/box_len(2)*no_vxls(2))+1,...
                floor(tool_spheres(i,3)/box_len(3)*no_vxls(3))+1  )*voxel_dim
            coll_check = 1;
            return;
        end
    end
end



% Test this
% 0.5879   -0.0035    0.8090    0.5223
% 0.0161   -0.9997   -0.0160    0.4881
% 0.8088    0.0224   -0.5877    0.1512
%  0         0         0    1.0000