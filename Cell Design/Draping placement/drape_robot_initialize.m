%%%%%%%% Draping robot AND TOOL DEFINITION %%%%%%%%%%%%%
robot.base_T_robot = eye(4);
robot.base_T_robot(1:3,4) = [0;0;0];
robot.base_T_robot(1:3,1:3) = eul2rotm([0.0,0,0]);

robot.robot_ree_T_tee = eye(4);
robot.robot_ree_T_tee(1:3,4) = [-0.049; 0; 0.133]; % For Gripper
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

robot_tool = fullfile( stl_path, 'roller.stl' );

motor_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);

% Optimizer Bounds
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = -theta_lb;

IKoptions = optimoptions('fmincon','Algorithm', 'sqp');
IKoptions.MaxIterations = 1500;
IKoptions.MaxFunctionEvaluations = 1e7;
IKoptions.OptimalityTolerance = 1e-8;
IKoptions.StepTolerance = 1e-8;
IKoptions.Display = 'off';
IKoptions.SpecifyObjectiveGradient = false;
IKoptions.ObjectiveLimit = 1e-8;


% nloptions.MaxIterations = 5000;
% nloptions.MaxFunctionEvaluations = 1e7;
% nloptions.OptimalityTolerance = 1e-8;
% nloptions.StepTolerance = 1e-8;
% nloptions.Display = 'iter';
% nloptions.SpecifyObjectiveGradient = false;
% nloptions.ObjectiveLimit = 1e-8;

% nloptions = optimoptions( 'fminunc', 'MaxIterations',5000,...
%     'MaxFunctionEvaluations', 1e7, 'OptimalityTolerance', 1e-8,...
%     'StepTolerance', 1e-8, 'Display', 'iter', 'SpecifyObjectiveGradient', false,...
%     'ObjectiveLimit', 1e-8, 'HessUpdate','bfgs' );


SimtrajOptions = optimoptions('fmincon','Algorithm', 'sqp');
SimtrajOptions.MaxIterations = 1500;
SimtrajOptions.MaxFunctionEvaluations = 1e12;
SimtrajOptions.OptimalityTolerance = 1e-12;
SimtrajOptions.StepTolerance = 1e-12;
SimtrajOptions.Display = 'off';
SimtrajOptions.SpecifyObjectiveGradient = false;
SimtrajOptions.ObjectiveLimit = 1e-12;

% SimtrajOptions = optimoptions( 'fminunc', 'MaxIterations',5000,...
%     'MaxFunctionEvaluations', 1e7, 'OptimalityTolerance', 1e-8,...
%     'StepTolerance', 1e-8, 'Display', 'off', 'SpecifyObjectiveGradient', false,...
%     'ObjectiveLimit', 1e-8, 'HessUpdate','bfgs' );


% Original TCP is index 8
[T,ang]=gen_tcp();
for i=1:15
    T(:,:,i) = robot.robot_ree_T_tee * T(:,:,i);
end
robot.robot_ree_T_tee = T;
[T,ang]=gen_tcp();
tool_tcps = T;
clear('T');