function [violations,ovl_viol] = compute_reach( X,red_vec,xyz_bxbybz,cap_map  )
    rotation = quat2rotm(X(4:7)');
    violations = size(xyz_bxbybz,1);
    ovl_viol = 0;
   
% Checking orientation
    for i = 1:size(red_vec,2)
        set_vecs = red_vec{i};
        % Applying Translation for current part position
        set_vecs(:,1:3) = (rotation * set_vecs(:,1:3)' + X(1:3))';
        set_vecs(:,4:6) = (rotation * set_vecs(:,4:6)')';
        flag = true;
        for j = 1:size(set_vecs,1)
            if set_vecs(j,7)==1
                continue;
            end
            key = gen_cmap_key( set_vecs(j,1:3) );
            if isKey( cap_map,key )
                ori_cone = cap_map(key);
                angle = acos( ori_cone(1) * set_vecs(j,4) + ori_cone(2) * set_vecs(j,5) +...
                        ori_cone(3) * set_vecs(j,6) );
                if (angle >= ori_cone(4) && angle <= ori_cone(5))    
                    if flag
                        violations = violations - 1;
                        flag = false;
                    end
                else
                    ovl_viol = ovl_viol + 1;
                end
            else
                ovl_viol = ovl_viol + 1;
            end
        end
    end
end