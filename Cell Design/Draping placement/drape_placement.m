clear all;
close all;
clc;

addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/STL_plot/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/manipulator_planners/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/'));

plot_point = false;
compute_all = false;

% Load Dependencies
% Paths to Different Files
disp('Loading dependencies..........')
stl_path = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes';
iiwa_stls = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/iiwa7_stls';
path_to_maps = '/home/rmalhan/Work/USC/Composite Automation/Modules/Cap Maps';

disp('Initializing Robot Parameters.......')
run drape_robot_initialize.m
% run ascent_init.m
% run dome_init.m
% run ge90_init.m
% run ramp_init.m
% run airfoil_init.m
% run lockheed_init.m
% run concave_init.m
% run vertical_init.m
run demo_lm_init.m;


load( fullfile( path_to_maps, 'cap_map.mat' ) );
load( fullfile( path_to_maps, 'kine_map.mat' ) );
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);

disp('Initialized Robot Parameters. Loading Part.......')
[v_part, f_part, n_part, name_part] = stlRead(part_name); % read tool stl
v_part(:,1:3) = v_part(:,1:3)./1000;

disp('Generating range vectors for tolerance.......')
% This cell will contain information of point and bz vectors for the flange
% under tolerances. Cap map can be directly checked for these vectors by
% applying translation to the points which is faster.
red_vec = ff_voxel_sets( range_x, range_y, robot.robot_ree_T_tee, xyz_bxbybz,...
                    sphere_approx,edt_map,mold_voxels.box_len/1000,mold_voxels.no_vxls,mold_voxels.voxel_dim,vxl_T_orig,tcp_idx,tool_tcps );

% Determine the total number of collision free orientations including
% tolerances. tot_ori is required to normalize the reachability cost in
% compute_reach function
tot_ori = 0;
for i = 1:size(red_vec,2)
    vecs = red_vec{i};
    tot_ori = tot_ori + size( find(vecs(:,end)==0),1 );
end


% Plotting points and part
if plot_point
    figure(1);
    hold on;
    part_plt(v_part, f_part, name_part, [0.64,0.64,0.64],1); % plot tool stl
%     scatter3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),20,'b','filled' )
    for i = 1:size(grp_idx)
        plot3( xyz_bxbybz(grp_idx(i,1):grp_idx(i,2),1),xyz_bxbybz(grp_idx(i,1):grp_idx(i,2),2),...
            xyz_bxbybz(grp_idx(i,1):grp_idx(i,2),3),'b','linewidth',3 )
    end
%     set(gcf,'color',[0.2,0.2,0.2]);
    set(gcf,'color',[1,1,1]);
    axis off;
    for i = 1:1
        vecs = red_vec{i};
        scatter3( xyz_bxbybz(i,1),xyz_bxbybz(i,2),xyz_bxbybz(i,3),50,'r','filled' )
        quiver3( vecs(:,1),vecs(:,2),vecs(:,3),...
                    vecs(:,4),vecs(:,5),vecs(:,6),'k','AutoScaleFactor',1);
    end
    daspect([1,1,1]);
    camlight;
    return;
end


if compute_all
%     % Completely Random Samples
%     req_samples = 50;
%     samples = [];
%     no_successful = 0;
%     tic;
%     for i=1:req_samples
%         fprintf('Sample: %d, ',i)
%         X = [ randsample(0:0.01:1,1); randsample(-1:0.01:1,1) ; origin_z; eul2quat( [randsample(-pi:0.01:pi,1),0,0],'ZYX' )' ];
%         
%         X_fixed = [origin_z;0;0];
%         
%         init_seeds{i} = [];
%         [joint_angles,status,error] = verification( X([1,2,4,7]), X_fixed, xyz_bxbybz,...
%                         grp_idx, robot.robot_ree_T_tee, tolerance,theta_lb,theta_ub,SimtrajOptions, seg_time,velocities,motor_vel,...
%                         sphere_approx,edt_map,mold_voxels.box_len/1000,mold_voxels.no_vxls,mold_voxels.voxel_dim,vxl_T_orig,...
%                         init_seeds{i},tcp_idx,tool_tcps);
%         if status
%             fprintf('Success\n')
%             no_successful = no_successful + 1;
%         else
%             fprintf('Failure\n')
%         end
%         samples = [samples, X];
%     end
%     fprintf('Number of Successful Samples: %d\n',no_successful)
%     toc;
%     return;
%     csvwrite(fullfile(csv_path,'random_samples.csv'),samples);
    
    
    

    % %%%%%%%%%%%%%%%%%%%% GENERATION OF INITIAL SEEDS  %%%%%%%%%%%%%%%%%%%%%%%%%
    req_samples = 50;
    tic;
    disp('Generating random seeds using capability map.......')
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Randomly Sample X in the workspace
    no_samples = 0;
    max_samples = 30 * req_samples;
    Samples = [];
    for i = 1:max_samples
        X = [ randsample(0:0.01:1,1); randsample(-1:0.01:1,1) ; origin_z; eul2quat( [randsample(-pi:0.01:pi,1),0,0],'ZYX' )' ];
        viol = chck_apprx_reach( X,red_vec,xyz_bxbybz,cap_map  );
        Samples = [Samples; [X',viol]];
        no_samples = no_samples + 1;
    end
    Samples = sortrows(Samples,8);
    Samples =  Samples(:,1:7)';
    
    apprx_sol = [];
    for i = 1:req_samples
        viol = check_C_reachability( Samples(:,i),red_vec,xyz_bxbybz,cap_map  );
        if viol == 0
            apprx_sol = [apprx_sol, Samples(:,i)];
        end
    end
    fprintf('Number of Approximate Solutions having zero violations:  %d\n', size(apprx_sol,2))
    csvwrite(fullfile(csv_path,'C_map_samples.csv'),apprx_sol);
    toc;
    
    
    
    % % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Maximizing Reachability
    disp('Maximizing reach over the waypoints......')
    samples = csvread(fullfile(csv_path,'C_map_samples.csv'));
    prtz_sol = [];
    tic;
    for idx = 1:size(samples,2)
        fprintf('Improving sample no:  %d\n',idx)
        X = samples(:,end);
        [init_viol,max_Oviol] = compute_reach( X,red_vec,xyz_bxbybz,cap_map  );
        fprintf('Initial Reach Violation:  %d\n',max_Oviol);
        prev_Oviol = max_Oviol;
        visited_samples = zeros(7,1);
        
        % Using Gradient Descent to minimize overall reach
        while true
            opt_idx = 0;
            states = get_actions( X,visited_samples );
            for i = 1:size(states,2)
                visited_samples = [visited_samples, states(:,i)];
                [viol,Oviol] = compute_reach( states(:,i),red_vec,xyz_bxbybz,cap_map  );
                if viol <= init_viol
                    if Oviol < max_Oviol
                        max_Oviol = Oviol;
                        opt_idx = i;
                    end
                end
            end

            if max_Oviol == prev_Oviol
                break;
            else
                X = states(:,opt_idx);
                prev_Oviol = max_Oviol;
            end
        end
        prtz_sol = [prtz_sol, [X;max_Oviol]];
        fprintf('Final Reach Violation:  %d\n',max_Oviol);
    end
    toc;
    prtz_sol = sortrows( prtz_sol',8,'ascend' );
    prtz_sol = prtz_sol(:,1:7)';
    csvwrite(fullfile(csv_path,'R_samples.csv'),prtz_sol);
    
    
    

%     % % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Maximizing Prf Idx after
%     % Reachability
%     disp('Maximizing performance over the waypoints......')
%     samples = csvread(fullfile(csv_path,'R_samples.csv'));
%     prtz_sol = [];
%     tic;
%     parfor idx = 1:size(samples,2)
%         fprintf('Improving sample no:  %d\n',idx)
%         X = samples(:,idx);
%         [init_viol,max_Oviol,max_prf] = compute_performance( X,red_vec,xyz_bxbybz,kine_map,cap_map  );
%         fprintf('Initial Reach Violation:  %d\n',max_prf);
%         prev_prf = max_prf;
%         visited_samples = zeros(7,1);
%         
%         % Using Gradient Descent to minimize overall reach
%         while true
%             opt_idx = 0;
%             states = get_actions( X,visited_samples );
%             for i = 1:size(states,2)
%                 visited_samples = [visited_samples, states(:,i)];
%                 [viol,Oviol,prf] = compute_performance( states(:,i),red_vec,xyz_bxbybz,kine_map,cap_map  );
%                 if viol <= init_viol && Oviol <= max_Oviol
%                     if prf > max_prf
%                         max_prf = prf;
%                         opt_idx = i;
%                     end
%                 end
%             end
% 
%             if max_prf == prev_prf
%                 break;
%             else
%                 X = states(:,opt_idx);
%                 prev_prf = max_prf;
%             end
%         end
%         prtz_sol = [prtz_sol, [X;max_prf]];
%         fprintf('Final Reach Violation:  %d\n',max_prf);
%     end
%     toc;
%     prtz_sol = sortrows( prtz_sol',8,'descend' );
%     prtz_sol = prtz_sol(:,1:7)';
%     csvwrite(fullfile(csv_path,'P_samples.csv'),prtz_sol);
%     
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% % % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Maximizing Prf Idx after
%     % Reachability
%     disp('Maximizing performance over the waypoints......')
%     samples = csvread(fullfile(csv_path,'C_map_samples.csv'));
%     prtz_sol = [];
%     tic;
%     parfor idx = 1:size(samples,2)
%         fprintf('Improving sample no:  %d\n',idx)
%         X = samples(:,idx);
%         [init_viol,max_Oviol,max_prf] = compute_performance( X,red_vec,xyz_bxbybz,kine_map,cap_map  );
%         fprintf('Initial Reach Violation:  %d\n',max_prf);
%         prev_prf = max_prf;
%         visited_samples = zeros(7,1);
%         
%         % Using Gradient Descent to minimize overall reach
%         while true
%             opt_idx = 0;
%             states = get_actions( X,visited_samples );
%             for i = 1:size(states,2)
%                 visited_samples = [visited_samples, states(:,i)];
%                 [viol,Oviol,prf] = compute_performance( states(:,i),red_vec,xyz_bxbybz,kine_map,cap_map  );
%                 if viol <= init_viol && Oviol <= max_Oviol
%                     if prf > max_prf
%                         max_prf = prf;
%                         opt_idx = i;
%                     end
%                 end
%             end
% 
%             if max_prf == prev_prf
%                 break;
%             else
%                 X = states(:,opt_idx);
%                 prev_prf = max_prf;
%             end
%         end
%         prtz_sol = [prtz_sol, [X;max_prf]];
%         fprintf('Final Reach Violation:  %d\n',max_prf);
%     end
%     toc;
%     prtz_sol = sortrows( prtz_sol',8,'descend' );
%     prtz_sol = prtz_sol(:,1:7)';
%     csvwrite(fullfile(csv_path,'trial_samples.csv'),prtz_sol);
% 
% 
%     
%     
%     
%     % % %%%%%%%%%%%%%%%%% Maximizing Reachability under manip constraint
%     disp('Maximizing reach over the waypoints......')
%     samples = csvread(fullfile(csv_path,'trial_samples.csv'));
%     prtz_sol = [];
%     tic;
%     parfor idx = 1:size(samples,2)
%         fprintf('Improving sample no:  %d\n',idx)
%         X = samples(:,idx);
%         [init_viol,max_Oviol,init_prf] = compute_performance( X,red_vec,xyz_bxbybz,kine_map,cap_map  );
%         fprintf('Initial Reach Violation:  %d\n',max_Oviol);
%         prev_Oviol = max_Oviol;
%         visited_samples = zeros(7,1);
%         
%         % Using Gradient Descent to minimize overall reach
%         while true
%             opt_idx = 0;
%             states = get_actions( X,visited_samples );
%             for i = 1:size(states,2)
%                 visited_samples = [visited_samples, states(:,i)];
%                 [viol,Oviol,prf] = compute_performance( states(:,i),red_vec,xyz_bxbybz,kine_map,cap_map  );
%                 if viol <= init_viol && prf >= init_prf
%                     if Oviol < max_Oviol
%                         max_Oviol = Oviol;
%                         opt_idx = i;
%                     end
%                 end
%             end
% 
%             if max_Oviol == prev_Oviol
%                 break;
%             else
%                 X = states(:,opt_idx);
%                 prev_Oviol = max_Oviol;
%             end
%         end
%         prtz_sol = [prtz_sol, [X;max_Oviol]];
%         fprintf('Final Reach Violation:  %d\n',max_Oviol);
%     end
%     toc;
%     prtz_sol = sortrows( prtz_sol',8,'ascend' );
%     prtz_sol = prtz_sol(:,1:7)';
%     csvwrite(fullfile(csv_path,'S32samples.csv'),prtz_sol);
end


% % Verification Stage (IK and continuity)
% samples = csvread(fullfile(csv_path,'R_samples.csv'));
% disp('Verifying IK and discarding discontinuous samples')
% % verf_sol = [];
% tic;
% counter = 1;
% for i = 1:size(samples,2)
%     fprintf('IK for Sample:  %d\n',i);
%     [seeds,status] = verf_sample( samples(:,i),xyz_bxbybz,tolerance,SimtrajOptions,...
%                     theta_lb,theta_ub,robot.robot_ree_T_tee,grp_idx,tcp_idx,velocities,motor_vel );
%     init_seeds{counter} = seeds;
%     counter = counter + 1;
% %     verf_sol = [verf_sol,samples(:,i)];
% %     if status
% %         fprintf('Success\n')
% %         init_seeds{counter} = seeds;
% %         verf_sol = [verf_sol,samples(:,i)];
% %         counter = counter + 1;
% %     else
% %         fprintf('Failed\n')
% %     end
% end
% toc;
% % if size(verf_sol,2)==0
% %     disp('No Solution Found. Exiting')
% %     return;
% % end
% % fprintf('Number of Samples Verified: %d\n',size(verf_sol,2));
% % csvwrite(fullfile(csv_path,'verf_samples.csv'),verf_sol);
% save(fullfile(csv_path,'init_seeds.mat'),'init_seeds')







% Hack to just generate one trajectory
X_fixed = [origin_z;0;0];
X_0 = [0;0;0;1;0;0;0];

[joint_angles,status,error] = verification( X_0([1,2,4,7]), X_fixed, xyz_bxbybz,...
                        grp_idx, robot.robot_ree_T_tee, tolerance,theta_lb,theta_ub,SimtrajOptions, seg_time,velocities,motor_vel,...
                        sphere_approx,edt_map,mold_voxels.box_len/1000,mold_voxels.no_vxls,mold_voxels.voxel_dim,vxl_T_orig,...
                        [],tcp_idx,tool_tcps);
return;









%%%%%%%%%%%%%%%%%%%%%% EXACT SOLUTION  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X_fixed = [origin_z;0;0];


% C map - Random samples using C map
% R samples - Max reachability over C map samples
% P samples - Max reachability then manip over C map samples
% trial samples - Max manip over C map samples
% S32 samples - Max manip then reachability over C map samples
poses{1} = csvread(fullfile(csv_path,'random_samples.csv'));
poses{2} = csvread(fullfile(csv_path,'C_map_samples.csv'));
poses{3} = csvread(fullfile(csv_path,'R_samples.csv'));
poses{4} = csvread(fullfile(csv_path,'P_samples.csv'));
poses{5} = csvread(fullfile(csv_path,'trial_samples.csv'));
poses{6} = csvread(fullfile(csv_path,'S32samples.csv'));
% poses{5} = csvread(fullfile(csv_path,'verf_samples.csv'));
% load(fullfile(csv_path,'init_seeds.mat'));

% 1 for Yes. 0 for No. Sequence is: random, C map, R map.
test_samples = [ 0,0,1,0,0,0,0 ];

for smp = 1:size(test_samples,2)
    switch smp
        case 1
            type_smp = 'Random';
        case 2
            type_smp = 'C map';
        case 3
            type_smp = 'R idx';
        case 4
            type_smp = 'P idx';
        case 5
            type_smp = 'Trial idx';
        case 6
            type_smp = 'S23';
        case 7
            type_smp = 'Verf';
    end
    
    if test_samples(1,smp) == 0
        continue;
    else
        fprintf('Sample Type:  %s\n',type_smp)
        samples = poses{smp};
    end
    no_success = 0;
    tic;
    for i = 1:size(samples,2)
%     for i = 3
        fprintf('Testing Sample:  %d,   ',i);
        X_0 = samples(:,i);
        init_seeds{i} = [];
        csvwrite('/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Draping placement/Simulations/pose.csv',X_0')
        
        [joint_angles,status,error] = verification( X_0([1,2,4,7]), X_fixed, xyz_bxbybz,...
                        grp_idx, robot.robot_ree_T_tee, tolerance,theta_lb,theta_ub,SimtrajOptions, seg_time,velocities,motor_vel,...
                        sphere_approx,edt_map,mold_voxels.box_len/1000,mold_voxels.no_vxls,mold_voxels.voxel_dim,vxl_T_orig,...
                        init_seeds{i},tcp_idx,tool_tcps);

        if status
            no_success = no_success + 1;
%             csvwrite('/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Draping placement/Simulations/joint_angles.csv',joint_angles)
%             csvwrite('/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Draping placement/Simulations/pose.csv',X_0')
            fprintf('Success\n')
            continue;
        else
%             csvwrite('/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Draping placement/Simulations/joint_angles.csv',joint_angles)
%             csvwrite('/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Draping placement/Simulations/pose.csv',X_0')
            fprintf('Failed\n')
        end
    end
    toc;
    fprintf('Success Rate for %s is:  %d\n', type_smp,no_success/size(samples,2)*100);
    fprintf('Number of Solutions for %s is:  %d\n', type_smp,no_success);
end
    
    











%     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Gradient Descent on the Random Samples
%     disp('Conducting gradient descent over the seeds.......')
%     apprx_sol = [];
%     for idx = 1:req_samples
%         X = Samples(idx,1:7)';
%         least_viol = chck_apprx_reach( X,red_vec,xyz_bxbybz,cap_map  );
%     %     fprintf('Initial Violations:  %d\n', least_viol)
% %         if least_viol == 0
% %     %         disp('Solution found. Storing Solution.')
% %             apprx_sol = [apprx_sol; [X',least_viol]];
% %             continue;
% %         end
%         iterates = X;
%         flag = true;
%         while flag
%             opt_idx = 0;
%             states = get_actions( X );
%             for i = 1:size(states,2)
%                 state_viol = chck_apprx_reach( states(:,i),red_vec,xyz_bxbybz,cap_map  );
%                 if state_viol <= least_viol
%                     least_viol = state_viol;
%                     opt_idx = i;
%                 end
%             end
% 
%             if opt_idx == 0
%     %             disp('Solution not found. Trying another Sample')
%                 break;
%             end
%             iterates = [iterates, states(:,opt_idx) ];
%             if isequal( iterates(1:7,end),iterates(1:7,end-1))
%                 flag = false;
%             end
%         end
%     %     disp('Solution minimized. Storing Solution.')
%         apprx_sol = [apprx_sol; [iterates(:,end)',least_viol]];
%     end
%     toc;
% 
%     temp_sol = sortrows(apprx_sol,8);
%     apprx_sol = [];
%     for i = 1:size(temp_sol,1)
%         if temp_sol(i,end) == 0
%             apprx_sol = [apprx_sol, temp_sol(i,1:7)'];
%         end
%     end
%     fprintf('Number of Approximate Solutions having zero violations:  %d\n', size(apprx_sol,2))
%     csvwrite(fullfile(csv_path,'C_map_samples.csv'),apprx_sol);












% % Verification Stage (IK and continuity)
% samples = csvread(fullfile(csv_path,'R_samples.csv'));
% disp('Verifying IK and discarding discontinuous samples')
% verf_sol = [];
% tic;
% counter = 1;
% for i = 1:size(samples,2)
%     fprintf('Testing Sample:  %d\n',i);
%     [seeds,status] = verf_sample( samples(:,i),xyz_bxbybz,tolerance,SimtrajOptions,theta_lb,theta_ub,robot.robot_ree_T_tee,grp_idx );
%     if status
%         init_seeds{counter} = seeds;
%         verf_sol = [verf_sol,samples(:,i)];
%         counter = counter + 1;
%     end
% end
% toc;
% if size(verf_sol,2)==0
%     disp('No Solution Found. Exiting')
%     return;
% end
% fprintf('Number of Samples Verified: %d\n',size(verf_sol,2));
% csvwrite(fullfile(csv_path,'verf_samples.csv'),verf_sol);
% save(fullfile(csv_path,'init_seeds.mat'),'init_seeds')
    
    
    
% solutions = sortrows(solutions,8,'descend');
% csvwrite(fullfile(csv_path,'solutions.csv'),solutions);

% % Debug which constraint is violated or why solution is not found
% for i = 1:size(grp_idx,1)
%     frst_idx = true;
%     for j = grp_idx(i,1)+1:grp_idx(i,2)
%         j
%         theta = joint_angles(j,:)';
%         theta_prev = joint_angles(j-1,:)';
%         get_constraints(theta, xyz_bxbybz(j,:), robot.robot_ree_T_tee, tolerance,...
%                 theta_prev,seg_time,motor_vel,j,frst_idx);
%         frst_idx = false;
%     end
% end


% figure(1); hold on; daspect([1,1,1])
% plot( data(:,1),data(:,2),'r','linewidth',2 )
% plot( data(:,1),data(:,3),'b','linewidth',2 )
% plot( data(:,1),data(:,4),'m','linewidth',2 )
% plot( data(:,1),data(:,5),'g','linewidth',2 )
% plot( data(:,1),data(:,6),'k','linewidth',2 )
% xlabel('Velocity Constraint in mm/s')
% ylabel('% Samples having complete solution (50 sample set)')
% set(gcf,'color',[1,1,1])
% title('Comparison between layers')
% legend('Random','Pos&Ori','Prf Max','Continuity','No max prf')



% figure(1);
% camlight; camlight;
% 
% for i = 1:size(apprx_sol,2)
%     X = apprx_sol(:,i);
%     T = eye(4);
%     T(1:3,4) = X(1:3)';
%     T(1:3,1:3) = quat2rotm( X(4:7)' ); 
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,T);
%     part_plt(v_part_transf, f_part, name_part, [0.64,0.64,0.64]); % plot tool stl
% %     temp = (quat2rotm(X(4:7)') * xyz_bxbybz(:,1:3)' + X(1:3))';
% %     scatter3( temp(:,1),temp(:,2),temp(:,3),20,'r','filled' );
% end
% 
% bck_color = [ 0.2,0.2,0.2 ];
% set(gcf,'color',bck_color);
% set(gca,'color',bck_color);
% axis off;
% joints1 = [0;0;0;-pi/2;0;pi/2;0];
% % Plot draping robot
% plot_robot(joints1,robot_tool,robot);
% daspect([1,1,1]);
% return;




% %%%%%%%%%%%%%%% New Architecture. Pos/Ori and Continuity iterative scheme

% Collision values for tool paths are same for every part pose as only tool
% is being considered. Orientation might still effect the c_cost but they
% are coming out to be the same due to zero violations and pretty much
% similar reachability over all orientations. 

% Better way might be an increase in overall reachability than manip or
% anything. Then verification by IK and quantification of continuity.
% samples = csvread(fullfile(csv_path,'C_map_samples.csv'));
% apprx_sol = [];
% tool_path = {};
% for idx = 1:size(samples,2)
%     fprintf('Stage-1. Sample:  %d\n',idx);
%     X = samples(:,idx);
%     red_vec_upd = red_vec;
%     isConvrg = false;
%     prev_X = X;
%     while ~isConvrg
%         [tool_path{idx},c_cost(idx,1)] = get_initial_guesses( X,red_vec_upd,cap_map,xyz_bxbybz,grp_idx,range_y );
%         % Modifying redundancy vectors as per new tool path
%         red_vec_upd = ff_voxel_sets( range_x, range_y, robot.robot_ree_T_tee, tool_path{idx},...
%                     sphere_approx,edt_map,mold_voxels.box_len/1000,mold_voxels.no_vxls,mold_voxels.voxel_dim,vxl_T_orig );
%                 
%         least_viol = chck_apprx_reach( X,red_vec_upd,tool_path{idx},cap_map  );
% %         fprintf('Using gradient descent to bring down violations\n');
%         
%         while true
%             opt_idx = 0;
%             states = get_actions( X );
%             for i = 1:size(states,2)
%                 state_viol = chck_apprx_reach( states(:,i),red_vec_upd,tool_path{idx},cap_map  );
%                 if state_viol < least_viol
%                     least_viol = state_viol;
%                     opt_idx = i;
%                 end
%             end
% 
%             if opt_idx == 0
%                 break;
%             else
%                 X = states(:,opt_idx);
%             end
%         end
%         if isequal(prev_X,X)
%             isConvrg = true;
%             apprx_sol = [apprx_sol, X];
%         else
%             prev_X = X;
%         end
%     end
% end
% csvwrite(fullfile(csv_path,'M_samples.csv'),apprx_sol);
% save( fullfile(csv_path,'tool_path.mat'),'tool_path' )



% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Maximizing performance measures
% disp('Maximizing kinematic performance over the seeds.......')
% samples = csvread(fullfile(csv_path,'C_map_samples.csv'));
% load(fullfile(csv_path,'tool_path.mat'));
% 
% prtz_sol = [];
% tic;
% for idx = 1:size(samples,2)
%     red_vec_upd = ff_voxel_sets( range_x, range_y, robot.robot_ree_T_tee, tool_path{idx},...
%                     sphere_approx,edt_map,mold_voxels.box_len/1000,mold_voxels.no_vxls,mold_voxels.voxel_dim,vxl_T_orig );
%                 
%     fprintf('Improving sample no:  %d\n',idx)
%     X = samples(:,idx);
%     [init_viol,max_prf] = compute_performance( X,red_vec_upd,tool_path{idx},kine_map,cap_map  );
%     prev_prf = max_prf;
%     % Using Gradient Descent to maximize performance
%     flag = true;
%     opt_idx = 0;
%     while flag
%         states = get_actions( X );
%         for i = 1:size(states,2)
%             [viol,prf] = compute_performance( states(:,i),red_vec_upd,tool_path{idx},kine_map,cap_map  );
%             if viol <= init_viol
%                 if prf > max_prf
%                     max_prf = prf;
%                     opt_idx = i;
%                 end
%             end
%         end
% 
%         if opt_idx == 0
%             disp('Couldnt Increase Performance')
%             break;
%         else
%             if max_prf == prev_prf
%                 flag = false;
%             else
%                 X = states(:,opt_idx);
%                 prev_prf = max_prf;
%             end
%         end
%     end
%     prtz_sol = [prtz_sol, X];
% end
% toc;
% csvwrite(fullfile(csv_path,'K_map_samples.csv'),prtz_sol);