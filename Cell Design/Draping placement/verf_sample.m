function [init_seeds,status] = verf_sample( X,xyz_bxbybz,tolerance,SimtrajOptions,...
                        theta_lb,theta_ub,ff_T_tcps,grp_idx,tcp_idx,velocities,motor_vel )
    % Simulate the trajectory for the given part pose and find error
    rotation = quat2rotm(X(4:7)');
    
    % Translating and rotating points
    xyz_bxbybz(:,1:3) = (rotation * xyz_bxbybz(:,1:3)' + X(1:3))';
    
    % Rotating Orientations
    xyz_bxbybz(:,4:6) = (rotation * xyz_bxbybz(:,4:6)')';
    xyz_bxbybz(:,7:9) = (rotation * xyz_bxbybz(:,7:9)')';
    xyz_bxbybz(:,10:12) = (rotation * xyz_bxbybz(:,10:12)')';
    
    status = true;
    I = eye(4);
    for i = 1:size(grp_idx,1)
        joint_angle = [0.31189,0.2209,-0.1785,-1.5357,0.0176,1.3463,0]';
        frst_idx = true;
        for j = grp_idx(i,1):grp_idx(i,2)
            ff_T_ee = ff_T_tcps(:,:,tcp_idx(j));
            [joint_angle,f_val] = fmincon( @(theta) trajSim_error( theta, xyz_bxbybz(j,:),...
                ff_T_ee),joint_angle, [],[],[],[],theta_lb,theta_ub,...
                @(theta)z_constr(theta, xyz_bxbybz(j,:), ff_T_ee, tolerance,velocities(j,:)',motor_vel,...
                    joint_angle,frst_idx),SimtrajOptions );
            ff_base = get_iiwa_FK_mex(joint_angle,I) * ff_T_ee;
            frst_idx = false;
            init_seeds(:,j) = joint_angle;
            % Position Error
            if norm( xyz_bxbybz(j,1:3) - ff_base(1:3,4)' ) > tolerance(1)
                status = false;
%                 return;
            end
            % Y-Orientation Error
            if acos( xyz_bxbybz(j,7)*ff_base(1,2) + ...
                    xyz_bxbybz(j,8)*ff_base(2,2)+ xyz_bxbybz(j,9)*ff_base(3,2) ) > tolerance(2)
                status = false;
%                 return;
            end
            % Z-Orientation Error
            angle = acos( xyz_bxbybz(j,10)*ff_base(1,3) +...
                xyz_bxbybz(j,11)*ff_base(2,3) + xyz_bxbybz(j,12)*ff_base(3,3) );
            if angle > tolerance(4) + 1e-4
                status = false;
%                 return;
            end
        end
    end
end