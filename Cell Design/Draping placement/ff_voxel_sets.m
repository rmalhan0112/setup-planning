function red_vec = ff_voxel_sets( range_x, range_y, ff_T_ee, xyz_bxbybz,tool_spheres,...
                    edt_map,box_len,no_vxls,voxel_dim,vxl_T_orig,tcp_idx,tool_tcps )
    red_vec = {};
    I = eye(4);
    for i = 1:size(ff_T_ee,3)
        % Provide all transformations from flange to discrete tcps
        T_inv(:,:,i) = I;
        T_inv(1:3,1:3,i) = ff_T_ee(1:3,1:3,i)' ;
        T_inv(1:3,4,i) = -ff_T_ee(1:3,1:3,i)' * ff_T_ee(1:3,4,i);
        
        % Provide all transformations from roller origin to discrete tcps
        inv_tcps(:,:,i) = I;
        inv_tcps(1:3,1:3,i) = tool_tcps(1:3,1:3,i)' ;
        inv_tcps(1:3,4,i) = -tool_tcps(1:3,1:3,i)' * tool_tcps(1:3,4,i);
    end
    
%     figure(1);
%     hold on;
%     xlim([-0.1,0.7]);
%     ylim([-0.1,0.7]);
%     zlim([-0.0467,0.8]);
%     daspect([1,1,1])      
% %     [p,q,r] = sphere;
%     camlight;
%     axis off;
%     set(gcf,'color',[0.2,0.2,0.2])
    
    
%     fl_path = [];
    
    for pt = 1:size(xyz_bxbybz,1)
        red_data = [];
        point = xyz_bxbybz(pt,:);
        R_pt = [ point(4:6)',point(7:9)',point(10:12)' ];
        for i = 1:size(range_x,1)
            for j = 1:size(range_y,1)
                w_T_ee = I;
                w_T_ee(1:3,1:3) = R_pt * eul2rotm( [ 0,range_y(j),range_x(i) ],'ZYX' );
                w_T_ee(1:3,4) = point(1:3)';
                w_T_ff = w_T_ee * T_inv(:,:,tcp_idx(pt));
                red_data = [ red_data; [w_T_ff(1:3,4)',w_T_ff(1:3,3)',...
                        iscolliding( tool_spheres,edt_map,box_len,no_vxls,voxel_dim,vxl_T_orig*w_T_ee*inv_tcps(:,:,tcp_idx(pt)) )] ];
                    
                
%                 % Plot tool and check
% %                 if i==1 && j==1
% %                     fl_path = [fl_path; [w_T_ff(1,4),w_T_ff(2,4),w_T_ff(3,4)] ];
% %                 end
%                 
% %                 if i==1 && j==1
%                 if iscolliding( tool_spheres,edt_map,box_len,no_vxls,voxel_dim,vxl_T_orig*w_T_ee*inv_tcps(:,:,tcp_idx(pt)) )==1
%                     c = [1,0,0];
%                 else
%                     c = [0,1,0];
%                 end
%                 
%                 [v_part, f_part, n_part, name_part] = stlRead('Mold_Ascent.stl'); % read tool stl
%                 v_part(:,1:3) = v_part(:,1:3)./1000;
%                 part_plt(v_part, f_part, name_part, [0.64,0.64,0.64]); % plot tool stl
% 
%                 
%                 [v_part, f_part, n_part, name_part] = stlRead('roller.stl'); % read tool stl
%                 
%                 [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,w_T_ee*inv_tcps(:,:,tcp_idx(pt)));
%                 part_plt(v_part_transf, f_part, name_part, c); % plot tool stl
%                 
%                 
% %                 quiver3( w_T_ff(1,4),w_T_ff(2,4),w_T_ff(3,4),w_T_ff(1,1),w_T_ff(2,1),w_T_ff(3,1),'r','AutoScaleFactor',0.1 );
% %                 quiver3( w_T_ff(1,4),w_T_ff(2,4),w_T_ff(3,4),w_T_ff(1,2),w_T_ff(2,2),w_T_ff(3,2),'g','AutoScaleFactor',0.1 );
% %                 quiver3( w_T_ff(1,4),w_T_ff(2,4),w_T_ff(3,4),w_T_ff(1,3),w_T_ff(2,3),w_T_ff(3,3),'b','AutoScaleFactor',0.1 );
% %                 
%                 
% %                 voxel_T_ee = w_T_ee*inv_tcps(:,:,tcp_idx(pt));
% %                 tool_spheres(:,1:3) = (voxel_T_ee(1:3,1:3)*tool_spheres(:,1:3)' + voxel_T_ee(1:3,4))';
% %                 tool_spheres(:,4) = tool_spheres(:,4)./1000;
% %                 for i=1:size(tool_spheres,1)
% %                     surf( p*tool_spheres(i,4)+tool_spheres(i,1),...
% %                             q*tool_spheres(i,4)+tool_spheres(i,2),...
% %                             r*tool_spheres(i,4)+tool_spheres(i,3) );
% %                 end
%                 
%                 pause(0.0000001);
% %                 cla;
% %                 end
            end
        end
        
%         plot3(fl_path(:,1),fl_path(:,2),fl_path(:,3),'b','linewidth',3);
        
        red_vec{pt} = red_data;
    end
 end