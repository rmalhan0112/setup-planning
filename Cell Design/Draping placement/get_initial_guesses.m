function [tool_path,c_cost] = get_initial_guesses( X,red_vec,cap_map,xyz_bxbybz,grp_idx,range_y )
    tool_path = xyz_bxbybz;
    
    r_indices = compute_reachindices( X,red_vec,cap_map  );
%     rotation = quat2rotm(X(4:7)');
%     xyz_bxbybz(:,1:3) = (rotation * xyz_bxbybz(:,1:3)' + X(1:3))';
%     xyz_bxbybz(:,4:6) = (rotation * xyz_bxbybz(:,4:6)')';
    
    % Determine initial pose for minimum continuity cost
    % Use some hacked up way. Replace with Dijkstra
    for i = 1:size(grp_idx,1)
        wp1 = r_indices(grp_idx(i,1),:).*range_y';
        paths = [];
        for j = 1:size(wp1,2)
            curr_ori = wp1(j);
            tot_cost(j) = 0;
            counter = 1;
            for k = grp_idx(i,1)+1:grp_idx(i,2)
                [cost,idx] = min( exp(abs( r_indices(k,:).*range_y' - curr_ori )) );
                paths(counter,j) = idx;
                tot_cost(j) = tot_cost(j) + cost;
                counter = counter + 1;
            end
        end
        [cost,idx] = min( tot_cost );
        path_idx = [ idx; paths(:,idx) ];
        c_cost(i,1) = cost;
        
        for j = 1:size(path_idx,1)
            set_vecs = red_vec{grp_idx(i,1)};
            % We donot want to translate and rotate the position and
            % orientation in this stage as that operation will be conducted
            % in the verification stage for each pose.
%             set_vecs(:,4:6) = ( rotation * set_vecs(:,4:6)')';
        
            % Manipulating the tool path
            % Applying Translation for current part position
%             tool_path(grp_idx(i,1),1:3) = xyz_bxbybz(grp_idx(i,1),1:3);
            bz = set_vecs(path_idx(j),4:6);
            bx = xyz_bxbybz(grp_idx(i),4:6);
            by = cross(bz,bx);
            by = by / norm(by);
            bx = cross(by,bz);
            bx = bx / norm(bx);
            tool_path(grp_idx(i,1),4:6) = bx;
            tool_path(grp_idx(i,1),7:9) = by;
            tool_path(grp_idx(i,1),10:12) = bz;
        end
%         [joints,status] = get_iiwa_IK( [0.31189,0.2209,-0.1785,-1.5357,0.0176,1.3463,0]',...
%             point,tolerance,SimtrajOptions,theta_lb,theta_ub,transformation );
%         init_jts = [init_jts, [0.31189,0.2209,-0.1785,-1.5357,0.0176,1.3463,0]' ];
%         if status
%             init_jts = [init_jts, joints];
%         else
%             init_jts = [init_jts, [0.31189,0.2209,-0.1785,-1.5357,0.0176,1.3463,0]'];
%         end
    end
    c_cost = mean(c_cost);
end