function [error] = eval_reach_cost( X, X_fixed, xyz_bxbybz,...
                                    grp_idx, ff_T_tcps, tolerance,theta_lb,theta_ub,SimtrajOptions,seg_time,velocities,motor_vel,...
                                    tool_spheres,edt_map,box_len,no_vlxs,voxel_dim,vxl_T_orig,init_seeds,tcp_idx,inv_tcps)
    global status;
    X = [ X(1:2); X_fixed(1); X(3); X_fixed(2:3); X(4) ];
    X(4:7) = X(4:7)/norm(X(4:7));
    
    % Simulate the trajectory for the given part pose and find error
    rotation = quat2rotm(X(4:7)');
    
    % Translating and rotating points
    xyz_bxbybz(:,1:3) = (rotation * xyz_bxbybz(:,1:3)' + X(1:3))';
    
    % Rotating Orientations
    xyz_bxbybz(:,4:6) = (rotation * xyz_bxbybz(:,4:6)')';
    xyz_bxbybz(:,7:9) = (rotation * xyz_bxbybz(:,7:9)')';
    xyz_bxbybz(:,10:12) = (rotation * xyz_bxbybz(:,10:12)')';
    
    orig_T_w = eye(4);  % World to origin of the part
    orig_T_w(1:3,1:3) = rotation';
    orig_T_w(1:3,4) = -rotation' * X(1:3);
    
    % Initialize Copies of Manipulator
    error = 0;
    status = true;
    app_coll = false;
    no_fail = 0;
    I = eye(4);
    
    for i = 1:size(grp_idx,1)
        joint_angle = [0.31189,0.2209,-0.1785,-1.5357,0.0176,1.3463,0]';
        frst_idx = true;
        for j = grp_idx(i,1):grp_idx(i,2)
            ff_T_ee = ff_T_tcps(:,:,tcp_idx(j));    % Which TCP to be used. FK will give location of new TCP
            % Finding the trajectory
            [joint_angle,f_val] = fmincon( @(theta) trajSim_error( theta, xyz_bxbybz(j,:),...
                ff_T_ee),joint_angle, [],[],[],[],theta_lb,theta_ub,...
                @(theta) get_constraints(theta, xyz_bxbybz(j,:), ff_T_ee, tolerance,joint_angle,...
                seg_time,velocities(j,:),motor_vel,j,frst_idx,tool_spheres,edt_map,box_len,no_vlxs,voxel_dim,...
                vxl_T_orig,orig_T_w,inv_tcps(:,:,tcp_idx(j)),app_coll),...
                SimtrajOptions );
            frst_idx = false;
            error = error + f_val;
            joint_angles(j,:) = joint_angle';
            w_T_ee = get_iiwa_FK_mex(joint_angle,I) * ff_T_ee;
            
            % Position Error
            if norm( xyz_bxbybz(j,1:3) - w_T_ee(1:3,4)' ) > tolerance(1)
                status = false;
            end
            % Y-Orientation Error
            if acos( xyz_bxbybz(j,7)*w_T_ee(1,2) + ...
                    xyz_bxbybz(j,8)*w_T_ee(2,2)+ xyz_bxbybz(j,9)*w_T_ee(3,2) ) > tolerance(2)
                status = false;
            end
            % Z-Orientation Error
            angle = acos( xyz_bxbybz(j,10)*w_T_ee(1,3) +...
                xyz_bxbybz(j,11)*w_T_ee(2,3) + xyz_bxbybz(j,12)*w_T_ee(3,3) );
            if angle > tolerance(4) + 1e-4
                status = false;
            end

            % Check for collision. Collision Function should be provided
            % roller origin wrt voxel frame of the mold
            if iscolliding( tool_spheres,edt_map,box_len,no_vlxs,voxel_dim,vxl_T_orig*orig_T_w*w_T_ee*inv_tcps(:,:,tcp_idx(j)) )==1
                app_coll = true;
                [joint_angle,f_val] = fmincon( @(theta) trajSim_error( theta, xyz_bxbybz(j,:),...
                ff_T_ee),joint_angle, [],[],[],[],theta_lb,theta_ub,...
                @(theta) get_constraints(theta, xyz_bxbybz(j,:), ff_T_ee, tolerance,joint_angle,...
                seg_time,velocities(j,:),motor_vel,j,frst_idx,tool_spheres,edt_map,box_len,no_vlxs,voxel_dim,...
                vxl_T_orig,orig_T_w,inv_tcps(:,:,tcp_idx(j)),app_coll),...
                SimtrajOptions );
                w_T_ee = get_iiwa_FK_mex(joint_angle,I) * ff_T_ee;
                if iscolliding( tool_spheres,edt_map,box_len,no_vlxs,voxel_dim,vxl_T_orig*orig_T_w*w_T_ee )==1
                    status = false;
                end
                % Position Error
                if norm( xyz_bxbybz(j,1:3) - w_T_ee(1:3,4)' ) > tolerance(1)
                    status = false;
                end
                % Y-Orientation Error
                if acos( xyz_bxbybz(j,7)*w_T_ee(1,2) + ...
                        xyz_bxbybz(j,8)*w_T_ee(2,2)+ xyz_bxbybz(j,9)*w_T_ee(3,2) ) > tolerance(2)
                    status = false;
                end
                % Z-Orientation Error
                angle = acos( xyz_bxbybz(j,10)*w_T_ee(1,3) +...
                    xyz_bxbybz(j,11)*w_T_ee(2,3) + xyz_bxbybz(j,12)*w_T_ee(3,3) );
                if angle > tolerance(4) + 1e-4
                    status = false;
                end
                app_coll = false;
            end
            if ~status 
                no_fail = no_fail + 1;
                if no_fail <= 2
                    status = true;
                end
            end
        end
    end
    
    if status
        error = 1e-15;
    end
end