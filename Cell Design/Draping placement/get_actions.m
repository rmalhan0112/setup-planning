function states = get_actions( pose,visited_samples )
    step = 0.01;
    states = [];
    for i = [1,2,4,7]
        states = [ states, pose ];
        states(i,end) = states(i,end) + step;
        states(4:7,end) = states(4:7,end)/norm(states(4:7,end));
        
        if ismember(states(:,end)',visited_samples','rows')
            states(:,end) = [];
        end
        
        states = [ states, pose ];
        states(i,end) = states(i,end) - step;
        states(4:7,end) = states(4:7,end)/norm(states(4:7,end));
        
        if ismember(states(:,end)',visited_samples','rows')
            states(:,end) = [];
        end
    end
end