function [violations,ovl_viol,obj] = compute_performance( X,red_vec,xyz_bxbybz,kine_map,cap_map,tot_ori  )
    rotation = quat2rotm(X(4:7)');
%     no_pts = size(xyz_bxbybz,1);
    violations = size(xyz_bxbybz,1);
    ovl_viol = 0;
%     overall_reach = tot_ori;
    performance = [];
% Checking orientation
    for i = 1:size(red_vec,2)
        set_vecs = red_vec{i};
        % Applying Translation for current part position
        set_vecs(:,1:3) = (rotation * set_vecs(:,1:3)' + X(1:3))';
        set_vecs(:,4:6) = (rotation * set_vecs(:,4:6)')';
        flag = true;
        measure = [];
        for j = 1:size(set_vecs,1)
            if set_vecs(j,7)==1
                continue;
            end
            key = gen_cmap_key( set_vecs(j,1:3) );
            if isKey( cap_map,key )
                ori_cone = cap_map(key);
                angle = acos( ori_cone(1) * set_vecs(j,4) + ori_cone(2) * set_vecs(j,5) +...
                        ori_cone(3) * set_vecs(j,6) );
                if (angle >= ori_cone(4) && angle <= ori_cone(5))    
                    if flag
                        violations = violations - 1;
                        flag = false;
                    end
                    prf_idx = kine_map(key);
                    measure = [measure; prf_idx(1)];
                else
%                     overall_reach = overall_reach - 1;
                    ovl_viol = ovl_viol + 1;
                end
            else
%                 overall_reach = overall_reach - 1;
                ovl_viol = ovl_viol + 1;
            end
        end
        performance = [performance; mean(measure)]; % Accumulating for all waypoints   
    end
%     obj = 0.5 * (performance/(no_pts*0.11)) +...
%         0.5 * ( overall_reach/tot_ori );
    obj = min(performance);
end