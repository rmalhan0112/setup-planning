poses = [
0.5600    0.6400    0.1000    0.2295         0         0    0.9733;
% 0.5600    0.6300    0.1000    0.2295         0         0    0.9733;
% 0.5600    0.6200    0.1000    0.2295         0         0    0.9733;
0.5600    0.6100    0.1000    0.2295         0         0    0.9733;
0.5600    0.6100    0.1000    0.2390         0         0    0.9710;
0.5600    0.6000    0.1000    0.2390         0         0    0.9710;
% 0.5700    0.6000    0.1000    0.2390         0         0    0.9710;
0.5700    0.6000    0.1000    0.2413         0         0    0.9704
];


figure(1); hold on; daspect([1,1,1]);
plot_robot([0.31189,0.2209,-0.1785,-1.5357,0.0176,1.3463,0]',false,robot);
set(gcf,'color',[0.2,0.2,0.2]);
axis off;
camlight;


for i = 1:size(poses,1)
    yo = eye(4);
    yo(1:3,4) = poses(i,1:3)';
    yo(1:3,1:3) = quat2rotm(poses(i,4:7));
    [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,yo);
    if i == size(poses,1)
        part_plt(v_part_transf, f_part, name_part, [0.64,0.64,0.64],1); % plot tool stl
    else
        part_plt(v_part_transf, f_part, name_part, [0.64,0.64,0.64],0.3); % plot tool stl
    end
end