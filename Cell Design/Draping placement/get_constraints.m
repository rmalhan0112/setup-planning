function [c,ceq] = get_constraints( theta, xyz_bxbybz, ff_T_ee, tolerance,theta_prev,seg_time,velocity,motor_vel,idx,frst_idx,...
                                        tool_spheres,edt_map,box_len,no_vxls,voxel_dim,vxl_T_orig,prt_T_w,inv_tcp,app_coll )
    ceq = [];
%     FK_all = get_iiwa_FK_all_joints_mex(theta,eye(4));  %iiwa7
    FK_all = get_iiwa14_FK_all_joints_mex(theta,eye(4));   % iiwa14
    w_T_ee = FK_all(33:36,:) * ff_T_ee;
    
%     % Plotting
%     if app_coll
%         cll = iscolliding( tool_spheres,edt_map,box_len,no_vxls,voxel_dim,vxl_T_orig*prt_T_w*w_T_ee*inv_tcp );
%         if cll==1
%             c = [1,0,0];
%         else
%             c = [0,1,0];
%         end
%     end
    

%     [v_part, f_part, n_part, name_part] = stlRead('roller.stl'); % read tool stl
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,w_T_ee*inv_tcp);
%     robot.base_T_robot = eye(4);
%     robot.robot_ree_T_tee = ff_T_ee;
%     plot_robot(theta,false,robot);
%     part_plt(v_part_transf, f_part, name_part, c); % plot tool stl

    
    
    % Z-Orientation Error
    angle = real (acos( xyz_bxbybz(10)*w_T_ee(1,3) +...
        xyz_bxbybz(11)*w_T_ee(2,3) + xyz_bxbybz(12)*w_T_ee(3,3) ) );
    c = angle - tolerance(4);
    
    if app_coll
        % Collision clearance - (d-r) <= 0
        c = [c; 0.005 - coll_error( tool_spheres,edt_map,box_len,no_vxls,voxel_dim,vxl_T_orig*prt_T_w*w_T_ee*inv_tcp )];
    end
    
    jac_curr = get_iiwa_GeoJac( FK_all );
    % Instantaneous Velocity Constraint
    c = [c; abs(pinv(jac_curr) * velocity') - motor_vel];
    
    % Transition Velocity Constraint
    c = [c; abs(theta-theta_prev)./motor_vel - seg_time(idx)];  % Bounding by Time
    
    % Continuity Constraint
    if ~frst_idx
%         FK_all_prev = get_iiwa_FK_all_joints_mex(theta_prev,eye(4));    %iiwa7
        FK_all_prev = get_iiwa14_FK_all_joints_mex(theta_prev,eye(4));   % iiwa14
        
        jac_prev = get_iiwa_GeoJac( FK_all_prev );
        c = [c; -corr2( jac_prev,jac_curr ) + 0.9];
    end
    
    % Force Constraint
%     if any( c > 0 )
%         disp('Bug')
%     end
end