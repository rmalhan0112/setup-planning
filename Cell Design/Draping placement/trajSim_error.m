function error = trajSim_error( theta, xyz_bxbybz, ff_T_ee)
    ff_base = get_iiwa_FK_mex(theta,eye(4)) * ff_T_ee;
    error = 0.5 * ( 0.25*(1 - ( xyz_bxbybz(7)*ff_base(1,2) + xyz_bxbybz(8)*ff_base(2,2) + xyz_bxbybz(9)*ff_base(3,2))) +...
            (xyz_bxbybz(1)-ff_base(1,4))^2 + (xyz_bxbybz(2)-ff_base(2,4))^2 + (xyz_bxbybz(3)-ff_base(3,4))^2 );
end