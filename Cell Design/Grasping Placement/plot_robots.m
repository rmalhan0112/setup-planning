clear all;
close all;
clc


part_name = 'Mold_ascent';
% part_name = 'GE90'; 
% part_name = 'custom';


roller = 'roller.stl';
gripper = 'robotiq.stl';
solv_IK = true;


voxel_T_part = eye(4);
world_T_part = eye(4);

if isequal(part_name,'Mold_ascent')
    grsp1 = fullfile( '/home/rmalhan/Work/USC/Composite Automation/Modules/cell_layout/grasping_region/Mold Ascent','grasp_voxels1.mat' );
    grsp2 = fullfile( '/home/rmalhan/Work/USC/Composite Automation/Modules/cell_layout/grasping_region/Mold Ascent','grasp_voxels2.mat' );
    voxel_T_part(1:3,4) = [ 200;200;46.73 ]'./1000;
    % Demo Pose
%     world_T_part(1:3,4) = [0.7138;    0.1087;    0.0532];    
%     world_T_part(1:3,1:3) = quat2rotm( [0.6220   -0.0005   -0.0020    0.7830] );

    % Actual Pose Setup Planner
%     world_T_part(1:3,4) = [0.829;  0.217;  0.0467];    
%     world_T_part(1:3,1:3) = quat2rotm( [0.5334, 0,  0,  0.8458] );
    
%     % Pose being used for grasp planner exp
%     world_T_part(1:3,4) = [0.8;  0.25;  0.0467];    
%     world_T_part(1:3,1:3) = quat2rotm( [0, 0,  0,  1] );
    
%     % Pose for MSEC2019
    world_T_part(1:3,4) = [0.35;  -0.19;  0.0467];    
    world_T_part(1:3,1:3) = quat2rotm( [1, 0,  0,  0] );
    

    x1 = [ 0.25,  0.48,  -0.88 ]; % Solution  
    x2 = [ 0.71, -1.47, 0.65 ];
    part = 'Mold_Ascent.stl';
    
    joints1 = [0.65808,0.8036,0.27772,-1.3121,-0.3636,1.2209,-1.0419]';
    
elseif isequal(part_name,'GE90')
    grsp1 = fullfile( '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/GE90','grasp_voxels1.mat' );
    grsp2 = fullfile( '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/GE90','grasp_voxels2.mat' );
    voxel_T_part(1:3,4) = [ 100;  138.062;  24.7876 ]./1000;
    world_T_part(1:3,4) = [0.6;    -0.1;    0.02478];    
    world_T_part(1:3,1:3) = quat2rotm( [ 0.8176         0         0    0.5757 ] );
    x1 = [ 0.6,  -1,  1.55 ];
    x2 = [ 0.4012, -0.0980, -1.571 ];
    part = 'GE90_scaled.stl';
    
    % First Point For IK Calc.
    pt = [ 13.486,178.91,70.403,-0.11056,0.6838,0.72124,0.98972,0.0095081,0.14271,0.090725,0.7296,-0.67783 ];
    pt(1,1:3) = pt(1,1:3)./1000;
    pt(1,1:3) = (world_T_part(1:3,1:3) * pt(1,1:3)' + world_T_part(1:3,4))';
    
    pt(1,4:6) = (world_T_part(1:3,1:3) * pt(1,4:6)')';
    pt(1,7:9) = (world_T_part(1:3,1:3) * pt(1,7:9)')';
    pt(1,10:12) = (world_T_part(1:3,1:3) * pt(1,10:12)')';
    
    
elseif isequal(part_name,'custom')
    grsp1 = fullfile( '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/Custom','grasp_voxels1.mat' );
    grsp2 = fullfile( '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/Custom','grasp_voxels2.mat' );
    voxel_T_part(1:3,4) = [ 80;  80;  85.4 ]./1000;
    world_T_part(1:3,4) = [0.5;    0.1;    0.0854];    
    world_T_part(1:3,1:3) = eul2rotm( [deg2rad(20),0,0],'ZYX' );
    x1 = [ 0.8; 0.2; 1.57 ];
    x2 = [ 0.9; 0.5; -pi ];
    part = 'custom.stl';
    
    pt = [13.043,7.2625,-0.08,-0.9996,0.028338,0,0.028338,0.9996,-0,-0,-0,-1];
    pt(1,1:3) = pt(1,1:3)./1000;
    pt(1,1:3) = (world_T_part(1:3,1:3) * pt(1,1:3)' + world_T_part(1:3,4))';
    
    pt(1,4:6) = (world_T_part(1:3,1:3) * pt(1,4:6)')';
    pt(1,7:9) = (world_T_part(1:3,1:3) * pt(1,7:9)')';
    pt(1,10:12) = (world_T_part(1:3,1:3) * pt(1,10:12)')';
    
end
        


% Robot - 1
grasp_T_vxl1 = eye(4);
% Put the values of xyphi here
grasp_T_vxl1(1:3,4) = [ x1(1); x1(2); 0 ];
grasp_T_vxl1(1:3,1:3) = eul2rotm( [x1(3),0,0],'ZYX' );


% Robot - 2
grasp_T_vxl2 = eye(4);
% Put the values of xyphi here
grasp_T_vxl2(1:3,4) = [ x2(1); x2(2); 0 ];
grasp_T_vxl2(1:3,1:3) = eul2rotm( [x2(3),0,0],'ZYX' );




% creates a 3D space in which the robot will operate
run map_initialization.m;
load(grsp1)
load(grsp2)

%%%%%%%% Draping robot AND TOOL DEFINITION %%%%%%%%%%%%%
robot1.base_T_robot = eye(4);
robot1.base_T_robot(1:3,4) = [0;0;0];
robot1.base_T_robot(1:3,1:3) = eul2rotm([0.0,0,0]);

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [-0.049; 0;  0.133]; % For roller tool
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%




%%%%%%%% Grasping robot AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
robot2.base_T_robot = world_T_part * inv(voxel_T_part) * inv(grasp_T_vxl1);

%%%%% Tool to Robot transformation is defined here
robot2.robot_ree_T_tee = eye(4);
robot2.robot_ree_T_tee(1:3,4) = [0; 0; 0.132]; % For robotiq tool
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


%%%%%%%% Grasping robot 2 AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
robot3.base_T_robot = world_T_part * inv(voxel_T_part) * inv(grasp_T_vxl2);

%%%%% Tool to Robot transformation is defined here
robot3.robot_ree_T_tee = eye(4);
robot3.robot_ree_T_tee(1:3,4) = [0; 0; 0.132]; % For robotiq tool
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%






theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 1500;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-8;
options.StepTolerance = 1e-8;
options.Display = 'off';
options.SpecifyObjectiveGradient = false;
options.ObjectiveLimit = 1e-8; 
    
    

    
% [joints1,status] = get_iiwa_IK( [0;0;0;-pi/2;0;pi/2;0],...
%         pt,[0.003,0.0524],options,theta_lb,theta_ub,robot1,false,3 );
% joints1'















% Check if IK is possible for all points in region
% cones = [];
% no_samples = 50;
% values = [-1:0.01:1];
% 
% for i=1:size(grasp_voxels1,1)
%     counter = 1;
%     while counter < no_samples
%         bx_dash = randsample(values,1);
%         by_dash = randsample(values,1);
%         bz_dash = randsample(values,1);
%         vec = [bx_dash,by_dash,bz_dash];
%         vec = vec / norm(vec);
%         if acos( vec(1)*grasp_voxels1(i,4) + vec(2)*grasp_voxels1(i,5) + vec(3)*grasp_voxels1(i,6) ) > grasp_voxels1(i,7)
%             continue;
%         end
%         counter = counter + 1;
%         cones = [ cones; [grasp_voxels1(i,1:3),vec ]];
%     end
% end
%         
% viol = size(cones,1)
% for i=1:size(cones,1)
%     [joints2,status] = get_IK_index( cones,i,world_T_part,voxel_T_part,robot2,base_T_robot2 );
%     if status
%         viol = viol - 1;
%     end
% end
% viol





% Check if IK is possible for all points in region
% cones = [];
% for i=1:size(grasp_voxels2,1)
%     counter = 1;
%     while counter < no_samples
%         bx_dash = randsample(values,1);
%         by_dash = randsample(values,1);
%         bz_dash = randsample(values,1);
%         vec = [bx_dash,by_dash,bz_dash];
%         vec = vec / norm(vec);
%         if acos( vec(1)*grasp_voxels2(i,4) + vec(2)*grasp_voxels2(i,5) + vec(3)*grasp_voxels2(i,6) ) > grasp_voxels2(i,7)
%             continue;
%         end
%         counter = counter + 1;
%         cones = [ cones; [grasp_voxels2(i,1:3),vec ]];
%     end
% end
%         
% viol = size(cones,1)
% for i=1:size(cones,1)
%     [joints2,status] = get_IK_index( cones,i,world_T_part,voxel_T_part,robot3,base_T_robot3 );
%     if status
%         viol = viol - 1;
%     end
% end
% viol


% viol = size(grasp_voxels2,1);
% for i=1:size(grasp_voxels2,1)
%     [joints3,status] = get_IK_index( grasp_voxels2,i,world_T_part,voxel_T_part,robot3,base_T_robot3 );
%     if status
%         viol = viol - 1;
%     end
% end
% viol




figure(1);
hold on;
[v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
v_part(:,1:3) = v_part(:,1:3)./1000;
bck_color = [ 0.2,0.2,0.2 ];
set(gcf,'color',bck_color);
set(gca,'color',bck_color);
axis off
camlight;

disp('Robot2')
robot2.base_T_robot(1:3,4)'
rotm2eul( robot2.base_T_robot(1:3,1:3),'ZYX' )

disp('Robot3')
robot3.base_T_robot(1:3,4)'
rotm2eul( robot3.base_T_robot(1:3,1:3),'ZYX' )


if solv_IK
    % For grasping point in the middle
    [joints2,status] = get_IK_index( grasp_voxels1,35,world_T_part,voxel_T_part,robot2,robot2.base_T_robot );
    joints2'
    [joints3,status] = get_IK_index( grasp_voxels2,20,world_T_part,voxel_T_part,robot3,robot3.base_T_robot );
    joints3'
else
    joints1 = [pi/4;0;0;-pi/2;0;pi/2;0];
    joints2 = [0;0;0;-pi/2;0;pi/2;0];
    joints3 = [0;0;0;-pi/2;0;pi/2;0];
end

joints1 = [0;0;0;-pi/2;0;pi/2;0];
% Plot draping robot
plot_robot(joints1,roller,robot1);

% Plot grasping robot
plot_robot(joints2,gripper,robot2);

% Plot grasping robot
plot_robot(joints3,gripper,robot3);

daspect([1,1,1])

% Plot part
% Computing the transformation of part to draping robot base
[v_part_transf,n_part_transf] = stlTransform(v_part,n_part,world_T_part);
part_plt(v_part_transf, f_part, name_part, [0.642,0.642,0.642]); % plot tool stl
daspect([1,1,1]);

% Plot voxel frame
world_T_vxl = world_T_part * inv(voxel_T_part);
quiver3( world_T_vxl(1,4),world_T_vxl(2,4),world_T_vxl(3,4),...
        world_T_vxl(1,1),world_T_vxl(2,1),world_T_vxl(3,1),'r','linewidth',2, 'AutoScaleFactor', 0.1);
quiver3( world_T_vxl(1,4),world_T_vxl(2,4),world_T_vxl(3,4),...
        world_T_vxl(1,2),world_T_vxl(2,2),world_T_vxl(3,2),'g','linewidth',2, 'AutoScaleFactor', 0.1);
quiver3( world_T_vxl(1,4),world_T_vxl(2,4),world_T_vxl(3,4),...
        world_T_vxl(1,3),world_T_vxl(2,3),world_T_vxl(3,3),'b','linewidth',2, 'AutoScaleFactor', 0.1);
    
    
    
    
% Plot grasp voxels 1 and 2.
figure(1)
hold on;
daspect([1,1,1]);
xlabel('X')
ylabel('Y')
zlabel('Z')

% 1
wrld_T_vlx = world_T_part * inv(voxel_T_part);
grasp_voxels1(:,1:3) = grasp_voxels1(:,1:3)./1000;
R = wrld_T_vlx(1:3,1:3);
t = wrld_T_vlx(1:3,4);
transf_voxels = R*grasp_voxels1(:,1:3)' + t;
grasp_voxels1(:,1:3) = transf_voxels';
grasp_voxels1(:,4:6) = (R * grasp_voxels1(:,4:6)')';
    
scatter3( grasp_voxels1(:,1),grasp_voxels1(:,2),grasp_voxels1(:,3),20,'g','filled' );
quiver3( grasp_voxels1(:,1),grasp_voxels1(:,2),grasp_voxels1(:,3),...
         grasp_voxels1(:,4),grasp_voxels1(:,5),grasp_voxels1(:,6),'k');
     
rot = R * eul2rotm( [0,0,deg2rad(80)],'ZYX' );

% 2     
wrld_T_vlx = world_T_part * inv(voxel_T_part);
grasp_voxels2(:,1:3) = grasp_voxels2(:,1:3)./1000;
R = wrld_T_vlx(1:3,1:3);
t = wrld_T_vlx(1:3,4);
transf_voxels = R*grasp_voxels2(:,1:3)' + t;
grasp_voxels2(:,1:3) = transf_voxels';
grasp_voxels2(:,4:6) = (R * grasp_voxels2(:,4:6)')';

rot2 = R;

scatter3( grasp_voxels2(:,1),grasp_voxels2(:,2),grasp_voxels2(:,3),20,'g','filled' );
quiver3( grasp_voxels2(:,1),grasp_voxels2(:,2),grasp_voxels2(:,3),...
         grasp_voxels2(:,4),grasp_voxels2(:,5),grasp_voxels2(:,6),'k');
     
grid('off')




% Generate files for grasp voxel image
flange_pts(:,1:3) = grasp_voxels1(:,1:3);
for i=1:size(flange_pts,1)
    flange_pts(i,4:7) = rotm2quat( rot );
end
% csvwrite('grsp1_img.csv',flange_pts);

grip_points(:,1:3) = grasp_voxels1(:,1:3);
grip_points(:,1:3) = grip_points(:,1:3) + grasp_voxels1(:,4:6)*0.137;
grip_points(:,4:7) = flange_pts(:,4:7);
csvwrite('grsp1_img_robq.csv',grip_points);


clear('flange_pts')
clear('grip_points')
% Generate files for grasp voxel image
flange_pts(:,1:3) = grasp_voxels2(:,1:3);
for i=1:size(flange_pts,1)
    flange_pts(i,4:7) = rotm2quat( rot2 );
end
% csvwrite('grsp2_img.csv',flange_pts);

grip_points(:,1:3) = grasp_voxels2(:,1:3);
grip_points(:,1:3) = grip_points(:,1:3) + grasp_voxels2(:,4:6)*0.137;
grip_points(:,4:7) = flange_pts(:,4:7);
csvwrite('grsp2_img_robq.csv',grip_points);