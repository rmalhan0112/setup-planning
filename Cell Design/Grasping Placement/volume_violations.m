function no_violations = volume_violations( x,grasp_voxels, cap_map,approach_vecs )
% x is the x,y,phi values of origin of grasp voxels wrt world.
    grasp_voxels(:,1:3) = grasp_voxels(:,1:3)./1000;
    R = eul2rotm( [x(3),0,0],'ZYX' );
    t = [ x(1);x(2);0 ];
    grasp_voxels(:,1:3) = (R * grasp_voxels(:,1:3)' + t)';
    appr_vec = (R * approach_vecs(:,1:3)')';
    
    
%     % Plotting
%     figure(1);
%     hold on;
%     scatter3( grasp_voxels(:,1),grasp_voxels(:,2),grasp_voxels(:,3),20,'g','filled' );
%     quiver3( grasp_voxels(:,1),grasp_voxels(:,2),grasp_voxels(:,3),...
%         grasp_voxels(:,4),grasp_voxels(:,5),grasp_voxels(:,6),'k');
%     joints = [0;pi/4;0;-pi/4;0;pi/2;0];
% 
% 
%     % Plot draping robot
%     plot_robot(joints,robot1,false);
%     daspect([1,1,1])
    no_vxls = size(grasp_voxels,1);
    no_violations = no_vxls * size(appr_vec,1);
    % Compute violations by accessing the capability map
    for i = 1:no_vxls
        key = gen_cmap_key( grasp_voxels(i,1:3) );
        if isKey( cap_map,key )
            ori_cone = cap_map(key);
            for p = 1:size(appr_vec,1)
                angle = acos( ori_cone(1) * appr_vec(p,1) + ori_cone(2) * appr_vec(p,2) +...
                        ori_cone(3) * appr_vec(p,3) );
                if (angle >= ori_cone(4) && angle <= ori_cone(5))
                    no_violations = no_violations - 1;
                end
            end
        end
    end
end