stl_path = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes';
csv_path = '/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Grasping Placement/Ascent';
part_name = fullfile( stl_path, 'Mold_Ascent.stl' );
load(fullfile(csv_path,'mold_voxels.mat'))

% Mold hemisphere closer to draping robot. Viewing draping robot from the
% front. Robot 1 is to right. Robot 2 is to the left

% Declarations for robot-1
file_vxl1 = 'grasp_voxels1';
% Define cone for robot 1
cone_axis1 = [ 0;1;0 ];
cone_axis1 = eul2rotm( [0,0,-deg2rad(15)],'ZYX' ) * cone_axis1;
cone_angle1 = deg2rad(15);

% Bounds for generating grasping voxels
x_min1 = 300;
x_max1 = 700;
y_min1 = 210;
y_max1 = 285;
z_min1 = 90;
z_max1 = 160;


% Declarations for robot-2
file_vxl2 = 'grasp_voxels2';
% Define cone for robot 2
cone_axis2 = [ 0;-1;0 ];
cone_axis2 = eul2rotm( [0,0,deg2rad(15)],'ZYX' ) * cone_axis2;
cone_angle2 = deg2rad(15);

% Bounds for generating grasping voxels
x_min2 = 300;
x_max2 = 700;
y_min2 = 690;
y_max2 = 765;
z_min2 = 90;
z_max2 = 160;
