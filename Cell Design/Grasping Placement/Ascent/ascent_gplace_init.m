stl_path = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes';
csv_path = '/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/Cell Design/Grasping Placement/Ascent';
part_name = fullfile( stl_path, 'Mold_Ascent.stl' );
load(fullfile(csv_path,'mold_voxels.mat'))

[v_part, f_part, n_part, name_part] = stlRead(part_name); % read tool stl
v_part(:,1:3) = v_part(:,1:3)./1000;
mold_boundaries = [ min(v_part(:,1)),min(v_part(:,2));
                    max(v_part(:,1)),min(v_part(:,2));
                    max(v_part(:,1)),max(v_part(:,2));
                    min(v_part(:,1)),max(v_part(:,2))   ];
mold_boundaries = mold_boundaries';
if robot_no==1
    path_to_grspvxl1 = fullfile( csv_path, strcat('grasp_voxels1', '.mat') );
    load(path_to_grspvxl1);
    grasp_voxels = grasp_voxels1;
else
    path_to_grspvxl2 = fullfile( csv_path, strcat('grasp_voxels2', '.mat') );
    load(path_to_grspvxl2);
    grasp_voxels = grasp_voxels2;
end

lb = [0;-1.5;-3.14];
ub = [1.5;1.5;3.14];
workspace_bndr = [ lb(1), ub(2);
                   ub(1), ub(2);
                   ub(1), lb(2);
                   lb(1), lb(2)   ];
               
               
% The gripper needs to grip along the YZ plane. So following values will be
% 