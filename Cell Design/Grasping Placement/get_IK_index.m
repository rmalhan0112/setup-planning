function [joints,status] = get_IK_index( grasp_voxels,index,world_T_part,voxel_T_part,robot,base_T_robot )
    theta_lb(1) = -2.967059728390360;
    theta_lb(2) = -2.094395102393195;
    theta_lb(3) = -2.967059728390360;
    theta_lb(4) = -2.094395102393195;
    theta_lb(5) = -2.967059728390360;
    theta_lb(6) = -2.094395102393195;
    theta_lb(7) = -3.054326190990077;
    theta_lb = theta_lb';
    theta_ub = - theta_lb;

    options = optimoptions('fmincon','Algorithm', 'interior-point');
    options.MaxIterations = 1500;
    options.MaxFunctionEvaluations = 1e7;
    options.OptimalityTolerance = 1e-8;
    options.StepTolerance = 1e-8;
    options.Display = 'off';
    options.SpecifyObjectiveGradient = false;
    options.ObjectiveLimit = 1e-8; 
    
% Define point and frame for grasping robot 1
    wrld_T_vlx = world_T_part * inv(voxel_T_part);
    grasp_voxels(index,1:3) = grasp_voxels(index,1:3)./1000;
    R = wrld_T_vlx(1:3,1:3);
    t = wrld_T_vlx(1:3,4);
    point(1,1:3) = (R*grasp_voxels(index,1:3)' + t)';

    bx = [1,0,0];
    bz = grasp_voxels(index,4:6);
    by = cross(bz,bx);
    by = by / norm( by );
    bx = cross( by,bz );
    bx = bx / norm( bx );
    
    point( 1,4:12 ) = [ (R*bx')', (R*by')',  (R*bz')' ];
    temp = ( inv(base_T_robot) * [point(1,1:3)';1] )';
    point(1,1:3) = temp(1:3)';
    
    point(1,4:6) = ( base_T_robot(1:3,1:3)' * point(1,4:6)' )';
    point(1,7:9) = ( base_T_robot(1:3,1:3)' * point(1,7:9)' )';
    point(1,10:12) = ( base_T_robot(1:3,1:3)' * point(1,10:12)' )';
    
    [joints,status] = get_iiwa_IK( [0;0;0;-pi/2;0;pi/2;0],...
        point,[0.003,0.0524,0.0524,0.0524],options,theta_lb,theta_ub,robot.robot_ree_T_tee );
end