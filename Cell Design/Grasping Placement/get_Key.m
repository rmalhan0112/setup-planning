function key = get_Key( point,no_vxls,box_len,voxel_dim )
    % Takes the point in the voxel origin.
    voxel_coord = [  floor(point(1)/box_len(1)*no_vxls(1)),...
                        floor(point(2)/box_len(2)*no_vxls(2)),...
                        floor(point(3)/box_len(3)*no_vxls(3)) ]*voxel_dim;
    key = voxel_coord(1)*10^6 + voxel_coord(2)*10^3 + voxel_coord(3);
end