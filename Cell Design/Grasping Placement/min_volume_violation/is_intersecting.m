function is_intersecting = is_intersecting( ori_cones, grasp_cone )

% Sample within the approach cone
    no_samples = 100;
    violations = floor(no_samples*0.95);
    values = [-1:0.01:1];
    counter = 1;
    while counter < no_samples
        bx_dash = randsample(values,1);
        by_dash = randsample(values,1);
        bz_dash = randsample(values,1);
        vec = [bx_dash,by_dash,bz_dash];
        vec = vec / norm(vec);
        if acos( vec(1)*grasp_cone(1) + vec(2)*grasp_cone(2) + vec(3)*grasp_cone(3) ) > grasp_cone(4)
            continue;
        end
        
        for i = 1:size(ori_cones,1)
           if  acos( vec(1)*ori_cones(i,1) + vec(2)*ori_cones(i,2) + vec(3)*ori_cones(i,3) ) < ori_cones(i,4)
               violations = violations - 1;
               break;
           end
        end
        counter = counter + 1;
    end
    
    if violations < 1
        is_intersecting = true;
    else
        is_intersecting = false;
    end
end




% Plot commands for voxel and approach vectors
% figure(1); hold on; daspect([1,1,1]);
% quiver3( zeros( size(ori_cones,1),1 ),zeros( size(ori_cones,1),1 ),zeros( size(ori_cones,1),1 ),...
%          ori_cones(:,1),ori_cones(:,2),ori_cones(:,3),'r'       );
% quiver3( 0,0,0, grasp_cone(1),grasp_cone(2),grasp_cone(3),'k' );


















% % test come sampling.
% ori_cone = [0.5964    0.0292   -0.8022    0.9529];
% 
% bx = ori_cone(1);
% by = ori_cone(2);
% bz = ori_cone(3);
% beta = ori_cone(4);
% alpha = asin( bz );
% gamma = atan2( by,bx );
%  
% figure(1);
% daspect([1,1,1])
% hold on;
% quiver3( 0,0,0,bx,by,bz,'k','linewidth',3 );
%  
% n = 200;
% 
% cones = [];
% counter = 1;
% while counter < n
% 
% 
%     bx_dash = randsample([-1:0.001:1],1);
%     by_dash = randsample([-1:0.001:1],1);
%     bz_dash = randsample([-1:0.001:1],1);
%     vec = [bx_dash,by_dash,bz_dash];
%     vec = vec / norm(vec);
%     
%     if acos( vec(1)*bx + vec(2)*by + vec(3)*bz ) > beta
%         continue;
%     end
%     cones = [ cones; vec ];
%     counter = counter + 1;
% end
% n = size(cones,1);
% quiver3( zeros(n,1),zeros(n,1),zeros(n,1),cones(:,1),cones(:,2),cones(:,3),'r','linewidth',1 );








%     theta = randsample( [-beta:0.001:beta],1 );
%     phi = randsample( [-beta:0.001:beta],1 );
%     bx_dash = cos( alpha+theta ) * cos(gamma+phi);
%     by_dash = cos( alpha+theta ) * sin(gamma+phi);
%     bz_dash = sin(alpha+theta);