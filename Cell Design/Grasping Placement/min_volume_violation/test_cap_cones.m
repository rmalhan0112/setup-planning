% Plot the voxel vectors and check the capability map
% load grasp_cap.mat
% load reach_map.mat

cones = [];
for i=1:size(grasp_cap,2)
    if ~grasp_cap{i}.reachable
        continue;
    end
    key = reach_map.index_voxel(num2str(i));
    coord = get_coord_key( key );
    if coord(1) < 0.2 || coord(1) > 0.9 || coord(2) < -0.9 || coord(2) > 0.9 || ...
            coord(3) < 0 || coord(3) > 0.3
        continue;
    end
    ori_cones = grasp_cap{i}.ori_cones;
    cones = [ cones; [ coord(1)*ones(size(ori_cones,1),1),...
                                coord(2)*ones(size(ori_cones,1),1),...
                                coord(3)*ones(size(ori_cones,1),1),...
                                ori_cones ] ];
end


figure(1);
hold on;
% Plot draping robot
plot_robot(joints,robot1,false);
quiver3( cones(:,1),cones(:,2),cones(:,3),cones(:,4),cones(:,5),cones(:,6),'r' );


function coord = get_coord_key( key )
    coord = [ double(str2num(key(1:3)))/50,...
            double(str2num(key(4:6)))/50 - 1,...
            double(str2num(key(7:9)))/50];
end