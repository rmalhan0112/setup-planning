if isequal(part_name, 'Mold_Ascent')

    if wich_robot==1
        load( fullfile( path_name, 'grasp_voxels1.mat' ) );
        grasp_voxels = grasp_voxels1;
        % Boundary Conditions and Initial Point
        % Defining the Lower and Upper Bounds
        % Lower Bound for X,Y,phi
        lb(1) = 0.2;
        lb(2) = -0.8;
        lb(3) = -pi/2;

        ub(1) = 1.2;
        ub(2) = 1.2;
        ub(3) = 0;

        % Initial location to start.
%         x0 = [ 0.81;-0.57; 0.1650; 0.9863 ];   % Solution-1
%         x0 = [ -0.2,  0.32,  0.25 ];   % Solution-2
%         x0 = [ 0.9; -0.5; 2.7 ];    % Purturb to recover solution-1
%         x0 = [ 0.82; -0.55; 0.1650; 0.9863 ];    % Purturb to recover solution-1
%         x0 = [ 0.4; -0.5; 0 ];    % Purturb to recover solution-1
    else
        load( fullfile( path_name, 'grasp_voxels2.mat' ) );
        grasp_voxels = grasp_voxels2;
        % Defining the Lower and Upper Bounds
        % Lower Bound for X,Y,phi
        lb(1) = 0.2;
        lb(2) = -0.6;
        lb(3) = 0;

        ub(1) = 1.2;
        ub(2) = 0.5;
        ub(3) = pi/2;
        % Initial location to start.
    %     x0 = [ 0.4;0.1;0 ]; % Working Start Pose
    end
elseif isequal(part_name, 'GE90')
    if wich_robot==1
        load( fullfile( path_name, 'grasp_voxels1.mat' ) );
        grasp_voxels = grasp_voxels1;
        % Boundary Conditions and Initial Point
        % Defining the Lower and Upper Bounds
        % Lower Bound for X,Y,phi
        lb(1) = 0.45;
        lb(2) = -2;
        lb(3) = 0.3491;

        ub(1) = 1.6;
        ub(2) = -0.5;
        ub(3) = 2.8;
        % Initial location to start.
        x0 = [ 0.6;-1;1.55 ];

    else
        load( fullfile( path_name, 'grasp_voxels2.mat' ) );
        grasp_voxels = grasp_voxels2;
        % Defining the Lower and Upper Bounds
        % Lower Bound for X,Y,phi
        lb(1) = 0.3;
        lb(2) = -1.3;
        lb(3) = -2.618;

        ub(1) = 1.2;
        ub(2) = 1;
        ub(3) = -0.6981;
        % Initial location to start.
    %     x0 = [ 0.4;0.1;0 ]; % Working Start Pose
        x0 = [ 0.4;0.3;-pi/2 ];
    end
    
elseif isequal(part_name, 'custom')
    if wich_robot==1
        load( fullfile( path_name, 'grasp_voxels1.mat' ) );
        grasp_voxels = grasp_voxels1;
        % Boundary Conditions and Initial Point
        % Defining the Lower and Upper Bounds
        % Lower Bound for X,Y,phi
        lb(1) = 0.5;
        lb(2) = -1;
        lb(3) = 0.8727;

        ub(1) = 2.2;
        ub(2) = 1;
        ub(3) = 2.269;
        % Initial location to start.
        x0 = [ 0.8; 0.2; 1.57 ];

    else
        load( fullfile( path_name, 'grasp_voxels2.mat' ) );
        grasp_voxels = grasp_voxels2;
        % Defining the Lower and Upper Bounds
        % Lower Bound for X,Y,phi
        lb(1) = 0.6;
        lb(2) = -1.2;
        lb(3) = -4;

        ub(1) = 2;
        ub(2) = 1.2;
        ub(3) = -2.44;
        % Initial location to start.
        x0 = [ 0.9; 0.5; -pi ];
    end
end