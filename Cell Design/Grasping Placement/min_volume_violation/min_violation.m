clear all;
close all;
clc;

part_name = 'Mold_Ascent';
% part_name = 'GE90';
% part_name = 'custom';
wich_robot = 1;

if isequal(part_name, 'Mold_Ascent')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/Mold Ascent';
elseif isequal(part_name, 'GE90')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/GE90';
elseif isequal(part_name, 'custom')
    path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/Custom';
end


disp('Loading dependencies..........')
% load grasp_cap.mat
% load reach_map_v2.mat
load c_map.mat
load r_map.mat
disp('Files loaded. Initializing Optimization Parameters.......')


% creates a 3D space in which the robot will operate
run map_initialization.m;
run opt_config.m;


%%%%%%%% Draping robot AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.1]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% Optimization routine parameters
A = []; b = [];
Aeq = []; beq = [];
CONFIG.options = optimoptions('fmincon');
% options = optimoptions(@fmincon,'Algorithm','active-set','MaxIterations',1e5);
CONFIG.options.MaxIterations = 1e5;
CONFIG.options.MaxFunctionEvaluations = 1e7;
CONFIG.options.OptimalityTolerance = 1e-10;
CONFIG.options.StepTolerance = 1e-10;
CONFIG.options.Display = 'iter';
CONFIG.options.ObjectiveLimit = 1e-15;


% Sample around the grasp cone axis
disp('Sampling within approach cone....')
axis = grasp_voxels(1,4:6);
angle = grasp_voxels(1,7);

no_samples = 100;
values = [-1:0.01:1];
counter = 1;
approach_vecs = [];
while counter < no_samples
    bx_dash = randsample(values,1);
    by_dash = randsample(values,1);
    bz_dash = randsample(values,1);
    vec = [bx_dash,by_dash,bz_dash];
    vec = vec / norm(vec);
    if acos( vec(1) * axis(1) + vec(2) * axis(2) + vec(3) * axis(3) ) > angle
        continue;
    end
    approach_vecs = [ approach_vecs; vec ];
    counter = counter + 1;
end
    


disp('Solving for Robot Placement......')
% [x,err] = fmincon( @(x)volume_violations( x,grasp_voxels, c_map,r_map,approach_vecs ),...
%                         x0,A,b,Aeq,beq,lb,ub,[],CONFIG.options );


options = optimoptions( @fminunc, 'MaxIterations', 400, 'MaxFunctionEvaluations', 1e7, 'OptimalityTolerance', 1e-16,...
    'StepTolerance',1e-16, 'Display', 'iter','SpecifyObjectiveGradient',false);

[x,err] = fminunc( @(x)volume_violations( x,grasp_voxels, c_map,r_map,approach_vecs ),x0,options );


% for i=1:2000
%     x = [ randsample(-1:0.01:1,1), randsample(-1:0.01:1.2,1), randsample(0.2:0.01:3,1) ];
%     if volume_violations( x,grasp_voxels, c_map,r_map,approach_vecs ) < 15
%         x
%     end
% end


% % Plot
% grasp_voxels(:,1:3) = grasp_voxels(:,1:3)./1000;
% R = eul2rotm( [x0(3),0,0],'ZYX' );
% t = [ x0(1);x0(2);0 ];
% transf_voxels = R*grasp_voxels(:,1:3)' + t;
% grasp_voxels(:,1:3) = transf_voxels';
% grasp_voxels(:,4:6) = (R * grasp_voxels(:,4:6)')';
% 
% 
% 
% figure(1);
% hold on;
% % xlim([0,50])
% % ylim([0,100])
% % zlim([0,10])
% scatter3( grasp_voxels(:,1),grasp_voxels(:,2),grasp_voxels(:,3),20,'g','filled' );
% quiver3( grasp_voxels(:,1),grasp_voxels(:,2),grasp_voxels(:,3),...
%     grasp_voxels(:,4),grasp_voxels(:,5),grasp_voxels(:,6),'k');
% joints = [0;pi/4;0;-pi/4;0;pi/2;0];
% 
% 
% % Plot draping robot
% plot_robot(joints,robot1,false);
% daspect([1,1,1])