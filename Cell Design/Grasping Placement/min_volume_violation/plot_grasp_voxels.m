part_name = 'Mold_Ascent.stl';
load mold_voxels.mat;

path_name = '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/grasping_region/Mold Ascent';
figure(1);
hold on;
[v_part, f_part, n_part, name_part] = stlRead(fullfile( '/home/rmalhan/Work/USC/Composite Automation/Journal/cell_layout/Part Stl',part_name )); % read tool stl
v_part = v_part./1000;
T = eye(4);
T(1:3,4) = mold_voxels.orig_trans'/1000;
[v_part_transf,n_part_transf] = stlTransform(v_part,n_part,T);
part_plt(v_part_transf, f_part, name_part, [0.2,0,0]); % plot tool stl

hold on;
daspect([1,1,1]);
xlabel('X')
ylabel('Y')
zlabel('Z')

load( fullfile( path_name, 'grasp_voxels1.mat' ) );
grasp_voxels = grasp_voxels1;
scatter3( grasp_voxels(:,1)/1000,grasp_voxels(:,2)/1000,grasp_voxels(:,3)/1000,20,'g','filled' );
quiver3( grasp_voxels(:,1)/1000,grasp_voxels(:,2)/1000,grasp_voxels(:,3)/1000,...
         grasp_voxels(:,4),grasp_voxels(:,5),grasp_voxels(:,6),'k');


     
load( fullfile( path_name, 'grasp_voxels2.mat' ) );
grasp_voxels = grasp_voxels2;
scatter3( grasp_voxels(:,1)/1000,grasp_voxels(:,2)/1000,grasp_voxels(:,3)/1000,20,'g','filled' );
quiver3( grasp_voxels(:,1)/1000,grasp_voxels(:,2)/1000,grasp_voxels(:,3)/1000,...
         grasp_voxels(:,4),grasp_voxels(:,5),grasp_voxels(:,6),'k');
