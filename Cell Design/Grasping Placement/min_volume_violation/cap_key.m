function key = cap_key(x,y,z)
    key = num2str( x*10^6 + y*10^3 + z );
end