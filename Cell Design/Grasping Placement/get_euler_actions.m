function states = get_euler_actions( pose,visited_samples )
    step = 0.01;
    states = [];
    for i = [1,2,3]
        states = [ states, pose ];
        states(i,end) = states(i,end) + step;
        
        if ismember(states(:,end)',visited_samples','rows')
            states(:,end) = [];
        end
        
        states = [ states, pose ];
        states(i,end) = states(i,end) - step;
        
        if ismember(states(:,end)',visited_samples','rows')
            states(:,end) = [];
        end
    end
end