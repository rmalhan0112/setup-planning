clear all;
close all;
clc;

addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/STL_plot/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/manipulator_planners/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/'));

run ascent_grasp_init.m
% run airfoil_grasp_init.m
% run lockheed_grasp_init.m


gripper_translation1 = -cone_axis1' * 137; % in mm
% gripper_translation1 = -cone_axis1' * 0; % in mm

gripper_translation2 = -cone_axis2' * 137; % in mm
% gripper_translation2 = -cone_axis2' * 0; % in mm



grasp_voxels1 = [];  % Matrix with first three columns as xyz, next three as cone axis, last col as cone_angle
grasp_voxels2 = [];
counter = 1;

% Standard voxel sequence for robot 1
for y = 1:mold_voxels.no_vxls(2)
    for x = 1:mold_voxels.no_vxls(1)
        for z = 1:mold_voxels.no_vxls(3)
            coord = [ x-1,y-1,z-1 ]*mold_voxels.voxel_dim;
            
            if ~( (coord(1) >= x_min1 && coord(1) <= x_max1) ...
                    && (coord(2) >= y_min1 && coord(2) <= y_max1)...
                    && (coord(3) >= z_min1 && coord(3) <= z_max1)  )
                continue;
            end
        
            coord = coord + gripper_translation1;
            grasp_voxels1 = [grasp_voxels1; [coord, cone_axis1', cone_angle1] ];
            counter = counter + 1;
        end
    end
end




% Standard voxel sequence for robot 2
for y = 1:mold_voxels.no_vxls(2)
    for x = 1:mold_voxels.no_vxls(1)
        for z = 1:mold_voxels.no_vxls(3)
            coord = [ x-1,y-1,z-1 ]*mold_voxels.voxel_dim;
            
            if ~( (coord(1) >= x_min2 && coord(1) <= x_max2) ...
                    && (coord(2) >= y_min2 && coord(2) <= y_max2)...
                    && (coord(3) >= z_min2 && coord(3) <= z_max2)  )
                continue;
            end
        
            coord = coord + gripper_translation2;
            grasp_voxels2 = [grasp_voxels2; [coord, cone_axis2', cone_angle2] ];
            counter = counter + 1;
        end
    end
end


path_to_grspvxl1 = fullfile( csv_path, strcat('grasp_voxels1', '.mat') );
csv_to_grspvxl1 = fullfile( csv_path, strcat('grasp_voxels1', '.csv') );
save( path_to_grspvxl1,'grasp_voxels1' );
grsp_coord = grasp_voxels1(:,1:3) - mold_voxels.orig_trans;
save( csv_to_grspvxl1,'grsp_coord' );


path_to_grspvxl2 = fullfile( csv_path, strcat('grasp_voxels2', '.mat') );
csv_to_grspvxl2 = fullfile( csv_path, strcat('grasp_voxels2', '.csv') );
save( path_to_grspvxl2,'grasp_voxels2' );
grsp_coord = grasp_voxels1(:,1:3) - mold_voxels.orig_trans;
save( csv_to_grspvxl2,'grsp_coord' );

% Plot grasp voxels.
load( path_to_grspvxl1 )
load( path_to_grspvxl2 )



coordinates1 = [];
for i=1:size(grasp_voxels1,1)
    coordinates1 = [ coordinates1; grasp_voxels1(i,1:3) ];
end

coordinates2 = [];
for i=1:size(grasp_voxels2,1)
    coordinates2 = [ coordinates2; grasp_voxels2(i,1:3) ];
end


figure(1)
camlight;
hold on;
[v_part, f_part, n_part, name_part] = stlRead(part_name); % read tool stl
transform = eye(4);
transform(1:3,4) = mold_voxels.orig_trans';
[v_part_transf,n_part_transf] = stlTransform(v_part,n_part,transform);
part_plt(v_part_transf, f_part, name_part, [0.642,0.642,0.642]); % plot tool stl
set(gcf,'color',[0.2,0.2,0.2])
set(gca,'color',[0.2,0.2,0.2],'Xcolor',[1,1,1],'Ycolor',[1,1,1],'Zcolor',[1,1,1],...
               'FontSize',20)

daspect([1,1,1]);
xlabel('X axis (mm)')
ylabel('Y axis (mm)')
zlabel('Z axis (mm)')


scatter3( coordinates1(:,1),coordinates1(:,2),coordinates1(:,3),20,'g','filled' );
quiver3( coordinates1(:,1),coordinates1(:,2),coordinates1(:,3),...
         grasp_voxels1(:,4),grasp_voxels1(:,5),grasp_voxels1(:,6),'k');
     
     
scatter3( coordinates2(:,1),coordinates2(:,2),coordinates2(:,3),20,'g','filled' );
quiver3( coordinates2(:,1),coordinates2(:,2),coordinates2(:,3),...
         grasp_voxels2(:,4),grasp_voxels2(:,5),grasp_voxels2(:,6),'k');