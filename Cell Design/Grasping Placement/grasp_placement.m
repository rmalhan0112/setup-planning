clear all;
close all;
clc;

robot_no = 2;

addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/Setup Planning/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/STL_plot/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/manipulator_planners/'));
addpath(genpath('/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/'));
path_to_maps = '/home/rmalhan/Work/USC/Composite Automation/Modules/Cap Maps';

disp('Loading dependencies..........')
run ascent_gplace_init.m
% run airfoil_gplace_init.m
% run lockheed_gplace_init.m

load( fullfile( path_to_maps, 'cap_map.mat' ) );

disp('Files loaded. Initializing Optimization Parameters for IK.......')
%%%%%%%% Draping robot AND TOOL DEFINITION %%%%%%%%%%%%%
robot.base_T_robot = eye(4);
robot.base_T_robot(1:3,4) = [0;0;0];
robot.base_T_robot(1:3,1:3) = eul2rotm([0.0,0,0]);

robot.robot_ree_T_tee = eye(4);
robot_ree_T_tee(1:3,4) = [0; 0; -0.137]; % For Gripper
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% Optimizer Bounds
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = -theta_lb;

% IKoptions = optimoptions('fmincon','Algorithm', 'interior-point');
IKoptions = optimoptions('fmincon','Algorithm', 'sqp');
IKoptions.MaxIterations = 5000;
IKoptions.MaxFunctionEvaluations = 1e7;
IKoptions.OptimalityTolerance = 1e-8;
IKoptions.StepTolerance = 1e-8;
IKoptions.Display = 'off';
IKoptions.SpecifyObjectiveGradient = false;
IKoptions.ObjectiveLimit = 1e-8;


I = eye(4);
init_guess = [ 0;0;0;0;0;0;0 ];
tolerance(1) = 0.003;
tolerance(2) = 0.0524;
tolerance(3) = 0.0524;
tolerance(4) = 0.0524;



% Sample around the grasp cone axis
disp('Sampling within approach cone....')
axis = grasp_voxels(1,4:6);
angle = grasp_voxels(1,7);

no_samples = 100;
values = [-1:0.01:1];
counter = 1;
approach_vecs = [];
while counter < no_samples
    bx_dash = randsample(values,1);
    by_dash = randsample(values,1);
    bz_dash = randsample(values,1);
    vec = [bx_dash,by_dash,bz_dash];
    vec = vec / norm(vec);
    if acos( vec(1) * axis(1) + vec(2) * axis(2) + vec(3) * axis(3) ) > angle
        continue;
    end
    approach_vecs = [ approach_vecs; vec ];
    counter = counter + 1;
end



disp('Solving for Robot Placement......')
% Randomly Sample Using Capability Map
pose_viol = [];
required_samples = 1500;
counter = 1;
tic;
while counter <= required_samples
    x = [ randsample(lb(1):0.01:ub(1),1), randsample(lb(2):0.01:ub(2),1), randsample(lb(3):0.01:ub(3),1) ];
    % Check if the entire mold is within the workspace
    rot = eul2rotm([x(3),0,0]);
    rot = rot(1:2,1:2);
    transl = x(1:2)';
    part_bndr = (rot * mold_boundaries + transl)';
    in = inpolygon(part_bndr(:,1),part_bndr(:,2),workspace_bndr(:,1),workspace_bndr(:,2));
    if any(in==0)
        continue;
    end
    counter = counter + 1;
    pose_viol = [pose_viol; x, volume_violations( x,grasp_voxels, cap_map,approach_vecs ) ];
end
toc;
pose_viol = sortrows(pose_viol,4);


%%%%%%%%%%%%%%%%%%% Gradient Descent to minimize violations %%%%%%%%%%%%%%
disp('Minimizing violations over the waypoints......')
prtz_sol = [];
tic;
for idx = 1:100
    fprintf('Improving sample no:  %d\n',idx)
    X = pose_viol(idx,1:3)';
    max_viol = volume_violations( X,grasp_voxels, cap_map,approach_vecs );
    fprintf('Initial Reach Violation:  %d\n',max_viol);
    visited_samples = zeros(3,1);

    % Using Gradient Descent to minimize overall reach
    while true
        opt_idx = 0;
        states = get_euler_actions( X,visited_samples );
        for i = 1:size(states,2)
            visited_samples = [visited_samples, states(:,i)];
            viol = volume_violations( states(:,i),grasp_voxels, cap_map,approach_vecs );
            if viol < max_viol
                max_viol = viol;
                opt_idx = i;
            end
        end
        
        if opt_idx == 0
            break;
        else
            X = states(:,opt_idx);
        end
    end
    prtz_sol = [prtz_sol; X', volume_violations( X,grasp_voxels, cap_map,approach_vecs )];
    fprintf('Final Reach Violation:  %d\n',max_viol);
end
toc;
prtz_sol = sortrows(prtz_sol,4);
solutions = prtz_sol(1:50,1:3);


%%%%%%%%%%%%%%%%%%%%%%% Checking reachability using IK %%%%%%%%%%%%%%%%%
