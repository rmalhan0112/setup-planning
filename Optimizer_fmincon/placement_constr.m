function [c,ceq] = placement_constr( theta, prev_config,pt_idx,velocity_vec,force_vec,joint_vel )
    c = [];
    ceq = [];
    
% Instantaneous velocity Constraint
    FK_all = get_iiwa_FK_all_joints_mex( theta,eye(4) );
    ee_base = FK_all(33:36,:);
    Jac = get_iiwa_GeoJac( FK_all ); % Wxyz, Vxyz
    Jac = [  Jac(4:6,:); Jac(1:3,:)  ];
    J_inv = pinv( Jac(1:3,:) );
    joint_vec = J_inv * velocity_vec';  % Upper Bound
    c = [ c; [ (joint_vec - joint_vel)] ];
    c = [ c; [ (-joint_vec - joint_vel)] ];
    
% Force Constraint
    Pborg = [ 0, -ee_base(3,4),  ee_base(2,4);
            ee_base(3,4),  0,  -ee_base(1,4);
            -ee_base(2,4), ee_base(1,4),  0];
    Pborg = Pborg * ee_base(1:3,1:3);    
    force_vec = [ ee_base(1:3,1:3),eye(3); Pborg, ee_base(1:3,1:3) ] * force_vec';
    joint_torq = Jac' * force_vec;  % Upper Bound
    c = [ c; [ (joint_torq - [ 176; 176; 110; 110; 110; 40; 40 ])] ];
    c = [ c; [ (-joint_torq - [ 176; 176; 110; 110; 110; 40; 40 ])] ];
    
    if pt_idx > 1
% Transition velocity Constraint
%         time = ratio_update( theta, prev_config, seg_time, distance );
%         c = [ c; -theta - joint_vel*time + prev_config ];
%         c = [ c; theta - prev_config - joint_vel*time];
        
% Correlation Based Continuity Estimate
        FK_prev = get_iiwa_FK_all_joints_mex( prev_config,eye(4) );
        Jac_prev = get_iiwa_GeoJac( FK_prev ); % Wxyz, Vxyz
        Jac_prev = [  Jac_prev(4:6,:); Jac_prev(1:3,:)  ];
        c = [c;  -corr2( Jac,Jac_prev ) + 0.9];
    end
end