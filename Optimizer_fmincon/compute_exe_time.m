function exe_time = compute_exe_time( x, xyz_bxbybz,tolerances, IKoptions, theta_lb, theta_ub, init_reach, joint_vel,velocity_vecs, Force_vecs )
%     x = [ x; (1.000 - x(4)*x(4) - x(5)*x(5) - x(6)*x(6))^0.5 ];

    global robot1;
    
    fid = fopen( 'C:\Work\USC\Composite Automation\Journal\setup-planning\Optimizer_fmincon\good_seed.csv', 'a' );
    
    exe_time = 0;
    transf_xyzbxbybz = transf_points( x,xyz_bxbybz, robot1 );
    no_pts = size( xyz_bxbybz, 1 );
    
    joint_angles = [];
    joint_config = [0;0;0;0;0;0;0];
    [joint_config,status] = get_iiwa_IK( joint_config,transf_xyzbxbybz(1,:),tolerances(1,:),...
                                            IKoptions, theta_lb, theta_ub,true,3 );
                                        
    for i = 1:no_pts
        [joint_config,status] = constr_IK( joint_config,transf_xyzbxbybz(i,:),tolerances(i,:),...
                                            IKoptions, theta_lb, theta_ub,true,3, i, velocity_vecs, Force_vecs );
        if status
            joint_angles = [joint_angles, joint_config ];
        end
    end
    reach = size( joint_angles,2 );
    
    if reach < 10
        exe_time = inf;
        fprintf( fid, 'Pose:   %d,%d,%d,%d,%d,%d,%d\n Time:    %d\n Reach:    %d\n\n\n',...
                x(1),x(2),x(3),x(4),x(5),x(6),x(7),exe_time,size(joint_angles,2) );
        fclose( fid );
        return;
    end
    remainder = no_pts - reach;
    
% Evaluate Velocity
    tot_time = [];
    for i = 1 : reach-1
        jconfig_init = joint_angles(:,i);
        jconfig_fin = joint_angles(:,i+1);
        delta_j = jconfig_fin - jconfig_init;
        time_vec = [];
        for j=1:7
            time_vec = [ time_vec; abs((delta_j(j)/joint_vel(j))) ];
        end
        tot_time = [ tot_time; max(time_vec) ];
    end
    tot_time = [ tot_time; max(tot_time)*remainder  ];
    exe_time = sum(tot_time);
    
    fprintf( fid, 'Pose:   %d,%d,%d,%d,%d,%d,%d\n Time:    %d\n Reach:    %d\n\n\n',...
                x(1),x(2),x(3),x(4),x(5),x(6),x(7),exe_time,size(joint_angles,2) );
    fclose( fid );
end