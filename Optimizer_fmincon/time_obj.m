clear all;
close all;

global CONFIG;
global robot1;


q = eul2quat( [deg2rad(10),deg2rad(-10),deg2rad(-10)],'ZYX' );
x = [ 0.4;0.15;0.25; q' ];

% q = eul2quat( [deg2rad(0),deg2rad(-20),deg2rad(0)],'ZYX' );
% x = [ 0.45;0.1;0.2; q' ];


CONFIG.pose_seed = x;


warning 'off';

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
joint_vel = joint_vel*0.95;

joint_torques = [ 25; 25; 25; 25; 25; 25; 25 ]; % Nm

CONFIG.tool_stl = 'tool.stl';

% creates a 3D space in which the robot will operate
run map_initialization.m;
% define all the planning params in this file
run hybrid_planner_params.m;
% part poses and init and goal states are defined in the following file
run simulations_data.m; 

CONFIG.workspace = [0.9,0.9,0.3]; %first element is Max X value,then Y then Z.
CONFIG.PRIMITIVE_DELTA_ANGLE = 1.0*CONFIG.DEG2RAD;
if strcmpi(CONFIG.SEARCH_SPACE, 'xyzabc')
    % this for loop adds discrete xyz
    for jid = 1:3
        curr_val = CONFIG.DISCRETE_MAP_BOUNDS(jid);
        CONFIG.DISCRETE_STATE_BOUNDS = [CONFIG.DISCRETE_STATE_BOUNDS; curr_val];
    end
end
CONFIG.DISP_MAP = zeros(CONFIG.DISCRETE_MAP_BOUNDS(1), CONFIG.DISCRETE_MAP_BOUNDS(2), ...
                                                            CONFIG.DISCRETE_MAP_BOUNDS(3));

new_map = false;
if new_map
    disp('Computing EDT...');
    EDT_vals = bwdistsc(CONFIG.MAP) - bwdistsc(imcomplement(CONFIG.MAP));
    save('computed_3D_EDT.mat', 'EDT_vals');
else
    load computed_3D_EDT.mat;
end
CONFIG.EDT_vals = EDT_vals;
CONFIG.max_EDT = max(EDT_vals(:));
CONFIG.min_EDT = min(EDT_vals(:));
%% %%%%%%%%%%%%% END ENVIRONMENT INITIALIZATION %%%%%%%%%%%%%%%%%%%

%%%%%%%% ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% CONFIG.robot_sphere_diamter = 85/1000;
CONFIG.robot_sphere_diamter = 120/1000;
CONFIG.tool_sphere_diamter = [17.0/1000; 34/1000];

robot1_base = eye(4);
robot1_base(1:3,4) = [0;0;0];
robot1_base(1:3,1:3) = eul2rotm([0.0,0,0]);

ROBOT_TOOL = {};
ROBOT_TOOL{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000; % Pointy Tool

ROBOT_TOOL_FMM = {};
ROBOT_TOOL_FMM{end+1} = [ 0, 0, 0, 1000;  0, 0, 100, 1000]'./1000;

%%%%% Tool to Robot transformation is defined here
robot1.robot_ree_T_tee = eye(4);
robot1.robot_ree_T_tee(1:3,4) = [0; 0; 0.1]; % For Pointy tool

robot1_tool = {};
robot1_tool{end+1} = ROBOT_TOOL;
robot1_tool{end+1} = ROBOT_TOOL_FMM;
robot1_tool{end+1} = robot1.robot_ree_T_tee;

robot1 = initialize_robot('iiwa7', CONFIG, robot1_base, robot1_tool);
CONFIG.DISCRETE_STATE_BOUNDS = robot1.DISCRETE_JOINT_BOUNDS;
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


%% %%%%%% CURVE DATA %%%%%%%%%%%%%

% HS Part
% curve_file_name  = 'HS_points.csv'; % n x 12
% xyz_bxbybz = dlmread(curve_file_name);
% xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
% tol_file_name  = 'HS_tolfile.csv'; % n x 4 
% tolerances = dlmread(tol_file_name);
% no_pts = size(xyz_bxbybz,1);
% %%%%%%%% END CURVE DATA %%%%%%%%%%%%%
% fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);


% Ascent Part
curve_file_name  = 'Armor_points.csv'; % n x 12
xyz_bxbybz = dlmread(curve_file_name);
xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)./1000;
no_pts = size(xyz_bxbybz,1);

tolerances = ones(no_pts,2);
tolerances(:,1) = tolerances(:,1).*0.002;
tolerances(:,2) = tolerances(:,2).*0.2618;

Force_vecs = zeros(no_pts,3);
Force_vecs(:,3) = ones(no_pts,1).*35;


%%%%%%%% END CURVE DATA %%%%%%%%%%%%%
fprintf('Number of Points considered for evaluation:  \n%d\n ',no_pts);

path_len = 0;
for i=1:no_pts-1
    path_len = path_len + norm(xyz_bxbybz(i+1,1:3)-xyz_bxbybz(i,1:3));
end
seg_len = path_len / (no_pts-1);
seg_time = seg_len / desired_velc;

%% Optimizer
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = - theta_lb;

options = optimoptions('fmincon','Algorithm', 'interior-point');
options.MaxIterations = 3000;
options.MaxFunctionEvaluations = 1e7;
options.OptimalityTolerance = 1e-6;
options.StepTolerance = 1e-6;
options.Display = 'off';
options.SpecifyObjectiveGradient = false;


% Optimization routine parameters
A = []; b = [];

% Defining the Lower and Upper Bounds
% Lower Bound for X,Y,Z
lb(1) = 0;
lb(2) = -1;
lb(3) = 0;
% lb(3) = 0.1267;
lb(4) = -1;
lb(5) = -1;
lb(6) = -1;
% lb(5) = 0;
% lb(6) = 0;
lb(7) = -1;

ub(1) = 1;
ub(2) = 1;
ub(3) = 1;
% ub(3) = 0.1267;
ub(4) = 1;
ub(5) = 1;
ub(6) = 1;
% ub(5) = 0;
% ub(6) = 0;
ub(7) = 1;

% Choosing x0 from sampling
x0 = CONFIG.pose_seed;

% % SOLVE USING fmincon % % 
disp('Solving for Optimum Part Placement......')

Aeq = []; beq = [];
CONFIG.options = optimoptions('fmincon');
% options = optimoptions(@fmincon,'Algorithm','active-set','MaxIterations',1e5);
CONFIG.options.MaxIterations = 1e5;
CONFIG.options.MaxFunctionEvaluations = 1e7;
CONFIG.options.OptimalityTolerance = 1e-6;
CONFIG.options.StepTolerance = 1e-6;
CONFIG.options.Display = 'iter';
CONFIG.options.ConstraintTolerance = 1e-3;

% Compute Initial Reachability
transf_xyzbxbybz = transf_points( CONFIG.pose_seed,xyz_bxbybz, robot1 );
no_pts = size( xyz_bxbybz, 1 );

CONFIG.init_reach = 0;
joint_config = [0;0;0;0;0;0;0];
% velocity_vecs = csvread( 'HS Velocity Vectors.csv' );
velocity_vecs = csvread( 'armor_dist_vecs.csv' );
velocity_vecs = velocity_vecs./0.05;

for i = 1:no_pts
    [joint_config,status] = constr_IK( joint_config,transf_xyzbxbybz(i,:),tolerances(i,:),CONFIG.IKoptions,...
                                        CONFIG.theta_lb, CONFIG.theta_ub,true,2, i, velocity_vecs, Force_vecs );
    if status
        CONFIG.init_reach = CONFIG.init_reach + 1;
    end
end
    
tic;
x = fmincon( @(x)compute_exe_time(x, xyz_bxbybz,tolerances, CONFIG.IKoptions, CONFIG.theta_lb, ...
                            CONFIG.theta_ub, CONFIG.init_reach, joint_vel, velocity_vecs, Force_vecs ),...
                            x0,A,b,Aeq,beq,lb,ub,@(x)quat_Constr( x ),CONFIG.options );
toc;



%% Final Values

% idx: 37
% x = [0.4863;
%     0.3334;
%     0.4298;
%    -0.6681;
%    -0.2970;
%     0.0409;
%     0.6810 ];

% idx: 81
% x = [ 0.3379;
%    -0.6819;
%     0.5279;
%     0.4322;
%    -0.0927;
%     0.5160;
%     0.7340 ];

% idx: 3
% x = [ 0.5329;
%     0.5843;
%     0.6494;
%    -0.3504;
%     0.2014;
%     0.7053;
%    -0.5873 ];



%% Functions

