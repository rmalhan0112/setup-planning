function transf_xyzbxbybz = transf_points(Xk,xyz_bxbybz, robot1)
    R = quat2rotm( Xk(4:7)' );
    t = Xk(1:3);
    transf_xyzbxbybz(:,1:3) = (R*xyz_bxbybz(:,1:3)' + t)';
    transf_xyzbxbybz(:,4:6) = (R*xyz_bxbybz(:,4:6)')';
    transf_xyzbxbybz(:,7:9) = (R*xyz_bxbybz(:,7:9)')';
    transf_xyzbxbybz(:,10:12) = (R*xyz_bxbybz(:,10:12)')';
    for i=1:size(transf_xyzbxbybz,1)
        T = eye(4);
        T(1:3,1:3) = [ transf_xyzbxbybz(i,4:6)', transf_xyzbxbybz(i,7:9)', transf_xyzbxbybz(i,10:12)' ];
        T(1:3,4) = transf_xyzbxbybz(i,1:3)';
        Tf = ((robot1.robot_ree_T_tee' \ T'))';
        
        transf_xyzbxbybz(i,1:3) = Tf(1:3,4)';
        transf_xyzbxbybz(i,4:6) = Tf(1:3,1)';
        transf_xyzbxbybz(i,7:9) = Tf(1:3,2)';
        transf_xyzbxbybz(i,10:12) = Tf(1:3,3)';
    end
end