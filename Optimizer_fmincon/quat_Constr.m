function [c,ceq] = quat_Constr( x )
    ceq = [];
    c = [];
    c = [ c; (x'*x - 1) ];
end