function [joints,status] = constr_IK( init_guess,point,tolerance,options,theta_lb,theta_ub,...
                            seg_time,pt_idx, velocity_vecs, Force_vecs,joint_vel )
    global robot1;
    prev_config = init_guess;   % Init_guess provided should be the previous config in path
% Update the initial guess checking if IK exists
    if(pt_idx==1)
        [init_guess,status] = get_iiwa_IK( init_guess,point,tolerance,...
                                        options, theta_lb, theta_ub,false,3 );
    end
    
% Attempt IK with init_guess.
% Singularity and collision constraints on robot.
        A = [ eye(7); -eye(7) ];
        b = [ joint_vel*seg_time(pt_idx) + prev_config; joint_vel*seg_time(pt_idx) - prev_config ];
        [joints] = fmincon(@(theta)compute_redundancy_cost(theta,point),init_guess,...
                    A,b,[],[],theta_lb,theta_ub,...
                    @(theta)placement_constr(theta,prev_config,pt_idx,velocity_vecs(pt_idx,:),Force_vecs(pt_idx,:),joint_vel),options);
                    
        
% Verify the Solution
        ee_base = get_iiwa_FK_mex( joints,eye(4) );
        ee_base = ee_base * robot1.robot_ree_T_tee;
% Error Position
        err_xyz = norm(point(1:3)'-ee_base(1:3,4));
% Error Orientation
%         err_bx = acos( point(4)*ee_base(1,1) + point(5)*ee_base(2,1) + point(6)*ee_base(3,1) );
%         err_by = acos( point(7)*ee_base(1,2) + point(8)*ee_base(2,2) + point(9)*ee_base(3,2) );
        err_bz = acos( point(10)*ee_base(1,3) + point(11)*ee_base(2,3) + point(12)*ee_base(3,3) );
%         if err_xyz > tolerance(1) || err_bz > tolerance(2) || err_bx > tolerance(2) || err_by > tolerance(2)
        if err_xyz > tolerance(1) || err_bz > tolerance(2)
%         if err_xyz > tolerance(1)
            status = false;
        elseif ~IK_validity_check( prev_config,joints,pt_idx,...
                                velocity_vecs(pt_idx,:),joint_vel, Force_vecs(pt_idx,:) )
            status = false;
        else
            status = true;
        end
end