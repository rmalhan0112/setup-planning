force_vec = [ 0,0,40 ];

for i = 1:3:size(joint_angles,1)
    jts = joint_angles(i,:);
    FK_all = get_iiwa_FK_all_joints_mex( jts,eye(4) );
    Jac = get_iiwa_GeoJac( FK_all ); % Wxyz, Vxyz
    Jac_Cart = Jac( 4:6,: );
    joint_torq = abs(Jac_Cart' * force_vec');  % Upper Bound
    
    violation = (joint_torq - [ 25; 25; 25; 25; 25; 25; 25 ]);
    if ~all(violation < 0)
        disp('Violated')
    end
end