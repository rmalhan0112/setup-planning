function upd_time = ratio_update( theta, prev_config, seg_time, distance )
    global robot1;
    
    d_theta = theta - prev_config;
    d_theta = d_theta/5;
    
    dist = 0;
    for i=1:5
        transf_1 = get_iiwa_FK_mex( prev_config,eye(4) )*robot1.robot_ree_T_tee;
        transf_2 = get_iiwa_FK_mex( prev_config + d_theta, eye(4) )*robot1.robot_ree_T_tee;
        dist = dist + norm( transf_2(1:3,4) - transf_1(1:3,4) );
        prev_config = prev_config + d_theta;
    end
    
    ratio = dist/distance;
    upd_time = seg_time*ratio;
end