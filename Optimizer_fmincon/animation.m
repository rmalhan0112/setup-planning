close all;

v = VideoWriter('Ascent_layup.avi');

% joint_angles = csvread('Spiral37_test.csv');
open(v);
figure(1)
hold on;
view(60,20);
tool_stl = 'roller.stl'; % Tool to be attached to EE to visualize
% tool_stl = 'tool.stl'; % Tool to be attached to EE to visualize
part = 'Mold_Ascent.STL';

xlabel('X-Axis');
ylabel('Y-Axis');
zlabel('Z-Axis');


% Plot the Transformed Part
R_to_Part = eye(4);
R_to_Part(1:3,1:3) = quat2rotm( x(4:7)' );
R_to_Part(1:3,4) = [ x(1);x(2);x(3) ];
[v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
v_part = v_part./1000;
[v_part_transf,n_part_transf] = stlTransform(v_part,n_part,R_to_Part); 
part_plt(v_part_transf, f_part, name_part, [0.2,0,0]); % plot tool stl
ylim([-1,0.3]);zlim([0,1]);xlim([-0.2,1]);
pause(1); 

joints = [[0;pi/4;0;-pi/4;0;pi/2;0],joint_angles];
for i = 1:size(joints,2)
    state = joints(:,i);
    H = plot_robot(state,robot1,tool_stl);
    daspect([1,1,1]);
    if i==1
        pause(2);
    end
    for cnt = 1:5
        frame = getframe(gcf);
        writeVideo(v,frame);
    end
    if i<size(joint_angles,2)
        for rnge=1:9; delete(H{rnge}); end
    end
end
close(v);
