% pos = [];
% for i=1:size(capability_z,2)
%     if ~capability_z{i}.reachable
%         key = reach_map.index_voxel(num2str(i));
%         x = str2double( key(1:3) )./50;
%         y = str2double( key(4:6) )./50 - 1;
%         z = str2double( key(7:9) )./50;
%         
%         pos = [ pos; [x,y,z] ];
%     end
% end
% figure(1)
% plot_robot([0;0;0;0;0;0;0],robot1); 
% scatter3( pos(:,1),pos(:,2),pos(:,3),20,'r','filled' )
% daspect([1,1,1])
% return;
% 

% feasible_indices = csvread('feasible_indices.csv');

% folder = 'C:\Work\USC\Composite Automation\Journal\setup-planning\CSP\Ascent Sample Data 3';
% figure(1)
% hold on;


% prt_poses = csvread( 'HS_60_perturb.csv' );


% reach_poses = csvread('bonet_reach_poses.csv');
% prt_poses = [];
% feasible_poses = [];
% for i=1:48
%     if reach_poses(8,i) == no_pts % Reachable and not feasible
%         prt_poses = [prt_poses, reach_poses(1:7,i)];
%     end
% end

% prt_poses = csvread('HS_iteration_poses.csv');
% for i=1:size(prt_poses,2)
%     T_pose = eye(4);
%     T_pose(1:3,1:3) = quat2rotm( prt_poses(4:7,i)' );
%     T_pose(1:3,4) = prt_poses(1:3,i) + [0;-0.6;0];
%     T_pose(1:3,1:3) = eul2rotm( deg2rad([60,0,-100,]),'ZYX'  ) * T_pose(1:3,1:3);
%     if i>1
%         prt_poses(4:7) = rotm2quat( T_pose(1:3,1:3) )';
%     end
%     prt_poses(1:3,i) = T_pose(1:3,4);
% end

prt_poses = [];

% % HS
% prt_poses = [prt_poses, [0.65  -0.6    0.3    -0.8050    0.3387    0.1889   -0.4491]' ];    % RL0
% prt_poses = [prt_poses, [0.5   -0.35    0.69   0.5872   -0.6998   -0.3116    0.2614]' ];    %RL1 and RL2
% prt_poses = [prt_poses, [0.53   -0.35    0.68    0.4574   -0.7689   -0.3980    0.2027]' ];
% prt_poses = [prt_poses, [0.53   -0.33    0.7    0.4901   -0.7624   -0.3607    0.2204]' ];    
% prt_poses = [prt_poses, [0.5476   -0.28    0.7090    0.4631   -0.7790   -0.3681    0.2077]' ];    
% prt_poses = [prt_poses, [0.52   -0.3657    0.69    0.4631   -0.7790   -0.3681    0.2077]' ];    
% prt_poses = [prt_poses, [0.5476   -0.3657    0.7090    0.4553   -0.7618   -0.4080    0.2141]' ];    %RL3


figure(1); 
tool_stl = 'roller.stl';
% plot_robot([0;0;0;0;0;0;0],robot1,false); daspect([1,1,1])
% hold on;
% 
% part = 'GE90.STL';
% [v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
% v_part = v_part./1000;


part = 'Mold_Ascent.stl';
[v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
v_part = v_part./1000;


% figure(2)
% hold on;

% reach = [];

% feasible_indices = 1:30';
% temp = csvread('HS_Solutions.csv');

% for i = [5,6,7,8,9,10,34,36,37,61,62,72,73,74,90,91]
% indices = [1,2,4,8,10,12,16,17,22,25,26]';
for i = 1:44
%     i = indices(idx);
    title( strcat('Index:  ',num2str(i) )  )
    zlim([0,1.5]);
    ylim([-1,1]);
    xlim([-1,1]);
    temp = csvread(  strcat('Sample_',num2str(i),'.csv')  );
%     x = prt_poses(1:7,i);
    
%     poses = [ poses, [ 0.48; 0.34; 0.427; -0.6652; -0.2706; 0.0842; 0.69  ] ];
    for j=1:size(temp,2)
        x = temp(1:7,j);
        plot_robot([0;0;0;0;0;0;0],robot1,tool_stl);
%         x = poses(1:7,j);
        robot_T_part = eye(4);
        robot_T_part(1:3,1:3) = quat2rotm(x(4:7)');
        robot_T_part(1:3,4) = x(1:3);
        [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,robot_T_part);
        part_plt(v_part_transf, f_part, name_part, [0.2,0,0]); % plot tool stl
        daspect([1,1,1]);
        pause(0.3)
        cla;
    end
end

%     if i==38
%         continue;
%     end
%     if i==25 || i==37 || i==81 || i==3
%         reach = [ reach; -inf ];
%         continue;
%     end
    
%     X = prt_poses(:,i);
%     X = csvread( fullfile(folder, strcat('Sample_',num2str(i),'.csv')) );
    
%     ik_init = X(9,1);
%     ik_fin = X(10,1);
%     
%     reach = [ reach; X(8,end) ];
    
%     x_init = X(1:7,1);
%     x_fin = X(1:7,end);
%     
%     transf_xyzbxbybz = transf_points(X,xyz_bxbybz, robot1);
%     eval_vxl_reach(transf_xyzbxbybz, no_pts,Capability_Map)
%     
%     transf_xyzbxbybz = transf_points(x_fin,xyz_bxbybz, robot1);
%     eval_vxl_reach(transf_xyzbxbybz, no_pts,Capability_Map)
    
    
%     if X(8) > 160
%         X
%     end
%     
    
%     if X(end,1) < 30
%         if X(end-1,1) < 20
%             i
%         end
%     end
%     reach = [ reach; X(end,1) ];
    
%     xint = X(1:7,1);
%     xfin = X(1:7,end);
%     
%     transf_xyzbxbybz = transf_points( xint,xyz_bxbybz, robot1 );
%     no_pts = size( xyz_bxbybz, 1 );
%     
%     figure(1)
%     joint_angles = [];
%     joint_config = [0;0;0;0;0;0;0];
%     for i = 1:no_pts
%         [joint_config,status] = get_iiwa_IK( joint_config,transf_xyzbxbybz(i,:),tolerances(i,:),...
%                                             CONFIG.IKoptions, CONFIG.theta_lb, CONFIG.theta_ub,true,10 );
%         if status
%             joint_angles = [joint_angles; [i,1] ];
%         else
%             joint_angles = [joint_angles; [i,0] ];
%         end
%     end
%     
%     for i=1:size(joint_angles,1)
%         if joint_angles(i,2)==1
%             scatter( i , 2, 'g', 20, 'filled' )
%         else
%             scatter( i , 2, 'r', 20, 'filled' )
%         end
%     end
        
%     figure(2)    
%     transf_xyzbxbybz = transf_points( xfin,xyz_bxbybz, robot1 );
%     no_pts = size( xyz_bxbybz, 1 );
%     
%     joint_angles = [];
%     joint_config = [0;0;0;0;0;0;0];
%     for i = 1:no_pts
%         [joint_config,status] = get_iiwa_IK( joint_config,transf_xyzbxbybz(i,:),tolerances(i,:),...
%                                             CONFIG.IKoptions, CONFIG.theta_lb, CONFIG.theta_ub,true,10 );
%         if status
%             joint_angles = [joint_angles; [i,1] ];
%         else
%             joint_angles = [joint_angles; [i,0] ];
%         end
%     end
%     
%     for i=1:size(joint_angles,1)
%         if joint_angles(i,2)==1
%             scatter( i , 2, 'g', 20, 'filled' )
%         else
%             scatter( i , 2, 'r', 20, 'filled' )
%         end
%     end
%     
%     pause(0.0001)
            
% %     
%     robot_T_part = eye(4);
%     robot_T_part(1:3,1:3) = quat2rotm(X(4:7)');
%     robot_T_part(1:3,4) = X(1:3);
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,robot_T_part);
%     part_plt(v_part_transf, f_part, name_part, [0.2,0,0]); % plot tool stl
%     daspect([1,1,1]);
%     
%     robot_T_part = eye(4);
%     robot_T_part(1:3,1:3) = quat2rotm(xfin(4:7)');
%     robot_T_part(1:3,4) = xfin(1:3);
%     [v_part_transf,n_part_transf] = stlTransform(v_part,n_part,robot_T_part);
%     part_plt(v_part_transf, f_part, name_part, [0.2,0.2,0]); % plot tool stl
%     daspect([1,1,1]);
%     saveas(gcf, strcat('Figure',num2str(i),'.png'));
%     cla;
    
    
%     ik_init = X(9,1);
%     ik_fin = X(10,1);
% %     
%     if ik_fin-ik_init < 0
%         continue;
%     end
% %     
%     H = [H; ik_fin-ik_init];
%     
%     obj_init = X(8,1);
%     obj_fin = X(8,end);
%     
%     if (obj_fin-obj_init)/obj_init > 50
%         continue;
%     end
%     H = [H; [i,(obj_fin-obj_init)/obj_init] ];
    
    
%     scatter( i, ik_init, 100, 'r', 'filled' );
%     scatter( i, ik_fin, 100, 'g', 'filled' );
%     plot( [i,i],[ik_init,ik_fin], 'b', 'linewidth', 1 )
%     pause(0.3);
%     
%     cla;
% end

% scatter( H(:,1), H(:,2), 20, 'b', 'filled' );
% histogram( H , 20, 'FaceColor', 'r');
% title('Density x Reach')







function transf_xyzbxbybz = transf_points(Xk,xyz_bxbybz, robot1)
    R = quat2rotm( Xk(4:7)' );
    t = Xk(1:3);
    transf_xyzbxbybz(:,1:3) = (R*xyz_bxbybz(:,1:3)' + t)';
    transf_xyzbxbybz(:,4:6) = (R*xyz_bxbybz(:,4:6)')';
    transf_xyzbxbybz(:,7:9) = (R*xyz_bxbybz(:,7:9)')';
    transf_xyzbxbybz(:,10:12) = (R*xyz_bxbybz(:,10:12)')';
    for i=1:size(transf_xyzbxbybz,1)
        T = eye(4);
        T(1:3,1:3) = [ transf_xyzbxbybz(i,4:6)', transf_xyzbxbybz(i,7:9)', transf_xyzbxbybz(i,10:12)' ];
        T(1:3,4) = transf_xyzbxbybz(i,1:3)';
        Tf = ((robot1.robot_ree_T_tee' \ T'))';
        
        transf_xyzbxbybz(i,1:3) = Tf(1:3,4)';
        transf_xyzbxbybz(i,4:6) = Tf(1:3,1)';
        transf_xyzbxbybz(i,7:9) = Tf(1:3,2)';
        transf_xyzbxbybz(i,10:12) = Tf(1:3,3)';
    end
end



function obj_reach = eval_vxl_reach(points, no_pts,Capability_Map)
    reach = 0;
    obj_reach = 0;
    for i=1:no_pts
        % Voxelize
        x = int8(points(i,1)*50);
        y = int8(points(i,2)*50+50);
        z = int8(points(i,3)*50);
        bz = points(i,10:12)';
        
        key = char(strcat( to_string_v(x), to_string_v(y), to_string_v(z)));
        if Capability_Map.cap_map_z{ Capability_Map.voxel_index(key) }.reachable
            vectors = Capability_Map.cap_map_z{ Capability_Map.voxel_index(key) }.vectors;
            neighborhood_density = [];
            for j=1:size(vectors,1)
                prod = ( vectors(j,1)*bz(1) + vectors(j,2)*bz(2) + vectors(j,3)*bz(3) );
                if prod > 0.3420    % 70 Deg dot product value
                    neighborhood_density = [neighborhood_density; vectors(j,:)] ;
                    reach = reach + 1;
                end  
            end
            [U,D,V] = svd( neighborhood_density' * neighborhood_density );
            volume = abs( D(1,1)*D(2,2)*D(3,3) );
            obj_reach = obj_reach + volume*reach;
        end
    end
end

% xyz_bxbybz(:,1:3) = xyz_bxbybz(:,1:3)*1000;
% 
% close all;
% hold on;
% part = 'HS.STL';
% [v_part, f_part, n_part, name_part] = stlRead(part); % read tool stl
% % v_part = v_part./1000;
% part_plt(v_part, f_part, name_part, [1,0,0]);
% 
% 
% scatter3(xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),80,'k','filled');
% quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),xyz_bxbybz(:,4),xyz_bxbybz(:,5),xyz_bxbybz(:,6),'r','AutoScaleFactor',0.4 );
% quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),xyz_bxbybz(:,7),xyz_bxbybz(:,8),xyz_bxbybz(:,9),'g','AutoScaleFactor',0.4 );
% quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),xyz_bxbybz(:,10),xyz_bxbybz(:,11),xyz_bxbybz(:,12),'b','AutoScaleFactor',0.4 );
% pause(0.01)
% xlabel('X axis')
% ylabel('Y axis')
% zlabel('Z axis')
% 
% 
% set(gca,'fontweight','bold','fontsize',20)
% grid off
% set(gcf,'color','w')
% plot3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),'b','linewidth',2 )
% daspect([1,1,1])
% 
% 
% 
% 
% quiver3( 0,0,0,1,0,0,'r','AutoScaleFactor',0.1 );
% quiver3( 0,0,0,0,1,0,'g','AutoScaleFactor',0.1 );
% quiver3( 0,0,0,0,0,1,'b','AutoScaleFactor',0.1 );


% quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),xyz_bxbybz(:,4),xyz_bxbybz(:,5),xyz_bxbybz(:,6),'r','AutoScaleFactor',1 );
% quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),xyz_bxbybz(:,7),xyz_bxbybz(:,8),xyz_bxbybz(:,9),'g','AutoScaleFactor',1 );
% quiver3( xyz_bxbybz(:,1),xyz_bxbybz(:,2),xyz_bxbybz(:,3),xyz_bxbybz(:,10),xyz_bxbybz(:,11),xyz_bxbybz(:,12),'b','AutoScaleFactor',1 );


% quiver3( main_pts(:,1),main_pts(:,2),main_pts(:,3),main_pts(:,4),main_pts(:,5),main_pts(:,6),'r','AutoScaleFactor',1 );
% quiver3( main_pts(:,1),main_pts(:,2),main_pts(:,3),main_pts(:,7),main_pts(:,8),main_pts(:,9),'g','AutoScaleFactor',1 );
% quiver3( main_pts(:,1),main_pts(:,2),main_pts(:,3),main_pts(:,10),main_pts(:,11),main_pts(:,12),'b','AutoScaleFactor',1 );


% quiver3( transf_xyzbxbybz(:,1),transf_xyzbxbybz(:,2),transf_xyzbxbybz(:,3),transf_xyzbxbybz(:,4),transf_xyzbxbybz(:,5),transf_xyzbxbybz(:,6),'r','AutoScaleFactor',1 );
% quiver3( transf_xyzbxbybz(:,1),transf_xyzbxbybz(:,2),transf_xyzbxbybz(:,3),transf_xyzbxbybz(:,7),transf_xyzbxbybz(:,8),transf_xyzbxbybz(:,9),'g','AutoScaleFactor',1 );
% quiver3( transf_xyzbxbybz(:,1),transf_xyzbxbybz(:,2),transf_xyzbxbybz(:,3),transf_xyzbxbybz(:,10),transf_xyzbxbybz(:,11),transf_xyzbxbybz(:,12),'b','AutoScaleFactor',1 );



% quiver3( transf_xyzbxbybz(a:b,1),transf_xyzbxbybz(a:b,2),transf_xyzbxbybz(a:b,3),transf_xyzbxbybz(a:b,4),transf_xyzbxbybz(a:b,5),transf_xyzbxbybz(a:b,6),'r','AutoScaleFactor',1 );
% quiver3( transf_xyzbxbybz(a:b,1),transf_xyzbxbybz(a:b,2),transf_xyzbxbybz(a:b,3),transf_xyzbxbybz(a:b,7),transf_xyzbxbybz(a:b,8),transf_xyzbxbybz(a:b,9),'g','AutoScaleFactor',1 );
% quiver3( transf_xyzbxbybz(a:b,1),transf_xyzbxbybz(a:b,2),transf_xyzbxbybz(a:b,3),transf_xyzbxbybz(a:b,10),transf_xyzbxbybz(a:b,11),transf_xyzbxbybz(a:b,12),'b','AutoScaleFactor',1 );


% quiver3( xyz_bxbybz(a:b,1),xyz_bxbybz(a:b,2),xyz_bxbybz(a:b,3),xyz_bxbybz(a:b,4),xyz_bxbybz(a:b,5),xyz_bxbybz(a:b,6),'r','AutoScaleFactor',1 );
% quiver3( xyz_bxbybz(aa:bb,1),xyz_bxbybz(a:b,2),xyz_bxbybz(a:b,3),xyz_bxbybz(a:b,7),xyz_bxbybz(a:b,8),xyz_bxbybz(a:b,9),'g','AutoScaleFactor',1 );
% quiver3( xyz_bxbybz(aa:bb,1),xyz_bxbybz(a:b,2),xyz_bxbybz(a:b,3),xyz_bxbybz(a:b,10),xyz_bxbybz(a:b,11),xyz_bxbybz(a:b,12),'b','AutoScaleFactor',1 );



% [[-9.98864466e-01 -4.47949996e-02 -1.62230272e-02  1.07881061e+03]
% [ 4.47883114e-02 -9.98996199e-01  7.75538698e-04  7.59747635e+01]
% [-1.62414827e-02  4.80560561e-05  9.99868097e-01  4.71714088e+00]
% [ 0.00000000e+00  0.00000000e+00  0.00000000e+00  1.00000000e+00]]