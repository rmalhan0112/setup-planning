
err = 0;
for t1 = [ -pi: -pi/5: pi ]
    for t2 = [ -pi: -pi/5: pi ]

        T10 = [ cos(t1), -sin(t1), 0, 0;
                sin(t1), cos(t1), 0, 0;
                0,0,1,0;
                0,0,0,1];


        T21 = [ cos(t2),  -sin(t2),  0, l1;
                sin(t2), cos(t2), 0, 0;
                0,0,1,0;
                0,0,0,1];

        T32 = [ 1,0,0,l2;
            0,1,0,0;
            0,0,1,0;
            0,0,0,1];

        T = T10 * T21 * T32;
        R = T(1:2,1:2);
        J0 = [ -l1*sin(t1)-l2*sin(t1+t2),  -l2*sin(t1+t2);
                l1*cos(t1)+l2*cos(t1+t2),  l2*cos(t1+t2)];

        J3 = [ l1*sin(t2), 0;
               l1*cos(t2)+l2, l2 ];

        Fee = [ 10; 10 ];
        
        T0 = J0'*R*Fee;
        T3 = J3*Fee;
        
        err = err + abs(T3(1)-T0(1)) + abs(T3(2)-T0(2));
    end
end

err