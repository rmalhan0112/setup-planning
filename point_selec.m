 %% Automated Composite Sheet Layup Planner

% This code allows an interface for the user to select points on a CAD part
% which represent the start and end points for every move. The planner
% converts these points into structured trajectories and writes
% instructions to the robot to follow.

clear all;
close all
clc
dbstop if error;

%% Define part and Plot it

% Define the name of the part stored in the working directory
% part = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/Mold_Ascent.stl';
% part = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/vertical.stl';
part = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/concave.stl';
% part = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/lockheed.stl';
% part = 'GE90.stl';
% part = 'custom.stl';
% part = 'custom_scaled.stl';
% part = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/ramp.stl';
% part = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/custom.stl';
% part = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/airfoil.stl';
% part = '/home/rmalhan/Work/USC/Composite Automation/Modules/Meshes/dome.stl';
% part = 'GE90_scaled.stl';
% part = 'dome_scaled.stl';
% part = 'airfoil_scaled.stl';
% part = 'Mold-Final.stl';
% Note changing direction of Z axis in stl origin. make sure to change the
% dot product in filtering faces in curve_fit

%% Load Part and all the info
%STLREAD is a function obtaiend from matlab exchange. Refer to the file for
%more details.
[v, f, n, name] = stlRead( part );
delete(gca);
close all;    
    
% Choose what color the CAD part needs to be displayed.
col_matrix = [0.941176, 0.972549, 1];

% Plotting the CAD part in Figure-1
figure(1)
patch('Faces',f,'Vertices',v,'FaceVertexCData',col_matrix,'FaceColor',col_matrix);
% set(gca,'zdir','reverse')
xlabel('X-Axis')
ylabel('Y-Axis')
zlabel('Z-Axis')
hold on


%% User-Interface for Selection of Points
  
% Define object for Data Cursor
dcm_obj = datacursormode(figure(1));
set(dcm_obj,'SnapToDataVertex','off')
strt_pts = [];
end_pts = [];
key=0;
flag = 0;
grp=[];
idx = 1;
xyz_bxbybz = [];
daspect([1,1,1])

% Keep Selecting Start and End points alternatively.
while 1
    key=0;
    fprintf('Select Start Point')
    while key==0
        try key = waitforbuttonpress; catch flag=1; break; end 
    end
    if flag==1 
        fprintf('\nSelection Compelte\n'); 
        break; 
    end
    c_info = getCursorInfo(dcm_obj);
    strt_pt = c_info.Position;
    strt_pts = [strt_pts;strt_pt];
    
    key=0;
    fprintf('Select End Point')
    while key==0
        try key = waitforbuttonpress; catch flag=1; break; end
    end
    if flag==1 
        fprintf('\nSelection Complete\n'); 
        break; 
    end
    c_info = getCursorInfo(dcm_obj);
    end_pt = c_info.Position;
    end_pts = [end_pts;end_pt];
    
    [temp_pts,temp_normals,temp_faces] = curve_fit(strt_pt,end_pt,v,f,n);  %Call the curve_fit function
    
    %Append the Group Indices
    size_points = size(temp_pts);
    idx_start = idx;
    idx = idx + size_points(1,1)-1;
    idx_end = idx;
    range = [idx_start,idx_end];
    grp = [grp;range]; %Append the Group Indices
    % Appending Group Indices complete
    
    [bx,by,bz] = TCP(temp_pts,temp_normals); %compute_TCP function
    xyz_bxbybz = [xyz_bxbybz; horzcat( temp_pts,bx,by,bz )];
%     plot_tcp(bx,by,bz,temp_pts);
    
    %Plot the Points
    scatter3(temp_pts(:,1),temp_pts(:,2),temp_pts(:,3),200,'*','b'); %Plot the Points
    idx = idx + 1;
    daspect([1,1,1])
end
% Temporary tolerance definitions
% tolerances = zeros(size(xyz_bxbybz,1),4);
% tolerances(:,2) = 0.18; % Manually define 7 Degrees tolerance
% tolerances(:,1) = 0.005;
% tolerances(:,3) = 0.0175;
% tolerances(:,4) = 0.0175;
% csvwrite('Ascent_tol_file.csv',tolerances);

csvwrite('concave_points.csv',xyz_bxbybz);
csvwrite('concave_grps.csv',grp);


%% Plot the Tool_TCP

function plot_tcp(bx,by,bz,points)
    bx = bx .* 1;
    by = by .* 1;
    bz = bz .* 1;
    
    hold on
    % Plot the X Vector
    quiver3(points(:,1),points(:,2),points(:,3),...
       bx(:,1),bx(:,2),bx(:,3),'r','linewidth',2,'AutoScaleFactor',0.8,'MaxHeadSize',0.6);
    hold on
    % Plot the Y Vector
    quiver3(points(:,1),points(:,2),points(:,3),...
       by(:,1),by(:,2),by(:,3),'g','linewidth',2,'AutoScaleFactor',0.8,'MaxHeadSize',0.6);
    hold on
    % Plot the Z vector
    quiver3(points(:,1),points(:,2),points(:,3),...
       bz(:,1),bz(:,2),bz(:,3),'b','linewidth',2,'AutoScaleFactor',0.8,'MaxHeadSize',0.6);
end