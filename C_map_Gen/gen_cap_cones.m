% load 'vctr_nghs.mat'
% load 'vectr_sphere.mat'
% 
% cap_map = containers.Map(int32(1),[1,1,1,1,1]);
% remove(cap_map,1);
% sph_map_keys = keys(vectr_sphere);
% 
% for i = 1:262701
%     if mod(i,100)==0
%         fprintf('%d\n',i)
%     end
%     key = index_key{i};
%     ori = double(iiwa_ori_map(key))/100;
%     if size(ori)==0
%         continue;
%     end
%     ori = ori(:,7:9);
%     for i = 1:size(ori)
%         ori(i,:) = ori(i,:) / norm(ori(i,:));
%     end
%     cap_map(key) = axis_angles( vectr_sphere,ori,sph_map_keys,neighbors );
% end
% 
% save('cap_map.mat','cap_map')

% figure(1)
% hold on;
% daspect([1,1,1])
% 
% % Visualize cones
% while true
%     no = randsample( 1:262701,1 );
%     ori = double(iiwa_ori_map(index_key{no}))/100;
%     if size(ori)==0
%         continue;
%     end
%     ori = ori(:,7:9);
%     for i = 1:size(ori)
%         ori(i,:) = ori(i,:) / norm(ori(i,:));
%     end
%     quiver3( zeros(size(ori,1),1),zeros(size(ori,1),1),zeros(size(ori,1),1),...
%               ori(:,1),ori(:,2),ori(:,3),'k'  );
%     vec = cap_map(index_key{no});
%     quiver3(0,0,0,vec(1),vec(2),vec(3),'m','AutoScaleFactor',2);
%     rad2deg([vec(4),vec(5)])
%     cla;
% end


% idx defect = 72748, 181058, 22182, 38234, 109617
% Mean as metric fucked up 63493