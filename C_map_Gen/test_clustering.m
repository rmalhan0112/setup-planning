% % Create dense resolution voxelized square of side length 1
% resolution = 10;    % i.e 10 divisions per 1 unit
% vectr_sphere = containers.Map( int32(1),[1,1,1,1] );
% remove(vectr_sphere,1);
% 
% sphr_pts = csvread('sphere_pts.csv');
% 
% % coords = [];
% % for x = -1:1/resolution:1
% %     for y = -1:1/resolution:1
% %         for z = -1:1/resolution:1
%          for i = 1:size(sphr_pts,1)
%             x = int32((sphr_pts(i,1)+1)*resolution);
%             y = int32((sphr_pts(i,2)+1)*resolution);
%             z = int32((sphr_pts(i,3)+1)*resolution);
%             
%             key = x*10^4 + y*100 + z;
%             vectr_sphere(key) = [x,y,z,0];
% %             coords = [coords; x,y,z];
%         end
% %     end
% % end
% 
% save('vectr_sphere.mat','vectr_sphere')
% return;


% % Precomputing Neighbors
% load 'vectr_sphere.mat'
% map_keys = keys(vectr_sphere);
% neighbors = containers.Map(int32(1),[int32(1);int32(1)]);
% remove(neighbors,1);
% 
% for i = 1:size(map_keys,2)
%     key = map_keys{i};
%     curr_ori = vectr_sphere(key);
%     curr_ori = double(curr_ori(1:3))/resolution - 1;
%     curr_ori = curr_ori / norm(curr_ori);
%     ngh = [];
%     for idx = 1:size(map_keys,2)
%         j = map_keys{idx};
%         vec = vectr_sphere(j);
%         vec = double(vec(1:3))/resolution - 1;
%         vec = vec / norm(vec);
%         if acos( vec(1)*curr_ori(1) + vec(2)*curr_ori(2) + vec(3)*curr_ori(3) ) < 0.1745
%             ngh = [ngh; j];
%         end
%     end
%     neighbors(key) = ngh;
% end
% 
% save('vctr_nghs.mat','neighbors')

% for i = 1:size(ori_keys,1)
%     key = ori_keys(i);
%     vec = vectr_sphere(key);
%     vec(end) = 1;
%     vectr_sphere(key) = vec;
% %     end
%     ngh = [];
%     for idx = 1:size(map_keys,2)
%         j = map_keys{idx};
%         vec = vectr_sphere(j);
%         vec = double(vec(1:3))/resolution - 1;
%         vec = vec / norm(vec);
%         if acos( vec(1)*ori_vecs(i,1) + vec(2)*ori_vecs(i,2) + vec(3)*ori_vecs(i,3) ) < 0.1745
%             ngh = [ngh; j];
%         end
%     end
%     neighbors{i} = ngh;
% end

% return;


% clear all;
% close all;
% clc;

resolution = 10;
load 'vectr_sphere.mat'
load 'vctr_nghs.mat'
map_keys = keys(vectr_sphere);
% ori = csvread( 'cones4.csv' );

% Get a specific index from iiwa map
no = 35000;
ori = double(iiwa_ori_map(index_key{no}))/100;
ori = ori(:,7:9);
for i = 1:size(ori)
    ori(i,:) = ori(i,:) / norm(ori(i,:));
end
%%%%%%%%%%%%%%%

ori_keys = [];
ori_vecs = [];
for i = 1:size(ori,1)
    x = int32((ori(i,1)+1)*resolution);
    y = int32((ori(i,2)+1)*resolution);
    z = int32((ori(i,3)+1)*resolution);
    key = x*10^4 + y*100 + z;
    if isKey( vectr_sphere,key )
        ori_keys = [ori_keys; key];
    end
end

ori_keys = int32(unique(ori_keys,'rows'));
for idx = 1:size(ori_keys,1)
    i = ori_keys(idx,1);
    vec = vectr_sphere(i);
    vec = double(vec(1:3))/resolution - 1;
    vec = vec / norm(vec);
    ori_vecs = [ori_vecs; vec];
end


% Making the neighborhood of green vector green
for i = 1:size(ori_keys,1)
    ngh = neighbors( ori_keys(i) );
    for j = 1:size(ngh,1)
        vec = vectr_sphere(ngh(j));
        vec(end) = 1;
        vectr_sphere(ngh(j)) = vec;
    end
end
        


disp('Plotting')
figure(1)
daspect([1,1,1]);
hold on;
reach_coords = [];
unr_coords = [];
for i = 1:size(map_keys,2)
    key = map_keys{i};
    vec = vectr_sphere(key);
    if vec(end) == 1
        vec = double(vec(1:3))/resolution - 1;
        vec = vec / norm(vec);
        reach_coords = [ reach_coords; vec ];
    else
        vec = double(vec(1:3))/resolution - 1;
        vec = vec / norm(vec);
        unr_coords = [ unr_coords; vec ];
    end
       
end

vec = mean(unr_coords,1);
vec = vec / norm(vec);

beta = [];
for j = 1:size(ori,1)
    beta = [ beta; acos( vec(1)*ori(j,1) + vec(2)*ori(j,2) +...
                    vec(3)*ori(j,3)) ];
end
range = [ min(beta),max(beta) ];
rad2deg([range(1,1),range(1,2)])  

quiver3( 0,0,0,vec(1),vec(2),vec(3),'m','AutoScaleFactor',2 )
scatter3(reach_coords(:,1),reach_coords(:,2),reach_coords(:,3),'g','filled');
scatter3(unr_coords(:,1),unr_coords(:,2),unr_coords(:,3),'r','filled');

return;



pr_axes = pca(unr_coords)';
quiver3( zeros(3,1),zeros(3,1),zeros(3,1),pr_axes(1,:)',pr_axes(2,:)',pr_axes(3,:)','m','AutoScaleFactor',2 )
scatter3(reach_coords(:,1),reach_coords(:,2),reach_coords(:,3),'g','filled');
scatter3(unr_coords(:,1),unr_coords(:,2),unr_coords(:,3),'r','filled');
return;


pr_axes = [pr_axes; -pr_axes];

range = [];
for i = 1:6
    vec = pr_axes(i,:);
    beta = [];
    for j = 1:size(reach_coords,1)
        beta = [ beta; acos(dot(vec,reach_coords(j,:))) ];
    end
    range = [range; i,min(beta),max(beta),max(beta)-min(beta)];
end
range = sortrows(range,4);
range = range(1:2,:);
range = sortrows(range,2);

rad2deg([range(1,2),range(1,3)])

% quiver3( zeros(3,1),zeros(3,1),zeros(3,1),coeff(1,:)',coeff(2,:)',coeff(3,:)','m','AutoScaleFactor',2 )

% quiver3( 0,0,0,coeff(1,1)',coeff(2,1)',coeff(3,1)','m','AutoScaleFactor',2 )
% quiver3( 0,0,0,-coeff(1,1)',-coeff(2,1)',-coeff(3,1)','m','AutoScaleFactor',2 )

vec = pr_axes(range(1,1),:);
quiver3( 0,0,0,vec(1),vec(2),vec(3),'m','AutoScaleFactor',2 );

scatter3(reach_coords(:,1),reach_coords(:,2),reach_coords(:,3),'g','filled');
scatter3(unr_coords(:,1),unr_coords(:,2),unr_coords(:,3),'r','filled');

return;




clear all;
close all;
clc;







figure(1);
hold on;
daspect([1,1,1]);
quiver3(0,0,0,1,0,0,'r','linewidth',4,'AutoScaleFactor',2);
quiver3(0,0,0,0,1,0,'g','linewidth',4,'AutoScaleFactor',2);
quiver3(0,0,0,0,0,1,'b','linewidth',4,'AutoScaleFactor',2);

ori = csvread( 'cones1.csv' );

[az,elv] = cart2sph( ori(:,1),ori(:,2),ori(:,3) );
xlim([-pi/2,pi/2]);
ylim([-pi,pi]);
% scatter( elv,az );

quiver3( zeros(size(ori,1),1),zeros(size(ori,1),1),zeros(size(ori,1),1),...
              ori(:,1),ori(:,2),ori(:,3),'k'  );
return;


while true
    no = randsample( 1:262701,1 );
    ori = double(iiwa_ori_map(index_key{no}))/100;
    if size(ori)==0
        continue;
    end
    ori = ori(:,7:9);
    for i = 1:size(ori)
        ori(i,:) = ori(i,:) / norm(ori(i,:));
    end
    quiver3( zeros(size(ori,1),1),zeros(size(ori,1),1),zeros(size(ori,1),1),...
              ori(:,1),ori(:,2),ori(:,3),'k'  );
    cla;
end


% counter = 1;
% cones1 = [];
% cones2 = [];
% while counter < 500
%     dir_vec = [0;0;1];
%     rotation = eul2rotm( [randsample(-pi:0.01:pi,1),randsample(-pi:0.01:pi,1),randsample(-pi:0.01:pi,1)],'ZYX' );
%     dir_vec = rotation * dir_vec;
%     
%     if acos( dot(dir_vec,[0;0;1]) ) < deg2rad(30)
%         cones1 = [ cones1; dir_vec' ];
%         counter = counter + 1;
%     end
%     
%     if acos( dot(dir_vec,[0;0;-1]) ) < deg2rad(60)
%         cones2 = [ cones2; dir_vec' ];
%         counter = counter + 1;
%     end
% end
% 
% cones3 = (eul2rotm( [0,0,pi/2],'ZYX' ) * cones1')';
% 
% cones = [cones1;cones2];


% for ctr = 1:10
%     idx = randsample( 1:260938, 1 );
% %     idx = 94617;
%     Vec = iiwa_capability.orientation{idx};
%     Vec = double(Vec(:,7:9))/100;
% 
%     for i = 1:size(Vec,1)
%         Vec(i,:) = Vec(i,:)/norm(Vec(i,:));
%     end
% 
%     S = gen_clusters( Vec );
% 
%     hold on;
%     daspect([1,1,1]);
%     color=['y','m','c','r','g','b','w','k','y','m','c','r','g','b','w','k'];
%     o=1;
%     clusters = {};
%     cluster = [];
%     for m = 1:length(S)
%         if S(m)~=0
%             cluster = [ cluster; [Vec(S(m),1),Vec(S(m),2),Vec(S(m),3)] ];
%         else
%             clusters{end+1} = cluster;
%             cluster = [];
%         end 
%     end
% 
%     for i=1:size(clusters,2)
%         cones = clusters{i};
%         quiver3(zeros(size(cones,1),1),zeros(size(cones,1),1),zeros(size(cones,1),1),cones(:,1),cones(:,2),cones(:,3),color(i));
%     end
%     cla;
% end



% figure(1)
% hold on;
% daspect([1,1,1])
% quiver3( zeros(size(cones,1),1),zeros(size(cones,1),1),zeros(size(cones,1),1),cones(:,1),cones(:,2),cones(:,3),'r' );
% quiver3( 0,0,0,0,0,1,'k' );
% return;