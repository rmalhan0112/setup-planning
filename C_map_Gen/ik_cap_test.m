clear all;
clc;
close all;

load cap_map.mat

%%%%%%%% Draping robot AND TOOL DEFINITION %%%%%%%%%%%%%
robot.base_T_robot = eye(4);
robot.base_T_robot(1:3,4) = [0;0;0];
robot.base_T_robot(1:3,1:3) = eul2rotm([0.0,0,0]);

robot.robot_ree_T_tee = eye(4);
% robot_ree_T_tee(1:3,4) = [0; 0; 0]; % For Gripper
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%

% Optimizer Bounds
theta_lb(1) = -2.967059728390360;
theta_lb(2) = -2.094395102393195;
theta_lb(3) = -2.967059728390360;
theta_lb(4) = -2.094395102393195;
theta_lb(5) = -2.967059728390360;
theta_lb(6) = -2.094395102393195;
theta_lb(7) = -3.054326190990077;
theta_lb = theta_lb';
theta_ub = -theta_lb;

IKoptions = optimoptions('fmincon','Algorithm', 'sqp');
IKoptions.MaxIterations = 5000;
IKoptions.MaxFunctionEvaluations = 1e7;
IKoptions.OptimalityTolerance = 1e-8;
IKoptions.StepTolerance = 1e-8;
IKoptions.Display = 'off';
IKoptions.SpecifyObjectiveGradient = false;
IKoptions.ObjectiveLimit = 1e-8;


I = eye(4);
init_guess = [ 0;0;0;0;0;0;0 ];
tolerance(1) = 0.003;
tolerance(2) = 0.0524;
tolerance(3) = 0.0524;
tolerance(4) = 0.0524;

sol = 0;
counter = 0;
tot_no = 5000;
tot_time = 0;
cap_succ = 0;
ik_succ = 0;
fails = 0;

while counter <= tot_no
    flag = false;
%     theta = get_random_theta();
%     T = get_iiwa_FK_mex( theta,I );
    point(1,1) = randsample( 0:0.0001:1,1 );
    point(1,2) = randsample( -1:0.0001:1,1 );
    point(1,3) = randsample( 0:0.0001:1,1 );
    rot = eul2rotm( [randsample( -pi:0.001:pi,3 )],'ZYX' );
    point(1,4:6) = rot(:,1);
    point(1,7:9) = rot(:,2);
    point(1,10:12) = rot(:,3);
    
    counter = counter + 1;
    
    % Test Using Capability Map
    key = gen_cmap_key( point(1:3) );
    if isKey( cap_map, key )
        vec = cap_map(key);
        angle = acos( vec(1) * point(10) + vec(2) * point(11) + vec(3) * point(12) );
        if (angle >= vec(4) && angle <= vec(5))
            cap_succ = cap_succ + 1;
            flag = true;
        end
    end
    
    [joints,status] = get_iiwa_IK( init_guess,point,tolerance,IKoptions,theta_lb,theta_ub,robot.robot_ree_T_tee );
    
    if status
        ik_succ =  ik_succ + 1;
        if ~flag
            fails = fails + 1;
        end
    end
end
cap_succ
ik_succ
fails
