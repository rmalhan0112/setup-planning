% clear all;
% close all;
% clc;

% iiwa_ori_map = containers.Map( int32(1),[1,2,3] );
% remove(iiwa_ori_map,1);
% 
% for z = 0 : 1: 50
%     for x = 0 : 1: 50
%         for y = 0 : 1: 100
%             key = int32(x*100000 + y*100 + z);
%             iiwa_ori_map(key) = [];
%         end
%     end
% end

I = eye(4);
counter = 0;

while counter < 5000000000
    theta = get_random_theta();
    FK_all = get_iiwa_FK_all_joints_mex( theta,I );
    ee_base = FK_all(33:36,:);
    
    if ( (ee_base(1,4) > 1 || ee_base(1,4) < 0) || (ee_base(2,4) > 1 || ee_base(2,4) < -1) ||...
            (ee_base(3,4) > 1 || ee_base(3,4) < 0) )
            continue;
    end
    counter = counter + 1;
    
    key = int32(ee_base(1,4)*50)*100000 + int32(ee_base(2,4)*50+50)*100 + int32(ee_base(3,4)*50);
    bx = [ int8(ee_base(1,1)*100), int8(ee_base(2,1)*100), int8(ee_base(3,1)*100) ];
    by = [ int8(ee_base(1,2)*100), int8(ee_base(2,2)*100), int8(ee_base(3,2)*100) ];
    bz = [ int8(ee_base(1,3)*100), int8(ee_base(2,3)*100), int8(ee_base(3,3)*100) ];
    
    iiwa_ori_map(key) = [iiwa_ori_map(key); [ bx,by,bz ] ];
end