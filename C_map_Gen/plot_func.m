figure(1);
hold on;
daspect([1,1,1]);
quiver3(0,0,0,1,0,0,'r','linewidth',4,'AutoScaleFactor',2);
quiver3(0,0,0,0,1,0,'g','linewidth',4,'AutoScaleFactor',2);
quiver3(0,0,0,0,0,1,'b','linewidth',4,'AutoScaleFactor',2);

ori = csvread( 'cones9.csv' );
quiver3( zeros(size(ori,1),1),zeros(size(ori,1),1),zeros(size(ori,1),1),...
              ori(:,1),ori(:,2),ori(:,3),'k'  );
return;
