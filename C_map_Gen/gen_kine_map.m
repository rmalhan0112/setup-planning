% Sequence of Data stored in iiwa kine mat file is:
% 1x6 Velocity Vector, Manipulability Index, K, MSV, MVR
% In all 1x10 Vector

kine_map = containers.Map(int32(1),[1,1,1,1,1]);
remove(kine_map,1);


load('/home/rmalhan/Work/USC/Composite Automation/Modules/Cap Maps/index_key.mat')

for i = 1:262701
    if mod(i,100)==0
        fprintf('%d\n',i)
    end
    key = index_key{i};
    vec = iiwa_kin(key);
    if size(vec,1)==0
        continue;
    end
    kine_map(key) = mean(vec(:,7:10),1 );
end