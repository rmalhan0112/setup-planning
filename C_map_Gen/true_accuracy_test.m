clear all;
clc;
close all;

load cap_map.mat

failure = 0;
no_samples = 500000;
I = eye(4);

for i = 1:no_samples
    theta = get_random_theta();
    T = get_iiwa_FK_mex( theta,I );
    key = gen_cmap_key( T(1:3,4)' );
    if isKey( cap_map, key )
        vec = cap_map(key);
        angle = acos( vec(1)*T(1,3) + vec(2)*T(2,3) + vec(3)*T(3,3) );
        if ~(angle >= vec(4) && angle <= vec(5))
            failure = failure + 1;
        end
    end
end
fprintf('Map accuracy to predict reachable theta is: %d\n',(1 - failure/no_samples)*100)