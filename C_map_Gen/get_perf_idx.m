clear all;
close all;
clc;

load(fullfile('/home/rmalhan/Work/USC/Composite Automation/Modules/Cap Maps/index_key.mat'));

joint_vel = deg2rad([ 98; 98; 100; 130; 140; 180; 180 ]);
joint_torq = [ 176; 176; 110; 110; 110; 40; 40 ];
jv_norm = joint_vel'*joint_vel;

I = eye(4);
counter = 0;

iiwa_dyn = containers.Map(int32(1),[1,1,1,1]);    % Store Manipulability, Condition Number, MSV, MVR
remove(iiwa_dyn,1);

for i = 1:262701
    iiwa_dyn(index_key{i}) = [];
end

counter = 0;
while counter < 100000000
    theta = get_random_theta();
    FK_all = get_iiwa_FK_all_joints_mex( theta,I );
    ee_base = FK_all(33:36,:);
    
    if ( (ee_base(1,4) > 1 || ee_base(1,4) < 0) || (ee_base(2,4) > 1 || ee_base(2,4) < -1) ||...
            (ee_base(3,4) > 1 || ee_base(3,4) < 0) )
            continue;
    end
    counter = counter + 1;
    
    if mod(counter,100000)==0
        fprintf('%d\n',counter);
    end
    key = int32(ee_base(1,4)*50)*100000 + int32(ee_base(2,4)*50+50)*100 + int32(ee_base(3,4)*50);
    Jac = get_iiwa_GeoJac( FK_all ); % Wxyz, Vxyz
    
%     manip = (abs(det(Jac*Jac')))^0.5;   % Manipulability
    [U,D,V] = svd(Jac');
    manip = D(1,1)*D(2,2)*D(3,3)*D(4,4)*D(5,5)*D(6,6);
    k = D(1,1) / D(6,6);    % Condition Number
    msv = D(6,6);   % Minimum Singular Value
    
%     velocity_vec = Jac * joint_vel;
%     mvr = ((velocity_vec'*velocity_vec) / jv_norm)^0.5;
    iiwa_dyn(key) = [iiwa_dyn(key); [ manip,k,msv ]];
    
%     force_vec = Jac' \ joint_torq;
end