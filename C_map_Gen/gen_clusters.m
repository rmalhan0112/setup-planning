function S = gen_clusters( Vec )
    neig_thresh=deg2rad(15);
    cone_thresh=deg2rad(89);

    % Creating neighbourhood matrix
    for i=1:size(Vec,1)
        for j=1:size(Vec,1)
            if i==j
                A(i,j)=0;
                B(i,j)=0;
            elseif i<j
                if acos(dot(Vec(i,:),Vec(j,:)))<=neig_thresh
                    A(i,j)=1;
                else
                    A(i,j)=0;
                end

                if acos(dot(Vec(i,:),Vec(j,:)))<=cone_thresh
                    B(i,j)=1;
                else
                    B(i,j)=0;
                end
            else
                A(i,j)=A(j,i);
                B(i,j)=B(j,i);
            end
        end
    end

    S=[];

    count=1;
    while true
        D=[];
        C=[];

        if isempty(C)
            a=randsample(size(Vec,1),1);
            if ismember(a,S)
                continue;
            end
            b=1;
        end


        C=[C,a];
        while b <= length(C)
            for c=1:size(Vec,1)
                if A(C(b),c)==1 && ismember(c,C)==0 && ismember(c,S)==0
                    C=[C,c];
                end
            end
            b=b+1;
        end


        mVec=[max(Vec(C,1))+min(Vec(C,1)),max(Vec(C,2))+min(Vec(C,1)),max(Vec(C,3))+min(Vec(C,1))]/2;
        [minValue,k] = min(vecnorm((Vec(C,:)-mVec)'))
        D=[D,C(k)];
        for l=1:length(C)

            if B(C(k),C(l))==1 && ismember(C(l),S)==0 && ismember(C(l),D)==0
                D=[D,C(l)];
            end

        end
        if length(D)<30
            count=count+1;
            if count>3
                break;
            end
            continue;
        end

        S=[S,D,0];
        if length(find(S))>=size(Vec,1)
            break;
        end
    end
end