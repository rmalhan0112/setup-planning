function cone = axis_angles( vxl_sphr,ori,map_keys,neighbors )
    vectr_sphere = containers.Map( vxl_sphr.keys,vxl_sphr.values );
    
    ori_keys = [];
    ori_vecs = [];
    for i = 1:size(ori,1)
        x = int32((ori(i,1)+1)*10);
        y = int32((ori(i,2)+1)*10);
        z = int32((ori(i,3)+1)*10);
        key = x*10^4 + y*100 + z;
        if isKey( vectr_sphere,key )
            ori_keys = [ori_keys; key];
        end
    end

    ori_keys = int32(unique(ori_keys,'rows'));
    for idx = 1:size(ori_keys,1)
        i = ori_keys(idx,1);
        vec = vectr_sphere(i);
        vec = double(vec(1:3))/10 - 1;
        vec = vec / norm(vec);
        ori_vecs = [ori_vecs; vec];
    end


    % Making the neighborhood of green vector green
    for i = 1:size(ori_keys,1)
        ngh = neighbors( ori_keys(i) );
        for j = 1:size(ngh,1)
            vec = vectr_sphere(ngh(j));
            vec(end) = 1;
            vectr_sphere(ngh(j)) = vec;
        end
    end

    reach_coords = [];
    unr_coords = [];
    for i = 1:size(map_keys,2)
        key = map_keys{i};
        vec = vectr_sphere(key);
        if vec(end) == 1
            vec = double(vec(1:3))/10 - 1;
            vec = vec / norm(vec);
            reach_coords = [ reach_coords; vec ];
        else
            vec = double(vec(1:3))/10 - 1;
            vec = vec / norm(vec);
            unr_coords = [ unr_coords; vec ];
        end

    end

    vec = mean(unr_coords,1);
    vec = vec / norm(vec);

    beta = [];
    for j = 1:size(ori,1)
        beta = [ beta; acos( vec(1)*ori(j,1) + vec(2)*ori(j,2) +...
                        vec(3)*ori(j,3)) ];
    end
    beta1 = min(beta);
    beta2 = max(beta);
    if beta1 < 0.0873
        beta1 = 0;
    end
    
    if beta2 > 3.05
        beta2 = pi;
    end
    
    cone = [vec,beta1,beta2];
    
    
    
    

    
%     for i = 1:size(ori,1)
%         x = int32((ori(i,1)+1)*10);
%         y = int32((ori(i,2)+1)*10);
%         z = int32((ori(i,3)+1)*10);
%         key = x*10^4 + y*100 + z;
%         if isKey( vectr_sphere,key )
%             vec = vectr_sphere(key);
%             vec(end) = 1;
%             vectr_sphere(key) = vec;
%         end
%     end
% 
%     reach_coords = [];
%     unr_coords = [];
%     for i = 1:size(keys,2)
%         key = keys{i};
%         vec = vectr_sphere(key);
%         if vec(end) == 1
%             vec = double(vec(1:3))/10 - 1;
%             vec = vec / norm(vec);
%             reach_coords = [ reach_coords; vec ];
%         else
%             vec = double(vec(1:3))/10 - 1;
%             vec = vec / norm(vec);
%             unr_coords = [ unr_coords; vec ];
%         end
% 
%     end
% 
%     vec = mean(unr_coords,1);
%     vec = vec / norm(vec);
% 
%     beta = [];
%     for j = 1:size(reach_coords,1)
%         beta = [ beta; acos(dot(vec,reach_coords(j,:))) ];
%     end
%     beta1 = min(beta);
%     beta2 = max(beta);
%     if beta1 < 0.0873
%         beta1 = 0;
%     end
%     
%     if beta2 > 3.05
%         beta2 = pi;
%     end
%     
%     cone = [vec,beta1,beta2];
end