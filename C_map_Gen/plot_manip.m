clear all;
close all;

load kine_map.mat
load('/home/rmalhan/Work/USC/Composite Automation/Modules/Cap Maps/index_key.mat') 


%%%%%%%% Draping robot AND TOOL DEFINITION %%%%%%%%%%%%%
robot.base_T_robot = eye(4);
robot.base_T_robot(1:3,4) = [0;0;0];
robot.base_T_robot(1:3,1:3) = eul2rotm([0.0,0,0]);

robot.robot_ree_T_tee = eye(4);
%%%%%%%% END ROBOT AND TOOL DEFINITION %%%%%%%%%%%%%


manip_sphr = [];
for z = 0 : 1: 25
    for x = 0 : 1: 50
        for y = 0 : 1: 100
            key = int32(x*100000 + y*100 + z);
            if ~isKey(kine_map,key)
                continue;
            end
            prf = kine_map(key);
            manip_sphr = [ manip_sphr; double(x)/50,double(y)/50-1,double(z)/50,prf(1) ];
        end
    end
end


disp('Plotting')
figure(1);
hold on;
joints1 = [0;0;0;0;0;0;0];
plot_robot(joints1,false,robot);
daspect([1,1,1]);
bck_color = [ 0.2,0.2,0.2 ];
set(gcf,'color',bck_color);
set(gca,'color',bck_color,'Xcolor',[1,1,1],'Ycolor',[1,1,1],'Zcolor',[1,1,1],'FontSize',20,'FontWeight','Bold');
camlight;camlight;
scatter3( manip_sphr(:,1),manip_sphr(:,2),manip_sphr(:,3),50,manip_sphr(:,4),'filled' );
xlabel('X Axis (m)','FontSize',20,'FontWeight','Bold','rotation', 58)
zlabel('Z Axis (m)','FontSize',20,'FontWeight','Bold')
ylabel('Y Axis (m)','FontSize',20,'FontWeight','Bold','rotation', -18)
c = colorbar;
c.FontSize = 20;
c.Color = [1,1,1];
c.FontWeight = 'Bold';