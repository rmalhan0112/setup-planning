function key = gen_cmap_key( coordinates )
    key = int32(coordinates(1)*50)*100000 + int32(coordinates(2)*50+50)*100 + int32(coordinates(3)*50);
end