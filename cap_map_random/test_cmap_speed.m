clear all;
close all;
clc;

load c_map.mat
load r_map.mat

I = eye(4);

tic;
for i=1:1000000
    theta = get_random_theta();
    FK_all = get_iiwa_FK_all_joints_mex( theta,I );
    ee_base = FK_all(33:36,:);
    
    if ( (ee_base(1,4) > 1 || ee_base(1,4) < 0) || (ee_base(2,4) > 1 || ee_base(2,4) < -1) ||...
            (ee_base(3,4) > 1 || ee_base(3,4) < 0) )
            continue;
    end
    
    key = gen_cmap_key( ee_base(1:3,4) );
    if r_map( key )
        ori_cones = c_map(key);
        if size(ori_cones,1) < 4
            disp('Yes')
        end
        for j = 1 : size(ori_cones,1)
            if acos( ori_cones(j,1)*ee_base(1,3) + ori_cones(j,2)*ee_base(2,3) + ori_cones(j,3)*ee_base(3,3) ) < ori_cones(j,4)
                break;
            end
        end
    end
end
toc;