#include "get_iiwa_FK.hpp"
#include <iostream>
#include <cmath>
#include <stdlib.h>
#include "eigen-eigen-5a0156e40feb/Eigen/Core"
#include "eigen-eigen-5a0156e40feb/Eigen/Dense"
#include "eigen-eigen-5a0156e40feb/Eigen/Geometry"
#include <ctime>
#include <string>
#include <fstream>
#include <ios>
#include <map>
#include <vector>
#include <random>
// #include <thread>
#define PI 3.1415926535897
// #define delta 1.57
// #define delta 0.785
// #define delta 0.2618
#define delta 0.00872665
// #define delta 0.174533

using namespace std;

int get_dimension( int joint_no, Eigen::VectorXd theta_UB , Eigen::VectorXd theta_LB )
{
	int dimension;
	dimension = floor(( (theta_UB[joint_no-1]-theta_LB[joint_no-1]) / delta ) + 1);
	return dimension;
}


Eigen::VectorXd assign_thetas( int joint_no, int n, Eigen::VectorXd theta_LB )
{
	Eigen::VectorXd theta(n);
	theta[0] = theta_LB[joint_no-1];
	for (int i=1; i<theta.rows(); i++)
	{
		theta[i] = theta[i-1] + delta;
	}
	return theta;
}


string insert_leading_zeros( int val )
{
	string reply;
	if (val < 10)
		reply = "00" + to_string(val); 
	else if (val < 100)
		reply = '0' + to_string(val); 
	else
		reply = to_string(val); 
	return reply;
}


int main(int arc, char *argv[])
{
	string folder;
	string file;
	folder = "Random_5";

	clock_t strt;
	double duration;
	long int node_id = 0;
	int counter = 1;
	// long int reset_val = 50000000;
	ofstream data_file;

// // Create first file to write
// 	file = folder + "/" + "Vxl_data_" + to_string(counter) + ".csv";
// 	data_file.open( file );


// Defining the Upper Bounds
	Eigen::VectorXd theta_UB = Eigen::VectorXd::Ones(7);
	theta_UB[0] = 2.967059728390360;
	theta_UB[1] = 2.094395102393195;
	theta_UB[2] = 2.967059728390360;
	theta_UB[3] = 2.094395102393195;
	theta_UB[4] = 2.967059728390360;
	theta_UB[5] = 2.094395102393195;
	theta_UB[6] = 3.054326190990077;

// Defining the Upper Bounds
	Eigen::VectorXd theta_LB = Eigen::VectorXd::Ones(7);
	// theta_LB[0] = -2.967059728390360;
	theta_LB[0] = -1.047197551196598;
	// theta_LB[1] = -2.094395102393195;
	theta_LB[1] = -1.570796326794897;
	theta_LB[2] = -2.967059728390360;
	theta_LB[3] = -2.094395102393195;
	theta_LB[4] = -2.967059728390360;
	theta_LB[5] = -2.094395102393195;
	theta_LB[6] = -3.054326190990077;

	// int dt1, dt2, dt3, dt4, dt5, dt6, dt7;
	// dt1 = get_dimension( 1, theta_UB , theta_LB );
	// dt2 = get_dimension( 2, theta_UB , theta_LB );
	// dt3 = get_dimension( 3, theta_UB , theta_LB );
	// dt4 = get_dimension( 4, theta_UB , theta_LB );
	// dt5 = get_dimension( 5, theta_UB , theta_LB );
	// dt6 = get_dimension( 6, theta_UB , theta_LB );
	// dt7 = get_dimension( 7, theta_UB , theta_LB );

	// Eigen::VectorXd theta_1_range(dt1); 
	// theta_1_range = assign_thetas(1, dt1, theta_LB);
	
	// Eigen::VectorXd theta_2_range( dt2 ); 
	// theta_2_range = assign_thetas(2, dt2, theta_LB);

	// Eigen::VectorXd theta_3_range( dt3 ); 
	// theta_3_range = assign_thetas(3, dt3, theta_LB);

	// Eigen::VectorXd theta_4_range( dt4 ); 
	// theta_4_range = assign_thetas(4, dt4, theta_LB);

	// Eigen::VectorXd theta_5_range( dt5 ); 
	// theta_5_range = assign_thetas(5, dt5, theta_LB);

	// Eigen::VectorXd theta_6_range( dt6 ); 
	// theta_6_range = assign_thetas(6, dt6, theta_LB);

	// Eigen::VectorXd theta_7_range( dt7 ); 
	// theta_7_range = assign_thetas(7, dt7, theta_LB);
	
	// cout<< dt1 << "\t" << dt2 << "\t" << dt3 << "\t" << dt4 << "\t"  << dt5 << "\t" << dt6 << "\t" << dt7 <<endl;
	// random_device rd; // obtain a random number from hardware
 //    mt19937 eng(rd()); // seed the generator
 //    uniform_int_distribution<> r_dt1(0, dt1-1); // define the range
	// uniform_int_distribution<> r_dt2(0, dt2-1);
	// uniform_int_distribution<> r_dt3(0, dt3-1);
	// uniform_int_distribution<> r_dt4(0, dt4-1);
	// uniform_int_distribution<> r_dt5(0, dt5-1);
	// uniform_int_distribution<> r_dt6(0, dt6-1);
	// uniform_int_distribution<> r_dt7(0, dt7-1);


// Main Loops
	Eigen::MatrixXd ee_base(4,4);
	Eigen::MatrixXd I = Eigen::MatrixXd::Identity(4,4);
	Eigen::VectorXd joint_config(7);
	Eigen::VectorXd xyz_UB(3);
	Eigen::VectorXd xyz_LB(3);
	Eigen::VectorXd ee_position(3);
	Eigen::VectorXd error(3);
	
	int x;
	int y;
	int z;
	string voxel_name;
	int bx1,bx2,bx3, by1,by2,by3, bz1,bz2,bz3;
	xyz_UB[0] = 1;
	xyz_UB[1] = 1;
	xyz_UB[2] = 1;

	xyz_LB[0] = 0.0;
	xyz_LB[1] = -1;	
	xyz_LB[2] = 0.0;
	double r1,r2,r3,r4,r5,r6,r7;

// // Initializing Parallel Computing
// 	const int n_threads = std::thread::hardware_cocurrency();
// 	cout<< "Initializing Multi-thread computing......." <<endl;
// 	cout<< "Threads Available:  " << n_threads <<endl;
	
	// strt = std::clock();
	while (true)
	{
		// joint_config[0] =  theta_1_range[r_dt1(eng)];
		// joint_config[1] =  theta_2_range[r_dt2(eng)];
		// joint_config[2] =  theta_3_range[r_dt3(eng)];
		// joint_config[3] =  theta_4_range[r_dt4(eng)];
		// joint_config[4] =  theta_5_range[r_dt5(eng)];
		// joint_config[5] =  theta_6_range[r_dt6(eng)];
		// joint_config[6] =  theta_7_range[r_dt7(eng)];
		r1 = ((double) rand()/(RAND_MAX));
		r2 = ((double) rand()/(RAND_MAX));
		r3 = ((double) rand()/(RAND_MAX));
		r4 = ((double) rand()/(RAND_MAX));
		r5 = ((double) rand()/(RAND_MAX));
		r6 = ((double) rand()/(RAND_MAX));
		r7 = ((double) rand()/(RAND_MAX));
		
		joint_config[0] =  theta_LB[0] + (r1*(theta_UB[0]-theta_LB[0]));
		joint_config[1] =  theta_LB[1] + (r2*(theta_UB[1]-theta_LB[1]));
		joint_config[2] =  theta_LB[2] + (r3*(theta_UB[2]-theta_LB[2]));
		joint_config[3] =  theta_LB[3] + (r4*(theta_UB[3]-theta_LB[3]));
		joint_config[4] =  theta_LB[4] + (r5*(theta_UB[4]-theta_LB[4]));
		joint_config[5] =  theta_LB[5] + (r6*(theta_UB[5]-theta_LB[5]));
		joint_config[6] =  theta_LB[6] + (r7*(theta_UB[6]-theta_LB[6]));

		ee_base = get_iiwa_FK( joint_config, I );
		ee_position[0] = ee_base(0,3);
		ee_position[1] = ee_base(1,3);
		ee_position[2] = ee_base(2,3);
		
		// Check if XYZ are less than upper bounds
		error = xyz_UB - ee_position;
		if  (error[0] < 0.0 || error[1] < 0.0 || error[2] < 0.0)
			continue;
		// Check if XYZ are greater than lower bounds
		error = ee_position - xyz_LB;
		if  (error[0] < 0.0 || error[1] < 0.0 || error[2] < 0.0)
			continue;
		
		x = (int) (ee_base(0,3)*50);
    	y = (int) ((ee_base(1,3)*50)+50);
    	z = (int) (ee_base(2,3)*50);
    	bx1 = (int) (ee_base(0,0)*100);
    	bx2 = (int) (ee_base(1,0)*100);
    	bx3 = (int) (ee_base(2,0)*100);
    	by1 = (int) (ee_base(0,1)*100);
    	by2 = (int) (ee_base(1,1)*100);
    	by3 = (int) (ee_base(2,1)*100);
    	bz1 = (int) (ee_base(0,2)*100);
    	bz2 = (int) (ee_base(1,2)*100);
    	bz3 = (int) (ee_base(2,2)*100);

    	voxel_name = insert_leading_zeros(x) + insert_leading_zeros(y) + insert_leading_zeros(z);
    	data_file.open( folder + "/" + voxel_name + ".csv", ios::app );
    	data_file << bx1 << "," << bx2 << "," << bx3 << ","
    				<< by1 << "," << by2 << "," << by3 << ","
    				<< bz1 << "," << bz2 << "," << bz3 << endl;
        data_file.close();
	}
	
	cout<< "Voxel Data Collection Succesfully Completed" << endl;
	// duration = ( std::clock() - strt ) / (double) CLOCKS_PER_SEC;
	// cout<< duration <<endl;
	return 0;
}
