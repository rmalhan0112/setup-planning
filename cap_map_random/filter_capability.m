figure(1);
plot_robot([0;0;0;0;0;0;0],robot1,false); daspect([1,1,1]); hold on;
xlabel('X Axis')
ylabel('Y Axis')
zlabel('Z Axis')
% cap_not_manip = [];
% cap_manip = [];
% cap_notreach = [];

filtered_map = {};
filtered_map = capability_z;

for i=1:262701
    key = reach_map.index_voxel(char(num2str(i)));
    x = str2double(key(1:3))./50;
    y = str2double(key(4:6))./50-1;
    z = str2double(key(7:9))./50;
    
%     if z > 0.5
%         continue;
%     end
    
    
    if size( iiwa_manipulability{i},1 )~=0
        if mean( double(iiwa_manipulability{i}(:,2)) )/1000 > 0.05
%             cap_manip = [ cap_manip; [x,y,z, [0,1,0]  ] ];
            continue;
        else
%             cap_not_manip = [ cap_not_manip; [x,y,z, [0,0,1]  ] ];
            filtered_map{i}.reachable = false;
            continue;
        end
    else
%         cap_notreach = [ cap_notreach; [x,y,z, [1,0,0]  ] ];
    end
    
    
%     if capability_z{i}.reachable
%         cap_reach = [ cap_reach; [x,y,z] ];
%     else
%         cap_notreach = [ cap_notreach; [x,y,z] ];
%     end
end

% scatter3( cap_notreach(:,1),cap_notreach(:,2),cap_notreach(:,3),20, cap_notreach(:,4:end), 'filled', 'MarkerFaceAlpha',0.2 );
% scatter3( cap_not_manip(:,1),cap_not_manip(:,2),cap_not_manip(:,3),20, cap_not_manip(:,4:end), 'filled', 'MarkerFaceAlpha',0.5 );
% scatter3( cap_manip(:,1),cap_manip(:,2),cap_manip(:,3),20, cap_manip(:,4:end), 'filled', 'MarkerFaceAlpha',0.2 );
