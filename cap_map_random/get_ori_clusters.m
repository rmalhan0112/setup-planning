% clear all;
% close all;
% clc;
% 
% load capability_z.mat
% load iiwa_capability.mat

ori_cap_z = {};

for idx = 1:size(reach_map.reachability,2)
    if mod(idx,10000)==0
        disp(idx)
    end
    if ~reach_map.reachability{idx}
        continue;
    end

    ori_vectors = capability_z{idx}.ori_vecs;
    ori_cap_z{idx}.ori_vecs = ori_vectors;
    
    upd_cones = [];
    clusters = {};
    if size(capability_z{idx}.ori_cones,1)~=0
        for i=1:size(capability_z{idx}.ori_cones,1)
            centroid = capability_z{idx}.ori_cones(i,1:3);
            beta = capability_z{idx}.ori_cones(i,4);
            vecs = [];
            for j=1:size(ori_vectors,1)
                angle = real(acos( dot(ori_vectors(j,:),centroid) ));
                if angle < beta
                    vecs = [vecs; ori_vectors(j,:) ];
                end
            end
            if size(vecs,1) >= 20
                clusters{end+1} = vecs;
                upd_cones = [upd_cones; [ centroid,beta ] ];
            end
        end 
    end
    
    if size(clusters,2)~=0
        ori_cap_z{idx}.clusters = clusters;
        ori_cap_z{idx}.ori_cones = upd_cones;
    else
        reach_map.reachability{idx} = false;
    end
end

save('ori_cap_z.mat','ori_cap_z');
save('reach_map.mat','reach_map');