function str = to_string_v(x)
    if x<10
        str = strcat( '00',num2str(x) );
        return;
    end
    
    if x<100
        str = strcat( '0',num2str(x) );
        return;
    end
    str = num2str(x);
end