counter = 1;
clear('c_map')
clear('r_map')
c_map = containers.Map( int32(1),[1,2,3] );
r_map = containers.Map( int32(1),true );

remove(c_map,1);
remove(r_map,1);

for z = 0 : 1: 50
    for x = 0 : 1: 50
        for y = 0 : 1: 100
            key = int32(x*100000 + y*100 + z);
            if capability_z{counter}.reachable
                r_map(key) = true;
                c_map(key) = capability_z{counter}.ori_cones;
            else
                r_map(key) = false;
            end
            counter = counter + 1;
        end
    end
end
