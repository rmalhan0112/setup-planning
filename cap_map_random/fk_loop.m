clear all;
close all;
clc;

% folder = ('Voxel Data');
delta = 0.0873;

I = eye(4);
th1 = -1.047197551196598:delta:2.967059;
n = size(th1,2);

tic;
disp("Loading the Voxel Index File.......");
load 'voxel_index.mat';


voxel_data = {};
disp("Initializing Voxel Data cells.......");
for i=1:size(voxel_index,1)
    voxel_data{i}.normals = {};
end
toc;
disp("Begin Data Collection.......");

for i=1:1:n
    fprintf("Theta Remaining:  %d\n", (n-i));
    for th2 = -1.570796326794897:delta:2.094395
        for th3 = -2.967059:delta:2.967059
            for th4 = -2.094395:delta:2.094395
                for th5 = -2.967059:delta:2.967059
                    for th6 = -2.094395:delta:2.094395
                        ee_base = get_iiwa_FK_mex( [th1(i);th2;th3;th4;th5;th6;0], I );
                        
                        if (~all(([1;1;1]-ee_base(1:3,4))>0)) || (~all((ee_base(1:3,4)-[0;-1;0])>0))
                            continue;
                        end
                        
                        x = uint8(ee_base(1,4)*100);
                        y = uint8(ee_base(2,4)*100+100);
                        z = uint8(ee_base(3,4)*100);
                        nx = int8(ee_base(1,3)*100);
                        ny = int8(ee_base(2,3)*100);
                        nz = int8(ee_base(3,3)*100);
                        key = char(strcat( to_string_v(x), to_string_v(y), to_string_v(z)));
                        voxel_data{ voxel_index(key) }.normals{end+1} = [ nx,ny,nz ];
                        
%                         if abs(th2-temp2)<1e-6
%                             file = fullfile(  folder, strcat('VX_data_',num2str(i),'.mat')  );
%                             save( file, 'Samples', '-v7.3' );
%                             local_map = containers.Map;
%                         end
                    end    
                end 
            end 
        end    
    end
end
% file = fullfile(  folder, strcat('VX_data_',num2str(counters(end)),'.mat')  );
% save( file, 'Samples', '-v7.3' );
% save( 'voxel_data.mat', '-v7.3' );
% clear all;


function str = to_string_v(x)
    if x<10
        str = strcat( '00',num2str(x) );
        return;
    end
    
    if x<100
        str = strcat( '0',num2str(x) );
        return;
    end
    str = num2str(x);
end

% function str = to_string_nr(x)
%     if x<10
%         str = strcat( "0",num2str(x) );
%         return;
%     end
%     str = num2str(x);
% end
