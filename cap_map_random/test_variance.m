% clear all;
% close all;
% clc;
q0 = [-1:0.001:1];
q1 = [-1:0.001:1];
q2 = [-1:0.001:1];
q3 = [-1:0.001:1];
counter = 0;
vectors = [];
while counter~=500
    sample=  [ randsample(q0,1),...
                randsample(q1,1),...
                randsample(q2,1),...
                randsample(q3,1)];
    sample = sample / norm(sample);
    R = quat2rotm( sample );
    if rad2deg( abs( acos( dot(R(:,3),[0;0;1]) ) ) ) < 70
        vectors = [vectors; R(:,3)'];
        counter = counter + 1;
    end
end

figure(2)
hold on;
% Plot main frame
quiver3( 0,0,0,1,0,0,'r','AutoScaleFactor',1 );
quiver3( 0,0,0,0,1,0,'g','AutoScaleFactor',1 );
quiver3( 0,0,0,0,0,1,'b','AutoScaleFactor',1 );
% Plot the vectors
quiver3( zeros(size(vectors,1),1),zeros(size(vectors,1),1),zeros(size(vectors,1),1),vectors(:,1),vectors(:,2),vectors(:,3),'k','AutoScaleFactor',0.5 );
daspect([1,1,1])

% Conducting the Singular Value Decomposition
no_elemtns = size(vectors,1); 
mean_val = mean(vectors,1);
% A = vectors - repmat(mean_val,no_elemtns,1);
A = vectors;
[U,D,V] = svd(A'*A);
volume = abs( D(1,1)*D(2,2)*D(3,3))