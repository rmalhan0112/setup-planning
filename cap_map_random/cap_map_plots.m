% vol_data = [];
% no_pca = [];
% no_vecs = [];
% beta1 = [];
% vxl_neg_z = [];
% reach = [];

figure(1);
plot_robot([0;0;0;0;0;0;0],robot1,false); daspect([1,1,1]); hold on;
xlabel('X Axis')
ylabel('Y Axis')
zlabel('Z Axis')
manip = [];
cap_reach = [];
cap_notreach = [];

for i=1:262701
%     if ~reach_map.reachability{i}
%         continue;
%     end
%     i=randsample([1:1:262701],1);
    key = reach_map.index_voxel(char(num2str(i)));
    x = str2double(key(1:3))./50;
    y = str2double(key(4:6))./50-1;
    z = str2double(key(7:9))./50;
    
    if z > 0.5
        continue;
    end
    
    if size( iiwa_manipulability{i},1 )~=0
        manip = [manip; [x,y,z, mean(double(iiwa_manipulability{i}(:,1)))/1000,  mean(double(iiwa_manipulability{i}(:,2)))/1000]  ];
    end
    
    if capability_z{i}.reachable
        cap_reach = [ cap_reach; [x,y,z] ];
    else
        cap_notreach = [ cap_notreach; [x,y,z] ];
    end
%     x = str2num(key(1:3));
%     y = str2num(key(4:6));
%     z = str2num(key(7:9));
%     if cap_map_z{i}.reachable
%         vol_data = [vol_data; [x,y,z, cap_map_z{i}.volm] ]; 
%         no_pca = [no_pca; [x,y,z, cap_map_z{i}.no_pcs] ]; 
%         no_vecs = [no_vecs; [x,y,z, cap_map_z{i}.no_vec] ];
%         beta1 = [beta1; [x,y,z, cap_map_z{i}.beta1] ];
%         vectors = cap_map_z{i}.vectors;
%         plot_robot([0;0;0;0;0;0;0],robot1); daspect([1,1,1]); hold on;
%         zero_mat = ones( size(vectors,1),1 );
%         quiver3( zero_mat*x,...
%                 zero_mat*y,...
%                 zero_mat*z, vectors(:,1),vectors(:,2),vectors(:,3), 'r', 'AutoScaleFactor', 0.02 );
%         pause(0.00001);
%         cla;
        
%         if voxel_map{ i }.reachable
%             reach = [reach; [ x,y,z ]];
    end
        
%         if cap_map_z{i}.reachable
%             if cap_map_z{i}.volm>800
%                 no_vecs = [no_vecs; [ x,y,z,cap_map_z{i}.no_vec ]];
%             end
%         end
%         dot_prod = [];
%         for j=1:size(vectors)
%             prod = dot(vectors(j,:),[0;0;-1]);
%             if prod>0
%                 dot_prod = [ dot_prod;  ];
%             end
%         end
%         [val,idx] = min(dot_prod);
%         if acos( abs(val) ) < 0.3491
%             vxl_neg_z = [vxl_neg_z; [x,y,z]];
%         end
%         
%     end
% end
figure(1)
hold on;
scatter3( manip(:,1),manip(:,2),manip(:,3),20, manip(:,end), 'filled', 'MarkerFaceAlpha',0.2 );

% scatter3( cap_reach(:,1),cap_reach(:,2),cap_reach(:,3),20, 'g', 'filled', 'MarkerFaceAlpha',0.2 );
% scatter3( cap_notreach(:,1),cap_notreach(:,2),cap_notreach(:,3),20, 'r', 'filled', 'MarkerFaceAlpha',0.2 );
set(gcf,'color','w')

% scatter3( vol_data(:,1),vol_data(:,2),vol_data(:,3),10, vol_data(:,end), 'filled', 'MarkerFaceAlpha',0.2 );
% scatter3( no_pca(:,1),no_pca(:,2),no_pca(:,3),10, no_pca(:,end), 'filled', 'MarkerFaceAlpha',0.2 );
% scatter3( no_vecs(:,1),no_vecs(:,2),no_vecs(:,3),10, no_vecs(:,end), 'filled', 'MarkerFaceAlpha',0.2 );
% scatter3( vxl_neg_z(:,1),vxl_neg_z(:,2),vxl_neg_z(:,3),10, 'g', 'filled', 'MarkerFaceAlpha',0.2 );
% scatter3( reach(:,1),reach(:,2),reach(:,3),10, 'g', 'filled', 'MarkerFaceAlpha',0.2 );
% plot_robot( [0;0;0;0;0;0;0], robot1 );
% daspect([1,1,1]);
% colorbar;
