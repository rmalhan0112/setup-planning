% disp('Loading Index_Voxel');  load index_voxel.mat;
% disp('Loading ends');    
    
size_cap = size(cap_map_z,2);
reach_vol = [];
parfor i=1:size_cap
    key = index_voxel(char(num2str(i)));
    x = str2double(key(1:3))/50;
    y = str2double(key(4:6))/50-1;
    z = str2double(key(7:9))/50;
    if cap_map_z{i}.reachable == 1 && cap_map_z{i}.pca==true
        reach_vol = [ reach_vol; [x,y,z,cap_map_z{i}.volm] ];
    end
end