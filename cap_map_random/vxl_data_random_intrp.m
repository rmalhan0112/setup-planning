clear all;
clear all;
close all;
clc;

file = {};
file{1} = fullfile('C:\Work\USC\Composite Automation\Journal\Random11');
file{2} = fullfile('C:\Work\USC\Composite Automation\Journal\Random12');
file{3} = fullfile('C:\Work\USC\Composite Automation\Journal\Random13');

% load voxel_index.mat;
% load index_voxel.mat;
% load voxel_map5.mat
load iiwa_capability.mat
disp('Loading ends');

% Merge all the data together.
for i=1:size(iiwa_capability.index_voxel,1)
    if mod(i,10000)==0
        disp(i)
    end
    key = iiwa_capability.index_voxel(char(num2str(i)));
    
    vectors = [];
    velc_vec = [];
    forc_vec = [];
    if iiwa_capability.reachability{i}
        vectors = iiwa_capability.orientation{i};
        velc_vec = iiwa_capability.velocity{i};
        forc_vec = iiwa_capability.force{i};
    end
    for j=1:3
        fileid = fullfile( file{j},strcat(key,'.csv'));
        if exist (fileid , 'file')
            values = importdata( fileid );
            vectors = [ vectors; values(:,1:9) ];
            velc_vec = [ velc_vec; values(:,10:15) ];
            forc_vec = [ forc_vec; values(:,16:21) ];
        end
    end
    if iiwa_capability.reachability{i}
        iiwa_capability.orientation{i} = unique( vectors,'rows' );
        iiwa_capability.velocity{i} = unique( velc_vec,'rows' );
        iiwa_capability.force{i} = unique( forc_vec,'rows' );
        continue;
    elseif size(vectors,1)~=0
        iiwa_capability.reachability{i} = true;
        iiwa_capability.orientation{i} = unique( vectors,'rows' );
        iiwa_capability.velocity{i} = unique( velc_vec,'rows' );
        iiwa_capability.force{i} = unique( forc_vec,'rows' );
        continue;
    else
        iiwa_capability.reachability{i} = false;
    end
end

% disp('Saving');
% save('iiwa_capability.mat');

















% reach_map = {};
% ori = {};
% velc = {};
% forc = {};
% 
% % Merge all the data together.
% for i=1:size(index_voxel,1)
%     key = index_voxel(char(num2str(i)));
%     
%     vectors = [];
%     velc_vec = [];
%     forc_vec = [];
%     if voxel_map5{i}.reachable
%         vectors = voxel_map5{i}.vectors;
%     end
%     for j=1:3
%         fileid = fullfile( file{j},strcat(key,'.csv'));
%         if exist (fileid , 'file')
%             values = csvread( fileid );
%             vectors = [ vectors; values(:,1:9) ];
%             velc_vec = [ velc_vec; values(:,10:15) ];
%             forc_vec = [ forc_vec; values(:,16:21) ];
%         end
%     end
%     if size(velc_vec,1)~=0
%         reach_map{i} = true;    
%         ori{i} = unique( vectors,'rows' );
%         velc{i} = unique( velc_vec,'rows' );
%         forc{i} = unique( forc_vec,'rows' );
%     else
%         reach_map{i} = false;
%     end 
% end
% iiwa_capability.reachability = reach_map;
% iiwa_capability.orientation = ori;
% iiwa_capability.velocity = velc;
% iiwa_capability.force = forc;
% 
% disp('Saving');
% save('iiwa_capability.mat');
