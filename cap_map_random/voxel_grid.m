clear all;
close all;
clc;

% voxel_index = containers.Map;
index_voxel = containers.Map;
counter = 1;
for z = 0:1:50
    for x = 0:1:50
        for y = 0:1:100
            key = char(strcat( to_string_v(x), to_string_v(y), to_string_v(z)));
%             voxel_index(key) = counter;
            index_voxel( char(num2str(counter)) ) = key;
            counter = counter + 1;
        end
    end
end
save('index_voxel.mat','-v7.3');


function str = to_string_v(x)
    if x<10
        str = strcat( '00',num2str(x) );
        return;
    end
    
    if x<100
        str = strcat( '0',num2str(x) );
        return;
    end
    str = num2str(x);
end