function cones = get_cones( vectors )
    Y = [];
    for i = 1:size(vectors,1)
        for j=i+1:size(vectors,1)
            Y = [Y, dist(vectors(i,:),vectors(j,:))];  
        end
    end

    Z = linkage(Y,'centroid');

    % Adaptively cluster based on centroid angle
    for count = 4:-1:1
        T = cluster(Z,'maxclust',count);
        if count==1
            break;
        end
        is_redundant = false;
        centroids = {};
        clusters = {};
        V1 = [];
        V2 = [];
        V3 = [];
        V4 = [];
        for i=1:size(vectors,1)
            if T(i)==1
                V1 = [ V1; vectors(i,:) ];
                clusters{1} = V1;
            elseif T(i)==2
                V2 = [ V2; vectors(i,:) ];
                clusters{2} = V2;
            elseif T(i)==3
                V3 = [ V3; vectors(i,:) ];
                clusters{3} = V3;
            elseif T(i)==4
                V4 = [ V4; vectors(i,:) ];
                clusters{4} = V4;
            end
        end
        if size(V1,1)~=0
            centroids{1} = sum(V1,1)/size(V1,1);
            centroids{1} = centroids{1}/norm(centroids{1});
        end
        if size(V2,1)~=0
            centroids{2} = sum(V2,1)/size(V2,1);
            centroids{2} = centroids{2}/norm(centroids{2});
        end
        if size(V3,1)~=0
            centroids{3} = sum(V3,1)/size(V3,1);
            centroids{3} = centroids{3}/norm(centroids{3});
        end
        if size(V4,1)~=0
            centroids{4} = sum(V4,1)/size(V4,1);
            centroids{4} = centroids{4}/norm(centroids{4});
        end

        for k=1:count
           for l=k+1:count
               angle = acos( centroids{k}(1)*centroids{l}(1) +...
                            centroids{k}(2)*centroids{l}(2)+...
                            centroids{k}(3)*centroids{l}(3)  );
                if angle < 1.57
                    is_redundant = true;
                    break;
                end
           end
           if is_redundant
               break;
           end
        end
        if ~is_redundant
            break;
        end
    end
    
% Clustering complete. Conducting SVD
    cones = [];
    for i = 1:size(clusters,2)
        vecs = clusters{i};
        if size(vecs,1) < 5
            continue;
        end
        dot_prod = [];
        for j=1:size(vecs,1)
           dot_prod = [dot_prod; (centroids{i}(1)*vecs(i,1) + ...
                                    centroids{i}(2)*vecs(i,2) + ...
                                    centroids{i}(3)*vecs(i,3))  ];
        end
        dot_prod = sortrows(dot_prod, 'descend');
        cones = [ cones; [centroids{i},acos( dot_prod(end) ) ] ];
    end
%     for i = 1:size(clusters,2)
%         [U,D,V] = svd(clusters{i}'*clusters{i});
%         idx = [1,2,3];
%         for l=1:3
%             if ( U(1,l)*centroids{i}(1) + ...
%                     U(2,l)*centroids{i}(2) + ...
%                     U(3,l)*centroids{i}(3)  ) > 0.95
%                 idx(l) = [];
%                 break;
%             end
%         end
%         D(idx(1),idx(1)) = D(idx(1),idx(1))^0.5/D(l,l)^0.5;
%         D(idx(2),idx(2)) = D(idx(2),idx(2))^0.5/D(l,l)^0.5;
%         % Assign Values to cone
%         cones = [ cones; [ centroids{i},atan( D(idx(1),idx(1))/2 ),...
%                             atan( D(idx(2),idx(2))/2 )  ] ];
%     end
    
end