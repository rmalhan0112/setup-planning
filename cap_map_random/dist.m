function d = dist(v1,v2)
    dot_prod = v1(1)*v2(1) + v1(2)*v2(2) + v1(3)*v2(3);
    d = real (acos( dot_prod ) );
end